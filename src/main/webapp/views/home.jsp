<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<html xmlns="http://www.w3.org/1999/xhtml">
<head>


<!-- Meta Tags -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>TELECOM Transfers Viamericas</title>
</head>
<body>
	<h1>TELECOM Transfers Viamericas</h1>

	<form action="${pageContext.request.contextPath}/transfers/createRecipient" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Crear Recipient">
	</form>
	
	<form action="${pageContext.request.contextPath}/transfers/createSender" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Crear Sender">
	</form>
	
	<form action="${pageContext.request.contextPath}/transfers/createNewOrder" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Crear New Order">
	</form>
	
	<form action="${pageContext.request.contextPath}/transfers/confirmTransactionCredit" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Confirmar Transaccion Credit">
	</form>
	
	<form action="${pageContext.request.contextPath}/transfers/createSenderProfileCard" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Create Sender Profile">
	</form>
	
	<form action="${pageContext.request.contextPath}/get/payment/locations" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Payment Locations">
	</form>
	
<!-- no funciona este metodo, no funciona -->
	<%-- <form action="${pageContext.request.contextPath}/get/agency/locations" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Agency Locations">
	</form> --%>
	
	<form action="${pageContext.request.contextPath}/get/currency/country" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Currency By Country">
	</form>
	
	<form action="${pageContext.request.contextPath}/get/payment/modes" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Payments Modes">
	</form>
	
	<form action="${pageContext.request.contextPath}/get/countrys" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Get Countries">
	</form>
	
	<form action="${pageContext.request.contextPath}/get/countrysCitys" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Get Countries-Citys">
	</form>
	
	<form action="${pageContext.request.contextPath}/get/countrysStates" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Get Countries-States">
	</form>
	
	<form action="${pageContext.request.contextPath}/get/statesCitys" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="States - Citys">
	</form>
	
	<form action="${pageContext.request.contextPath}/get/CitysByZipCode" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Citys by ZIp">
	</form>
	
	<form action="${pageContext.request.contextPath}/get/BeneficiariesBySender" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Beneficiarios por Sender">
	</form>
	
	<form action="${pageContext.request.contextPath}/get/Cities" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Cities">
	</form>
	
	<form action="${pageContext.request.contextPath}/put/Cities" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Put Cities in BD">
	</form>
	
	<form action="${pageContext.request.contextPath}/get/ExchangeRate" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Exchange Rate">
	</form>
	
	<form action="${pageContext.request.contextPath}/get/costPaymentLocationNetwork" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Cost Payment Location Network">
	</form>
	
	<form action="${pageContext.request.contextPath}/get/orderFees" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Get order Fees">
	</form>
	
		<form action="${pageContext.request.contextPath}/validate/senderProfile" method="post" >
		<input id="json" name="json" size="100" type="text"/> 
		<input type="submit" value="Validate Sender Profile">
	</form>
	
</body>
</html>