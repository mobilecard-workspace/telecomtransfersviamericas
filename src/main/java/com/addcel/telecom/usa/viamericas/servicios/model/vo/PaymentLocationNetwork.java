package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class PaymentLocationNetwork {

	private String idCountry;
	
	private String idModePayCurrency;
	
	private String idPaymentMode;
	
	private double amount;
	
	private String onlyNetwork;
	
	private Boolean withCost;

	public String getIdCountry() {
		return idCountry;
	}

	public void setIdCountry(String idCountry) {
		this.idCountry = idCountry;
	}

	public String getIdModePayCurrency() {
		return idModePayCurrency;
	}

	public void setIdModePayCurrency(String idModePayCurrency) {
		this.idModePayCurrency = idModePayCurrency;
	}

	public String getIdPaymentMode() {
		return idPaymentMode;
	}

	public void setIdPaymentMode(String idPaymentMode) {
		this.idPaymentMode = idPaymentMode;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getOnlyNetwork() {
		return onlyNetwork;
	}

	public void setOnlyNetwork(String onlyNetwork) {
		this.onlyNetwork = onlyNetwork;
	}

	public Boolean getWithCost() {
		return withCost;
	}

	public void setWithCost(Boolean withCost) {
		this.withCost = withCost;
	}
	
}
