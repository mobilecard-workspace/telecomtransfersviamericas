/**
 * EditableSender.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto;

public class EditableSender  implements java.io.Serializable {
    private java.lang.String idSender;

    private java.lang.String FName;

    private java.lang.String MName;

    private java.lang.String LName;

    private java.lang.String SLName;

    private java.lang.String address;

    private java.lang.String phone1;

    private java.lang.String idState;

    private java.lang.String idCity;

    private java.lang.String ZIP;

    private java.lang.String address2;

    private java.lang.Boolean sendSMS;

    private java.lang.String birthDate;

    private java.lang.String countryOfBirth;

    private java.lang.String phone1Type;

    private java.lang.Boolean promotionalMessages;

    public EditableSender() {
    }

    public EditableSender(
           java.lang.String idSender,
           java.lang.String FName,
           java.lang.String MName,
           java.lang.String LName,
           java.lang.String SLName,
           java.lang.String address,
           java.lang.String phone1,
           java.lang.String idState,
           java.lang.String idCity,
           java.lang.String ZIP,
           java.lang.String address2,
           java.lang.Boolean sendSMS,
           java.lang.String birthDate,
           java.lang.String countryOfBirth,
           java.lang.String phone1Type,
           java.lang.Boolean promotionalMessages) {
           this.idSender = idSender;
           this.FName = FName;
           this.MName = MName;
           this.LName = LName;
           this.SLName = SLName;
           this.address = address;
           this.phone1 = phone1;
           this.idState = idState;
           this.idCity = idCity;
           this.ZIP = ZIP;
           this.address2 = address2;
           this.sendSMS = sendSMS;
           this.birthDate = birthDate;
           this.countryOfBirth = countryOfBirth;
           this.phone1Type = phone1Type;
           this.promotionalMessages = promotionalMessages;
    }


    /**
     * Gets the idSender value for this EditableSender.
     * 
     * @return idSender
     */
    public java.lang.String getIdSender() {
        return idSender;
    }


    /**
     * Sets the idSender value for this EditableSender.
     * 
     * @param idSender
     */
    public void setIdSender(java.lang.String idSender) {
        this.idSender = idSender;
    }


    /**
     * Gets the FName value for this EditableSender.
     * 
     * @return FName
     */
    public java.lang.String getFName() {
        return FName;
    }


    /**
     * Sets the FName value for this EditableSender.
     * 
     * @param FName
     */
    public void setFName(java.lang.String FName) {
        this.FName = FName;
    }


    /**
     * Gets the MName value for this EditableSender.
     * 
     * @return MName
     */
    public java.lang.String getMName() {
        return MName;
    }


    /**
     * Sets the MName value for this EditableSender.
     * 
     * @param MName
     */
    public void setMName(java.lang.String MName) {
        this.MName = MName;
    }


    /**
     * Gets the LName value for this EditableSender.
     * 
     * @return LName
     */
    public java.lang.String getLName() {
        return LName;
    }


    /**
     * Sets the LName value for this EditableSender.
     * 
     * @param LName
     */
    public void setLName(java.lang.String LName) {
        this.LName = LName;
    }


    /**
     * Gets the SLName value for this EditableSender.
     * 
     * @return SLName
     */
    public java.lang.String getSLName() {
        return SLName;
    }


    /**
     * Sets the SLName value for this EditableSender.
     * 
     * @param SLName
     */
    public void setSLName(java.lang.String SLName) {
        this.SLName = SLName;
    }


    /**
     * Gets the address value for this EditableSender.
     * 
     * @return address
     */
    public java.lang.String getAddress() {
        return address;
    }


    /**
     * Sets the address value for this EditableSender.
     * 
     * @param address
     */
    public void setAddress(java.lang.String address) {
        this.address = address;
    }


    /**
     * Gets the phone1 value for this EditableSender.
     * 
     * @return phone1
     */
    public java.lang.String getPhone1() {
        return phone1;
    }


    /**
     * Sets the phone1 value for this EditableSender.
     * 
     * @param phone1
     */
    public void setPhone1(java.lang.String phone1) {
        this.phone1 = phone1;
    }


    /**
     * Gets the idState value for this EditableSender.
     * 
     * @return idState
     */
    public java.lang.String getIdState() {
        return idState;
    }


    /**
     * Sets the idState value for this EditableSender.
     * 
     * @param idState
     */
    public void setIdState(java.lang.String idState) {
        this.idState = idState;
    }


    /**
     * Gets the idCity value for this EditableSender.
     * 
     * @return idCity
     */
    public java.lang.String getIdCity() {
        return idCity;
    }


    /**
     * Sets the idCity value for this EditableSender.
     * 
     * @param idCity
     */
    public void setIdCity(java.lang.String idCity) {
        this.idCity = idCity;
    }


    /**
     * Gets the ZIP value for this EditableSender.
     * 
     * @return ZIP
     */
    public java.lang.String getZIP() {
        return ZIP;
    }


    /**
     * Sets the ZIP value for this EditableSender.
     * 
     * @param ZIP
     */
    public void setZIP(java.lang.String ZIP) {
        this.ZIP = ZIP;
    }


    /**
     * Gets the address2 value for this EditableSender.
     * 
     * @return address2
     */
    public java.lang.String getAddress2() {
        return address2;
    }


    /**
     * Sets the address2 value for this EditableSender.
     * 
     * @param address2
     */
    public void setAddress2(java.lang.String address2) {
        this.address2 = address2;
    }


    /**
     * Gets the sendSMS value for this EditableSender.
     * 
     * @return sendSMS
     */
    public java.lang.Boolean getSendSMS() {
        return sendSMS;
    }


    /**
     * Sets the sendSMS value for this EditableSender.
     * 
     * @param sendSMS
     */
    public void setSendSMS(java.lang.Boolean sendSMS) {
        this.sendSMS = sendSMS;
    }


    /**
     * Gets the birthDate value for this EditableSender.
     * 
     * @return birthDate
     */
    public java.lang.String getBirthDate() {
        return birthDate;
    }


    /**
     * Sets the birthDate value for this EditableSender.
     * 
     * @param birthDate
     */
    public void setBirthDate(java.lang.String birthDate) {
        this.birthDate = birthDate;
    }


    /**
     * Gets the countryOfBirth value for this EditableSender.
     * 
     * @return countryOfBirth
     */
    public java.lang.String getCountryOfBirth() {
        return countryOfBirth;
    }


    /**
     * Sets the countryOfBirth value for this EditableSender.
     * 
     * @param countryOfBirth
     */
    public void setCountryOfBirth(java.lang.String countryOfBirth) {
        this.countryOfBirth = countryOfBirth;
    }


    /**
     * Gets the phone1Type value for this EditableSender.
     * 
     * @return phone1Type
     */
    public java.lang.String getPhone1Type() {
        return phone1Type;
    }


    /**
     * Sets the phone1Type value for this EditableSender.
     * 
     * @param phone1Type
     */
    public void setPhone1Type(java.lang.String phone1Type) {
        this.phone1Type = phone1Type;
    }


    /**
     * Gets the promotionalMessages value for this EditableSender.
     * 
     * @return promotionalMessages
     */
    public java.lang.Boolean getPromotionalMessages() {
        return promotionalMessages;
    }


    /**
     * Sets the promotionalMessages value for this EditableSender.
     * 
     * @param promotionalMessages
     */
    public void setPromotionalMessages(java.lang.Boolean promotionalMessages) {
        this.promotionalMessages = promotionalMessages;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EditableSender)) return false;
        EditableSender other = (EditableSender) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idSender==null && other.getIdSender()==null) || 
             (this.idSender!=null &&
              this.idSender.equals(other.getIdSender()))) &&
            ((this.FName==null && other.getFName()==null) || 
             (this.FName!=null &&
              this.FName.equals(other.getFName()))) &&
            ((this.MName==null && other.getMName()==null) || 
             (this.MName!=null &&
              this.MName.equals(other.getMName()))) &&
            ((this.LName==null && other.getLName()==null) || 
             (this.LName!=null &&
              this.LName.equals(other.getLName()))) &&
            ((this.SLName==null && other.getSLName()==null) || 
             (this.SLName!=null &&
              this.SLName.equals(other.getSLName()))) &&
            ((this.address==null && other.getAddress()==null) || 
             (this.address!=null &&
              this.address.equals(other.getAddress()))) &&
            ((this.phone1==null && other.getPhone1()==null) || 
             (this.phone1!=null &&
              this.phone1.equals(other.getPhone1()))) &&
            ((this.idState==null && other.getIdState()==null) || 
             (this.idState!=null &&
              this.idState.equals(other.getIdState()))) &&
            ((this.idCity==null && other.getIdCity()==null) || 
             (this.idCity!=null &&
              this.idCity.equals(other.getIdCity()))) &&
            ((this.ZIP==null && other.getZIP()==null) || 
             (this.ZIP!=null &&
              this.ZIP.equals(other.getZIP()))) &&
            ((this.address2==null && other.getAddress2()==null) || 
             (this.address2!=null &&
              this.address2.equals(other.getAddress2()))) &&
            ((this.sendSMS==null && other.getSendSMS()==null) || 
             (this.sendSMS!=null &&
              this.sendSMS.equals(other.getSendSMS()))) &&
            ((this.birthDate==null && other.getBirthDate()==null) || 
             (this.birthDate!=null &&
              this.birthDate.equals(other.getBirthDate()))) &&
            ((this.countryOfBirth==null && other.getCountryOfBirth()==null) || 
             (this.countryOfBirth!=null &&
              this.countryOfBirth.equals(other.getCountryOfBirth()))) &&
            ((this.phone1Type==null && other.getPhone1Type()==null) || 
             (this.phone1Type!=null &&
              this.phone1Type.equals(other.getPhone1Type()))) &&
            ((this.promotionalMessages==null && other.getPromotionalMessages()==null) || 
             (this.promotionalMessages!=null &&
              this.promotionalMessages.equals(other.getPromotionalMessages())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdSender() != null) {
            _hashCode += getIdSender().hashCode();
        }
        if (getFName() != null) {
            _hashCode += getFName().hashCode();
        }
        if (getMName() != null) {
            _hashCode += getMName().hashCode();
        }
        if (getLName() != null) {
            _hashCode += getLName().hashCode();
        }
        if (getSLName() != null) {
            _hashCode += getSLName().hashCode();
        }
        if (getAddress() != null) {
            _hashCode += getAddress().hashCode();
        }
        if (getPhone1() != null) {
            _hashCode += getPhone1().hashCode();
        }
        if (getIdState() != null) {
            _hashCode += getIdState().hashCode();
        }
        if (getIdCity() != null) {
            _hashCode += getIdCity().hashCode();
        }
        if (getZIP() != null) {
            _hashCode += getZIP().hashCode();
        }
        if (getAddress2() != null) {
            _hashCode += getAddress2().hashCode();
        }
        if (getSendSMS() != null) {
            _hashCode += getSendSMS().hashCode();
        }
        if (getBirthDate() != null) {
            _hashCode += getBirthDate().hashCode();
        }
        if (getCountryOfBirth() != null) {
            _hashCode += getCountryOfBirth().hashCode();
        }
        if (getPhone1Type() != null) {
            _hashCode += getPhone1Type().hashCode();
        }
        if (getPromotionalMessages() != null) {
            _hashCode += getPromotionalMessages().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EditableSender.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "EditableSender"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "IdSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "FName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "MName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "LName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SLName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "SLName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "Address"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phone1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "Phone1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "IdState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "IdCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ZIP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "ZIP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "Address2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sendSMS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "SendSMS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("birthDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "BirthDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryOfBirth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "CountryOfBirth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phone1Type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "Phone1Type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promotionalMessages");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "PromotionalMessages"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
