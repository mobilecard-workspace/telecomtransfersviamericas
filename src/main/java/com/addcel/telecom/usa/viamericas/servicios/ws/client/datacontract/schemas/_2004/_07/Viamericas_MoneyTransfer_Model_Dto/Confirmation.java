/**
 * Confirmation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto;

public class Confirmation  implements java.io.Serializable {
    private java.lang.String idReceiver;

    private java.math.BigDecimal depositValue;

    private java.lang.String idCashier;

    public Confirmation() {
    }

    public Confirmation(
           java.lang.String idReceiver,
           java.math.BigDecimal depositValue,
           java.lang.String idCashier) {
           this.idReceiver = idReceiver;
           this.depositValue = depositValue;
           this.idCashier = idCashier;
    }


    /**
     * Gets the idReceiver value for this Confirmation.
     * 
     * @return idReceiver
     */
    public java.lang.String getIdReceiver() {
        return idReceiver;
    }


    /**
     * Sets the idReceiver value for this Confirmation.
     * 
     * @param idReceiver
     */
    public void setIdReceiver(java.lang.String idReceiver) {
        this.idReceiver = idReceiver;
    }


    /**
     * Gets the depositValue value for this Confirmation.
     * 
     * @return depositValue
     */
    public java.math.BigDecimal getDepositValue() {
        return depositValue;
    }


    /**
     * Sets the depositValue value for this Confirmation.
     * 
     * @param depositValue
     */
    public void setDepositValue(java.math.BigDecimal depositValue) {
        this.depositValue = depositValue;
    }


    /**
     * Gets the idCashier value for this Confirmation.
     * 
     * @return idCashier
     */
    public java.lang.String getIdCashier() {
        return idCashier;
    }


    /**
     * Sets the idCashier value for this Confirmation.
     * 
     * @param idCashier
     */
    public void setIdCashier(java.lang.String idCashier) {
        this.idCashier = idCashier;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Confirmation)) return false;
        Confirmation other = (Confirmation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idReceiver==null && other.getIdReceiver()==null) || 
             (this.idReceiver!=null &&
              this.idReceiver.equals(other.getIdReceiver()))) &&
            ((this.depositValue==null && other.getDepositValue()==null) || 
             (this.depositValue!=null &&
              this.depositValue.equals(other.getDepositValue()))) &&
            ((this.idCashier==null && other.getIdCashier()==null) || 
             (this.idCashier!=null &&
              this.idCashier.equals(other.getIdCashier())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdReceiver() != null) {
            _hashCode += getIdReceiver().hashCode();
        }
        if (getDepositValue() != null) {
            _hashCode += getDepositValue().hashCode();
        }
        if (getIdCashier() != null) {
            _hashCode += getIdCashier().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Confirmation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Confirmation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("depositValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "DepositValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCashier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdCashier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
