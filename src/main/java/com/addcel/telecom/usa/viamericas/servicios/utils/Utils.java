package com.addcel.telecom.usa.viamericas.servicios.utils;

import static com.addcel.telecom.usa.viamericas.servicios.utils.Constantes.FORMATO_FECHA_ENCRIPT;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.utils.AddcelCrypto;
import com.google.api.client.extensions.appengine.http.urlfetch.UrlFetchTransport;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.extensions.appengine.auth.oauth2.AppIdentityCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.urlshortener.Urlshortener;
import com.google.api.services.urlshortener.UrlshortenerScopes;



public class Utils {

	private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);
	
	private static final SimpleDateFormat SDF = new SimpleDateFormat(FORMATO_FECHA_ENCRIPT);
	
	private static final String URL_HEADER_ADDCEL = "/usr/java/resources/images/Addcel/MobileCard_Antad_header_600.PNG";
			
	public Utils() {
		
	}
	
	public static String encryptJson(String json){
		return AddcelCrypto.encryptSensitive(SDF.format(new Date()), json);
	}
	
	public static String decryptJson(String json){
		return AddcelCrypto.decryptSensitive(json);
	}

	public static String encryptHard(String json){
		return AddcelCrypto.encryptHard(json);
	}
	
	public static String formatoMontoPayworks(String monto){
		String varTotal = "000";
		String pesos = null;
        String centavos = null;
		if(monto.contains(".")){
            pesos=monto.substring(0, monto.indexOf("."));
            centavos=monto.substring(monto.indexOf(".")+1,monto.length());
            if(centavos.length()<2){
                centavos = centavos.concat("0");
            }else{
                centavos=centavos.substring(0, 2);
            }            
            varTotal=pesos+ "." + centavos;
        }else{
            varTotal=monto.concat(".00");
        } 
		return varTotal;		
	}
	
	public static String cambioAcento(String cadena){
		try{
			if(cadena != null){
				cadena = cadena.replaceAll("Ã¡", "á");
				cadena = cadena.replaceAll("Ã³", "ó");
			}
		}catch(Exception e){
			LOGGER.error("Error al cambiar acentos: " + e.getMessage());
		}
		return cadena;
	}
	
	public static boolean sendMail(String destinatario, String asunto,
			String cuerpo, Properties props, String from, String host, int port, String username, String password) {
		try {
			Session session = Session.getInstance(props);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(destinatario));
			message.setSubject(asunto);
			if (cuerpo.indexOf("cid:identifierCID00") > 0) {
				BodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(cuerpo, "text/html");
				MimeMultipart multipart = new MimeMultipart("related");
				multipart.addBodyPart(messageBodyPart);
				messageBodyPart = new MimeBodyPart();
				DataSource fds = new FileDataSource(URL_HEADER_ADDCEL);
				messageBodyPart.setDataHandler(new DataHandler(fds));
				messageBodyPart.setHeader("Content-ID", "identifierCID00");
				multipart.addBodyPart(messageBodyPart);
				message.setContent(multipart);
			} else {
				message.setContent(cuerpo, "text/html");
			}
			Transport transport = session.getTransport("smtp");
			transport.connect(host, port, username, password);
			message.saveChanges();
			transport.sendMessage(message, message.getAllRecipients());
			LOGGER.info("Correo enviado exitosamente: " + destinatario);
			return true;
		} catch (Exception e) {
			LOGGER.error("ERROR - SEND MAIL: " + e);
			return false;
		}
	}
	
	private static final String patron_large = "yyyy/MM/dd HH:mm:ss";
	
	private static final SimpleDateFormat formato_large = new SimpleDateFormat(patron_large, new Locale("ES", "co"));
	
	public static String getFechaActual(){
		String resp = null;
		try{
			resp = formato_large.format(new Date());
		}catch(Exception e){
			LOGGER.error("Error en getFechaActual: {}",e );
		}
		return resp;
	}
	
	public static void main(String[] args) {
		try {
			System.out.println("URL: "+Utils.urlShortener("http://199.231.161.38:8081/MCTransfersViamericas/get/transferByid?id="));
			//Instantiating the oAuth Service of Scribe-Java API
//	        OAuthService oAuthService = new ServiceBuilder()
//	                //Google Api Provider - Google's URL Shortener API is part of Google Platform APIs
//	                .provider(GoogleApi.class)
//	                /*
//	                    Using "anonymous" as API Key & Secret because Google's URL Shortener service
//	                    does not necessarily requires App identification and/or User Information Access
//	                 */
//	                .apiKey("ddb937256597af8c998e1d51da8269c713a34faa")
//	                .apiSecret("anonymous")
//	                //OAuth 2.0 scope for the Google URL Shortener API
//	                .scope("https://www.googleapis.com/auth/urlshortener")
//	                //build it!
//	                .build();
//
//	        //Instantiating oAuth Request of type POST and with Google URL Shortener API End Point URL
//	        OAuthRequest oAuthRequest = new OAuthRequest(Verb.POST, "https://www.googleapis.com/urlshortener/v1/url");
//	        //set the content type header to application/json - this is the type of content you are sending as payload
//	        oAuthRequest.addHeader("Content-Type", "application/json");
//	        //Preparing JSON payload to send url to Google URL Shortener
//	        String json = "{\"longUrl\": \"http://199.231.161.38:8081/MCTransfersViamericas/get/transferByid?id=\"}";
//	        //add xml payload to request
//	        oAuthRequest.addPayload(json);
//	        //send the request
//	        Response response = oAuthRequest.send();
//	        //print the response from server
//	        System.out.println("response.getBody() = " + response.getBody());
//	        //determining the generic type of map
//	        Type typeOfMap = new TypeToken<Map<String, String>>() {}.getType();
//	        //desrialize json to map
//	        Map<String, String> responseMap = new GsonBuilder().create().fromJson(response.getBody(), typeOfMap);
//	        //print id which is actually the shortened url
//	        System.out.println("Shortened URL = " + responseMap.get("id"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String urlShortener(String url){
		try {
//			GoogleCredential credential = new GoogleCredential().setAccessToken(accessToken);
//			Plus plus = new Plus.builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance(), credential)
//			    .setApplicationName("Google-PlusSample/1.0")
//			    .build();
//			OAuthService oAuthService = new ServiceBuilder()
//					.provider(GoogleApi.class)
//					.apiKey("ddb937256597af8c998e1d51da8269c713a34faa")
//					.apiSecret("anyonymous")
//                    .scope("https://accounts.google.com/o/oauth2/token") 
//                    .build();
//			
//			Token requestToken = oAuthService.getRequestToken();
//			
//		    OAuthRequest oAuthRequest = new OAuthRequest(Verb.POST, "https://accounts.google.com/o/oauth2/auth");
//		    oAuthRequest.addHeader("Content-Type", "application/json");
//		    String json = "{\"longUrl\": \""+url+"\"}";
//		    oAuthRequest.addPayload(json);
//
//		    oAuthService.signRequest(requestToken, oAuthRequest);
//		    
//		    Response response = oAuthRequest.send();
//		    
//		    @SuppressWarnings("serial")
//			Type typeOfMap = new TypeToken<Map<String, String>>() {}.getType();
//		    Map<String, String> responseMap = new GsonBuilder().create().fromJson(response.getBody(), typeOfMap);
//		    url = responseMap.get("id");

		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("OCURRIO UN ERROR EN URL SHORTENER - {}", e.getMessage());
		}
		return url;
	}
	
	static Urlshortener newUrlshortener() {
	    AppIdentityCredential credential =
	        new AppIdentityCredential(Arrays.asList(UrlshortenerScopes.URLSHORTENER));
	    return new Urlshortener.Builder(new UrlFetchTransport(), new JacksonFactory(), credential)
	        .build();
	  }
	
}
