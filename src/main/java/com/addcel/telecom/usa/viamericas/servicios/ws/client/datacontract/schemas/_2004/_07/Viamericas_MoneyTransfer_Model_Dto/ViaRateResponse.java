/**
 * ViaRateResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto;

public class ViaRateResponse  implements java.io.Serializable {
    private java.math.BigDecimal newExchangeRate;

    private java.math.BigDecimal originalExchangeRate;

    private java.math.BigDecimal totalPayReceiver;

    private java.math.BigDecimal viaRate;

    public ViaRateResponse() {
    }

    public ViaRateResponse(
           java.math.BigDecimal newExchangeRate,
           java.math.BigDecimal originalExchangeRate,
           java.math.BigDecimal totalPayReceiver,
           java.math.BigDecimal viaRate) {
           this.newExchangeRate = newExchangeRate;
           this.originalExchangeRate = originalExchangeRate;
           this.totalPayReceiver = totalPayReceiver;
           this.viaRate = viaRate;
    }


    /**
     * Gets the newExchangeRate value for this ViaRateResponse.
     * 
     * @return newExchangeRate
     */
    public java.math.BigDecimal getNewExchangeRate() {
        return newExchangeRate;
    }


    /**
     * Sets the newExchangeRate value for this ViaRateResponse.
     * 
     * @param newExchangeRate
     */
    public void setNewExchangeRate(java.math.BigDecimal newExchangeRate) {
        this.newExchangeRate = newExchangeRate;
    }


    /**
     * Gets the originalExchangeRate value for this ViaRateResponse.
     * 
     * @return originalExchangeRate
     */
    public java.math.BigDecimal getOriginalExchangeRate() {
        return originalExchangeRate;
    }


    /**
     * Sets the originalExchangeRate value for this ViaRateResponse.
     * 
     * @param originalExchangeRate
     */
    public void setOriginalExchangeRate(java.math.BigDecimal originalExchangeRate) {
        this.originalExchangeRate = originalExchangeRate;
    }


    /**
     * Gets the totalPayReceiver value for this ViaRateResponse.
     * 
     * @return totalPayReceiver
     */
    public java.math.BigDecimal getTotalPayReceiver() {
        return totalPayReceiver;
    }


    /**
     * Sets the totalPayReceiver value for this ViaRateResponse.
     * 
     * @param totalPayReceiver
     */
    public void setTotalPayReceiver(java.math.BigDecimal totalPayReceiver) {
        this.totalPayReceiver = totalPayReceiver;
    }


    /**
     * Gets the viaRate value for this ViaRateResponse.
     * 
     * @return viaRate
     */
    public java.math.BigDecimal getViaRate() {
        return viaRate;
    }


    /**
     * Sets the viaRate value for this ViaRateResponse.
     * 
     * @param viaRate
     */
    public void setViaRate(java.math.BigDecimal viaRate) {
        this.viaRate = viaRate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ViaRateResponse)) return false;
        ViaRateResponse other = (ViaRateResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.newExchangeRate==null && other.getNewExchangeRate()==null) || 
             (this.newExchangeRate!=null &&
              this.newExchangeRate.equals(other.getNewExchangeRate()))) &&
            ((this.originalExchangeRate==null && other.getOriginalExchangeRate()==null) || 
             (this.originalExchangeRate!=null &&
              this.originalExchangeRate.equals(other.getOriginalExchangeRate()))) &&
            ((this.totalPayReceiver==null && other.getTotalPayReceiver()==null) || 
             (this.totalPayReceiver!=null &&
              this.totalPayReceiver.equals(other.getTotalPayReceiver()))) &&
            ((this.viaRate==null && other.getViaRate()==null) || 
             (this.viaRate!=null &&
              this.viaRate.equals(other.getViaRate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNewExchangeRate() != null) {
            _hashCode += getNewExchangeRate().hashCode();
        }
        if (getOriginalExchangeRate() != null) {
            _hashCode += getOriginalExchangeRate().hashCode();
        }
        if (getTotalPayReceiver() != null) {
            _hashCode += getTotalPayReceiver().hashCode();
        }
        if (getViaRate() != null) {
            _hashCode += getViaRate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ViaRateResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ViaRateResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newExchangeRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NewExchangeRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originalExchangeRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "OriginalExchangeRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalPayReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "TotalPayReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("viaRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ViaRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
