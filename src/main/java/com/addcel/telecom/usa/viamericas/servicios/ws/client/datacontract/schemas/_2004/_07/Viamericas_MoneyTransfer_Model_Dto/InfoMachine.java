/**
 * InfoMachine.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto;

public class InfoMachine  implements java.io.Serializable {
    private java.lang.String browserType;

    private java.lang.String ipAddress;

    private java.lang.String partnerVersion;

    private java.lang.String phoneType;

    private java.lang.String softwareVersion;

    public InfoMachine() {
    }

    public InfoMachine(
           java.lang.String browserType,
           java.lang.String ipAddress,
           java.lang.String partnerVersion,
           java.lang.String phoneType,
           java.lang.String softwareVersion) {
           this.browserType = browserType;
           this.ipAddress = ipAddress;
           this.partnerVersion = partnerVersion;
           this.phoneType = phoneType;
           this.softwareVersion = softwareVersion;
    }


    /**
     * Gets the browserType value for this InfoMachine.
     * 
     * @return browserType
     */
    public java.lang.String getBrowserType() {
        return browserType;
    }


    /**
     * Sets the browserType value for this InfoMachine.
     * 
     * @param browserType
     */
    public void setBrowserType(java.lang.String browserType) {
        this.browserType = browserType;
    }


    /**
     * Gets the ipAddress value for this InfoMachine.
     * 
     * @return ipAddress
     */
    public java.lang.String getIpAddress() {
        return ipAddress;
    }


    /**
     * Sets the ipAddress value for this InfoMachine.
     * 
     * @param ipAddress
     */
    public void setIpAddress(java.lang.String ipAddress) {
        this.ipAddress = ipAddress;
    }


    /**
     * Gets the partnerVersion value for this InfoMachine.
     * 
     * @return partnerVersion
     */
    public java.lang.String getPartnerVersion() {
        return partnerVersion;
    }


    /**
     * Sets the partnerVersion value for this InfoMachine.
     * 
     * @param partnerVersion
     */
    public void setPartnerVersion(java.lang.String partnerVersion) {
        this.partnerVersion = partnerVersion;
    }


    /**
     * Gets the phoneType value for this InfoMachine.
     * 
     * @return phoneType
     */
    public java.lang.String getPhoneType() {
        return phoneType;
    }


    /**
     * Sets the phoneType value for this InfoMachine.
     * 
     * @param phoneType
     */
    public void setPhoneType(java.lang.String phoneType) {
        this.phoneType = phoneType;
    }


    /**
     * Gets the softwareVersion value for this InfoMachine.
     * 
     * @return softwareVersion
     */
    public java.lang.String getSoftwareVersion() {
        return softwareVersion;
    }


    /**
     * Sets the softwareVersion value for this InfoMachine.
     * 
     * @param softwareVersion
     */
    public void setSoftwareVersion(java.lang.String softwareVersion) {
        this.softwareVersion = softwareVersion;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InfoMachine)) return false;
        InfoMachine other = (InfoMachine) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.browserType==null && other.getBrowserType()==null) || 
             (this.browserType!=null &&
              this.browserType.equals(other.getBrowserType()))) &&
            ((this.ipAddress==null && other.getIpAddress()==null) || 
             (this.ipAddress!=null &&
              this.ipAddress.equals(other.getIpAddress()))) &&
            ((this.partnerVersion==null && other.getPartnerVersion()==null) || 
             (this.partnerVersion!=null &&
              this.partnerVersion.equals(other.getPartnerVersion()))) &&
            ((this.phoneType==null && other.getPhoneType()==null) || 
             (this.phoneType!=null &&
              this.phoneType.equals(other.getPhoneType()))) &&
            ((this.softwareVersion==null && other.getSoftwareVersion()==null) || 
             (this.softwareVersion!=null &&
              this.softwareVersion.equals(other.getSoftwareVersion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBrowserType() != null) {
            _hashCode += getBrowserType().hashCode();
        }
        if (getIpAddress() != null) {
            _hashCode += getIpAddress().hashCode();
        }
        if (getPartnerVersion() != null) {
            _hashCode += getPartnerVersion().hashCode();
        }
        if (getPhoneType() != null) {
            _hashCode += getPhoneType().hashCode();
        }
        if (getSoftwareVersion() != null) {
            _hashCode += getSoftwareVersion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InfoMachine.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "InfoMachine"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("browserType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "BrowserType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ipAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IpAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partnerVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PartnerVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phoneType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PhoneType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("softwareVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "SoftwareVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
