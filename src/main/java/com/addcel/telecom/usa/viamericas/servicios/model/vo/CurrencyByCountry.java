package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class CurrencyByCountry extends AbstractVO{

	private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetCurrenciesbyCountry_Result[] currency;

	public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetCurrenciesbyCountry_Result[] getCurrency() {
		return currency;
	}

	public void setCurrency(
			com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetCurrenciesbyCountry_Result[] currency) {
		this.currency = currency;
	}
	
}
