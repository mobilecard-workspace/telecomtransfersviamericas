/**
 * TransactionDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto;

public class TransactionDetail  implements java.io.Serializable {
    private java.lang.String idBranch;

    private java.math.BigDecimal idSender;

    private java.lang.String firstNameSender;

    private java.lang.String middleNameSender;

    private java.lang.String lastNameSender;

    private java.lang.String secondLastNameSender;

    private java.lang.String typeIdSender;

    private java.lang.String numberIdSender;

    private java.lang.String idCitySender;

    private java.lang.String nameCitySender;

    private java.lang.String idStateSender;

    private java.lang.String nameStateSender;

    private java.lang.String idCountrySender;

    private java.lang.String nameCountrySender;

    private java.lang.String addressSender;

    private java.math.BigDecimal phone1Sender;

    private java.math.BigDecimal phobe2Sender;

    private java.math.BigDecimal ZIPSender;

    private java.math.BigDecimal idReceiver;

    private java.math.BigDecimal idRecipient;

    private java.lang.String firstNameReceiver;

    private java.lang.String middleNameReceiver;

    private java.lang.String lastNameReceiver;

    private java.lang.String secondLastNameReceiver;

    private java.lang.String typeIdReceiver;

    private java.lang.String numberIdReceiver;

    private java.lang.String idCityReceiver;

    private java.lang.String nameCityReceiver;

    private java.lang.String idStateReceiver;

    private java.lang.String nameStateReceiver;

    private java.lang.String idCountryReceiver;

    private java.lang.String nameCountryReceiver;

    private java.lang.String addressReceiver;

    private java.lang.String phone1Receiver;

    private java.lang.String phone2Receiver;

    private java.lang.String ZIPReceiver;

    private java.lang.String emailReceiver;

    private java.math.BigDecimal telexReceiver;

    private java.lang.String payoutIdBranch;

    private java.lang.String payoutName;

    private java.lang.String payoutLocation;

    private java.lang.String idPayment;

    private java.lang.Integer idSenderPayment;

    private java.lang.String namePayment;

    private java.lang.String idCashier;

    private java.lang.String passwordReceiver;

    private java.util.Calendar transactionDate;

    private java.lang.String receiverDateAvailable;

    private java.lang.String modePayReceiver;

    private java.lang.String modPayCurrency;

    private java.math.BigDecimal rateChangeReceiver;

    private java.math.BigDecimal netAmountReceiver;

    private java.lang.String accountType;

    private java.lang.String accountNumber;

    private java.lang.String bankReceiver;

    private java.lang.String idFlagReceiver;

    private java.math.BigDecimal totalReceiver;

    private java.math.BigDecimal totalPayReceiver;

    private java.math.BigDecimal refReceiver;

    private java.lang.String idCurrency;

    private java.lang.String isoCurrency;

    private java.math.BigDecimal sourceCurrencyAmount;

    private java.math.BigDecimal sourceFeeAmount;

    private java.lang.String idCurrencySource;

    private java.lang.String isoCurrencySource;

    private java.lang.String notesReceiver;

    private java.lang.Boolean requireAdditionalInformation;

    private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.HoldMotive_Result[] holdMotives;

    public TransactionDetail() {
    }

    public TransactionDetail(
           java.lang.String idBranch,
           java.math.BigDecimal idSender,
           java.lang.String firstNameSender,
           java.lang.String middleNameSender,
           java.lang.String lastNameSender,
           java.lang.String secondLastNameSender,
           java.lang.String typeIdSender,
           java.lang.String numberIdSender,
           java.lang.String idCitySender,
           java.lang.String nameCitySender,
           java.lang.String idStateSender,
           java.lang.String nameStateSender,
           java.lang.String idCountrySender,
           java.lang.String nameCountrySender,
           java.lang.String addressSender,
           java.math.BigDecimal phone1Sender,
           java.math.BigDecimal phobe2Sender,
           java.math.BigDecimal ZIPSender,
           java.math.BigDecimal idReceiver,
           java.math.BigDecimal idRecipient,
           java.lang.String firstNameReceiver,
           java.lang.String middleNameReceiver,
           java.lang.String lastNameReceiver,
           java.lang.String secondLastNameReceiver,
           java.lang.String typeIdReceiver,
           java.lang.String numberIdReceiver,
           java.lang.String idCityReceiver,
           java.lang.String nameCityReceiver,
           java.lang.String idStateReceiver,
           java.lang.String nameStateReceiver,
           java.lang.String idCountryReceiver,
           java.lang.String nameCountryReceiver,
           java.lang.String addressReceiver,
           java.lang.String phone1Receiver,
           java.lang.String phone2Receiver,
           java.lang.String ZIPReceiver,
           java.lang.String emailReceiver,
           java.math.BigDecimal telexReceiver,
           java.lang.String payoutIdBranch,
           java.lang.String payoutName,
           java.lang.String payoutLocation,
           java.lang.String idPayment,
           java.lang.Integer idSenderPayment,
           java.lang.String namePayment,
           java.lang.String idCashier,
           java.lang.String passwordReceiver,
           java.util.Calendar transactionDate,
           java.lang.String receiverDateAvailable,
           java.lang.String modePayReceiver,
           java.lang.String modPayCurrency,
           java.math.BigDecimal rateChangeReceiver,
           java.math.BigDecimal netAmountReceiver,
           java.lang.String accountType,
           java.lang.String accountNumber,
           java.lang.String bankReceiver,
           java.lang.String idFlagReceiver,
           java.math.BigDecimal totalReceiver,
           java.math.BigDecimal totalPayReceiver,
           java.math.BigDecimal refReceiver,
           java.lang.String idCurrency,
           java.lang.String isoCurrency,
           java.math.BigDecimal sourceCurrencyAmount,
           java.math.BigDecimal sourceFeeAmount,
           java.lang.String idCurrencySource,
           java.lang.String isoCurrencySource,
           java.lang.String notesReceiver,
           java.lang.Boolean requireAdditionalInformation,
           com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.HoldMotive_Result[] holdMotives) {
           this.idBranch = idBranch;
           this.idSender = idSender;
           this.firstNameSender = firstNameSender;
           this.middleNameSender = middleNameSender;
           this.lastNameSender = lastNameSender;
           this.secondLastNameSender = secondLastNameSender;
           this.typeIdSender = typeIdSender;
           this.numberIdSender = numberIdSender;
           this.idCitySender = idCitySender;
           this.nameCitySender = nameCitySender;
           this.idStateSender = idStateSender;
           this.nameStateSender = nameStateSender;
           this.idCountrySender = idCountrySender;
           this.nameCountrySender = nameCountrySender;
           this.addressSender = addressSender;
           this.phone1Sender = phone1Sender;
           this.phobe2Sender = phobe2Sender;
           this.ZIPSender = ZIPSender;
           this.idReceiver = idReceiver;
           this.idRecipient = idRecipient;
           this.firstNameReceiver = firstNameReceiver;
           this.middleNameReceiver = middleNameReceiver;
           this.lastNameReceiver = lastNameReceiver;
           this.secondLastNameReceiver = secondLastNameReceiver;
           this.typeIdReceiver = typeIdReceiver;
           this.numberIdReceiver = numberIdReceiver;
           this.idCityReceiver = idCityReceiver;
           this.nameCityReceiver = nameCityReceiver;
           this.idStateReceiver = idStateReceiver;
           this.nameStateReceiver = nameStateReceiver;
           this.idCountryReceiver = idCountryReceiver;
           this.nameCountryReceiver = nameCountryReceiver;
           this.addressReceiver = addressReceiver;
           this.phone1Receiver = phone1Receiver;
           this.phone2Receiver = phone2Receiver;
           this.ZIPReceiver = ZIPReceiver;
           this.emailReceiver = emailReceiver;
           this.telexReceiver = telexReceiver;
           this.payoutIdBranch = payoutIdBranch;
           this.payoutName = payoutName;
           this.payoutLocation = payoutLocation;
           this.idPayment = idPayment;
           this.idSenderPayment = idSenderPayment;
           this.namePayment = namePayment;
           this.idCashier = idCashier;
           this.passwordReceiver = passwordReceiver;
           this.transactionDate = transactionDate;
           this.receiverDateAvailable = receiverDateAvailable;
           this.modePayReceiver = modePayReceiver;
           this.modPayCurrency = modPayCurrency;
           this.rateChangeReceiver = rateChangeReceiver;
           this.netAmountReceiver = netAmountReceiver;
           this.accountType = accountType;
           this.accountNumber = accountNumber;
           this.bankReceiver = bankReceiver;
           this.idFlagReceiver = idFlagReceiver;
           this.totalReceiver = totalReceiver;
           this.totalPayReceiver = totalPayReceiver;
           this.refReceiver = refReceiver;
           this.idCurrency = idCurrency;
           this.isoCurrency = isoCurrency;
           this.sourceCurrencyAmount = sourceCurrencyAmount;
           this.sourceFeeAmount = sourceFeeAmount;
           this.idCurrencySource = idCurrencySource;
           this.isoCurrencySource = isoCurrencySource;
           this.notesReceiver = notesReceiver;
           this.requireAdditionalInformation = requireAdditionalInformation;
           this.holdMotives = holdMotives;
    }


    /**
     * Gets the idBranch value for this TransactionDetail.
     * 
     * @return idBranch
     */
    public java.lang.String getIdBranch() {
        return idBranch;
    }


    /**
     * Sets the idBranch value for this TransactionDetail.
     * 
     * @param idBranch
     */
    public void setIdBranch(java.lang.String idBranch) {
        this.idBranch = idBranch;
    }


    /**
     * Gets the idSender value for this TransactionDetail.
     * 
     * @return idSender
     */
    public java.math.BigDecimal getIdSender() {
        return idSender;
    }


    /**
     * Sets the idSender value for this TransactionDetail.
     * 
     * @param idSender
     */
    public void setIdSender(java.math.BigDecimal idSender) {
        this.idSender = idSender;
    }


    /**
     * Gets the firstNameSender value for this TransactionDetail.
     * 
     * @return firstNameSender
     */
    public java.lang.String getFirstNameSender() {
        return firstNameSender;
    }


    /**
     * Sets the firstNameSender value for this TransactionDetail.
     * 
     * @param firstNameSender
     */
    public void setFirstNameSender(java.lang.String firstNameSender) {
        this.firstNameSender = firstNameSender;
    }


    /**
     * Gets the middleNameSender value for this TransactionDetail.
     * 
     * @return middleNameSender
     */
    public java.lang.String getMiddleNameSender() {
        return middleNameSender;
    }


    /**
     * Sets the middleNameSender value for this TransactionDetail.
     * 
     * @param middleNameSender
     */
    public void setMiddleNameSender(java.lang.String middleNameSender) {
        this.middleNameSender = middleNameSender;
    }


    /**
     * Gets the lastNameSender value for this TransactionDetail.
     * 
     * @return lastNameSender
     */
    public java.lang.String getLastNameSender() {
        return lastNameSender;
    }


    /**
     * Sets the lastNameSender value for this TransactionDetail.
     * 
     * @param lastNameSender
     */
    public void setLastNameSender(java.lang.String lastNameSender) {
        this.lastNameSender = lastNameSender;
    }


    /**
     * Gets the secondLastNameSender value for this TransactionDetail.
     * 
     * @return secondLastNameSender
     */
    public java.lang.String getSecondLastNameSender() {
        return secondLastNameSender;
    }


    /**
     * Sets the secondLastNameSender value for this TransactionDetail.
     * 
     * @param secondLastNameSender
     */
    public void setSecondLastNameSender(java.lang.String secondLastNameSender) {
        this.secondLastNameSender = secondLastNameSender;
    }


    /**
     * Gets the typeIdSender value for this TransactionDetail.
     * 
     * @return typeIdSender
     */
    public java.lang.String getTypeIdSender() {
        return typeIdSender;
    }


    /**
     * Sets the typeIdSender value for this TransactionDetail.
     * 
     * @param typeIdSender
     */
    public void setTypeIdSender(java.lang.String typeIdSender) {
        this.typeIdSender = typeIdSender;
    }


    /**
     * Gets the numberIdSender value for this TransactionDetail.
     * 
     * @return numberIdSender
     */
    public java.lang.String getNumberIdSender() {
        return numberIdSender;
    }


    /**
     * Sets the numberIdSender value for this TransactionDetail.
     * 
     * @param numberIdSender
     */
    public void setNumberIdSender(java.lang.String numberIdSender) {
        this.numberIdSender = numberIdSender;
    }


    /**
     * Gets the idCitySender value for this TransactionDetail.
     * 
     * @return idCitySender
     */
    public java.lang.String getIdCitySender() {
        return idCitySender;
    }


    /**
     * Sets the idCitySender value for this TransactionDetail.
     * 
     * @param idCitySender
     */
    public void setIdCitySender(java.lang.String idCitySender) {
        this.idCitySender = idCitySender;
    }


    /**
     * Gets the nameCitySender value for this TransactionDetail.
     * 
     * @return nameCitySender
     */
    public java.lang.String getNameCitySender() {
        return nameCitySender;
    }


    /**
     * Sets the nameCitySender value for this TransactionDetail.
     * 
     * @param nameCitySender
     */
    public void setNameCitySender(java.lang.String nameCitySender) {
        this.nameCitySender = nameCitySender;
    }


    /**
     * Gets the idStateSender value for this TransactionDetail.
     * 
     * @return idStateSender
     */
    public java.lang.String getIdStateSender() {
        return idStateSender;
    }


    /**
     * Sets the idStateSender value for this TransactionDetail.
     * 
     * @param idStateSender
     */
    public void setIdStateSender(java.lang.String idStateSender) {
        this.idStateSender = idStateSender;
    }


    /**
     * Gets the nameStateSender value for this TransactionDetail.
     * 
     * @return nameStateSender
     */
    public java.lang.String getNameStateSender() {
        return nameStateSender;
    }


    /**
     * Sets the nameStateSender value for this TransactionDetail.
     * 
     * @param nameStateSender
     */
    public void setNameStateSender(java.lang.String nameStateSender) {
        this.nameStateSender = nameStateSender;
    }


    /**
     * Gets the idCountrySender value for this TransactionDetail.
     * 
     * @return idCountrySender
     */
    public java.lang.String getIdCountrySender() {
        return idCountrySender;
    }


    /**
     * Sets the idCountrySender value for this TransactionDetail.
     * 
     * @param idCountrySender
     */
    public void setIdCountrySender(java.lang.String idCountrySender) {
        this.idCountrySender = idCountrySender;
    }


    /**
     * Gets the nameCountrySender value for this TransactionDetail.
     * 
     * @return nameCountrySender
     */
    public java.lang.String getNameCountrySender() {
        return nameCountrySender;
    }


    /**
     * Sets the nameCountrySender value for this TransactionDetail.
     * 
     * @param nameCountrySender
     */
    public void setNameCountrySender(java.lang.String nameCountrySender) {
        this.nameCountrySender = nameCountrySender;
    }


    /**
     * Gets the addressSender value for this TransactionDetail.
     * 
     * @return addressSender
     */
    public java.lang.String getAddressSender() {
        return addressSender;
    }


    /**
     * Sets the addressSender value for this TransactionDetail.
     * 
     * @param addressSender
     */
    public void setAddressSender(java.lang.String addressSender) {
        this.addressSender = addressSender;
    }


    /**
     * Gets the phone1Sender value for this TransactionDetail.
     * 
     * @return phone1Sender
     */
    public java.math.BigDecimal getPhone1Sender() {
        return phone1Sender;
    }


    /**
     * Sets the phone1Sender value for this TransactionDetail.
     * 
     * @param phone1Sender
     */
    public void setPhone1Sender(java.math.BigDecimal phone1Sender) {
        this.phone1Sender = phone1Sender;
    }


    /**
     * Gets the phobe2Sender value for this TransactionDetail.
     * 
     * @return phobe2Sender
     */
    public java.math.BigDecimal getPhobe2Sender() {
        return phobe2Sender;
    }


    /**
     * Sets the phobe2Sender value for this TransactionDetail.
     * 
     * @param phobe2Sender
     */
    public void setPhobe2Sender(java.math.BigDecimal phobe2Sender) {
        this.phobe2Sender = phobe2Sender;
    }


    /**
     * Gets the ZIPSender value for this TransactionDetail.
     * 
     * @return ZIPSender
     */
    public java.math.BigDecimal getZIPSender() {
        return ZIPSender;
    }


    /**
     * Sets the ZIPSender value for this TransactionDetail.
     * 
     * @param ZIPSender
     */
    public void setZIPSender(java.math.BigDecimal ZIPSender) {
        this.ZIPSender = ZIPSender;
    }


    /**
     * Gets the idReceiver value for this TransactionDetail.
     * 
     * @return idReceiver
     */
    public java.math.BigDecimal getIdReceiver() {
        return idReceiver;
    }


    /**
     * Sets the idReceiver value for this TransactionDetail.
     * 
     * @param idReceiver
     */
    public void setIdReceiver(java.math.BigDecimal idReceiver) {
        this.idReceiver = idReceiver;
    }


    /**
     * Gets the idRecipient value for this TransactionDetail.
     * 
     * @return idRecipient
     */
    public java.math.BigDecimal getIdRecipient() {
        return idRecipient;
    }


    /**
     * Sets the idRecipient value for this TransactionDetail.
     * 
     * @param idRecipient
     */
    public void setIdRecipient(java.math.BigDecimal idRecipient) {
        this.idRecipient = idRecipient;
    }


    /**
     * Gets the firstNameReceiver value for this TransactionDetail.
     * 
     * @return firstNameReceiver
     */
    public java.lang.String getFirstNameReceiver() {
        return firstNameReceiver;
    }


    /**
     * Sets the firstNameReceiver value for this TransactionDetail.
     * 
     * @param firstNameReceiver
     */
    public void setFirstNameReceiver(java.lang.String firstNameReceiver) {
        this.firstNameReceiver = firstNameReceiver;
    }


    /**
     * Gets the middleNameReceiver value for this TransactionDetail.
     * 
     * @return middleNameReceiver
     */
    public java.lang.String getMiddleNameReceiver() {
        return middleNameReceiver;
    }


    /**
     * Sets the middleNameReceiver value for this TransactionDetail.
     * 
     * @param middleNameReceiver
     */
    public void setMiddleNameReceiver(java.lang.String middleNameReceiver) {
        this.middleNameReceiver = middleNameReceiver;
    }


    /**
     * Gets the lastNameReceiver value for this TransactionDetail.
     * 
     * @return lastNameReceiver
     */
    public java.lang.String getLastNameReceiver() {
        return lastNameReceiver;
    }


    /**
     * Sets the lastNameReceiver value for this TransactionDetail.
     * 
     * @param lastNameReceiver
     */
    public void setLastNameReceiver(java.lang.String lastNameReceiver) {
        this.lastNameReceiver = lastNameReceiver;
    }


    /**
     * Gets the secondLastNameReceiver value for this TransactionDetail.
     * 
     * @return secondLastNameReceiver
     */
    public java.lang.String getSecondLastNameReceiver() {
        return secondLastNameReceiver;
    }


    /**
     * Sets the secondLastNameReceiver value for this TransactionDetail.
     * 
     * @param secondLastNameReceiver
     */
    public void setSecondLastNameReceiver(java.lang.String secondLastNameReceiver) {
        this.secondLastNameReceiver = secondLastNameReceiver;
    }


    /**
     * Gets the typeIdReceiver value for this TransactionDetail.
     * 
     * @return typeIdReceiver
     */
    public java.lang.String getTypeIdReceiver() {
        return typeIdReceiver;
    }


    /**
     * Sets the typeIdReceiver value for this TransactionDetail.
     * 
     * @param typeIdReceiver
     */
    public void setTypeIdReceiver(java.lang.String typeIdReceiver) {
        this.typeIdReceiver = typeIdReceiver;
    }


    /**
     * Gets the numberIdReceiver value for this TransactionDetail.
     * 
     * @return numberIdReceiver
     */
    public java.lang.String getNumberIdReceiver() {
        return numberIdReceiver;
    }


    /**
     * Sets the numberIdReceiver value for this TransactionDetail.
     * 
     * @param numberIdReceiver
     */
    public void setNumberIdReceiver(java.lang.String numberIdReceiver) {
        this.numberIdReceiver = numberIdReceiver;
    }


    /**
     * Gets the idCityReceiver value for this TransactionDetail.
     * 
     * @return idCityReceiver
     */
    public java.lang.String getIdCityReceiver() {
        return idCityReceiver;
    }


    /**
     * Sets the idCityReceiver value for this TransactionDetail.
     * 
     * @param idCityReceiver
     */
    public void setIdCityReceiver(java.lang.String idCityReceiver) {
        this.idCityReceiver = idCityReceiver;
    }


    /**
     * Gets the nameCityReceiver value for this TransactionDetail.
     * 
     * @return nameCityReceiver
     */
    public java.lang.String getNameCityReceiver() {
        return nameCityReceiver;
    }


    /**
     * Sets the nameCityReceiver value for this TransactionDetail.
     * 
     * @param nameCityReceiver
     */
    public void setNameCityReceiver(java.lang.String nameCityReceiver) {
        this.nameCityReceiver = nameCityReceiver;
    }


    /**
     * Gets the idStateReceiver value for this TransactionDetail.
     * 
     * @return idStateReceiver
     */
    public java.lang.String getIdStateReceiver() {
        return idStateReceiver;
    }


    /**
     * Sets the idStateReceiver value for this TransactionDetail.
     * 
     * @param idStateReceiver
     */
    public void setIdStateReceiver(java.lang.String idStateReceiver) {
        this.idStateReceiver = idStateReceiver;
    }


    /**
     * Gets the nameStateReceiver value for this TransactionDetail.
     * 
     * @return nameStateReceiver
     */
    public java.lang.String getNameStateReceiver() {
        return nameStateReceiver;
    }


    /**
     * Sets the nameStateReceiver value for this TransactionDetail.
     * 
     * @param nameStateReceiver
     */
    public void setNameStateReceiver(java.lang.String nameStateReceiver) {
        this.nameStateReceiver = nameStateReceiver;
    }


    /**
     * Gets the idCountryReceiver value for this TransactionDetail.
     * 
     * @return idCountryReceiver
     */
    public java.lang.String getIdCountryReceiver() {
        return idCountryReceiver;
    }


    /**
     * Sets the idCountryReceiver value for this TransactionDetail.
     * 
     * @param idCountryReceiver
     */
    public void setIdCountryReceiver(java.lang.String idCountryReceiver) {
        this.idCountryReceiver = idCountryReceiver;
    }


    /**
     * Gets the nameCountryReceiver value for this TransactionDetail.
     * 
     * @return nameCountryReceiver
     */
    public java.lang.String getNameCountryReceiver() {
        return nameCountryReceiver;
    }


    /**
     * Sets the nameCountryReceiver value for this TransactionDetail.
     * 
     * @param nameCountryReceiver
     */
    public void setNameCountryReceiver(java.lang.String nameCountryReceiver) {
        this.nameCountryReceiver = nameCountryReceiver;
    }


    /**
     * Gets the addressReceiver value for this TransactionDetail.
     * 
     * @return addressReceiver
     */
    public java.lang.String getAddressReceiver() {
        return addressReceiver;
    }


    /**
     * Sets the addressReceiver value for this TransactionDetail.
     * 
     * @param addressReceiver
     */
    public void setAddressReceiver(java.lang.String addressReceiver) {
        this.addressReceiver = addressReceiver;
    }


    /**
     * Gets the phone1Receiver value for this TransactionDetail.
     * 
     * @return phone1Receiver
     */
    public java.lang.String getPhone1Receiver() {
        return phone1Receiver;
    }


    /**
     * Sets the phone1Receiver value for this TransactionDetail.
     * 
     * @param phone1Receiver
     */
    public void setPhone1Receiver(java.lang.String phone1Receiver) {
        this.phone1Receiver = phone1Receiver;
    }


    /**
     * Gets the phone2Receiver value for this TransactionDetail.
     * 
     * @return phone2Receiver
     */
    public java.lang.String getPhone2Receiver() {
        return phone2Receiver;
    }


    /**
     * Sets the phone2Receiver value for this TransactionDetail.
     * 
     * @param phone2Receiver
     */
    public void setPhone2Receiver(java.lang.String phone2Receiver) {
        this.phone2Receiver = phone2Receiver;
    }


    /**
     * Gets the ZIPReceiver value for this TransactionDetail.
     * 
     * @return ZIPReceiver
     */
    public java.lang.String getZIPReceiver() {
        return ZIPReceiver;
    }


    /**
     * Sets the ZIPReceiver value for this TransactionDetail.
     * 
     * @param ZIPReceiver
     */
    public void setZIPReceiver(java.lang.String ZIPReceiver) {
        this.ZIPReceiver = ZIPReceiver;
    }


    /**
     * Gets the emailReceiver value for this TransactionDetail.
     * 
     * @return emailReceiver
     */
    public java.lang.String getEmailReceiver() {
        return emailReceiver;
    }


    /**
     * Sets the emailReceiver value for this TransactionDetail.
     * 
     * @param emailReceiver
     */
    public void setEmailReceiver(java.lang.String emailReceiver) {
        this.emailReceiver = emailReceiver;
    }


    /**
     * Gets the telexReceiver value for this TransactionDetail.
     * 
     * @return telexReceiver
     */
    public java.math.BigDecimal getTelexReceiver() {
        return telexReceiver;
    }


    /**
     * Sets the telexReceiver value for this TransactionDetail.
     * 
     * @param telexReceiver
     */
    public void setTelexReceiver(java.math.BigDecimal telexReceiver) {
        this.telexReceiver = telexReceiver;
    }


    /**
     * Gets the payoutIdBranch value for this TransactionDetail.
     * 
     * @return payoutIdBranch
     */
    public java.lang.String getPayoutIdBranch() {
        return payoutIdBranch;
    }


    /**
     * Sets the payoutIdBranch value for this TransactionDetail.
     * 
     * @param payoutIdBranch
     */
    public void setPayoutIdBranch(java.lang.String payoutIdBranch) {
        this.payoutIdBranch = payoutIdBranch;
    }


    /**
     * Gets the payoutName value for this TransactionDetail.
     * 
     * @return payoutName
     */
    public java.lang.String getPayoutName() {
        return payoutName;
    }


    /**
     * Sets the payoutName value for this TransactionDetail.
     * 
     * @param payoutName
     */
    public void setPayoutName(java.lang.String payoutName) {
        this.payoutName = payoutName;
    }


    /**
     * Gets the payoutLocation value for this TransactionDetail.
     * 
     * @return payoutLocation
     */
    public java.lang.String getPayoutLocation() {
        return payoutLocation;
    }


    /**
     * Sets the payoutLocation value for this TransactionDetail.
     * 
     * @param payoutLocation
     */
    public void setPayoutLocation(java.lang.String payoutLocation) {
        this.payoutLocation = payoutLocation;
    }


    /**
     * Gets the idPayment value for this TransactionDetail.
     * 
     * @return idPayment
     */
    public java.lang.String getIdPayment() {
        return idPayment;
    }


    /**
     * Sets the idPayment value for this TransactionDetail.
     * 
     * @param idPayment
     */
    public void setIdPayment(java.lang.String idPayment) {
        this.idPayment = idPayment;
    }


    /**
     * Gets the idSenderPayment value for this TransactionDetail.
     * 
     * @return idSenderPayment
     */
    public java.lang.Integer getIdSenderPayment() {
        return idSenderPayment;
    }


    /**
     * Sets the idSenderPayment value for this TransactionDetail.
     * 
     * @param idSenderPayment
     */
    public void setIdSenderPayment(java.lang.Integer idSenderPayment) {
        this.idSenderPayment = idSenderPayment;
    }


    /**
     * Gets the namePayment value for this TransactionDetail.
     * 
     * @return namePayment
     */
    public java.lang.String getNamePayment() {
        return namePayment;
    }


    /**
     * Sets the namePayment value for this TransactionDetail.
     * 
     * @param namePayment
     */
    public void setNamePayment(java.lang.String namePayment) {
        this.namePayment = namePayment;
    }


    /**
     * Gets the idCashier value for this TransactionDetail.
     * 
     * @return idCashier
     */
    public java.lang.String getIdCashier() {
        return idCashier;
    }


    /**
     * Sets the idCashier value for this TransactionDetail.
     * 
     * @param idCashier
     */
    public void setIdCashier(java.lang.String idCashier) {
        this.idCashier = idCashier;
    }


    /**
     * Gets the passwordReceiver value for this TransactionDetail.
     * 
     * @return passwordReceiver
     */
    public java.lang.String getPasswordReceiver() {
        return passwordReceiver;
    }


    /**
     * Sets the passwordReceiver value for this TransactionDetail.
     * 
     * @param passwordReceiver
     */
    public void setPasswordReceiver(java.lang.String passwordReceiver) {
        this.passwordReceiver = passwordReceiver;
    }


    /**
     * Gets the transactionDate value for this TransactionDetail.
     * 
     * @return transactionDate
     */
    public java.util.Calendar getTransactionDate() {
        return transactionDate;
    }


    /**
     * Sets the transactionDate value for this TransactionDetail.
     * 
     * @param transactionDate
     */
    public void setTransactionDate(java.util.Calendar transactionDate) {
        this.transactionDate = transactionDate;
    }


    /**
     * Gets the receiverDateAvailable value for this TransactionDetail.
     * 
     * @return receiverDateAvailable
     */
    public java.lang.String getReceiverDateAvailable() {
        return receiverDateAvailable;
    }


    /**
     * Sets the receiverDateAvailable value for this TransactionDetail.
     * 
     * @param receiverDateAvailable
     */
    public void setReceiverDateAvailable(java.lang.String receiverDateAvailable) {
        this.receiverDateAvailable = receiverDateAvailable;
    }


    /**
     * Gets the modePayReceiver value for this TransactionDetail.
     * 
     * @return modePayReceiver
     */
    public java.lang.String getModePayReceiver() {
        return modePayReceiver;
    }


    /**
     * Sets the modePayReceiver value for this TransactionDetail.
     * 
     * @param modePayReceiver
     */
    public void setModePayReceiver(java.lang.String modePayReceiver) {
        this.modePayReceiver = modePayReceiver;
    }


    /**
     * Gets the modPayCurrency value for this TransactionDetail.
     * 
     * @return modPayCurrency
     */
    public java.lang.String getModPayCurrency() {
        return modPayCurrency;
    }


    /**
     * Sets the modPayCurrency value for this TransactionDetail.
     * 
     * @param modPayCurrency
     */
    public void setModPayCurrency(java.lang.String modPayCurrency) {
        this.modPayCurrency = modPayCurrency;
    }


    /**
     * Gets the rateChangeReceiver value for this TransactionDetail.
     * 
     * @return rateChangeReceiver
     */
    public java.math.BigDecimal getRateChangeReceiver() {
        return rateChangeReceiver;
    }


    /**
     * Sets the rateChangeReceiver value for this TransactionDetail.
     * 
     * @param rateChangeReceiver
     */
    public void setRateChangeReceiver(java.math.BigDecimal rateChangeReceiver) {
        this.rateChangeReceiver = rateChangeReceiver;
    }


    /**
     * Gets the netAmountReceiver value for this TransactionDetail.
     * 
     * @return netAmountReceiver
     */
    public java.math.BigDecimal getNetAmountReceiver() {
        return netAmountReceiver;
    }


    /**
     * Sets the netAmountReceiver value for this TransactionDetail.
     * 
     * @param netAmountReceiver
     */
    public void setNetAmountReceiver(java.math.BigDecimal netAmountReceiver) {
        this.netAmountReceiver = netAmountReceiver;
    }


    /**
     * Gets the accountType value for this TransactionDetail.
     * 
     * @return accountType
     */
    public java.lang.String getAccountType() {
        return accountType;
    }


    /**
     * Sets the accountType value for this TransactionDetail.
     * 
     * @param accountType
     */
    public void setAccountType(java.lang.String accountType) {
        this.accountType = accountType;
    }


    /**
     * Gets the accountNumber value for this TransactionDetail.
     * 
     * @return accountNumber
     */
    public java.lang.String getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this TransactionDetail.
     * 
     * @param accountNumber
     */
    public void setAccountNumber(java.lang.String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the bankReceiver value for this TransactionDetail.
     * 
     * @return bankReceiver
     */
    public java.lang.String getBankReceiver() {
        return bankReceiver;
    }


    /**
     * Sets the bankReceiver value for this TransactionDetail.
     * 
     * @param bankReceiver
     */
    public void setBankReceiver(java.lang.String bankReceiver) {
        this.bankReceiver = bankReceiver;
    }


    /**
     * Gets the idFlagReceiver value for this TransactionDetail.
     * 
     * @return idFlagReceiver
     */
    public java.lang.String getIdFlagReceiver() {
        return idFlagReceiver;
    }


    /**
     * Sets the idFlagReceiver value for this TransactionDetail.
     * 
     * @param idFlagReceiver
     */
    public void setIdFlagReceiver(java.lang.String idFlagReceiver) {
        this.idFlagReceiver = idFlagReceiver;
    }


    /**
     * Gets the totalReceiver value for this TransactionDetail.
     * 
     * @return totalReceiver
     */
    public java.math.BigDecimal getTotalReceiver() {
        return totalReceiver;
    }


    /**
     * Sets the totalReceiver value for this TransactionDetail.
     * 
     * @param totalReceiver
     */
    public void setTotalReceiver(java.math.BigDecimal totalReceiver) {
        this.totalReceiver = totalReceiver;
    }


    /**
     * Gets the totalPayReceiver value for this TransactionDetail.
     * 
     * @return totalPayReceiver
     */
    public java.math.BigDecimal getTotalPayReceiver() {
        return totalPayReceiver;
    }


    /**
     * Sets the totalPayReceiver value for this TransactionDetail.
     * 
     * @param totalPayReceiver
     */
    public void setTotalPayReceiver(java.math.BigDecimal totalPayReceiver) {
        this.totalPayReceiver = totalPayReceiver;
    }


    /**
     * Gets the refReceiver value for this TransactionDetail.
     * 
     * @return refReceiver
     */
    public java.math.BigDecimal getRefReceiver() {
        return refReceiver;
    }


    /**
     * Sets the refReceiver value for this TransactionDetail.
     * 
     * @param refReceiver
     */
    public void setRefReceiver(java.math.BigDecimal refReceiver) {
        this.refReceiver = refReceiver;
    }


    /**
     * Gets the idCurrency value for this TransactionDetail.
     * 
     * @return idCurrency
     */
    public java.lang.String getIdCurrency() {
        return idCurrency;
    }


    /**
     * Sets the idCurrency value for this TransactionDetail.
     * 
     * @param idCurrency
     */
    public void setIdCurrency(java.lang.String idCurrency) {
        this.idCurrency = idCurrency;
    }


    /**
     * Gets the isoCurrency value for this TransactionDetail.
     * 
     * @return isoCurrency
     */
    public java.lang.String getIsoCurrency() {
        return isoCurrency;
    }


    /**
     * Sets the isoCurrency value for this TransactionDetail.
     * 
     * @param isoCurrency
     */
    public void setIsoCurrency(java.lang.String isoCurrency) {
        this.isoCurrency = isoCurrency;
    }


    /**
     * Gets the sourceCurrencyAmount value for this TransactionDetail.
     * 
     * @return sourceCurrencyAmount
     */
    public java.math.BigDecimal getSourceCurrencyAmount() {
        return sourceCurrencyAmount;
    }


    /**
     * Sets the sourceCurrencyAmount value for this TransactionDetail.
     * 
     * @param sourceCurrencyAmount
     */
    public void setSourceCurrencyAmount(java.math.BigDecimal sourceCurrencyAmount) {
        this.sourceCurrencyAmount = sourceCurrencyAmount;
    }


    /**
     * Gets the sourceFeeAmount value for this TransactionDetail.
     * 
     * @return sourceFeeAmount
     */
    public java.math.BigDecimal getSourceFeeAmount() {
        return sourceFeeAmount;
    }


    /**
     * Sets the sourceFeeAmount value for this TransactionDetail.
     * 
     * @param sourceFeeAmount
     */
    public void setSourceFeeAmount(java.math.BigDecimal sourceFeeAmount) {
        this.sourceFeeAmount = sourceFeeAmount;
    }


    /**
     * Gets the idCurrencySource value for this TransactionDetail.
     * 
     * @return idCurrencySource
     */
    public java.lang.String getIdCurrencySource() {
        return idCurrencySource;
    }


    /**
     * Sets the idCurrencySource value for this TransactionDetail.
     * 
     * @param idCurrencySource
     */
    public void setIdCurrencySource(java.lang.String idCurrencySource) {
        this.idCurrencySource = idCurrencySource;
    }


    /**
     * Gets the isoCurrencySource value for this TransactionDetail.
     * 
     * @return isoCurrencySource
     */
    public java.lang.String getIsoCurrencySource() {
        return isoCurrencySource;
    }


    /**
     * Sets the isoCurrencySource value for this TransactionDetail.
     * 
     * @param isoCurrencySource
     */
    public void setIsoCurrencySource(java.lang.String isoCurrencySource) {
        this.isoCurrencySource = isoCurrencySource;
    }


    /**
     * Gets the notesReceiver value for this TransactionDetail.
     * 
     * @return notesReceiver
     */
    public java.lang.String getNotesReceiver() {
        return notesReceiver;
    }


    /**
     * Sets the notesReceiver value for this TransactionDetail.
     * 
     * @param notesReceiver
     */
    public void setNotesReceiver(java.lang.String notesReceiver) {
        this.notesReceiver = notesReceiver;
    }


    /**
     * Gets the requireAdditionalInformation value for this TransactionDetail.
     * 
     * @return requireAdditionalInformation
     */
    public java.lang.Boolean getRequireAdditionalInformation() {
        return requireAdditionalInformation;
    }


    /**
     * Sets the requireAdditionalInformation value for this TransactionDetail.
     * 
     * @param requireAdditionalInformation
     */
    public void setRequireAdditionalInformation(java.lang.Boolean requireAdditionalInformation) {
        this.requireAdditionalInformation = requireAdditionalInformation;
    }


    /**
     * Gets the holdMotives value for this TransactionDetail.
     * 
     * @return holdMotives
     */
    public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.HoldMotive_Result[] getHoldMotives() {
        return holdMotives;
    }


    /**
     * Sets the holdMotives value for this TransactionDetail.
     * 
     * @param holdMotives
     */
    public void setHoldMotives(com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.HoldMotive_Result[] holdMotives) {
        this.holdMotives = holdMotives;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransactionDetail)) return false;
        TransactionDetail other = (TransactionDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idBranch==null && other.getIdBranch()==null) || 
             (this.idBranch!=null &&
              this.idBranch.equals(other.getIdBranch()))) &&
            ((this.idSender==null && other.getIdSender()==null) || 
             (this.idSender!=null &&
              this.idSender.equals(other.getIdSender()))) &&
            ((this.firstNameSender==null && other.getFirstNameSender()==null) || 
             (this.firstNameSender!=null &&
              this.firstNameSender.equals(other.getFirstNameSender()))) &&
            ((this.middleNameSender==null && other.getMiddleNameSender()==null) || 
             (this.middleNameSender!=null &&
              this.middleNameSender.equals(other.getMiddleNameSender()))) &&
            ((this.lastNameSender==null && other.getLastNameSender()==null) || 
             (this.lastNameSender!=null &&
              this.lastNameSender.equals(other.getLastNameSender()))) &&
            ((this.secondLastNameSender==null && other.getSecondLastNameSender()==null) || 
             (this.secondLastNameSender!=null &&
              this.secondLastNameSender.equals(other.getSecondLastNameSender()))) &&
            ((this.typeIdSender==null && other.getTypeIdSender()==null) || 
             (this.typeIdSender!=null &&
              this.typeIdSender.equals(other.getTypeIdSender()))) &&
            ((this.numberIdSender==null && other.getNumberIdSender()==null) || 
             (this.numberIdSender!=null &&
              this.numberIdSender.equals(other.getNumberIdSender()))) &&
            ((this.idCitySender==null && other.getIdCitySender()==null) || 
             (this.idCitySender!=null &&
              this.idCitySender.equals(other.getIdCitySender()))) &&
            ((this.nameCitySender==null && other.getNameCitySender()==null) || 
             (this.nameCitySender!=null &&
              this.nameCitySender.equals(other.getNameCitySender()))) &&
            ((this.idStateSender==null && other.getIdStateSender()==null) || 
             (this.idStateSender!=null &&
              this.idStateSender.equals(other.getIdStateSender()))) &&
            ((this.nameStateSender==null && other.getNameStateSender()==null) || 
             (this.nameStateSender!=null &&
              this.nameStateSender.equals(other.getNameStateSender()))) &&
            ((this.idCountrySender==null && other.getIdCountrySender()==null) || 
             (this.idCountrySender!=null &&
              this.idCountrySender.equals(other.getIdCountrySender()))) &&
            ((this.nameCountrySender==null && other.getNameCountrySender()==null) || 
             (this.nameCountrySender!=null &&
              this.nameCountrySender.equals(other.getNameCountrySender()))) &&
            ((this.addressSender==null && other.getAddressSender()==null) || 
             (this.addressSender!=null &&
              this.addressSender.equals(other.getAddressSender()))) &&
            ((this.phone1Sender==null && other.getPhone1Sender()==null) || 
             (this.phone1Sender!=null &&
              this.phone1Sender.equals(other.getPhone1Sender()))) &&
            ((this.phobe2Sender==null && other.getPhobe2Sender()==null) || 
             (this.phobe2Sender!=null &&
              this.phobe2Sender.equals(other.getPhobe2Sender()))) &&
            ((this.ZIPSender==null && other.getZIPSender()==null) || 
             (this.ZIPSender!=null &&
              this.ZIPSender.equals(other.getZIPSender()))) &&
            ((this.idReceiver==null && other.getIdReceiver()==null) || 
             (this.idReceiver!=null &&
              this.idReceiver.equals(other.getIdReceiver()))) &&
            ((this.idRecipient==null && other.getIdRecipient()==null) || 
             (this.idRecipient!=null &&
              this.idRecipient.equals(other.getIdRecipient()))) &&
            ((this.firstNameReceiver==null && other.getFirstNameReceiver()==null) || 
             (this.firstNameReceiver!=null &&
              this.firstNameReceiver.equals(other.getFirstNameReceiver()))) &&
            ((this.middleNameReceiver==null && other.getMiddleNameReceiver()==null) || 
             (this.middleNameReceiver!=null &&
              this.middleNameReceiver.equals(other.getMiddleNameReceiver()))) &&
            ((this.lastNameReceiver==null && other.getLastNameReceiver()==null) || 
             (this.lastNameReceiver!=null &&
              this.lastNameReceiver.equals(other.getLastNameReceiver()))) &&
            ((this.secondLastNameReceiver==null && other.getSecondLastNameReceiver()==null) || 
             (this.secondLastNameReceiver!=null &&
              this.secondLastNameReceiver.equals(other.getSecondLastNameReceiver()))) &&
            ((this.typeIdReceiver==null && other.getTypeIdReceiver()==null) || 
             (this.typeIdReceiver!=null &&
              this.typeIdReceiver.equals(other.getTypeIdReceiver()))) &&
            ((this.numberIdReceiver==null && other.getNumberIdReceiver()==null) || 
             (this.numberIdReceiver!=null &&
              this.numberIdReceiver.equals(other.getNumberIdReceiver()))) &&
            ((this.idCityReceiver==null && other.getIdCityReceiver()==null) || 
             (this.idCityReceiver!=null &&
              this.idCityReceiver.equals(other.getIdCityReceiver()))) &&
            ((this.nameCityReceiver==null && other.getNameCityReceiver()==null) || 
             (this.nameCityReceiver!=null &&
              this.nameCityReceiver.equals(other.getNameCityReceiver()))) &&
            ((this.idStateReceiver==null && other.getIdStateReceiver()==null) || 
             (this.idStateReceiver!=null &&
              this.idStateReceiver.equals(other.getIdStateReceiver()))) &&
            ((this.nameStateReceiver==null && other.getNameStateReceiver()==null) || 
             (this.nameStateReceiver!=null &&
              this.nameStateReceiver.equals(other.getNameStateReceiver()))) &&
            ((this.idCountryReceiver==null && other.getIdCountryReceiver()==null) || 
             (this.idCountryReceiver!=null &&
              this.idCountryReceiver.equals(other.getIdCountryReceiver()))) &&
            ((this.nameCountryReceiver==null && other.getNameCountryReceiver()==null) || 
             (this.nameCountryReceiver!=null &&
              this.nameCountryReceiver.equals(other.getNameCountryReceiver()))) &&
            ((this.addressReceiver==null && other.getAddressReceiver()==null) || 
             (this.addressReceiver!=null &&
              this.addressReceiver.equals(other.getAddressReceiver()))) &&
            ((this.phone1Receiver==null && other.getPhone1Receiver()==null) || 
             (this.phone1Receiver!=null &&
              this.phone1Receiver.equals(other.getPhone1Receiver()))) &&
            ((this.phone2Receiver==null && other.getPhone2Receiver()==null) || 
             (this.phone2Receiver!=null &&
              this.phone2Receiver.equals(other.getPhone2Receiver()))) &&
            ((this.ZIPReceiver==null && other.getZIPReceiver()==null) || 
             (this.ZIPReceiver!=null &&
              this.ZIPReceiver.equals(other.getZIPReceiver()))) &&
            ((this.emailReceiver==null && other.getEmailReceiver()==null) || 
             (this.emailReceiver!=null &&
              this.emailReceiver.equals(other.getEmailReceiver()))) &&
            ((this.telexReceiver==null && other.getTelexReceiver()==null) || 
             (this.telexReceiver!=null &&
              this.telexReceiver.equals(other.getTelexReceiver()))) &&
            ((this.payoutIdBranch==null && other.getPayoutIdBranch()==null) || 
             (this.payoutIdBranch!=null &&
              this.payoutIdBranch.equals(other.getPayoutIdBranch()))) &&
            ((this.payoutName==null && other.getPayoutName()==null) || 
             (this.payoutName!=null &&
              this.payoutName.equals(other.getPayoutName()))) &&
            ((this.payoutLocation==null && other.getPayoutLocation()==null) || 
             (this.payoutLocation!=null &&
              this.payoutLocation.equals(other.getPayoutLocation()))) &&
            ((this.idPayment==null && other.getIdPayment()==null) || 
             (this.idPayment!=null &&
              this.idPayment.equals(other.getIdPayment()))) &&
            ((this.idSenderPayment==null && other.getIdSenderPayment()==null) || 
             (this.idSenderPayment!=null &&
              this.idSenderPayment.equals(other.getIdSenderPayment()))) &&
            ((this.namePayment==null && other.getNamePayment()==null) || 
             (this.namePayment!=null &&
              this.namePayment.equals(other.getNamePayment()))) &&
            ((this.idCashier==null && other.getIdCashier()==null) || 
             (this.idCashier!=null &&
              this.idCashier.equals(other.getIdCashier()))) &&
            ((this.passwordReceiver==null && other.getPasswordReceiver()==null) || 
             (this.passwordReceiver!=null &&
              this.passwordReceiver.equals(other.getPasswordReceiver()))) &&
            ((this.transactionDate==null && other.getTransactionDate()==null) || 
             (this.transactionDate!=null &&
              this.transactionDate.equals(other.getTransactionDate()))) &&
            ((this.receiverDateAvailable==null && other.getReceiverDateAvailable()==null) || 
             (this.receiverDateAvailable!=null &&
              this.receiverDateAvailable.equals(other.getReceiverDateAvailable()))) &&
            ((this.modePayReceiver==null && other.getModePayReceiver()==null) || 
             (this.modePayReceiver!=null &&
              this.modePayReceiver.equals(other.getModePayReceiver()))) &&
            ((this.modPayCurrency==null && other.getModPayCurrency()==null) || 
             (this.modPayCurrency!=null &&
              this.modPayCurrency.equals(other.getModPayCurrency()))) &&
            ((this.rateChangeReceiver==null && other.getRateChangeReceiver()==null) || 
             (this.rateChangeReceiver!=null &&
              this.rateChangeReceiver.equals(other.getRateChangeReceiver()))) &&
            ((this.netAmountReceiver==null && other.getNetAmountReceiver()==null) || 
             (this.netAmountReceiver!=null &&
              this.netAmountReceiver.equals(other.getNetAmountReceiver()))) &&
            ((this.accountType==null && other.getAccountType()==null) || 
             (this.accountType!=null &&
              this.accountType.equals(other.getAccountType()))) &&
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.bankReceiver==null && other.getBankReceiver()==null) || 
             (this.bankReceiver!=null &&
              this.bankReceiver.equals(other.getBankReceiver()))) &&
            ((this.idFlagReceiver==null && other.getIdFlagReceiver()==null) || 
             (this.idFlagReceiver!=null &&
              this.idFlagReceiver.equals(other.getIdFlagReceiver()))) &&
            ((this.totalReceiver==null && other.getTotalReceiver()==null) || 
             (this.totalReceiver!=null &&
              this.totalReceiver.equals(other.getTotalReceiver()))) &&
            ((this.totalPayReceiver==null && other.getTotalPayReceiver()==null) || 
             (this.totalPayReceiver!=null &&
              this.totalPayReceiver.equals(other.getTotalPayReceiver()))) &&
            ((this.refReceiver==null && other.getRefReceiver()==null) || 
             (this.refReceiver!=null &&
              this.refReceiver.equals(other.getRefReceiver()))) &&
            ((this.idCurrency==null && other.getIdCurrency()==null) || 
             (this.idCurrency!=null &&
              this.idCurrency.equals(other.getIdCurrency()))) &&
            ((this.isoCurrency==null && other.getIsoCurrency()==null) || 
             (this.isoCurrency!=null &&
              this.isoCurrency.equals(other.getIsoCurrency()))) &&
            ((this.sourceCurrencyAmount==null && other.getSourceCurrencyAmount()==null) || 
             (this.sourceCurrencyAmount!=null &&
              this.sourceCurrencyAmount.equals(other.getSourceCurrencyAmount()))) &&
            ((this.sourceFeeAmount==null && other.getSourceFeeAmount()==null) || 
             (this.sourceFeeAmount!=null &&
              this.sourceFeeAmount.equals(other.getSourceFeeAmount()))) &&
            ((this.idCurrencySource==null && other.getIdCurrencySource()==null) || 
             (this.idCurrencySource!=null &&
              this.idCurrencySource.equals(other.getIdCurrencySource()))) &&
            ((this.isoCurrencySource==null && other.getIsoCurrencySource()==null) || 
             (this.isoCurrencySource!=null &&
              this.isoCurrencySource.equals(other.getIsoCurrencySource()))) &&
            ((this.notesReceiver==null && other.getNotesReceiver()==null) || 
             (this.notesReceiver!=null &&
              this.notesReceiver.equals(other.getNotesReceiver()))) &&
            ((this.requireAdditionalInformation==null && other.getRequireAdditionalInformation()==null) || 
             (this.requireAdditionalInformation!=null &&
              this.requireAdditionalInformation.equals(other.getRequireAdditionalInformation()))) &&
            ((this.holdMotives==null && other.getHoldMotives()==null) || 
             (this.holdMotives!=null &&
              java.util.Arrays.equals(this.holdMotives, other.getHoldMotives())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdBranch() != null) {
            _hashCode += getIdBranch().hashCode();
        }
        if (getIdSender() != null) {
            _hashCode += getIdSender().hashCode();
        }
        if (getFirstNameSender() != null) {
            _hashCode += getFirstNameSender().hashCode();
        }
        if (getMiddleNameSender() != null) {
            _hashCode += getMiddleNameSender().hashCode();
        }
        if (getLastNameSender() != null) {
            _hashCode += getLastNameSender().hashCode();
        }
        if (getSecondLastNameSender() != null) {
            _hashCode += getSecondLastNameSender().hashCode();
        }
        if (getTypeIdSender() != null) {
            _hashCode += getTypeIdSender().hashCode();
        }
        if (getNumberIdSender() != null) {
            _hashCode += getNumberIdSender().hashCode();
        }
        if (getIdCitySender() != null) {
            _hashCode += getIdCitySender().hashCode();
        }
        if (getNameCitySender() != null) {
            _hashCode += getNameCitySender().hashCode();
        }
        if (getIdStateSender() != null) {
            _hashCode += getIdStateSender().hashCode();
        }
        if (getNameStateSender() != null) {
            _hashCode += getNameStateSender().hashCode();
        }
        if (getIdCountrySender() != null) {
            _hashCode += getIdCountrySender().hashCode();
        }
        if (getNameCountrySender() != null) {
            _hashCode += getNameCountrySender().hashCode();
        }
        if (getAddressSender() != null) {
            _hashCode += getAddressSender().hashCode();
        }
        if (getPhone1Sender() != null) {
            _hashCode += getPhone1Sender().hashCode();
        }
        if (getPhobe2Sender() != null) {
            _hashCode += getPhobe2Sender().hashCode();
        }
        if (getZIPSender() != null) {
            _hashCode += getZIPSender().hashCode();
        }
        if (getIdReceiver() != null) {
            _hashCode += getIdReceiver().hashCode();
        }
        if (getIdRecipient() != null) {
            _hashCode += getIdRecipient().hashCode();
        }
        if (getFirstNameReceiver() != null) {
            _hashCode += getFirstNameReceiver().hashCode();
        }
        if (getMiddleNameReceiver() != null) {
            _hashCode += getMiddleNameReceiver().hashCode();
        }
        if (getLastNameReceiver() != null) {
            _hashCode += getLastNameReceiver().hashCode();
        }
        if (getSecondLastNameReceiver() != null) {
            _hashCode += getSecondLastNameReceiver().hashCode();
        }
        if (getTypeIdReceiver() != null) {
            _hashCode += getTypeIdReceiver().hashCode();
        }
        if (getNumberIdReceiver() != null) {
            _hashCode += getNumberIdReceiver().hashCode();
        }
        if (getIdCityReceiver() != null) {
            _hashCode += getIdCityReceiver().hashCode();
        }
        if (getNameCityReceiver() != null) {
            _hashCode += getNameCityReceiver().hashCode();
        }
        if (getIdStateReceiver() != null) {
            _hashCode += getIdStateReceiver().hashCode();
        }
        if (getNameStateReceiver() != null) {
            _hashCode += getNameStateReceiver().hashCode();
        }
        if (getIdCountryReceiver() != null) {
            _hashCode += getIdCountryReceiver().hashCode();
        }
        if (getNameCountryReceiver() != null) {
            _hashCode += getNameCountryReceiver().hashCode();
        }
        if (getAddressReceiver() != null) {
            _hashCode += getAddressReceiver().hashCode();
        }
        if (getPhone1Receiver() != null) {
            _hashCode += getPhone1Receiver().hashCode();
        }
        if (getPhone2Receiver() != null) {
            _hashCode += getPhone2Receiver().hashCode();
        }
        if (getZIPReceiver() != null) {
            _hashCode += getZIPReceiver().hashCode();
        }
        if (getEmailReceiver() != null) {
            _hashCode += getEmailReceiver().hashCode();
        }
        if (getTelexReceiver() != null) {
            _hashCode += getTelexReceiver().hashCode();
        }
        if (getPayoutIdBranch() != null) {
            _hashCode += getPayoutIdBranch().hashCode();
        }
        if (getPayoutName() != null) {
            _hashCode += getPayoutName().hashCode();
        }
        if (getPayoutLocation() != null) {
            _hashCode += getPayoutLocation().hashCode();
        }
        if (getIdPayment() != null) {
            _hashCode += getIdPayment().hashCode();
        }
        if (getIdSenderPayment() != null) {
            _hashCode += getIdSenderPayment().hashCode();
        }
        if (getNamePayment() != null) {
            _hashCode += getNamePayment().hashCode();
        }
        if (getIdCashier() != null) {
            _hashCode += getIdCashier().hashCode();
        }
        if (getPasswordReceiver() != null) {
            _hashCode += getPasswordReceiver().hashCode();
        }
        if (getTransactionDate() != null) {
            _hashCode += getTransactionDate().hashCode();
        }
        if (getReceiverDateAvailable() != null) {
            _hashCode += getReceiverDateAvailable().hashCode();
        }
        if (getModePayReceiver() != null) {
            _hashCode += getModePayReceiver().hashCode();
        }
        if (getModPayCurrency() != null) {
            _hashCode += getModPayCurrency().hashCode();
        }
        if (getRateChangeReceiver() != null) {
            _hashCode += getRateChangeReceiver().hashCode();
        }
        if (getNetAmountReceiver() != null) {
            _hashCode += getNetAmountReceiver().hashCode();
        }
        if (getAccountType() != null) {
            _hashCode += getAccountType().hashCode();
        }
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getBankReceiver() != null) {
            _hashCode += getBankReceiver().hashCode();
        }
        if (getIdFlagReceiver() != null) {
            _hashCode += getIdFlagReceiver().hashCode();
        }
        if (getTotalReceiver() != null) {
            _hashCode += getTotalReceiver().hashCode();
        }
        if (getTotalPayReceiver() != null) {
            _hashCode += getTotalPayReceiver().hashCode();
        }
        if (getRefReceiver() != null) {
            _hashCode += getRefReceiver().hashCode();
        }
        if (getIdCurrency() != null) {
            _hashCode += getIdCurrency().hashCode();
        }
        if (getIsoCurrency() != null) {
            _hashCode += getIsoCurrency().hashCode();
        }
        if (getSourceCurrencyAmount() != null) {
            _hashCode += getSourceCurrencyAmount().hashCode();
        }
        if (getSourceFeeAmount() != null) {
            _hashCode += getSourceFeeAmount().hashCode();
        }
        if (getIdCurrencySource() != null) {
            _hashCode += getIdCurrencySource().hashCode();
        }
        if (getIsoCurrencySource() != null) {
            _hashCode += getIsoCurrencySource().hashCode();
        }
        if (getNotesReceiver() != null) {
            _hashCode += getNotesReceiver().hashCode();
        }
        if (getRequireAdditionalInformation() != null) {
            _hashCode += getRequireAdditionalInformation().hashCode();
        }
        if (getHoldMotives() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getHoldMotives());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getHoldMotives(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransactionDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "TransactionDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idBranch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdBranch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstNameSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "FirstNameSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("middleNameSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "MiddleNameSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastNameSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "LastNameSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("secondLastNameSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "SecondLastNameSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeIdSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "TypeIdSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numberIdSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NumberIdSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCitySender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdCitySender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameCitySender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NameCitySender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idStateSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdStateSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameStateSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NameStateSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCountrySender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdCountrySender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameCountrySender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NameCountrySender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "AddressSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phone1Sender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Phone1Sender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phobe2Sender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Phobe2Sender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ZIPSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ZIPSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idRecipient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdRecipient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstNameReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "FirstNameReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("middleNameReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "MiddleNameReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastNameReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "LastNameReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("secondLastNameReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "SecondLastNameReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeIdReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "TypeIdReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numberIdReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NumberIdReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCityReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdCityReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameCityReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NameCityReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idStateReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdStateReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameStateReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NameStateReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCountryReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdCountryReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameCountryReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NameCountryReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "AddressReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phone1Receiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Phone1Receiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phone2Receiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Phone2Receiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ZIPReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ZIPReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emailReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "EmailReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telexReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "TelexReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payoutIdBranch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PayoutIdBranch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payoutName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PayoutName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payoutLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PayoutLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idPayment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdPayment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idSenderPayment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdSenderPayment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("namePayment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NamePayment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCashier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdCashier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passwordReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PasswordReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "TransactionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("receiverDateAvailable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ReceiverDateAvailable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modePayReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ModePayReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modPayCurrency");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ModPayCurrency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rateChangeReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "RateChangeReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("netAmountReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NetAmountReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "AccountType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "BankReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idFlagReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdFlagReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "TotalReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalPayReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "TotalPayReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "RefReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCurrency");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdCurrency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isoCurrency");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IsoCurrency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceCurrencyAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "SourceCurrencyAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceFeeAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "SourceFeeAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCurrencySource");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdCurrencySource"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isoCurrencySource");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IsoCurrencySource"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notesReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NotesReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requireAdditionalInformation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "RequireAdditionalInformation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("holdMotives");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "HoldMotives"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "HoldMotive_Result"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "HoldMotive_Result"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
