package com.addcel.telecom.usa.viamericas.servicios.services;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.apache.axis.AxisFault;
import org.apache.commons.lang.StringUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.telecom.usa.viamericas.servicios.model.mapper.DaoMapper;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.AgencyLocations;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.Bitacora;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.ConfirmTransaction;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.CostPaymentLocationNetworkResponse;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.Countrys;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.CountrysResp;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.CurrencyByCountry;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.ExchangeRate;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.ExchangeRateResponse;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.Order;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.OrderFees;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.PaymentLocationNetwork;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.PaymentLocations;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.PaymentMethods;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.PaymentModes;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.Recipient;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.Recipients;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.Sender;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.TransferDetails;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.Usuario;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.ZipCodesCity;
import com.addcel.telecom.usa.viamericas.servicios.utils.AddCelGenericMail;
import com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.SetNewOrder_Result;
import com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.Transaction;
import com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardPaymentResponse;
import com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.Receipt;
import com.addcel.telecom.usa.viamericas.servicios.ws.client.tempuri.IMTOnlineServiceProxy;
import com.addcel.utils.AddcelCrypto;
import com.google.gson.Gson;

@Service
public class TransfersViamericasService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TransfersViamericasService.class);
	
	@Autowired
	private DaoMapper mapper;
		
	private static IMTOnlineServiceProxy proxy = null;
	
	private static Gson gson = new Gson();

	static {
		proxy = new IMTOnlineServiceProxy();
	}
	
	public String createRecipient(String json) {
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Recipient objRecipient = new 
				com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Recipient();
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Recipient recipient = new 
				com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Recipient();
		Recipient recipientIn = null;
		try {	
			LOGGER.info("STARTING CREATE RECIPIENT REQUEST - JSON - "+json);
			recipientIn = gson.fromJson(json, Recipient.class);
			objRecipient.setAddress(recipientIn.getAddress());
			objRecipient.setFName(recipientIn.getFirstName());
			objRecipient.setEmail(recipientIn.getEmail());
			objRecipient.setIdCity(recipientIn.getIdCity());
			objRecipient.setIdCountry(recipientIn.getIdCountry());
			objRecipient.setLName(recipientIn.getLastName());
			objRecipient.setIdSender(recipientIn.getIdSender());
			objRecipient.setIdState(recipientIn.getIdState());
			
			objRecipient.setMName(recipientIn.getMName());
			objRecipient.setLName(recipientIn.getLName());
			objRecipient.setSLName(recipientIn.getSLName());
			objRecipient.setAddress2(recipientIn.getAddress2());
			
			objRecipient.setPhone1(recipientIn.getPhone1());
			objRecipient.setPhone2(recipientIn.getPhone2());
			objRecipient.setNameCity(recipientIn.getNameCity());
			objRecipient.setZIP(recipientIn.getZIP());
			objRecipient.setIdTypeId(recipientIn.getIdTypeId());
			objRecipient.setNumberId(recipientIn.getNumberId());
			objRecipient.setBirthDate(recipientIn.getBirthDate());
			objRecipient.setPhone1Type(recipientIn.getPhone1Type());
			objRecipient.setPhone2Type(recipientIn.getPhone2Type());
			objRecipient.setSendSMS(recipientIn.getSendSMS());
			
			recipient = proxy.createRecipient(getCredentialsCompliance(), objRecipient);
			
			mapper.createRecipient(recipient);
			
			LOGGER.debug("ID RECIPIENT: "+recipient.getIdRecipient()+", NUMBER ID: "+recipient.getNumberId());
		} catch(AxisFault a){
			String desc  = a.getFaultString();
			LOGGER.error("ERROR CREATE RECIPIENT REQUEST - ERROR: "+desc);
			recipient.setIdError(-1);
			recipient.setMensajeError(desc);
		} catch (Exception e) {
			LOGGER.error("ERROR CREATE RECIPIENT REQUEST - ERROR: "+e.getLocalizedMessage());
			e.printStackTrace();
			recipient.setIdError(-1);
			recipient.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(recipient);
			LOGGER.info("RESPONSE CREATE RECIPIENT - JSON - "+json);
		}
		return json;
	}

	public String createSender(String json) {
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Sender sender = new 
				com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Sender();
		Sender senderIn = null;
		try {
			LOGGER.info("STARTING CREATE SENDER REQUEST - JSON - "+json);
			senderIn = gson.fromJson(json, Sender.class);			
			sender.setFName(senderIn.getFirstName());
			sender.setLName(senderIn.getLastName());
			sender.setAddress(senderIn.getAddress());
			sender.setPhone1(senderIn.getPhone() != null && !senderIn.getPhone().equals("") ? senderIn.getPhone() : senderIn.getPhone1());
			sender.setIdCountry(senderIn.getIdCountry());
			sender.setIdState(senderIn.getIdState());
			sender.setIdCity(senderIn.getIdCity());
			sender.setZIP(getCitiesToSender(senderIn.getIdCity()));
			sender.setEmail(senderIn.getEmail());
			
			sender.setMName(senderIn.getMName());
			sender.setIdCountryIssuance(senderIn.getIdCountryIssuance());
			sender.setIdTypeId(senderIn.getIdTypeId());
			sender.setNumberId(senderIn.getNumberId());
			sender.setBirthDate(senderIn.getBirthDate());
			sender.setAddress2(senderIn.getAddress2());
			sender.setSendSMS(senderIn.getSendSMS());
			sender.setCountryOfBirth(senderIn.getCountryOfBirth());
			sender.setPhone1Type(senderIn.getPhone1Type());
			sender.setPhone2Type(senderIn.getPhone2Type());
			sender.setPromotionalMessages(senderIn.getPromotionalMessages());
			sender.setPreferredCountry(senderIn.getPreferredCountry());
			
			sender = proxy.createSender(getCredentialsCompliance(), sender);
			mapper.updateSenderByUsuario(sender.getIdSender(), senderIn.getIdUsuario());
			LOGGER.debug("SENDER RESPONSE: "+sender.toString());
		} catch(AxisFault a){
			String desc  = a.getFaultString();
			LOGGER.error("ERROR CREATE SENDER REQUEST - ERROR: "+desc);
			sender.setIdError(-1);
			sender.setMensajeError(desc);
		} catch (Exception e) {
			LOGGER.error("ERROR CREATE SENDER REQUEST - ERROR: "+e.getLocalizedMessage());
			e.printStackTrace();
			sender.setIdError(-1);
			sender.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(sender);
			LOGGER.info("RESPONSE CREATE SENDER - JSON - "+json);
		}
		return json;
	}

	public String createNewOrder(String json) {
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.Transaction transaction = new 
				com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.Transaction();
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.InfoMachine infomachine = new 
				com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.InfoMachine();
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.InfoGeoLocalization 
			infogeolocalization = new com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.InfoGeoLocalization();
		SetNewOrder_Result result = new SetNewOrder_Result();
		Order order = null;
		long idTransaccion = 0;
		try {
			LOGGER.info("STARTING NEW ORDER REQUEST - JSON - "+json);
			order = gson.fromJson(json, Order.class);									
			transaction.setAddressReceiver(order.getAddressReceiver());
			transaction.setRecFname(order.getRecipientFirstName());
			transaction.setRecLname(order.getRecipientLastName());
			transaction.setIdBranchPayReceiver(order.getIdBranchReciever());
			transaction.setIdStateReceiver(order.getIdStateReceiver());
			transaction.setIdCityReceiver(order.getIdCityRecivier());
			transaction.setIdCountryReceiver(order.getIdCountryReciever());
			transaction.setIdRecipient(order.getIdRecipient());
			transaction.setIdSender(order.getIdSender());
			transaction.setModePayReceiver(order.getModePayReciever());
			transaction.setAccountReceiver(order.getAccountReceiver());
			transaction.setModPayCurrencyReceiver(order.getModPayCurrencyReceiver());
			transaction.setTransactionDate("2017-04-03");
			transaction.setAmountToSend(order.getAmount());

			
			infogeolocalization.setLatituded(order.getLatitude());
			infogeolocalization.setLongituded(order.getLongitude());
			infogeolocalization.setPlaceName(order.getPlaceName());
			
			infomachine.setBrowserType("Mobile App");
			infomachine.setIpAddress("199.231.161.38");
			infomachine.setPartnerVersion(order.getAppVersion());
			infomachine.setPhoneType(order.getPhoneType());
			infomachine.setSoftwareVersion(order.getSoftware());
			
			result = proxy.setNewOrder(getCredentialsTransfers(), transaction, infomachine, infogeolocalization);
			
			result.setIdBitacora(insertaTransaccion(order, transaction, result).getIdBitacora());
			
			mapper.createOrder(transaction, result);
		} catch(AxisFault a){
			String desc  = a.getFaultString();
			LOGGER.error("ERROR NEW ORDER REQUEST - ERROR - "+desc);
			result.setIdError(-1);
			result.setMensajeError(desc);
		} catch (Exception e) {
			e.printStackTrace();
			result.setIdError(-1);
			result.setMensajeError(e.getLocalizedMessage());
			LOGGER.error("ERROR NEW ORDER REQUEST - ERROR - "+e.getLocalizedMessage());
		} finally {
			json = gson.toJson(result);
			LOGGER.info("RESULT NEW ORDER REQUEST - JSON - "+json);
		}
		return json;
	}

	private Bitacora insertaTransaccion(Order order, Transaction transaction, SetNewOrder_Result result) {
		Bitacora bitacora = new Bitacora();
		try {
			/** 
			 * id_usuario, id_proveedor, id_producto, bit_fecha, bit_hora, bit_concepto,
			bit_cargo, bit_ticket, bit_no_autorizacion, bit_codigo_error, bit_card_id, bit_status,
			imei, destino, tarjeta_compra, tipo, software, modelo, wkey, pase	
			 */
			bitacora.setIdUsuario(order.getIdUsuario());
			bitacora.setConcepto("Viamericas Money Transfer ");
			bitacora.setCargo(Double.valueOf(transaction.getAmountToSend()));
			bitacora.setCodigoError("-1");
			bitacora.setStatus(-1);
			bitacora.setImei(transaction.getImei());
			bitacora.setSoftware(transaction.getSoftware());
			bitacora.setModelo(transaction.getModelo());
			bitacora.setWkey(transaction.getWkey());
			bitacora.setPase(0);
			mapper.insertaTransaccion(bitacora);
			
			mapper.insertaBitacoraProsa(bitacora);
			
			LOGGER.info("TRANSACCION REGISTRADA: ID BITACORA - "+bitacora.getIdBitacora());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bitacora;
	}

	@Deprecated
	public String confirmTransaction(String json) {
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.Authorization authorization = new
				com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.Authorization();
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.Confirmation objConfirmation = new 
				com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.Confirmation();
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.Receipt receipt = null;
		try {
			authorization.setIdBranch("A00765");
			authorization.setIdUser("A00765");
			authorization.setPassword("BeOurPartner");
			
			objConfirmation.setDepositValue(new BigDecimal(1));
			objConfirmation.setIdCashier("10513");
			objConfirmation.setIdReceiver("17578");
			
			receipt = proxy.confirmTransaction(authorization, objConfirmation);
			LOGGER.info("RECEIPT: "+receipt.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		json = gson.toJson(receipt);
		return json;
	}
	
	/**
	 * 
	 * 
	 * @param json {"idReceiver":"", "idSender":""}
	 * @return
	 */
	public String confirmTransactionCash(String json) {
		com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.CashPayment cash = new 
				com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.CashPayment();
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.PaymentResponse response = 
				new com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.PaymentResponse();
		ConfirmTransaction transaction = null;
		try {
			LOGGER.info("STARTING CONFIRM TRANSACTION CASH REQUEST - JSON - "+json);
			transaction = gson.fromJson(json, ConfirmTransaction.class);	
			cash.setIdReceiver(transaction.getIdReciever());
			cash.setIdSender(transaction.getIdSender());
			
//			card.setIdSender(transaction.getIdSender());
//			card.setIdReceiver(transaction.getIdReciever());
//			card.setPaymentType(transaction.getPaymentType());
//			card.setCardNumber(transaction.getCardNumber());
//			card.setExpDate(transaction.getExpDate());
//			card.setCVV(transaction.getCvv());
//			card.setCardHolderFirstName(transaction.getCardFirstName());
//			card.setCardHolderLastName(transaction.getCardLastName());
//			card.setNickName(transaction.getNickName());
			response = proxy.confirmCashPaymentTransaction(getCredentialsPayment(), cash);
			
		} catch(AxisFault a){
			LOGGER.error("ERROR CONFIRM TRANSACTION CASH - ERROR: "+a.getFaultString());
			a.printStackTrace();
			response.setIdError(-1);
			response.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR CONFIRM TRANSACTION CASH - ERROR: "+e.getLocalizedMessage());
			e.printStackTrace();
			response.setIdError(-1);
			response.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(response);
		}
		LOGGER.info("RESPONSE CONFIRM TRANSACTION CASH REQUEST - JSON - "+json);
		return json;
	}
	
	public String confirmTransactionCredit(String json) {
		com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.CardPayment card = new 
				com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.CardPayment();
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardPaymentResponse response = 
				new com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardPaymentResponse();;
		ConfirmTransaction transaction = null;
		String correoRecipient = null;
		String idBitacora = null;
		try {
			LOGGER.info("STARTING CONFIRM TRANSACTION REQUEST - JSON - "+json);
			transaction = gson.fromJson(json, ConfirmTransaction.class);
			LOGGER.info("1");
			card.setIdSender(transaction.getIdSender());
			LOGGER.info("2");
			card.setIdReceiver(transaction.getIdReciever());
			LOGGER.info("3");
			card.setPaymentType(transaction.getPaymentType());
			LOGGER.info("4");
			card.setCardNumber(transaction.getCardNumber());
			LOGGER.info("5");
			card.setExpDate(transaction.getExpDate());
			LOGGER.info("6");
			card.setCVV(transaction.getCvv());
			LOGGER.info("7");
			card.setCardHolderFirstName(transaction.getCardFirstName());
			LOGGER.info("8");
			card.setCardHolderLastName(transaction.getCardLastName());
			LOGGER.info("9");
			card.setNickName(transaction.getNickName());
			LOGGER.info("10");
//			response = proxy.confirmCardPaymentTransaction(getCredentialsPayment(), card);
			response = setResponse();
			
			correoRecipient = mapper.getMailRecipientByIdBitacora(transaction.getIdBitacora(), transaction.getIdSender());
			
			actualizaBitacora(transaction, response);
			
			
			if("000".equals(response.getResponseCode())){
				LOGGER.info("ENVIANDO CORREO AL RECIPIENT - "+correoRecipient);
				if(correoRecipient != null){
					enviaCorreo(response, correoRecipient);
//					idBitacora = AddcelCrypto.decryptHard(String.valueOf(transaction.getIdBitacora()));
					response.setUrl("http://199.231.161.38:8081/MCTransfersViamericas/get/transferByid?id="+transaction.getIdBitacora());
//					response.setUrl("http://mobilecard.mx:8081/MCTransfersViamericas/get/transferByid?id="+transaction.getIdBitacora());
				}
			}
			
			LOGGER.info("TRANSACCION ACTUALIZADA - ID BITACORA - "+transaction.getIdBitacora());
		} /*catch(AxisFault a){
			LOGGER.error("ERROR CONFIRM TRANSACTION - ERROR: "+a.getFaultString());
			a.printStackTrace();
			response.setIdError(-1);
			response.setMensajeError(a.getFaultString());
		}*/ catch (Exception e) {
			LOGGER.error("ERROR CONFIRM TRANSACTION - ERROR: "+e.getLocalizedMessage());
			e.printStackTrace();
			response.setIdError(-1);
			response.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(response);
		}
		LOGGER.info("RESPONSE CONFIRM TRANSACTION REQUEST - JSON - "+json);
		return json;
	}
	
	private void enviaCorreo(com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardPaymentResponse response, String correo) {
		String bodyMail = null;
		String host = null;
	    int port = 0;
	    String username = null;
	    String password = null;
	    String from = null; 
	    Properties props = null;
		try {
			bodyMail = mapper.getParametro("@MENSAJE_TELECOMTRANSFERVIAMERICAS");
			host =  mapper.getParametro("@SMTP"); 
            port = Integer.parseInt( mapper.getParametro("@SMTP_PORT") ); 
            username =  mapper.getParametro("@SMTP_USER"); 
            password =  mapper.getParametro("@SMTP_PWD"); 
            from = mapper.getParametro("@SMTP_MAIL_SEND");
            props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
			AddCelGenericMail.generatedMail(response, bodyMail, correo);
//			AddCelGenericMail.sendMail(correo, AddCelGenericMail.generatedBodyMail(response, bodyMail, correo), props, 
//					from, host, port, username, password);
		} catch (Exception e) {
			LOGGER.error("Error al mandar el correo: "+e.getCause());
		}	
	}
	
	private Bitacora actualizaBitacora(ConfirmTransaction transaction, CardPaymentResponse response) {
		Bitacora bitacora = new Bitacora();
		try {
			LOGGER.info("actualizaBitacora");
			bitacora.setIdBitacora(transaction.getIdBitacora());
			bitacora.setNoAutorizacion(response.getApprovalCode());
						
			if("000".equals(response.getResponseCode())){
				LOGGER.info("entre al if");
				bitacora.setStatus(1);				
				bitacora.setCodigoError("0");
				bitacora.setTarjetaCompra(AddcelCrypto.encryptTarjeta(transaction.getCardNumber()));
				LOGGER.info("entre al if 2");
//				Calendar cal = response.getObjReceipt().getDateReceiver();
				Calendar cal =  Calendar.getInstance();
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				String formatted = format1.format(cal.getTime());
				LOGGER.info("entre al if 3");
				response.setDateReciever(formatted);
			} else {
				bitacora.setStatus(-1);
				bitacora.setCodigoError("-1");
			}
			LOGGER.info("actualizaBitacora 2");
			mapper.actualizaTransaccion(bitacora);
			LOGGER.info("actualizaBitacora 3");	
			mapper.insertaDatosTransfer(response, bitacora);
			LOGGER.info("actualizaBitacora 4");
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.info("ERROR EN ACTUALIZA BITACORA:"+e);
		}
		return null;
	}

	public String confirmTransactionACH(String json) {
		com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.Authorization authorization = new
				com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.Authorization();
		com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.CardPayment card = new 
				com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.CardPayment();
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardPaymentResponse response = null;
		ConfirmTransaction transaction = null;
		try {
			LOGGER.info("STARTING CONFIRM TRANSACTION ACH REQUEST - JSON - "+json);
			transaction = gson.fromJson(json, ConfirmTransaction.class);
			authorization.setIdBranch("A00037");
			authorization.setIdUser("MobileCardWs");
			authorization.setPassword("Viamericas123");
			
			card.setIdSender(transaction.getIdSender());
			card.setIdReceiver(transaction.getIdReciever());
			card.setPaymentType(transaction.getPaymentType());
			card.setCardNumber(transaction.getCardNumber());
			card.setExpDate(transaction.getExpDate());
			card.setCVV(transaction.getCvv());
			card.setCardHolderFirstName(transaction.getCardFirstName());
			card.setCardHolderLastName(transaction.getCardLastName());
			card.setNickName(transaction.getNickName());
			
			response = proxy.confirmCardPaymentTransaction(authorization, card);
			json = gson.toJson(response);
		} catch(AxisFault a){
			LOGGER.error("ERROR CONFIRM TRANSACTION - ERROR: "+a.getFaultString());
			response = new com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardPaymentResponse();
			response.setIdError(-1);
			response.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR CONFIRM TRANSACTION - ERROR: "+e.getLocalizedMessage());
			e.printStackTrace();
			response = new com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardPaymentResponse();
			response.setIdError(-1);
			response.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(response);
		}
		LOGGER.info("RESPONSE CONFIRM TRANSACTION REQUEST - JSON - "+json);
		return json;
	}
	
	public String createSenderProfile(String json) {
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardProfileRequest cardProfile = new
				com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardProfileRequest();
		com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.CreditCardProfile cardProfileResp = new 
				com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.CreditCardProfile();
		ConfirmTransaction transaction = null;
		try {
			LOGGER.info("STARTING CREATE SENDER PROFILE REQUEST - JSON - "+json);
			transaction = gson.fromJson(json, ConfirmTransaction.class);			
			cardProfile.setIdSender(transaction.getIdSender());
			cardProfile.setPaymentType(transaction.getPaymentType());
			cardProfile.setCardNumber(transaction.getCardNumber());
			cardProfile.setExpDate(transaction.getExpDate());
			cardProfile.setCVV(transaction.getCvv());
			cardProfile.setCardHolderFirstName(transaction.getCardFirstName());
			cardProfile.setCardHolderLastName(transaction.getCardLastName());
			cardProfile.setNickName(transaction.getNickName());
			cardProfile.setPaymentType(transaction.getPaymentType());
			
			
			
			LOGGER.info("SEND REQUEST TO VIAMERICAS - JSON - "+gson.toJson(cardProfile));
			cardProfileResp = proxy.createSenderCardPaymentProfile(getCredentialsPayment(), cardProfile);
			LOGGER.info("RESPONSE RECIEVED FROM VIAMERICAS - JSON - "+gson.toJson(cardProfileResp));
		} catch(AxisFault a){
			LOGGER.error("ERROR CREATE SENDER PROFILE REQUEST - ERROR - "+a.getFaultString());
			a.printStackTrace();
			cardProfileResp.setIdError(-1);
			cardProfileResp.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR CREATE SENDER PROFILE REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			cardProfileResp.setIdError(-1);
			cardProfileResp.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(cardProfileResp);
		}
		LOGGER.info("RESULT CREATE SENDER PROFILE REQUEST - JSON - "+json);
		return json;
	}

	public com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.CreditCardProfile createSenderProfile(com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.CardPayment card) {
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardProfileRequest cardProfile = new
				com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardProfileRequest();
		com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.CreditCardProfile cardProfileResp = new
				com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.CreditCardProfile();
		try {						
			cardProfile.setIdSender(card.getIdSender());
			cardProfile.setPaymentType(card.getPaymentType());
			cardProfile.setCardNumber(card.getCardNumber());
			cardProfile.setExpDate(card.getExpDate());
			cardProfile.setCVV(card.getCVV());
			cardProfile.setCardHolderFirstName(card.getCardHolderFirstName());
			cardProfile.setCardHolderLastName(card.getCardHolderLastName());
			cardProfile.setNickName(card.getNickName());
			cardProfile.setPaymentType(card.getPaymentType());
			cardProfileResp = proxy.createSenderCardPaymentProfile(getCredentialsPayment(), cardProfile);
		} catch(AxisFault a){
			LOGGER.error("ERROR CREATE SENDER PROFILE REQUEST - ERROR - "+a.getFaultString());
			cardProfileResp.setIdError(-1);
			cardProfileResp.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR CREATE SENDER PROFILE REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			cardProfileResp.setIdError(-1);
			cardProfileResp.setMensajeError(e.getLocalizedMessage());
		}
		LOGGER.info("RECEIPT: "+gson.toJson(cardProfileResp));
		return cardProfileResp;
	}
	
	public String validateSenderProfile(String json) {
		Usuario usuario = null;
		String isSender = null;
		try {
			LOGGER.info("STARTING VALIDATE SENDER REQUEST - JSON - "+json);
			usuario = gson.fromJson(json, Usuario.class);
			isSender = mapper.validateSender(usuario.getIdUsuario());
			if(isSender != null && StringUtils.isNotEmpty(isSender) && StringUtils.isNotBlank(isSender)){
				json = "{\"idUsuario\": "+usuario.getIdUsuario()+", \"idSender\":"+isSender+",  \"idError\":0 }";
			} else {
				json = "{\"idUsuario\": "+usuario.getIdUsuario()+", \"idError\":-1, "
						+ "\"mensajeError\":\"El usuario no esta registrado como sender.\"}";
			}
			
		} catch (Exception e) {
			LOGGER.error("ERROR CREATE SENDER PROFILE REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
		}
		LOGGER.info("RESULT VALIDATE SENDER REQUEST - JSON - "+json);
		return json;
	}
	
	public String getPaymentLocations(String json) {
		PaymentLocations locations = new PaymentLocations();
		Countrys countrys = null;
		try {
			LOGGER.info("STARTING PAYMENT LOCATIONS REQUEST - JSON - "+json);
			countrys = gson.fromJson(json, Countrys.class);
			locations.setLocations(proxy.getPaymentLocations(getCredentialsTransfers(), countrys.getIdCountry(), countrys.getIdCity(), 
					countrys.getCurrencyMode(), countrys.getPayMode()));
		} catch(AxisFault a){
			LOGGER.error("ERROR PAYMENT LOCATIONS - ERROR - "+a.getFaultString());
			locations.setIdError(-1);
			locations.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR PAYMENT LOCATIONS REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			locations.setIdError(-1);
			locations.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(locations);
		}
		LOGGER.info("RESULT PAYMENT LOCATIONS REQUEST - JSON - "+json);
		return json;
	}
	
	public String getAgencyLocations(String json) {
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.AgencyLocationRequest agencyLocationRequest = new 
				com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.AgencyLocationRequest();
		Countrys countrys = null;
		AgencyLocations locations = new AgencyLocations();
		try {
			LOGGER.info("STARTING AGENCY LOCATIONS REQUEST - JSON - "+json);
			countrys = gson.fromJson(json, Countrys.class);			
			agencyLocationRequest.setCityName(countrys.getCityName());
			agencyLocationRequest.setIdCountry(countrys.getIdCountry());
			agencyLocationRequest.setIdState(countrys.getIdState());
			locations.setAgencys(proxy.getAgenciesByLocation(getCredentialsTransfers(), agencyLocationRequest));
		} catch(AxisFault a){
			LOGGER.error("ERROR AGENCY LOCATIONS - ERROR - "+a.getFaultString());
			locations.setIdError(-1);
			locations.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR AGENCY LOCATIONS REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			locations.setIdError(-1);
			locations.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(locations);
		}
		LOGGER.info("RESULT AGENCY LOCATIONS REQUEST - JSON - "+json);
		return json;
	}
	
	public String getCurrencyByCountry(String json) {
		CurrencyByCountry currency = new CurrencyByCountry();
		Countrys countrys = null;
		try {
			LOGGER.info("STARTING CURRENCY BY COUNTRY REQUEST - JSON - "+json);
			countrys = gson.fromJson(json, Countrys.class);					
			currency.setCurrency(proxy.getPaymentCurrenciesbyCountry(getCredentialsTransfers(), countrys.getIdCountry()));
		} catch(AxisFault a){
			LOGGER.error("ERROR CURRENCY BY COUNTRY - ERROR - "+a.getFaultString());
			currency.setIdError(-1);
			currency.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR CURRENCY BY COUNTRY REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			currency.setIdError(-1);
			currency.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(currency);
		}
		LOGGER.info("RESULT CURRENCY BY COUNTRY REQUEST - JSON - "+json);
		return json;
	}
	
	public String getPaymentModes(String json) {
		PaymentModes payment = new PaymentModes();
		Countrys countrys = null;
		try {
			LOGGER.info("STARTING PAYMENT MODES REQUEST - JSON - "+json);
			countrys = gson.fromJson(json, Countrys.class);					
			payment.setPaymentModes(proxy.getPaymentModesbyCountry(getCredentialsTransfers(), countrys.getIdCountry(), countrys.getIdCurrency()));
		} catch(AxisFault a){
			LOGGER.error("ERROR PAYMENT MODES - ERROR - "+a.getFaultString());
			payment.setIdError(-1);
			payment.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR PAYMENT MODES REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			payment.setIdError(-1);
			payment.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(payment);
		}
		LOGGER.info("RESULT PAYMENT MODES REQUEST - JSON - "+json);
		return json;
	}
	
	public String getAgencyPaymentModes(String json) {
		PaymentMethods payment = new PaymentMethods();
		try {	
			LOGGER.info("STARTING AGENCY PAYMENT MODES REQUEST - JSON - "+json);
			payment.setPaymentMethod(proxy.getAgencyPaymentMethods(getCredentialsPayment()));
		} catch(AxisFault a){
			LOGGER.error("ERROR AGENCY PAYMENT MODES - ERROR - "+a.getFaultString());
			payment.setIdError(-1);
			payment.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR AGENCY PAYMENT MODES REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			payment.setIdError(-1);
			payment.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(payment);
		}
		LOGGER.info("RESULT AGENCY PAYMENT MODES REQUEST - JSON - "+json);
		return json;
	}

	public String getCountrys(String json) {
		CountrysResp resp = new CountrysResp();
		Countrys countrys = null;
		try {	
			LOGGER.info("STARTING GET COUNTRYS - JSON - "+json);
			countrys = gson.fromJson(json, Countrys.class);
			resp.setCountrys(proxy.getCountries(getCredentialsTransfers(), countrys.getOperation()));
		} catch(AxisFault a){
			LOGGER.error("ERROR GET COUNTRYS - ERROR - "+a.getFaultString());
			resp.setIdError(-1);
			resp.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR GET COUNTRYS REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			resp.setIdError(-1);
			resp.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(resp);
		}
		LOGGER.info("RESULT GET COUNTRYS REQUEST - JSON - "+json);
		return json;
	}

	public String getCountrysCitys(String json) {
		CountrysResp resp = new CountrysResp();
		Countrys countrys = null;
		try {
			LOGGER.info("STARTING GET COUNTRYS CITIES - JSON - "+json);
			countrys = gson.fromJson(json, Countrys.class);
			resp.setCountryCities(proxy.getCountryCities(getCredentialsTransfers(), countrys.getIdCountry()));
		} catch(AxisFault a){
			LOGGER.error("ERROR GET COUNTRYS CITIES - ERROR - "+a.getFaultString());
			resp.setIdError(-1);
			resp.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR GET COUNTRYS CITIES REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			resp.setIdError(-1);
			resp.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(resp);
		}
		LOGGER.info("RESULT GET COUNTRYS CITIES REQUEST - JSON - "+json);
		return json;
	}
	
	public String getCountrysStates(String json) {
		CountrysResp resp = new CountrysResp();
		Countrys countrys = null;
		try {
			LOGGER.info("STARTING GET COUNTRYS STATES - JSON - "+json);
			countrys = gson.fromJson(json, Countrys.class);
			resp.setCountryStates(proxy.getCountryStates(getCredentialsTransfers(), countrys.getIdCountry(), countrys.getOperation()));
		} catch(AxisFault a){
			LOGGER.error("ERROR GET COUNTRYS STATES - ERROR - "+a.getFaultString());
			resp.setIdError(-1);
			resp.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR GET COUNTRYS STATES REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			resp.setIdError(-1);
			resp.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(resp);
		}
		LOGGER.info("RESULT GET COUNTRYS STATES REQUEST - JSON - "+json);
		return json;
	}
	
	public String getStatesCitys(String json) {
		CountrysResp resp = new CountrysResp();
		Countrys countrys = null;
		try {
			LOGGER.info("STARTING GET STATES CITIES - JSON - "+json);
			countrys = gson.fromJson(json, Countrys.class);
			resp.setStateCities(proxy.getStateCities(getCredentialsTransfers(), countrys.getIdCountry(), countrys.getIdState(), countrys.getOperation()));
		} catch(AxisFault a){
			LOGGER.error("ERROR GET STATES CITIES - ERROR - "+a.getFaultString());
			resp.setIdError(-1);
			resp.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR GET STATES CITIES REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			resp.setIdError(-1);
			resp.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(resp);
		}
		LOGGER.info("RESULT GET STATES CITIES REQUEST - JSON - "+json);
		return json;
	}
	
	public String getCitysByZipCode(String json) {
		CountrysResp resp = new CountrysResp();
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.CityByZipCodeRequest request = 
				new com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.CityByZipCodeRequest();
		Countrys countrys = null;
		try {
			LOGGER.info("STARTING GET CITIES BY ZIP CODE - JSON - "+json);
			countrys = gson.fromJson(json, Countrys.class);
			request.setZipCode(countrys.getZipCode());
			resp.setCity(proxy.getCitiesByZipCode(getCredentialsTransfers(), request));
		} catch(AxisFault a){
			LOGGER.error("ERROR GET CITIES BY ZIP CODE - ERROR - "+a.getFaultString());
			resp.setIdError(-1);
			resp.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR GET CITIES BY ZIP CODE REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			resp.setIdError(-1);
			resp.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(resp);
		}
		LOGGER.info("RESULT GET CITIES BY ZIP CODE REQUEST - JSON - "+json);
		return json;
	}
	
	public String exchangeRate(String json) {
		ExchangeRate  exchange = null;
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetReceiverRate_Result[] result = null;
		ExchangeRateResponse resp = new ExchangeRateResponse();
		try {
			LOGGER.info("STARTING GET EXCHANGE RATE - JSON - "+json);
			exchange = gson.fromJson(json, ExchangeRate.class);
			result = proxy.getExchangeRate(getCredentialsTransfers(), exchange.getModePayment(), exchange.getModeCurrency(), exchange.getIdPayBranch(), "web");
			resp.setExchangeRateRes(result);
		} catch(AxisFault a){
			LOGGER.error("ERROR GET EXCHANGE RATE - ERROR - "+a.getFaultString());
			resp.setIdError(-1);
			resp.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR GET EXCHANGE RATE REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			resp.setIdError(-1);
			resp.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(resp);
		}
		LOGGER.info("RESULT GET EXCHANGE RATE REQUEST - JSON - "+json);
		return json;
	}
	
	/** {"idCountry":"COL", "idModePayCurrency":"N", "idPaymentMode":"P", "amount":"5", "onlyNetwork":"C", "withCost":"true"}**/
	public String costPaymentLocationNetwork(String json) {
		PaymentLocationNetwork location = null;
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.CostPaymentLocationNetwork[] result = null;
		CostPaymentLocationNetworkResponse response = new CostPaymentLocationNetworkResponse();
		try {
			LOGGER.info("STARTING GET COST PAYMENT LOCATION NETWORK - JSON - "+json);
			location = gson.fromJson(json, PaymentLocationNetwork.class);
			result = proxy.getCostPaymentLocationsNetworks(getCredentialsTransfers(), location.getIdCountry(), location.getIdModePayCurrency(), 
					location.getIdPaymentMode(), new BigDecimal(location.getAmount()), location.getOnlyNetwork(), location.getWithCost());
			response.setLocations(result);
		} catch(AxisFault a){
			LOGGER.error("ERROR GET COST PAYMENT LOCATION NETWORK - ERROR - "+a.getFaultString());
			response.setIdError(-1);
			response.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR GET COST PAYMENT LOCATION NETWORK REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			response.setIdError(-1);
			response.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(response);
		}
		LOGGER.info("RESULT GET COST PAYMENT LOCATION NETWORK REQUEST - JSON - "+json);
		return json;
	}
	/**
	 * JSON ENTRADA: 
	 * { 	"idModePay": "C", 	"modeCurrency": "N", 	"netAmount": "10", 	"idBranchPay": "BC0001", 	"idStateFrom": "", 	"idPayment": "K" }
	 * 
	 * JSON SALIDA: 
	 * 
	 * {"customerFixFee":"3.00","customerPercentageFee":"0.00","fundingFee":"1.00","stateTax":"0.00","totalFee":"4.00","__hashCodeCalc":false}
	 * 
	 * @param json
	 * @return
	 */
	public String getOrderFees(String json) {
		OrderFees order = null;
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.OrderExpenses[] result = null;
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.OrderExpenses response = new 
				com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.OrderExpenses();
		try {
			LOGGER.info("STARTING GET ORDER FEES - JSON - "+json);
			order = gson.fromJson(json, OrderFees.class);
			result = proxy.getOrderFees(getCredentialsTransfers(), order.getIdModePay(), order.getModeCurrency(), 
					order.getNetAmount(), order.getIdBranchPay(), order.getIdStateFrom(), order.getIdPayment());
			response = result[0];
		} catch(AxisFault a){
			LOGGER.error("ERROR GET ORDER FEES - ERROR - "+a.getFaultString());
			response.setIdError(-1);
			response.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR GET ORDER FEES REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			response.setIdError(-1);
			response.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(response);
		}
		LOGGER.info("RESULT GET ORDER FEES REQUEST - JSON - "+json);
		return json;
	}
	
	public String getRecipientsBySender(String json) {
		Recipients recipients = new Recipients();
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Recipient[] result = null;
//		com.addcel.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Recipient[] resultMx = null;
//		com.addcel.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Recipient[] resultUs = null;
//		com.addcel.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Recipient[] resultCo = null;
		Sender sender = null;
//		int totalSize = 0;
		try {
			LOGGER.info("STARTING RECIPIENTS BY SENDER - JSON - "+json);
			sender = gson.fromJson(json, Sender.class);
			result = proxy.getBeneficiariesBySender(getCredentialsCompliance(), sender.getIdSender(), sender.getIdCountry());
			LOGGER.info("RECIPIENTS FOUND - "+gson.toJson(result));
//			resultMx = proxy.getBeneficiariesBySender(getCredentialsCompliance(), sender.getIdSender(), "MEX");
//			resultUs = proxy.getBeneficiariesBySender(getCredentialsCompliance(), sender.getIdSender(), "USA");
//			resultCo = proxy.getBeneficiariesBySender(getCredentialsCompliance(), sender.getIdSender(), "COL");
//			totalSize = resultMx.length + resultUs.length + resultCo.length;
//			LOGGER.info("MX RECIPIENTS FOUND - "+gson.toJson(resultMx));
//			LOGGER.info("US RECIPIENTS FOUND - "+gson.toJson(resultUs));
//			LOGGER.info("CO RECIPIENTS FOUND - "+gson.toJson(resultCo));
//			result = new
//					com.addcel.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Recipient[totalSize];
//			
//			for(int i=0; i < resultMx.length; i++){
//				result[i] = resultMx[i];
//			}
//			for(int i=0; i < resultUs.length; i++){
//				result[i] = resultUs[i];
//			}
//			for(int i=0; i < resultCo.length; i++){
//				result[i] = resultCo[i];
//			}
			recipients.setRecipients(result);
		} catch(AxisFault a){
			LOGGER.error("ERROR RECIPIENTS BY SENDER - ERROR: "+a.getFaultString());
			recipients.setIdError(-1);
			recipients.setMensajeError(a.getFaultString());
		} catch (Exception e) {
			LOGGER.error("ERROR RECIPIENTS BY SENDER - ERROR: "+e.getLocalizedMessage());
			e.printStackTrace();
			recipients.setIdError(-1);
			recipients.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(recipients);
		}
		LOGGER.info("RESULT RECIPIENTS BY SENDER - JSON - "+json);
		return json;
	}
	
	public String getCities(String json) {
		List<com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.City> cityList = new
				ArrayList<com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.City>();
		Countrys countrys = null;
		ZipCodesCity zipCodes = new ZipCodesCity();
		try {
			LOGGER.info("STARTING GET CITIES IN BD - JSON - "+json);
			countrys = gson.fromJson(json, Countrys.class);
			cityList = mapper.getZipCodeByCity(countrys.getIdCity());
			  if(cityList.isEmpty()){
					zipCodes.setIdError(-1);
					zipCodes.setMensajeError("City not found");
			  } else {
				  zipCodes.setZipCodeList(cityList);
			  }
		} catch (Exception e) {
			LOGGER.error("ERROR GET CITIES IN BD REQUEST - ERROR - "+e.getLocalizedMessage());
			e.printStackTrace();
			zipCodes.setIdError(-1);
			zipCodes.setMensajeError(e.getLocalizedMessage());
		} finally {
			json = gson.toJson(zipCodes);
		}
		LOGGER.info("RESULT GET CITIES IN BD REQUEST - JSON - "+json);
		return json;
	}
	
	public String populateCities(String json) {
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.City city = null;
		int total = 0;
		try {
			Resource resource = new ClassPathResource("City_USA.xml");
			InputStream resourceInputStream = resource.getInputStream();
			SAXBuilder builder = new SAXBuilder();
			  try {
				Document document = (Document) builder.build(resourceInputStream);
				Element rootNode = document.getRootElement();
				List<?> list = rootNode.getChildren("City");
				for (int i = 0; i < list.size(); i++) {
				   Element node = (Element) list.get(i);
				   city = new 
							com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.City();
				   LOGGER.info("City Insert : " + node.getChildText("CityId"));
				   city.setZipCode(node.getChildText("ZipCode"));
				   city.setIdCity(node.getChildText("CityId"));
				   city.setCityName(node.getChildText("City"));
				   city.setIdState(node.getChildText("StateId"));
				   city.setIdCountry(node.getChildText("CountryId"));
				   mapper.insertCity(city);
				   total++;
				}
			  } catch (IOException io) {
				System.out.println(io.getMessage());
			  } catch (JDOMException jdomex) {
				System.out.println(jdomex.getMessage());
			  }
		} catch (Exception e) {
			e.printStackTrace();
		} 
		LOGGER.info("TOTAL CITIES INSERTS IN BD: "+total);
		return "TOTAL CITIES INSERTS IN BD: "+total;
	}
	
	public String getCitiesToSender(String idCity) {
		String zipCode = null;
		try {
			Resource resource = new ClassPathResource("City_USA.xml");
			InputStream resourceInputStream = resource.getInputStream();
			SAXBuilder builder = new SAXBuilder();
			  try {
				Document document = (Document) builder.build(resourceInputStream);
				Element rootNode = document.getRootElement();
				List<?> list = rootNode.getChildren("City");
				for (int i = 0; i < list.size(); i++) {
				   Element node = (Element) list.get(i);
				   if(node.getChildText("CityId").equals(idCity)){
					   zipCode = node.getChildText("ZipCode");
				   }
				}
			  } catch (IOException io) {
				System.out.println(io.getMessage());
			  } catch (JDOMException jdomex) {
				System.out.println(jdomex.getMessage());
			  }
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.info("RESULT: ID CITY: "+idCity+" - ZIP CODE: "+zipCode);
		return zipCode;
	}
	
	
	private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Authorization getCredentialsCompliance(){
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Authorization credentials = null;
		try {
			credentials = mapper.getCredentialsCompliance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return credentials;
	}
	
	private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.Authorization getCredentialsTransfers(){
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.Authorization credentials = null;
		try {
			credentials = mapper.getViamericasCredentials();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return credentials;
	}
	
	private com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.Authorization getCredentialsPayment(){
		com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.Authorization credentials = null;
		try {
			credentials = mapper.getViamericasPaymentCredentials();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return credentials;
	}

	public static void main(String[] args) {
		try {
			System.out.println("ID: "+AddcelCrypto.encryptHard("157504"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ModelAndView queryTransferById(String id, ModelMap modelo) {
		ModelAndView mav = new ModelAndView("consultaTransfer");
		TransferDetails response = null;
		try {
//			try {
//				id = AddcelCrypto.decryptHard(id);
//			} catch (Exception e) {
//				e.printStackTrace();
//				mav = new ModelAndView("error");
//				mav.addObject("mensajeError", "Transferencia no valida.");
//			}
			response = mapper.getTransferDetail(id);
			if(response != null){
				mav = new ModelAndView("consultaTransfer");
				modelo.put("transferForm", response);
				mav.addObject("transferForm", response);
				mav.addObject("NOMBRE", response.getNameReceiver());
				mav.addObject("transactionDate", response.getTransactionDate());
				mav.addObject("idBranch", response.getIdBranch());
				mav.addObject("idReceiver", response.getIdReceiver());
				mav.addObject("passwordReceiver", response.getPasswordReceiver());
				mav.addObject("dateReceiver", response.getDateReceiver());
				mav.addObject("nameSender", response.getNameSender());
				mav.addObject("phone1Sender", response.getPhone1Sender());
				mav.addObject("citySender", response.getCitySender());
				mav.addObject("stateSender", response.getStateSender());
				mav.addObject("zipSender", response.getZipSender());
				mav.addObject("nameCountrySender", response.getNameCountrySender());
				mav.addObject("nameReceiver", response.getNameReceiver());
				mav.addObject("phone1Receiver", response.getPhone1Receiver());
				mav.addObject("addressReceiver", response.getAddressReceiver());
				mav.addObject("nameCityReceiver", response.getNameCityReceiver());
				mav.addObject("nameCountryReceiver", response.getNameCountryReceiver());
				mav.addObject("accReceiver", response.getAccReceiver());
				mav.addObject("companyPayer", response.getCompanyPayer());
				mav.addObject("addressPayer", response.getAddressPayer());
				mav.addObject("phone1Payer", response.getPhone1Payer());
				mav.addObject("businessHours", response.getBusinessHours());
				mav.addObject("currencySrc", response.getCurrencySrc());
				mav.addObject("netAmountReceiver", response.getNetAmountReceiver());
				mav.addObject("originalFreeValue", response.getOriginalFreeValue());
				mav.addObject("transferTaxes", response.getTransferTaxes());
				mav.addObject("totalReceiver", response.getTotalReceiver());
				mav.addObject("fxRateCustomer", response.getFxRateCustomer());
				mav.addObject("currencyPayer", response.getCurrencyPayer());
				mav.addObject("rateChangeReceiver", response.getRateChangeReceiver());
				mav.addObject("totalPayReceiver", response.getTotalPayReceiver());
				mav.addObject("currencyPayer", response.getCurrencyPayer());
			} else {
				mav = new ModelAndView("error");
				mav.addObject("mensajeError", "Transferencia no valida.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			mav = new ModelAndView("error");
			mav.addObject("mensajeError", "Transferencia no valida.");
		}
		return mav;
	}
	
	public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardPaymentResponse setResponse(){
		com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardPaymentResponse response = new com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardPaymentResponse();
		LOGGER.info("11");
		response.setApprovalCode("CMC992");
		LOGGER.info("12");
		response.setInvoiceNumber("A00037531");
		LOGGER.info("12.1");
		response.setResponseCode("000");
		LOGGER.info("12.2");
		response.setResponseMessage("Success");
		LOGGER.info("12.3");
		response.setTransactionCurrency("");
		LOGGER.info("12.4");
		response.setTransactionId("140817A15-640DB0FC-5DB9-49F8-9E08-84E25E637B62");
		LOGGER.info("12.5");
		response.setTransactionDate("08/24/2017 03:50:12 PM");
		LOGGER.info("12.6");
		response.setObjReceipt(new Receipt());
		response.getObjReceipt().setIdBranch("A00037");
		LOGGER.info("12.7");
		response.getObjReceipt().setIdReceiver("531");
		LOGGER.info("12.8");
		response.getObjReceipt().setPasswordReceiver("13YY0037724610");
		LOGGER.info("12.9");
		response.setDateReciever("2017-08-14");
		LOGGER.info("12.10");
		response.getObjReceipt().setNameSender("CARLOS GARCIA");
		LOGGER.info("12.11");
		response.getObjReceipt().setPhone1Sender("5518307722");
		LOGGER.info("12.12");
		response.getObjReceipt().setCitySender("BOULDER");
		LOGGER.info("12.13");
		response.getObjReceipt().setStateSender("CO");
		LOGGER.info("12.14");
		response.getObjReceipt().setZipSender("80314");
		LOGGER.info("12.15");
		response.getObjReceipt().setNameCountrySender("USA");
		LOGGER.info("12.16");
		response.getObjReceipt().setNameReceiver("JOSUE  OCAMPO");
		LOGGER.info("12.17");
		response.getObjReceipt().setPhone1Receiver("5543653531");
		LOGGER.info("12.18");
		response.getObjReceipt().setAddressReceiver("EL RELOJ 666 COYOACAN");
		LOGGER.info("12.19");
		response.getObjReceipt().setNameCityReceiver("COYOACAN");
		LOGGER.info("12.20");
		response.getObjReceipt().setNameCountryReceiver("MEXICO");
		LOGGER.info("12.21");
		response.getObjReceipt().setAccReceiver("");
		LOGGER.info("12.22");
		response.getObjReceipt().setCompanyPayer("HAITIPAY");
		LOGGER.info("12.23");
		response.getObjReceipt().setAddressPayer("");
		LOGGER.info("12.24");
		response.getObjReceipt().setPhone1Payer("");
		LOGGER.info("12.25");
		response.getObjReceipt().setBusinessHours("");
		LOGGER.info("12.26");
		response.getObjReceipt().setCurrencySrc("USD");
		LOGGER.info("12.27");
		response.getObjReceipt().setNetAmountReceiver(new BigDecimal("10.1"));
		LOGGER.info("13");
		response.getObjReceipt().setOriginalFreeValue("13.0");
		LOGGER.info("14");
		response.getObjReceipt().setTransferTaxes(new BigDecimal("0.0"));
		LOGGER.info("15");
		response.getObjReceipt().setTotalReceiver(new BigDecimal("23.45"));
		LOGGER.info("16");
		response.getObjReceipt().setFxRateCustomer("1.00");
		LOGGER.info("17");
		response.getObjReceipt().setCurrencyPayer("MXN");
		LOGGER.info("18");
		response.getObjReceipt().setRateChangeReceiver(new BigDecimal("14.32"));
		LOGGER.info("19");
		response.getObjReceipt().setTotalPayReceiver(new BigDecimal("143.32"));
		LOGGER.info("20");
		response.getObjReceipt().setReceiptDisclaimer("Recipient may receive less due to fees charged by the recipient’s bank and foreign taxes."+ 
"El beneficiario puede recibir menos dinero debido a las comisiones cobradas por el banco del beneficiario y de los impuestos en el extranjero."+
"The provision of an incorrect account number or recipient institution identifier may result in the loss of the transfer amount. You have a right to dispute errors in your transaction. If you think there is an error, contact us within 180 days at 4641 Montgomery Avenue, Suite 400,  Bethesda, MD 20814. You can also contact us for a written explanation of your rights. You can cancel for a full refund within 30 minutes of payment, unless the funds have been picked up or deposited. For questions or complaints about Viamericas  Corporation, contact: Colorado Department of Regulatory Agencies 303-894-7575, www.dora.state.co.us/banking/index.htm.  Consumer Financial Protection Bureau 855-411-2372 855-729-2372 (TTY/TDD) www.consumerfinance.gov || La provisión incorrecta de un identificador de número de cuenta o institución receptora puede resultar en la pérdida de la cantidad de la transferencia.Usted tiene el derecho a disputar errores en su transacción. Si usted cree que hay un error, póngase en contacto con nosotros dentro de los 180 días  en la dirección 4641 Montgomery Avenue, Suite 400, Bethesda, MD 20814. También puede ponerse en contacto con nosotros para una explicación por escrito de sus derechos. Puedes cancelar para un reembolso completo dentro de los 30 minutos de pago, a menos que los fondos han sido recogidos o depositados. Para preguntas o quejas sobre Viamericas Corporation, comuníquese con:  Colorado Department of Regulatory Agencies 303-894-7575, www.dora.state.co.us/banking/index.htm. Oficina de Protección Financiera al Consumidor 855-411-2372 855-729-2372 (TTY / TDD) www.consumerfinance.gov"+
"When payments are made in any currency other than U.S. dollars, in addition to the commission charged for Viamericas' services, a currency exchange rate will be applied. U.S. dollars are converted into the foreign currency at this rate, which is set by Viamericas and is set forth above. Any difference between this rate provided to customers and the rate at which Viamericas or its payout agents obtain the foreign currency will be kept by Viamericas and/or its payout agent. || Cuando se realicen pagos en cualquier divisa diferente a US Dólares, adicional a la comisión que Viamericas cobra por sus servicios, una tasa de cambio será aplicada. Los US Dólares son convertidos a otras divisas aplicando dicha tasa de cambio, la cual es establecida por Viamericas y se encuentra en la parte superior de este recibo. Cualquier diferencia existente entre la tasa de cambio ofrecida a los clientes y la tasa de cambio a la cual Viamericas o cualquiera de sus agentes pagadores obtengan las divisas será para beneficio de Viamericas y/o sus agentes pagadores.The Terms and Conditions, Privacy Policy, and foreign exchange rates are available at www.govianex.com or at 1-800-401-7626."+
"Los Términos y Condiciones, Política de Privacidad y tasas de cambio es	n en www.govianex.com o llame gratis al (800) 401-7626.");
		LOGGER.info("21");
		return response;
	}

}