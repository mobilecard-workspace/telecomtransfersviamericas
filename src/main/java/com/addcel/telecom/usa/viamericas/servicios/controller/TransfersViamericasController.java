package com.addcel.telecom.usa.viamericas.servicios.controller;

import static com.addcel.telecom.usa.viamericas.servicios.utils.Constantes.TESTING_SERVICES_WEB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.telecom.usa.viamericas.servicios.services.TransfersViamericasService;

@Controller
public class TransfersViamericasController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TransfersViamericasController.class);
	
	private static final String PATH_TEST = "/test";
	
	private static final String PATH_CREATE_RECIPIENT = "/transfers/createRecipient";
	
	private static final String PATH_CREATE_SENDER = "/transfers/createSender";
	
	private static final String PATH_CREATE_NEW_ORDER = "/transfers/createNewOrder";
	
	private static final String PATH_CONFIRM_TRANSACTION_CREDIT = "/transfers/confirmTransactionCredit";
	
	private static final String PATH_CONFIRM_TRANSACTION_CASH = "/transfers/confirmTransactionCash";
	
	private static final String PATH_CONFIRM_TRANSACTION_ACH = "/transfers/confirmTransactionCreditACH";
	
	private static final String PATH_CONFIRM_SENDER_PROFILE = "/transfers/createSenderProfileCard";
	
	private static final String PATH_VALIDATE_SENDER = "/validate/senderProfile";
	
	private static final String PATH_GET_PAYMENT_LOCATIONS = "/get/payment/locations";
	
	private static final String PATH_GET_AGENCY_LOCATIONS = "/get/agency/locations";
	
	private static final String PATH_GET_CURRENCY_BY_COUNTRY = "/get/currency/country";
	
	private static final String PATH_GET_PAYMENTS_MODE = "/get/payment/modes";
	
	private static final String PATH_GET_AGENCY_PAYMENTS_MODE = "/get/agency/payment/modes";
	
	private static final String PATH_GET_COUNTRYS = "/get/countrys";
	
	private static final String PATH_GET_COUNTRYS_CITYS = "/get/countrysCitys";
	
	private static final String PATH_GET_COUNTRYS_STATES = "/get/countrysStates";
	
	private static final String PATH_GET_STATES_CITYS = "/get/statesCitys";
	
	private static final String PATH_GET_CITYS_BY_ZIPCODE = "/get/CitysByZipCode";
	
	private static final String PATH_GET_BENEFICIARIES_BY_SENDER = "/get/BeneficiariesBySender";
	
	private static final String PATH_GET_CITIES = "/get/Cities";
	
	private static final String PATH_POPULATE_CITIES = "/put/Cities";
	
	private static final String PATH_EXCHANGE_RATE = "/get/ExchangeRate";
	
	private static final String PATH_COST_PAYMENT_LOCATION_NETWORK = "/get/costPaymentLocationNetwork";
	
	private static final String PATH_GET_ORDER_FEES = "/get/orderFees";
	
	private static final String PATH_QUERY_TRANSFER = "/get/transferByid";
	
	private static final String VIEW_HOME = "home";
	
	private static final String REQ_PARAM_JSON = "json";
	
	@Autowired
	private TransfersViamericasService service;
	
	@RequestMapping(value = PATH_TEST, method=RequestMethod.GET)
	public ModelAndView home() {	
		LOGGER.info(TESTING_SERVICES_WEB);
		ModelAndView mav = new ModelAndView(VIEW_HOME);
		return mav;	
	}
	
	@RequestMapping(value = PATH_GET_ORDER_FEES, method=RequestMethod.POST)
	public @ResponseBody String getOrderFees(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.getOrderFees(json);
	}
	
	@RequestMapping(value = PATH_COST_PAYMENT_LOCATION_NETWORK, method=RequestMethod.POST)
	public @ResponseBody String costPaymentLocationNetwork(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.costPaymentLocationNetwork(json);
	}
	
	@RequestMapping(value = PATH_EXCHANGE_RATE, method=RequestMethod.POST)
	public @ResponseBody String exchangeRate(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.exchangeRate(json);
	}
	
	@RequestMapping(value = PATH_POPULATE_CITIES, method=RequestMethod.POST)
	public @ResponseBody String populateCities(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.populateCities(json);
	}
	
	@RequestMapping(value = PATH_GET_CITIES, method=RequestMethod.POST)
	public @ResponseBody String getCities(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.getCities(json);
	}
	
	@RequestMapping(value = PATH_GET_BENEFICIARIES_BY_SENDER, method=RequestMethod.POST)
	public @ResponseBody String getRecipientsBySender(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.getRecipientsBySender(json);
	}
	
	@RequestMapping(value = PATH_GET_CITYS_BY_ZIPCODE, method=RequestMethod.POST)
	public @ResponseBody String getCitysbyZipCode(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.getCitysByZipCode(json);
	}
	
	@RequestMapping(value = PATH_GET_STATES_CITYS, method=RequestMethod.POST)
	public @ResponseBody String getStatesCitys(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.getStatesCitys(json);
	}
	
	@RequestMapping(value = PATH_GET_COUNTRYS_STATES, method=RequestMethod.POST)
	public @ResponseBody String getCountrysStates(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.getCountrysStates(json);
	}
	
	@RequestMapping(value = PATH_GET_COUNTRYS_CITYS, method=RequestMethod.POST)
	public @ResponseBody String getCountrysCitys(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.getCountrysCitys(json);
	}
	
	@RequestMapping(value = PATH_GET_COUNTRYS, produces = "application/json;charset=UTF-8")
	public @ResponseBody String getCountrys(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.getCountrys(json);
	}
	
	@RequestMapping(value = PATH_GET_AGENCY_PAYMENTS_MODE, method=RequestMethod.POST)
	public @ResponseBody String getAgencyPaymentModes(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.getAgencyPaymentModes(json);
	}
	
	@RequestMapping(value = PATH_CREATE_RECIPIENT, method=RequestMethod.POST)
	public @ResponseBody String createRecipient(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.createRecipient(json);
	}
	
	@RequestMapping(value = PATH_CREATE_SENDER, method=RequestMethod.POST)
	public @ResponseBody String createSender(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.createSender(json);
	}
	
	@RequestMapping(value = PATH_CREATE_NEW_ORDER, method=RequestMethod.POST)
	public @ResponseBody String createNewOrder(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.createNewOrder(json);
	}
	
	@RequestMapping(value = PATH_CONFIRM_TRANSACTION_ACH, method=RequestMethod.POST)
	public @ResponseBody String confirmTransaction(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.confirmTransactionACH(json);
	}
	
	@RequestMapping(value = PATH_CONFIRM_TRANSACTION_CREDIT, method=RequestMethod.POST)
	public @ResponseBody String confirmTransactionCredit(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.confirmTransactionCredit(json);
	}
	
	@RequestMapping(value = PATH_CONFIRM_TRANSACTION_CASH, method=RequestMethod.POST)
	public @ResponseBody String confirmTransactionCash(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.confirmTransactionCash(json);
	}
	
	@RequestMapping(value = PATH_CONFIRM_SENDER_PROFILE, method=RequestMethod.POST)
	public @ResponseBody String createSenderProfile(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.createSenderProfile(json);
	}
	
	@RequestMapping(value = PATH_VALIDATE_SENDER, method=RequestMethod.POST)
	public @ResponseBody String validateSenderProfile(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.validateSenderProfile(json);
	}
	
	@RequestMapping(value = PATH_GET_PAYMENT_LOCATIONS, method=RequestMethod.POST)
	public @ResponseBody String getPaymentLocations(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.getPaymentLocations(json);
	}
	
	@RequestMapping(value = PATH_GET_AGENCY_LOCATIONS, method=RequestMethod.POST)
	public @ResponseBody String getAgencyLocations(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.getAgencyLocations(json);
	}
	
	@RequestMapping(value = PATH_GET_CURRENCY_BY_COUNTRY, method=RequestMethod.POST)
	public @ResponseBody String getCurrencyByCountry(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.getCurrencyByCountry(json);
	}
	
	@RequestMapping(value = PATH_GET_PAYMENTS_MODE, method=RequestMethod.POST)
	public @ResponseBody String getPaymentModes(@RequestParam(REQ_PARAM_JSON) String json) {
		return service.getPaymentModes(json);
	}
	
	@RequestMapping(value = PATH_QUERY_TRANSFER, method=RequestMethod.GET)
	public ModelAndView queryTransferById(@RequestParam("id") String json,  ModelMap modelo) {
		return service.queryTransferById(json, modelo);
	}
		
}
