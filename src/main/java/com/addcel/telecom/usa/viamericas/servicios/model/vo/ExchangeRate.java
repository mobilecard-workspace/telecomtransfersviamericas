package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class ExchangeRate {

	private String modePayment;
	
	private String modeCurrency;
	
	private String idPayBranch;
	
	private double monto;

	public String getModePayment() {
		return modePayment;
	}

	public void setModePayment(String modePayment) {
		this.modePayment = modePayment;
	}

	public String getModeCurrency() {
		return modeCurrency;
	}

	public void setModeCurrency(String modeCurrency) {
		this.modeCurrency = modeCurrency;
	}

	public String getIdPayBranch() {
		return idPayBranch;
	}

	public void setIdPayBranch(String idPayBranch) {
		this.idPayBranch = idPayBranch;
	}
	
}
