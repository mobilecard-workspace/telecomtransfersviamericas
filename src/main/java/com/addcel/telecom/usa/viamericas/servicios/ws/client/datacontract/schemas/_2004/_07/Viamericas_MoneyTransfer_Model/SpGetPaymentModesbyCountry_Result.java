/**
 * SpGetPaymentModesbyCountry_Result.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model;

public class SpGetPaymentModesbyCountry_Result  implements java.io.Serializable {
    private java.lang.String paymentModeId;

    private java.lang.String paymentModeName;

    public SpGetPaymentModesbyCountry_Result() {
    }

    public SpGetPaymentModesbyCountry_Result(
           java.lang.String paymentModeId,
           java.lang.String paymentModeName) {
           this.paymentModeId = paymentModeId;
           this.paymentModeName = paymentModeName;
    }


    /**
     * Gets the paymentModeId value for this SpGetPaymentModesbyCountry_Result.
     * 
     * @return paymentModeId
     */
    public java.lang.String getPaymentModeId() {
        return paymentModeId;
    }


    /**
     * Sets the paymentModeId value for this SpGetPaymentModesbyCountry_Result.
     * 
     * @param paymentModeId
     */
    public void setPaymentModeId(java.lang.String paymentModeId) {
        this.paymentModeId = paymentModeId;
    }


    /**
     * Gets the paymentModeName value for this SpGetPaymentModesbyCountry_Result.
     * 
     * @return paymentModeName
     */
    public java.lang.String getPaymentModeName() {
        return paymentModeName;
    }


    /**
     * Sets the paymentModeName value for this SpGetPaymentModesbyCountry_Result.
     * 
     * @param paymentModeName
     */
    public void setPaymentModeName(java.lang.String paymentModeName) {
        this.paymentModeName = paymentModeName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SpGetPaymentModesbyCountry_Result)) return false;
        SpGetPaymentModesbyCountry_Result other = (SpGetPaymentModesbyCountry_Result) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.paymentModeId==null && other.getPaymentModeId()==null) || 
             (this.paymentModeId!=null &&
              this.paymentModeId.equals(other.getPaymentModeId()))) &&
            ((this.paymentModeName==null && other.getPaymentModeName()==null) || 
             (this.paymentModeName!=null &&
              this.paymentModeName.equals(other.getPaymentModeName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPaymentModeId() != null) {
            _hashCode += getPaymentModeId().hashCode();
        }
        if (getPaymentModeName() != null) {
            _hashCode += getPaymentModeName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SpGetPaymentModesbyCountry_Result.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "spGetPaymentModesbyCountry_Result"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentModeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "PaymentModeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentModeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "PaymentModeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
