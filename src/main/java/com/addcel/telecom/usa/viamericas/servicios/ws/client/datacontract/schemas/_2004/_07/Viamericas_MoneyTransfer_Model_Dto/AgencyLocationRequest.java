/**
 * AgencyLocationRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto;

public class AgencyLocationRequest  implements java.io.Serializable {
    private java.lang.String agencyType;

    private java.lang.String idCountry;

    private java.lang.String idState;

    private java.lang.String cityName;

    private java.lang.String address;

    private java.lang.String zipCode;

    private java.math.BigDecimal miles;

    public AgencyLocationRequest() {
    }

    public AgencyLocationRequest(
           java.lang.String agencyType,
           java.lang.String idCountry,
           java.lang.String idState,
           java.lang.String cityName,
           java.lang.String address,
           java.lang.String zipCode,
           java.math.BigDecimal miles) {
           this.agencyType = agencyType;
           this.idCountry = idCountry;
           this.idState = idState;
           this.cityName = cityName;
           this.address = address;
           this.zipCode = zipCode;
           this.miles = miles;
    }


    /**
     * Gets the agencyType value for this AgencyLocationRequest.
     * 
     * @return agencyType
     */
    public java.lang.String getAgencyType() {
        return agencyType;
    }


    /**
     * Sets the agencyType value for this AgencyLocationRequest.
     * 
     * @param agencyType
     */
    public void setAgencyType(java.lang.String agencyType) {
        this.agencyType = agencyType;
    }


    /**
     * Gets the idCountry value for this AgencyLocationRequest.
     * 
     * @return idCountry
     */
    public java.lang.String getIdCountry() {
        return idCountry;
    }


    /**
     * Sets the idCountry value for this AgencyLocationRequest.
     * 
     * @param idCountry
     */
    public void setIdCountry(java.lang.String idCountry) {
        this.idCountry = idCountry;
    }


    /**
     * Gets the idState value for this AgencyLocationRequest.
     * 
     * @return idState
     */
    public java.lang.String getIdState() {
        return idState;
    }


    /**
     * Sets the idState value for this AgencyLocationRequest.
     * 
     * @param idState
     */
    public void setIdState(java.lang.String idState) {
        this.idState = idState;
    }


    /**
     * Gets the cityName value for this AgencyLocationRequest.
     * 
     * @return cityName
     */
    public java.lang.String getCityName() {
        return cityName;
    }


    /**
     * Sets the cityName value for this AgencyLocationRequest.
     * 
     * @param cityName
     */
    public void setCityName(java.lang.String cityName) {
        this.cityName = cityName;
    }


    /**
     * Gets the address value for this AgencyLocationRequest.
     * 
     * @return address
     */
    public java.lang.String getAddress() {
        return address;
    }


    /**
     * Sets the address value for this AgencyLocationRequest.
     * 
     * @param address
     */
    public void setAddress(java.lang.String address) {
        this.address = address;
    }


    /**
     * Gets the zipCode value for this AgencyLocationRequest.
     * 
     * @return zipCode
     */
    public java.lang.String getZipCode() {
        return zipCode;
    }


    /**
     * Sets the zipCode value for this AgencyLocationRequest.
     * 
     * @param zipCode
     */
    public void setZipCode(java.lang.String zipCode) {
        this.zipCode = zipCode;
    }


    /**
     * Gets the miles value for this AgencyLocationRequest.
     * 
     * @return miles
     */
    public java.math.BigDecimal getMiles() {
        return miles;
    }


    /**
     * Sets the miles value for this AgencyLocationRequest.
     * 
     * @param miles
     */
    public void setMiles(java.math.BigDecimal miles) {
        this.miles = miles;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AgencyLocationRequest)) return false;
        AgencyLocationRequest other = (AgencyLocationRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.agencyType==null && other.getAgencyType()==null) || 
             (this.agencyType!=null &&
              this.agencyType.equals(other.getAgencyType()))) &&
            ((this.idCountry==null && other.getIdCountry()==null) || 
             (this.idCountry!=null &&
              this.idCountry.equals(other.getIdCountry()))) &&
            ((this.idState==null && other.getIdState()==null) || 
             (this.idState!=null &&
              this.idState.equals(other.getIdState()))) &&
            ((this.cityName==null && other.getCityName()==null) || 
             (this.cityName!=null &&
              this.cityName.equals(other.getCityName()))) &&
            ((this.address==null && other.getAddress()==null) || 
             (this.address!=null &&
              this.address.equals(other.getAddress()))) &&
            ((this.zipCode==null && other.getZipCode()==null) || 
             (this.zipCode!=null &&
              this.zipCode.equals(other.getZipCode()))) &&
            ((this.miles==null && other.getMiles()==null) || 
             (this.miles!=null &&
              this.miles.equals(other.getMiles())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAgencyType() != null) {
            _hashCode += getAgencyType().hashCode();
        }
        if (getIdCountry() != null) {
            _hashCode += getIdCountry().hashCode();
        }
        if (getIdState() != null) {
            _hashCode += getIdState().hashCode();
        }
        if (getCityName() != null) {
            _hashCode += getCityName().hashCode();
        }
        if (getAddress() != null) {
            _hashCode += getAddress().hashCode();
        }
        if (getZipCode() != null) {
            _hashCode += getZipCode().hashCode();
        }
        if (getMiles() != null) {
            _hashCode += getMiles().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AgencyLocationRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "AgencyLocationRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("agencyType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "AgencyType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCountry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdCountry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cityName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "CityName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Address"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zipCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ZipCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("miles");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Miles"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
