package com.addcel.telecom.usa.viamericas.servicios.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.telecom.usa.viamericas.servicios.model.vo.Bitacora;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.TransferDetails;
import com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.Authorization;
import com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.City;
import com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.SetNewOrder_Result;
import com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.Transaction;
import com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardPaymentResponse;

public interface DaoMapper {

	String validateSender(long idUsuario);

	Authorization getViamericasCredentials();

	com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Authorization getCredentialsCompliance();

	com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.Authorization getViamericasPaymentCredentials();

	void insertCity(City city);

	List<City> getZipCodeByCity(@Param(value = "id") String idCity);

	void updateSenderByUsuario(@Param(value = "idSender") String idSender, @Param(value = "idUsuario") long idUsuario);
	
	void insertaTransaccion(Bitacora bitacora);
	
	void insertaBitacoraProsa(Bitacora bitacora);
	
	void actualizaTransaccion(Bitacora bitacora);
	
	public String getParametro(@Param(value = "clave") String valor);

	void createRecipient(
			com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Recipient recipient);

	void createOrder(@Param(value = "transaction") Transaction transaction, @Param(value = "result") SetNewOrder_Result result);

	String getMailRecipientByIdBitacora(@Param(value = "idBitacora") long idBitacora, @Param(value = "idSender") String idSender);

	void insertaDatosTransfer(@Param(value = "response") CardPaymentResponse response, @Param(value = "bitacora") Bitacora bitacora);

	TransferDetails getTransferDetail(@Param(value = "id") String id);
	
}
