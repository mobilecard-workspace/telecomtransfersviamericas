/**
 * CostPaymentLocation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto;

public class CostPaymentLocation  implements java.io.Serializable {
    private java.lang.String customerfixfee;

    private java.lang.String customerpercentageapplied;

    private java.lang.String customerpercentagefee;

    private java.lang.String idBranch;

    private java.lang.String idMainBranch;

    private java.lang.String latitude;

    private java.lang.String longitude;

    private java.lang.String nameBranch;

    private java.math.BigDecimal outputAmount;

    private java.math.BigDecimal outputRate;

    private java.lang.String viamericasfixfee;

    private java.lang.String viamericaspercentagefee;

    public CostPaymentLocation() {
    }

    public CostPaymentLocation(
           java.lang.String customerfixfee,
           java.lang.String customerpercentageapplied,
           java.lang.String customerpercentagefee,
           java.lang.String idBranch,
           java.lang.String idMainBranch,
           java.lang.String latitude,
           java.lang.String longitude,
           java.lang.String nameBranch,
           java.math.BigDecimal outputAmount,
           java.math.BigDecimal outputRate,
           java.lang.String viamericasfixfee,
           java.lang.String viamericaspercentagefee) {
           this.customerfixfee = customerfixfee;
           this.customerpercentageapplied = customerpercentageapplied;
           this.customerpercentagefee = customerpercentagefee;
           this.idBranch = idBranch;
           this.idMainBranch = idMainBranch;
           this.latitude = latitude;
           this.longitude = longitude;
           this.nameBranch = nameBranch;
           this.outputAmount = outputAmount;
           this.outputRate = outputRate;
           this.viamericasfixfee = viamericasfixfee;
           this.viamericaspercentagefee = viamericaspercentagefee;
    }


    /**
     * Gets the customerfixfee value for this CostPaymentLocation.
     * 
     * @return customerfixfee
     */
    public java.lang.String getCustomerfixfee() {
        return customerfixfee;
    }


    /**
     * Sets the customerfixfee value for this CostPaymentLocation.
     * 
     * @param customerfixfee
     */
    public void setCustomerfixfee(java.lang.String customerfixfee) {
        this.customerfixfee = customerfixfee;
    }


    /**
     * Gets the customerpercentageapplied value for this CostPaymentLocation.
     * 
     * @return customerpercentageapplied
     */
    public java.lang.String getCustomerpercentageapplied() {
        return customerpercentageapplied;
    }


    /**
     * Sets the customerpercentageapplied value for this CostPaymentLocation.
     * 
     * @param customerpercentageapplied
     */
    public void setCustomerpercentageapplied(java.lang.String customerpercentageapplied) {
        this.customerpercentageapplied = customerpercentageapplied;
    }


    /**
     * Gets the customerpercentagefee value for this CostPaymentLocation.
     * 
     * @return customerpercentagefee
     */
    public java.lang.String getCustomerpercentagefee() {
        return customerpercentagefee;
    }


    /**
     * Sets the customerpercentagefee value for this CostPaymentLocation.
     * 
     * @param customerpercentagefee
     */
    public void setCustomerpercentagefee(java.lang.String customerpercentagefee) {
        this.customerpercentagefee = customerpercentagefee;
    }


    /**
     * Gets the idBranch value for this CostPaymentLocation.
     * 
     * @return idBranch
     */
    public java.lang.String getIdBranch() {
        return idBranch;
    }


    /**
     * Sets the idBranch value for this CostPaymentLocation.
     * 
     * @param idBranch
     */
    public void setIdBranch(java.lang.String idBranch) {
        this.idBranch = idBranch;
    }


    /**
     * Gets the idMainBranch value for this CostPaymentLocation.
     * 
     * @return idMainBranch
     */
    public java.lang.String getIdMainBranch() {
        return idMainBranch;
    }


    /**
     * Sets the idMainBranch value for this CostPaymentLocation.
     * 
     * @param idMainBranch
     */
    public void setIdMainBranch(java.lang.String idMainBranch) {
        this.idMainBranch = idMainBranch;
    }


    /**
     * Gets the latitude value for this CostPaymentLocation.
     * 
     * @return latitude
     */
    public java.lang.String getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this CostPaymentLocation.
     * 
     * @param latitude
     */
    public void setLatitude(java.lang.String latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this CostPaymentLocation.
     * 
     * @return longitude
     */
    public java.lang.String getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this CostPaymentLocation.
     * 
     * @param longitude
     */
    public void setLongitude(java.lang.String longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the nameBranch value for this CostPaymentLocation.
     * 
     * @return nameBranch
     */
    public java.lang.String getNameBranch() {
        return nameBranch;
    }


    /**
     * Sets the nameBranch value for this CostPaymentLocation.
     * 
     * @param nameBranch
     */
    public void setNameBranch(java.lang.String nameBranch) {
        this.nameBranch = nameBranch;
    }


    /**
     * Gets the outputAmount value for this CostPaymentLocation.
     * 
     * @return outputAmount
     */
    public java.math.BigDecimal getOutputAmount() {
        return outputAmount;
    }


    /**
     * Sets the outputAmount value for this CostPaymentLocation.
     * 
     * @param outputAmount
     */
    public void setOutputAmount(java.math.BigDecimal outputAmount) {
        this.outputAmount = outputAmount;
    }


    /**
     * Gets the outputRate value for this CostPaymentLocation.
     * 
     * @return outputRate
     */
    public java.math.BigDecimal getOutputRate() {
        return outputRate;
    }


    /**
     * Sets the outputRate value for this CostPaymentLocation.
     * 
     * @param outputRate
     */
    public void setOutputRate(java.math.BigDecimal outputRate) {
        this.outputRate = outputRate;
    }


    /**
     * Gets the viamericasfixfee value for this CostPaymentLocation.
     * 
     * @return viamericasfixfee
     */
    public java.lang.String getViamericasfixfee() {
        return viamericasfixfee;
    }


    /**
     * Sets the viamericasfixfee value for this CostPaymentLocation.
     * 
     * @param viamericasfixfee
     */
    public void setViamericasfixfee(java.lang.String viamericasfixfee) {
        this.viamericasfixfee = viamericasfixfee;
    }


    /**
     * Gets the viamericaspercentagefee value for this CostPaymentLocation.
     * 
     * @return viamericaspercentagefee
     */
    public java.lang.String getViamericaspercentagefee() {
        return viamericaspercentagefee;
    }


    /**
     * Sets the viamericaspercentagefee value for this CostPaymentLocation.
     * 
     * @param viamericaspercentagefee
     */
    public void setViamericaspercentagefee(java.lang.String viamericaspercentagefee) {
        this.viamericaspercentagefee = viamericaspercentagefee;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CostPaymentLocation)) return false;
        CostPaymentLocation other = (CostPaymentLocation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.customerfixfee==null && other.getCustomerfixfee()==null) || 
             (this.customerfixfee!=null &&
              this.customerfixfee.equals(other.getCustomerfixfee()))) &&
            ((this.customerpercentageapplied==null && other.getCustomerpercentageapplied()==null) || 
             (this.customerpercentageapplied!=null &&
              this.customerpercentageapplied.equals(other.getCustomerpercentageapplied()))) &&
            ((this.customerpercentagefee==null && other.getCustomerpercentagefee()==null) || 
             (this.customerpercentagefee!=null &&
              this.customerpercentagefee.equals(other.getCustomerpercentagefee()))) &&
            ((this.idBranch==null && other.getIdBranch()==null) || 
             (this.idBranch!=null &&
              this.idBranch.equals(other.getIdBranch()))) &&
            ((this.idMainBranch==null && other.getIdMainBranch()==null) || 
             (this.idMainBranch!=null &&
              this.idMainBranch.equals(other.getIdMainBranch()))) &&
            ((this.latitude==null && other.getLatitude()==null) || 
             (this.latitude!=null &&
              this.latitude.equals(other.getLatitude()))) &&
            ((this.longitude==null && other.getLongitude()==null) || 
             (this.longitude!=null &&
              this.longitude.equals(other.getLongitude()))) &&
            ((this.nameBranch==null && other.getNameBranch()==null) || 
             (this.nameBranch!=null &&
              this.nameBranch.equals(other.getNameBranch()))) &&
            ((this.outputAmount==null && other.getOutputAmount()==null) || 
             (this.outputAmount!=null &&
              this.outputAmount.equals(other.getOutputAmount()))) &&
            ((this.outputRate==null && other.getOutputRate()==null) || 
             (this.outputRate!=null &&
              this.outputRate.equals(other.getOutputRate()))) &&
            ((this.viamericasfixfee==null && other.getViamericasfixfee()==null) || 
             (this.viamericasfixfee!=null &&
              this.viamericasfixfee.equals(other.getViamericasfixfee()))) &&
            ((this.viamericaspercentagefee==null && other.getViamericaspercentagefee()==null) || 
             (this.viamericaspercentagefee!=null &&
              this.viamericaspercentagefee.equals(other.getViamericaspercentagefee())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCustomerfixfee() != null) {
            _hashCode += getCustomerfixfee().hashCode();
        }
        if (getCustomerpercentageapplied() != null) {
            _hashCode += getCustomerpercentageapplied().hashCode();
        }
        if (getCustomerpercentagefee() != null) {
            _hashCode += getCustomerpercentagefee().hashCode();
        }
        if (getIdBranch() != null) {
            _hashCode += getIdBranch().hashCode();
        }
        if (getIdMainBranch() != null) {
            _hashCode += getIdMainBranch().hashCode();
        }
        if (getLatitude() != null) {
            _hashCode += getLatitude().hashCode();
        }
        if (getLongitude() != null) {
            _hashCode += getLongitude().hashCode();
        }
        if (getNameBranch() != null) {
            _hashCode += getNameBranch().hashCode();
        }
        if (getOutputAmount() != null) {
            _hashCode += getOutputAmount().hashCode();
        }
        if (getOutputRate() != null) {
            _hashCode += getOutputRate().hashCode();
        }
        if (getViamericasfixfee() != null) {
            _hashCode += getViamericasfixfee().hashCode();
        }
        if (getViamericaspercentagefee() != null) {
            _hashCode += getViamericaspercentagefee().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CostPaymentLocation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "CostPaymentLocation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerfixfee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Customerfixfee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerpercentageapplied");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Customerpercentageapplied"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerpercentagefee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Customerpercentagefee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idBranch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdBranch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idMainBranch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdMainBranch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameBranch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NameBranch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outputAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "OutputAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outputRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "OutputRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("viamericasfixfee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Viamericasfixfee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("viamericaspercentagefee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Viamericaspercentagefee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
