/**
 * AlternativeIdentifier.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto;

public class AlternativeIdentifier  implements java.io.Serializable {
    private java.lang.String idSender;

    private java.lang.String identifierCode;

    private java.lang.String identifierDescription;

    private java.lang.String identifierName;

    private java.lang.String identifierNumber;

    private java.lang.String regularExpression;

    public AlternativeIdentifier() {
    }

    public AlternativeIdentifier(
           java.lang.String idSender,
           java.lang.String identifierCode,
           java.lang.String identifierDescription,
           java.lang.String identifierName,
           java.lang.String identifierNumber,
           java.lang.String regularExpression) {
           this.idSender = idSender;
           this.identifierCode = identifierCode;
           this.identifierDescription = identifierDescription;
           this.identifierName = identifierName;
           this.identifierNumber = identifierNumber;
           this.regularExpression = regularExpression;
    }


    /**
     * Gets the idSender value for this AlternativeIdentifier.
     * 
     * @return idSender
     */
    public java.lang.String getIdSender() {
        return idSender;
    }


    /**
     * Sets the idSender value for this AlternativeIdentifier.
     * 
     * @param idSender
     */
    public void setIdSender(java.lang.String idSender) {
        this.idSender = idSender;
    }


    /**
     * Gets the identifierCode value for this AlternativeIdentifier.
     * 
     * @return identifierCode
     */
    public java.lang.String getIdentifierCode() {
        return identifierCode;
    }


    /**
     * Sets the identifierCode value for this AlternativeIdentifier.
     * 
     * @param identifierCode
     */
    public void setIdentifierCode(java.lang.String identifierCode) {
        this.identifierCode = identifierCode;
    }


    /**
     * Gets the identifierDescription value for this AlternativeIdentifier.
     * 
     * @return identifierDescription
     */
    public java.lang.String getIdentifierDescription() {
        return identifierDescription;
    }


    /**
     * Sets the identifierDescription value for this AlternativeIdentifier.
     * 
     * @param identifierDescription
     */
    public void setIdentifierDescription(java.lang.String identifierDescription) {
        this.identifierDescription = identifierDescription;
    }


    /**
     * Gets the identifierName value for this AlternativeIdentifier.
     * 
     * @return identifierName
     */
    public java.lang.String getIdentifierName() {
        return identifierName;
    }


    /**
     * Sets the identifierName value for this AlternativeIdentifier.
     * 
     * @param identifierName
     */
    public void setIdentifierName(java.lang.String identifierName) {
        this.identifierName = identifierName;
    }


    /**
     * Gets the identifierNumber value for this AlternativeIdentifier.
     * 
     * @return identifierNumber
     */
    public java.lang.String getIdentifierNumber() {
        return identifierNumber;
    }


    /**
     * Sets the identifierNumber value for this AlternativeIdentifier.
     * 
     * @param identifierNumber
     */
    public void setIdentifierNumber(java.lang.String identifierNumber) {
        this.identifierNumber = identifierNumber;
    }


    /**
     * Gets the regularExpression value for this AlternativeIdentifier.
     * 
     * @return regularExpression
     */
    public java.lang.String getRegularExpression() {
        return regularExpression;
    }


    /**
     * Sets the regularExpression value for this AlternativeIdentifier.
     * 
     * @param regularExpression
     */
    public void setRegularExpression(java.lang.String regularExpression) {
        this.regularExpression = regularExpression;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AlternativeIdentifier)) return false;
        AlternativeIdentifier other = (AlternativeIdentifier) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idSender==null && other.getIdSender()==null) || 
             (this.idSender!=null &&
              this.idSender.equals(other.getIdSender()))) &&
            ((this.identifierCode==null && other.getIdentifierCode()==null) || 
             (this.identifierCode!=null &&
              this.identifierCode.equals(other.getIdentifierCode()))) &&
            ((this.identifierDescription==null && other.getIdentifierDescription()==null) || 
             (this.identifierDescription!=null &&
              this.identifierDescription.equals(other.getIdentifierDescription()))) &&
            ((this.identifierName==null && other.getIdentifierName()==null) || 
             (this.identifierName!=null &&
              this.identifierName.equals(other.getIdentifierName()))) &&
            ((this.identifierNumber==null && other.getIdentifierNumber()==null) || 
             (this.identifierNumber!=null &&
              this.identifierNumber.equals(other.getIdentifierNumber()))) &&
            ((this.regularExpression==null && other.getRegularExpression()==null) || 
             (this.regularExpression!=null &&
              this.regularExpression.equals(other.getRegularExpression())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdSender() != null) {
            _hashCode += getIdSender().hashCode();
        }
        if (getIdentifierCode() != null) {
            _hashCode += getIdentifierCode().hashCode();
        }
        if (getIdentifierDescription() != null) {
            _hashCode += getIdentifierDescription().hashCode();
        }
        if (getIdentifierName() != null) {
            _hashCode += getIdentifierName().hashCode();
        }
        if (getIdentifierNumber() != null) {
            _hashCode += getIdentifierNumber().hashCode();
        }
        if (getRegularExpression() != null) {
            _hashCode += getRegularExpression().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AlternativeIdentifier.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "AlternativeIdentifier"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "IdSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identifierCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "identifierCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identifierDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "identifierDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identifierName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "identifierName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identifierNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "identifierNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regularExpression");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "regularExpression"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
