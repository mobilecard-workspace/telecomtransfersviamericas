/**
 * CardProfileRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto;

public class CardProfileRequest  implements java.io.Serializable {
    private java.lang.String CVV;

    private java.lang.String cardHolderFirstName;

    private java.lang.String cardHolderLastName;

    private java.lang.String cardNumber;

    private java.lang.String expDate;

    private java.lang.String idSender;

    private java.lang.String nickName;

    private java.lang.String paymentType;

    public CardProfileRequest() {
    }

    public CardProfileRequest(
           java.lang.String CVV,
           java.lang.String cardHolderFirstName,
           java.lang.String cardHolderLastName,
           java.lang.String cardNumber,
           java.lang.String expDate,
           java.lang.String idSender,
           java.lang.String nickName,
           java.lang.String paymentType) {
           this.CVV = CVV;
           this.cardHolderFirstName = cardHolderFirstName;
           this.cardHolderLastName = cardHolderLastName;
           this.cardNumber = cardNumber;
           this.expDate = expDate;
           this.idSender = idSender;
           this.nickName = nickName;
           this.paymentType = paymentType;
    }


    /**
     * Gets the CVV value for this CardProfileRequest.
     * 
     * @return CVV
     */
    public java.lang.String getCVV() {
        return CVV;
    }


    /**
     * Sets the CVV value for this CardProfileRequest.
     * 
     * @param CVV
     */
    public void setCVV(java.lang.String CVV) {
        this.CVV = CVV;
    }


    /**
     * Gets the cardHolderFirstName value for this CardProfileRequest.
     * 
     * @return cardHolderFirstName
     */
    public java.lang.String getCardHolderFirstName() {
        return cardHolderFirstName;
    }


    /**
     * Sets the cardHolderFirstName value for this CardProfileRequest.
     * 
     * @param cardHolderFirstName
     */
    public void setCardHolderFirstName(java.lang.String cardHolderFirstName) {
        this.cardHolderFirstName = cardHolderFirstName;
    }


    /**
     * Gets the cardHolderLastName value for this CardProfileRequest.
     * 
     * @return cardHolderLastName
     */
    public java.lang.String getCardHolderLastName() {
        return cardHolderLastName;
    }


    /**
     * Sets the cardHolderLastName value for this CardProfileRequest.
     * 
     * @param cardHolderLastName
     */
    public void setCardHolderLastName(java.lang.String cardHolderLastName) {
        this.cardHolderLastName = cardHolderLastName;
    }


    /**
     * Gets the cardNumber value for this CardProfileRequest.
     * 
     * @return cardNumber
     */
    public java.lang.String getCardNumber() {
        return cardNumber;
    }


    /**
     * Sets the cardNumber value for this CardProfileRequest.
     * 
     * @param cardNumber
     */
    public void setCardNumber(java.lang.String cardNumber) {
        this.cardNumber = cardNumber;
    }


    /**
     * Gets the expDate value for this CardProfileRequest.
     * 
     * @return expDate
     */
    public java.lang.String getExpDate() {
        return expDate;
    }


    /**
     * Sets the expDate value for this CardProfileRequest.
     * 
     * @param expDate
     */
    public void setExpDate(java.lang.String expDate) {
        this.expDate = expDate;
    }


    /**
     * Gets the idSender value for this CardProfileRequest.
     * 
     * @return idSender
     */
    public java.lang.String getIdSender() {
        return idSender;
    }


    /**
     * Sets the idSender value for this CardProfileRequest.
     * 
     * @param idSender
     */
    public void setIdSender(java.lang.String idSender) {
        this.idSender = idSender;
    }


    /**
     * Gets the nickName value for this CardProfileRequest.
     * 
     * @return nickName
     */
    public java.lang.String getNickName() {
        return nickName;
    }


    /**
     * Sets the nickName value for this CardProfileRequest.
     * 
     * @param nickName
     */
    public void setNickName(java.lang.String nickName) {
        this.nickName = nickName;
    }


    /**
     * Gets the paymentType value for this CardProfileRequest.
     * 
     * @return paymentType
     */
    public java.lang.String getPaymentType() {
        return paymentType;
    }


    /**
     * Sets the paymentType value for this CardProfileRequest.
     * 
     * @param paymentType
     */
    public void setPaymentType(java.lang.String paymentType) {
        this.paymentType = paymentType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CardProfileRequest)) return false;
        CardProfileRequest other = (CardProfileRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CVV==null && other.getCVV()==null) || 
             (this.CVV!=null &&
              this.CVV.equals(other.getCVV()))) &&
            ((this.cardHolderFirstName==null && other.getCardHolderFirstName()==null) || 
             (this.cardHolderFirstName!=null &&
              this.cardHolderFirstName.equals(other.getCardHolderFirstName()))) &&
            ((this.cardHolderLastName==null && other.getCardHolderLastName()==null) || 
             (this.cardHolderLastName!=null &&
              this.cardHolderLastName.equals(other.getCardHolderLastName()))) &&
            ((this.cardNumber==null && other.getCardNumber()==null) || 
             (this.cardNumber!=null &&
              this.cardNumber.equals(other.getCardNumber()))) &&
            ((this.expDate==null && other.getExpDate()==null) || 
             (this.expDate!=null &&
              this.expDate.equals(other.getExpDate()))) &&
            ((this.idSender==null && other.getIdSender()==null) || 
             (this.idSender!=null &&
              this.idSender.equals(other.getIdSender()))) &&
            ((this.nickName==null && other.getNickName()==null) || 
             (this.nickName!=null &&
              this.nickName.equals(other.getNickName()))) &&
            ((this.paymentType==null && other.getPaymentType()==null) || 
             (this.paymentType!=null &&
              this.paymentType.equals(other.getPaymentType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCVV() != null) {
            _hashCode += getCVV().hashCode();
        }
        if (getCardHolderFirstName() != null) {
            _hashCode += getCardHolderFirstName().hashCode();
        }
        if (getCardHolderLastName() != null) {
            _hashCode += getCardHolderLastName().hashCode();
        }
        if (getCardNumber() != null) {
            _hashCode += getCardNumber().hashCode();
        }
        if (getExpDate() != null) {
            _hashCode += getExpDate().hashCode();
        }
        if (getIdSender() != null) {
            _hashCode += getIdSender().hashCode();
        }
        if (getNickName() != null) {
            _hashCode += getNickName().hashCode();
        }
        if (getPaymentType() != null) {
            _hashCode += getPaymentType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CardProfileRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.PaymentSvc.Model.Dto", "CardProfileRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CVV");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.PaymentSvc.Model.Dto", "CVV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderFirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.PaymentSvc.Model.Dto", "CardHolderFirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.PaymentSvc.Model.Dto", "CardHolderLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.PaymentSvc.Model.Dto", "CardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.PaymentSvc.Model.Dto", "ExpDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.PaymentSvc.Model.Dto", "IdSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nickName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.PaymentSvc.Model.Dto", "NickName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.PaymentSvc.Model.Dto", "PaymentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
