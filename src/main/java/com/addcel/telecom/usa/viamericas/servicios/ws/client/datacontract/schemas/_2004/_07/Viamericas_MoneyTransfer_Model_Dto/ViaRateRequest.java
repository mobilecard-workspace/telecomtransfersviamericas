/**
 * ViaRateRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto;

public class ViaRateRequest  implements java.io.Serializable {
    private java.lang.String idBranchPayReceiver;

    private java.math.BigDecimal amountToSend;

    private java.lang.String idPayMode;

    private java.math.BigDecimal newRate;

    private java.lang.String idCurrencyMode;

    public ViaRateRequest() {
    }

    public ViaRateRequest(
           java.lang.String idBranchPayReceiver,
           java.math.BigDecimal amountToSend,
           java.lang.String idPayMode,
           java.math.BigDecimal newRate,
           java.lang.String idCurrencyMode) {
           this.idBranchPayReceiver = idBranchPayReceiver;
           this.amountToSend = amountToSend;
           this.idPayMode = idPayMode;
           this.newRate = newRate;
           this.idCurrencyMode = idCurrencyMode;
    }


    /**
     * Gets the idBranchPayReceiver value for this ViaRateRequest.
     * 
     * @return idBranchPayReceiver
     */
    public java.lang.String getIdBranchPayReceiver() {
        return idBranchPayReceiver;
    }


    /**
     * Sets the idBranchPayReceiver value for this ViaRateRequest.
     * 
     * @param idBranchPayReceiver
     */
    public void setIdBranchPayReceiver(java.lang.String idBranchPayReceiver) {
        this.idBranchPayReceiver = idBranchPayReceiver;
    }


    /**
     * Gets the amountToSend value for this ViaRateRequest.
     * 
     * @return amountToSend
     */
    public java.math.BigDecimal getAmountToSend() {
        return amountToSend;
    }


    /**
     * Sets the amountToSend value for this ViaRateRequest.
     * 
     * @param amountToSend
     */
    public void setAmountToSend(java.math.BigDecimal amountToSend) {
        this.amountToSend = amountToSend;
    }


    /**
     * Gets the idPayMode value for this ViaRateRequest.
     * 
     * @return idPayMode
     */
    public java.lang.String getIdPayMode() {
        return idPayMode;
    }


    /**
     * Sets the idPayMode value for this ViaRateRequest.
     * 
     * @param idPayMode
     */
    public void setIdPayMode(java.lang.String idPayMode) {
        this.idPayMode = idPayMode;
    }


    /**
     * Gets the newRate value for this ViaRateRequest.
     * 
     * @return newRate
     */
    public java.math.BigDecimal getNewRate() {
        return newRate;
    }


    /**
     * Sets the newRate value for this ViaRateRequest.
     * 
     * @param newRate
     */
    public void setNewRate(java.math.BigDecimal newRate) {
        this.newRate = newRate;
    }


    /**
     * Gets the idCurrencyMode value for this ViaRateRequest.
     * 
     * @return idCurrencyMode
     */
    public java.lang.String getIdCurrencyMode() {
        return idCurrencyMode;
    }


    /**
     * Sets the idCurrencyMode value for this ViaRateRequest.
     * 
     * @param idCurrencyMode
     */
    public void setIdCurrencyMode(java.lang.String idCurrencyMode) {
        this.idCurrencyMode = idCurrencyMode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ViaRateRequest)) return false;
        ViaRateRequest other = (ViaRateRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idBranchPayReceiver==null && other.getIdBranchPayReceiver()==null) || 
             (this.idBranchPayReceiver!=null &&
              this.idBranchPayReceiver.equals(other.getIdBranchPayReceiver()))) &&
            ((this.amountToSend==null && other.getAmountToSend()==null) || 
             (this.amountToSend!=null &&
              this.amountToSend.equals(other.getAmountToSend()))) &&
            ((this.idPayMode==null && other.getIdPayMode()==null) || 
             (this.idPayMode!=null &&
              this.idPayMode.equals(other.getIdPayMode()))) &&
            ((this.newRate==null && other.getNewRate()==null) || 
             (this.newRate!=null &&
              this.newRate.equals(other.getNewRate()))) &&
            ((this.idCurrencyMode==null && other.getIdCurrencyMode()==null) || 
             (this.idCurrencyMode!=null &&
              this.idCurrencyMode.equals(other.getIdCurrencyMode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdBranchPayReceiver() != null) {
            _hashCode += getIdBranchPayReceiver().hashCode();
        }
        if (getAmountToSend() != null) {
            _hashCode += getAmountToSend().hashCode();
        }
        if (getIdPayMode() != null) {
            _hashCode += getIdPayMode().hashCode();
        }
        if (getNewRate() != null) {
            _hashCode += getNewRate().hashCode();
        }
        if (getIdCurrencyMode() != null) {
            _hashCode += getIdCurrencyMode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ViaRateRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ViaRateRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idBranchPayReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdBranchPayReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amountToSend");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "AmountToSend"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idPayMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdPayMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NewRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCurrencyMode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdCurrencyMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
