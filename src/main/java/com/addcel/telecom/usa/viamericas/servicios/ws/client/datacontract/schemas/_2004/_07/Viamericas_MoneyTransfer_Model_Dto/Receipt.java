/**
 * Receipt.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto;

public class Receipt  implements java.io.Serializable {
    private java.lang.String ACC_RECEIVER;

    private java.lang.String ACH_SENDER;

    private java.lang.String ADDRESS_BRANCH;

    private java.lang.String ADDRESS_RECEIVER;

    private java.lang.String ADDRES_SENDER;

    private java.lang.String BANK_RECEIVER;

    private java.lang.String BRANCH_CITY_NAME;

    private java.lang.String BRANCH_PAY_RECEIVER;

    private java.lang.String BUSINESS_HOURS;

    private java.lang.String CARD_TRANID;

    private java.lang.String CITY_SENDER;

    private java.lang.String CLAVE_RECEIVER;

    private java.lang.String CURRENCY_SRC;

    private java.util.Calendar DATE_RECEIVER;

    private java.lang.String EXPL_TEXT;

    private java.lang.String ID_BRANCH;

    private java.lang.String ID_CASHIER;

    private java.lang.String ID_COMPANY;

    private java.lang.String ID_FLAG_RECEIVER;

    private java.lang.String ID_PAYMENT;

    private java.math.BigDecimal ID_RECEIVER;

    private java.math.BigDecimal ID_SENDER;

    private java.lang.String ISO_CODE;

    private java.lang.String MODE_PAY_RECEIVER;

    private java.lang.String MOD_PAY_CURRENCY;

    private java.lang.String NAME_BRANCH;

    private java.lang.String NAME_CITY;

    private java.lang.String NAME_COUNTRY;

    private java.lang.String NAME_COUNTRY_SENDER;

    private java.lang.String NAME_MAIN_BRANCH;

    private java.lang.String NAME_PAYMENT;

    private java.lang.String NAME_RECEIVER;

    private java.lang.String NAME_SENDER;

    private java.lang.String NAME_STATE_RECEIVER;

    private java.math.BigDecimal NET_AMOUNT_RECEIVER;

    private java.lang.Integer PAY_TIME;

    private java.lang.Integer PAY_TIME_WKD;

    private java.lang.String PHONE1_RECEIVER;

    private java.math.BigDecimal PHONE1_SENDER;

    private java.lang.String PHONE2_RECEIVER;

    private java.math.BigDecimal PHONE2_SENDER;

    private java.math.BigDecimal RATE_CHANGE_RECEIVER;

    private java.lang.String RECEIPT_DISCLAIMER;

    private java.lang.String RECEIVER_DATE_AVAILABLE;

    private java.lang.String STATE_SENDER;

    private java.lang.String TAX_PERCENTAGE;

    private java.math.BigDecimal TELEX_RECEIVER;

    private java.math.BigDecimal TOTAL_PAY_RECEIVER;

    private java.math.BigDecimal TOTAL_RECEIVER;

    private java.math.BigDecimal TRANSFER_TAXES;

    private java.lang.String ZIP_RECEIVER;

    private java.math.BigDecimal ZIP_SENDER;

    public Receipt() {
    }

    public Receipt(
           java.lang.String ACC_RECEIVER,
           java.lang.String ACH_SENDER,
           java.lang.String ADDRESS_BRANCH,
           java.lang.String ADDRESS_RECEIVER,
           java.lang.String ADDRES_SENDER,
           java.lang.String BANK_RECEIVER,
           java.lang.String BRANCH_CITY_NAME,
           java.lang.String BRANCH_PAY_RECEIVER,
           java.lang.String BUSINESS_HOURS,
           java.lang.String CARD_TRANID,
           java.lang.String CITY_SENDER,
           java.lang.String CLAVE_RECEIVER,
           java.lang.String CURRENCY_SRC,
           java.util.Calendar DATE_RECEIVER,
           java.lang.String EXPL_TEXT,
           java.lang.String ID_BRANCH,
           java.lang.String ID_CASHIER,
           java.lang.String ID_COMPANY,
           java.lang.String ID_FLAG_RECEIVER,
           java.lang.String ID_PAYMENT,
           java.math.BigDecimal ID_RECEIVER,
           java.math.BigDecimal ID_SENDER,
           java.lang.String ISO_CODE,
           java.lang.String MODE_PAY_RECEIVER,
           java.lang.String MOD_PAY_CURRENCY,
           java.lang.String NAME_BRANCH,
           java.lang.String NAME_CITY,
           java.lang.String NAME_COUNTRY,
           java.lang.String NAME_COUNTRY_SENDER,
           java.lang.String NAME_MAIN_BRANCH,
           java.lang.String NAME_PAYMENT,
           java.lang.String NAME_RECEIVER,
           java.lang.String NAME_SENDER,
           java.lang.String NAME_STATE_RECEIVER,
           java.math.BigDecimal NET_AMOUNT_RECEIVER,
           java.lang.Integer PAY_TIME,
           java.lang.Integer PAY_TIME_WKD,
           java.lang.String PHONE1_RECEIVER,
           java.math.BigDecimal PHONE1_SENDER,
           java.lang.String PHONE2_RECEIVER,
           java.math.BigDecimal PHONE2_SENDER,
           java.math.BigDecimal RATE_CHANGE_RECEIVER,
           java.lang.String RECEIPT_DISCLAIMER,
           java.lang.String RECEIVER_DATE_AVAILABLE,
           java.lang.String STATE_SENDER,
           java.lang.String TAX_PERCENTAGE,
           java.math.BigDecimal TELEX_RECEIVER,
           java.math.BigDecimal TOTAL_PAY_RECEIVER,
           java.math.BigDecimal TOTAL_RECEIVER,
           java.math.BigDecimal TRANSFER_TAXES,
           java.lang.String ZIP_RECEIVER,
           java.math.BigDecimal ZIP_SENDER) {
           this.ACC_RECEIVER = ACC_RECEIVER;
           this.ACH_SENDER = ACH_SENDER;
           this.ADDRESS_BRANCH = ADDRESS_BRANCH;
           this.ADDRESS_RECEIVER = ADDRESS_RECEIVER;
           this.ADDRES_SENDER = ADDRES_SENDER;
           this.BANK_RECEIVER = BANK_RECEIVER;
           this.BRANCH_CITY_NAME = BRANCH_CITY_NAME;
           this.BRANCH_PAY_RECEIVER = BRANCH_PAY_RECEIVER;
           this.BUSINESS_HOURS = BUSINESS_HOURS;
           this.CARD_TRANID = CARD_TRANID;
           this.CITY_SENDER = CITY_SENDER;
           this.CLAVE_RECEIVER = CLAVE_RECEIVER;
           this.CURRENCY_SRC = CURRENCY_SRC;
           this.DATE_RECEIVER = DATE_RECEIVER;
           this.EXPL_TEXT = EXPL_TEXT;
           this.ID_BRANCH = ID_BRANCH;
           this.ID_CASHIER = ID_CASHIER;
           this.ID_COMPANY = ID_COMPANY;
           this.ID_FLAG_RECEIVER = ID_FLAG_RECEIVER;
           this.ID_PAYMENT = ID_PAYMENT;
           this.ID_RECEIVER = ID_RECEIVER;
           this.ID_SENDER = ID_SENDER;
           this.ISO_CODE = ISO_CODE;
           this.MODE_PAY_RECEIVER = MODE_PAY_RECEIVER;
           this.MOD_PAY_CURRENCY = MOD_PAY_CURRENCY;
           this.NAME_BRANCH = NAME_BRANCH;
           this.NAME_CITY = NAME_CITY;
           this.NAME_COUNTRY = NAME_COUNTRY;
           this.NAME_COUNTRY_SENDER = NAME_COUNTRY_SENDER;
           this.NAME_MAIN_BRANCH = NAME_MAIN_BRANCH;
           this.NAME_PAYMENT = NAME_PAYMENT;
           this.NAME_RECEIVER = NAME_RECEIVER;
           this.NAME_SENDER = NAME_SENDER;
           this.NAME_STATE_RECEIVER = NAME_STATE_RECEIVER;
           this.NET_AMOUNT_RECEIVER = NET_AMOUNT_RECEIVER;
           this.PAY_TIME = PAY_TIME;
           this.PAY_TIME_WKD = PAY_TIME_WKD;
           this.PHONE1_RECEIVER = PHONE1_RECEIVER;
           this.PHONE1_SENDER = PHONE1_SENDER;
           this.PHONE2_RECEIVER = PHONE2_RECEIVER;
           this.PHONE2_SENDER = PHONE2_SENDER;
           this.RATE_CHANGE_RECEIVER = RATE_CHANGE_RECEIVER;
           this.RECEIPT_DISCLAIMER = RECEIPT_DISCLAIMER;
           this.RECEIVER_DATE_AVAILABLE = RECEIVER_DATE_AVAILABLE;
           this.STATE_SENDER = STATE_SENDER;
           this.TAX_PERCENTAGE = TAX_PERCENTAGE;
           this.TELEX_RECEIVER = TELEX_RECEIVER;
           this.TOTAL_PAY_RECEIVER = TOTAL_PAY_RECEIVER;
           this.TOTAL_RECEIVER = TOTAL_RECEIVER;
           this.TRANSFER_TAXES = TRANSFER_TAXES;
           this.ZIP_RECEIVER = ZIP_RECEIVER;
           this.ZIP_SENDER = ZIP_SENDER;
    }


    /**
     * Gets the ACC_RECEIVER value for this Receipt.
     * 
     * @return ACC_RECEIVER
     */
    public java.lang.String getACC_RECEIVER() {
        return ACC_RECEIVER;
    }


    /**
     * Sets the ACC_RECEIVER value for this Receipt.
     * 
     * @param ACC_RECEIVER
     */
    public void setACC_RECEIVER(java.lang.String ACC_RECEIVER) {
        this.ACC_RECEIVER = ACC_RECEIVER;
    }


    /**
     * Gets the ACH_SENDER value for this Receipt.
     * 
     * @return ACH_SENDER
     */
    public java.lang.String getACH_SENDER() {
        return ACH_SENDER;
    }


    /**
     * Sets the ACH_SENDER value for this Receipt.
     * 
     * @param ACH_SENDER
     */
    public void setACH_SENDER(java.lang.String ACH_SENDER) {
        this.ACH_SENDER = ACH_SENDER;
    }


    /**
     * Gets the ADDRESS_BRANCH value for this Receipt.
     * 
     * @return ADDRESS_BRANCH
     */
    public java.lang.String getADDRESS_BRANCH() {
        return ADDRESS_BRANCH;
    }


    /**
     * Sets the ADDRESS_BRANCH value for this Receipt.
     * 
     * @param ADDRESS_BRANCH
     */
    public void setADDRESS_BRANCH(java.lang.String ADDRESS_BRANCH) {
        this.ADDRESS_BRANCH = ADDRESS_BRANCH;
    }


    /**
     * Gets the ADDRESS_RECEIVER value for this Receipt.
     * 
     * @return ADDRESS_RECEIVER
     */
    public java.lang.String getADDRESS_RECEIVER() {
        return ADDRESS_RECEIVER;
    }


    /**
     * Sets the ADDRESS_RECEIVER value for this Receipt.
     * 
     * @param ADDRESS_RECEIVER
     */
    public void setADDRESS_RECEIVER(java.lang.String ADDRESS_RECEIVER) {
        this.ADDRESS_RECEIVER = ADDRESS_RECEIVER;
    }


    /**
     * Gets the ADDRES_SENDER value for this Receipt.
     * 
     * @return ADDRES_SENDER
     */
    public java.lang.String getADDRES_SENDER() {
        return ADDRES_SENDER;
    }


    /**
     * Sets the ADDRES_SENDER value for this Receipt.
     * 
     * @param ADDRES_SENDER
     */
    public void setADDRES_SENDER(java.lang.String ADDRES_SENDER) {
        this.ADDRES_SENDER = ADDRES_SENDER;
    }


    /**
     * Gets the BANK_RECEIVER value for this Receipt.
     * 
     * @return BANK_RECEIVER
     */
    public java.lang.String getBANK_RECEIVER() {
        return BANK_RECEIVER;
    }


    /**
     * Sets the BANK_RECEIVER value for this Receipt.
     * 
     * @param BANK_RECEIVER
     */
    public void setBANK_RECEIVER(java.lang.String BANK_RECEIVER) {
        this.BANK_RECEIVER = BANK_RECEIVER;
    }


    /**
     * Gets the BRANCH_CITY_NAME value for this Receipt.
     * 
     * @return BRANCH_CITY_NAME
     */
    public java.lang.String getBRANCH_CITY_NAME() {
        return BRANCH_CITY_NAME;
    }


    /**
     * Sets the BRANCH_CITY_NAME value for this Receipt.
     * 
     * @param BRANCH_CITY_NAME
     */
    public void setBRANCH_CITY_NAME(java.lang.String BRANCH_CITY_NAME) {
        this.BRANCH_CITY_NAME = BRANCH_CITY_NAME;
    }


    /**
     * Gets the BRANCH_PAY_RECEIVER value for this Receipt.
     * 
     * @return BRANCH_PAY_RECEIVER
     */
    public java.lang.String getBRANCH_PAY_RECEIVER() {
        return BRANCH_PAY_RECEIVER;
    }


    /**
     * Sets the BRANCH_PAY_RECEIVER value for this Receipt.
     * 
     * @param BRANCH_PAY_RECEIVER
     */
    public void setBRANCH_PAY_RECEIVER(java.lang.String BRANCH_PAY_RECEIVER) {
        this.BRANCH_PAY_RECEIVER = BRANCH_PAY_RECEIVER;
    }


    /**
     * Gets the BUSINESS_HOURS value for this Receipt.
     * 
     * @return BUSINESS_HOURS
     */
    public java.lang.String getBUSINESS_HOURS() {
        return BUSINESS_HOURS;
    }


    /**
     * Sets the BUSINESS_HOURS value for this Receipt.
     * 
     * @param BUSINESS_HOURS
     */
    public void setBUSINESS_HOURS(java.lang.String BUSINESS_HOURS) {
        this.BUSINESS_HOURS = BUSINESS_HOURS;
    }


    /**
     * Gets the CARD_TRANID value for this Receipt.
     * 
     * @return CARD_TRANID
     */
    public java.lang.String getCARD_TRANID() {
        return CARD_TRANID;
    }


    /**
     * Sets the CARD_TRANID value for this Receipt.
     * 
     * @param CARD_TRANID
     */
    public void setCARD_TRANID(java.lang.String CARD_TRANID) {
        this.CARD_TRANID = CARD_TRANID;
    }


    /**
     * Gets the CITY_SENDER value for this Receipt.
     * 
     * @return CITY_SENDER
     */
    public java.lang.String getCITY_SENDER() {
        return CITY_SENDER;
    }


    /**
     * Sets the CITY_SENDER value for this Receipt.
     * 
     * @param CITY_SENDER
     */
    public void setCITY_SENDER(java.lang.String CITY_SENDER) {
        this.CITY_SENDER = CITY_SENDER;
    }


    /**
     * Gets the CLAVE_RECEIVER value for this Receipt.
     * 
     * @return CLAVE_RECEIVER
     */
    public java.lang.String getCLAVE_RECEIVER() {
        return CLAVE_RECEIVER;
    }


    /**
     * Sets the CLAVE_RECEIVER value for this Receipt.
     * 
     * @param CLAVE_RECEIVER
     */
    public void setCLAVE_RECEIVER(java.lang.String CLAVE_RECEIVER) {
        this.CLAVE_RECEIVER = CLAVE_RECEIVER;
    }


    /**
     * Gets the CURRENCY_SRC value for this Receipt.
     * 
     * @return CURRENCY_SRC
     */
    public java.lang.String getCURRENCY_SRC() {
        return CURRENCY_SRC;
    }


    /**
     * Sets the CURRENCY_SRC value for this Receipt.
     * 
     * @param CURRENCY_SRC
     */
    public void setCURRENCY_SRC(java.lang.String CURRENCY_SRC) {
        this.CURRENCY_SRC = CURRENCY_SRC;
    }


    /**
     * Gets the DATE_RECEIVER value for this Receipt.
     * 
     * @return DATE_RECEIVER
     */
    public java.util.Calendar getDATE_RECEIVER() {
        return DATE_RECEIVER;
    }


    /**
     * Sets the DATE_RECEIVER value for this Receipt.
     * 
     * @param DATE_RECEIVER
     */
    public void setDATE_RECEIVER(java.util.Calendar DATE_RECEIVER) {
        this.DATE_RECEIVER = DATE_RECEIVER;
    }


    /**
     * Gets the EXPL_TEXT value for this Receipt.
     * 
     * @return EXPL_TEXT
     */
    public java.lang.String getEXPL_TEXT() {
        return EXPL_TEXT;
    }


    /**
     * Sets the EXPL_TEXT value for this Receipt.
     * 
     * @param EXPL_TEXT
     */
    public void setEXPL_TEXT(java.lang.String EXPL_TEXT) {
        this.EXPL_TEXT = EXPL_TEXT;
    }


    /**
     * Gets the ID_BRANCH value for this Receipt.
     * 
     * @return ID_BRANCH
     */
    public java.lang.String getID_BRANCH() {
        return ID_BRANCH;
    }


    /**
     * Sets the ID_BRANCH value for this Receipt.
     * 
     * @param ID_BRANCH
     */
    public void setID_BRANCH(java.lang.String ID_BRANCH) {
        this.ID_BRANCH = ID_BRANCH;
    }


    /**
     * Gets the ID_CASHIER value for this Receipt.
     * 
     * @return ID_CASHIER
     */
    public java.lang.String getID_CASHIER() {
        return ID_CASHIER;
    }


    /**
     * Sets the ID_CASHIER value for this Receipt.
     * 
     * @param ID_CASHIER
     */
    public void setID_CASHIER(java.lang.String ID_CASHIER) {
        this.ID_CASHIER = ID_CASHIER;
    }


    /**
     * Gets the ID_COMPANY value for this Receipt.
     * 
     * @return ID_COMPANY
     */
    public java.lang.String getID_COMPANY() {
        return ID_COMPANY;
    }


    /**
     * Sets the ID_COMPANY value for this Receipt.
     * 
     * @param ID_COMPANY
     */
    public void setID_COMPANY(java.lang.String ID_COMPANY) {
        this.ID_COMPANY = ID_COMPANY;
    }


    /**
     * Gets the ID_FLAG_RECEIVER value for this Receipt.
     * 
     * @return ID_FLAG_RECEIVER
     */
    public java.lang.String getID_FLAG_RECEIVER() {
        return ID_FLAG_RECEIVER;
    }


    /**
     * Sets the ID_FLAG_RECEIVER value for this Receipt.
     * 
     * @param ID_FLAG_RECEIVER
     */
    public void setID_FLAG_RECEIVER(java.lang.String ID_FLAG_RECEIVER) {
        this.ID_FLAG_RECEIVER = ID_FLAG_RECEIVER;
    }


    /**
     * Gets the ID_PAYMENT value for this Receipt.
     * 
     * @return ID_PAYMENT
     */
    public java.lang.String getID_PAYMENT() {
        return ID_PAYMENT;
    }


    /**
     * Sets the ID_PAYMENT value for this Receipt.
     * 
     * @param ID_PAYMENT
     */
    public void setID_PAYMENT(java.lang.String ID_PAYMENT) {
        this.ID_PAYMENT = ID_PAYMENT;
    }


    /**
     * Gets the ID_RECEIVER value for this Receipt.
     * 
     * @return ID_RECEIVER
     */
    public java.math.BigDecimal getID_RECEIVER() {
        return ID_RECEIVER;
    }


    /**
     * Sets the ID_RECEIVER value for this Receipt.
     * 
     * @param ID_RECEIVER
     */
    public void setID_RECEIVER(java.math.BigDecimal ID_RECEIVER) {
        this.ID_RECEIVER = ID_RECEIVER;
    }


    /**
     * Gets the ID_SENDER value for this Receipt.
     * 
     * @return ID_SENDER
     */
    public java.math.BigDecimal getID_SENDER() {
        return ID_SENDER;
    }


    /**
     * Sets the ID_SENDER value for this Receipt.
     * 
     * @param ID_SENDER
     */
    public void setID_SENDER(java.math.BigDecimal ID_SENDER) {
        this.ID_SENDER = ID_SENDER;
    }


    /**
     * Gets the ISO_CODE value for this Receipt.
     * 
     * @return ISO_CODE
     */
    public java.lang.String getISO_CODE() {
        return ISO_CODE;
    }


    /**
     * Sets the ISO_CODE value for this Receipt.
     * 
     * @param ISO_CODE
     */
    public void setISO_CODE(java.lang.String ISO_CODE) {
        this.ISO_CODE = ISO_CODE;
    }


    /**
     * Gets the MODE_PAY_RECEIVER value for this Receipt.
     * 
     * @return MODE_PAY_RECEIVER
     */
    public java.lang.String getMODE_PAY_RECEIVER() {
        return MODE_PAY_RECEIVER;
    }


    /**
     * Sets the MODE_PAY_RECEIVER value for this Receipt.
     * 
     * @param MODE_PAY_RECEIVER
     */
    public void setMODE_PAY_RECEIVER(java.lang.String MODE_PAY_RECEIVER) {
        this.MODE_PAY_RECEIVER = MODE_PAY_RECEIVER;
    }


    /**
     * Gets the MOD_PAY_CURRENCY value for this Receipt.
     * 
     * @return MOD_PAY_CURRENCY
     */
    public java.lang.String getMOD_PAY_CURRENCY() {
        return MOD_PAY_CURRENCY;
    }


    /**
     * Sets the MOD_PAY_CURRENCY value for this Receipt.
     * 
     * @param MOD_PAY_CURRENCY
     */
    public void setMOD_PAY_CURRENCY(java.lang.String MOD_PAY_CURRENCY) {
        this.MOD_PAY_CURRENCY = MOD_PAY_CURRENCY;
    }


    /**
     * Gets the NAME_BRANCH value for this Receipt.
     * 
     * @return NAME_BRANCH
     */
    public java.lang.String getNAME_BRANCH() {
        return NAME_BRANCH;
    }


    /**
     * Sets the NAME_BRANCH value for this Receipt.
     * 
     * @param NAME_BRANCH
     */
    public void setNAME_BRANCH(java.lang.String NAME_BRANCH) {
        this.NAME_BRANCH = NAME_BRANCH;
    }


    /**
     * Gets the NAME_CITY value for this Receipt.
     * 
     * @return NAME_CITY
     */
    public java.lang.String getNAME_CITY() {
        return NAME_CITY;
    }


    /**
     * Sets the NAME_CITY value for this Receipt.
     * 
     * @param NAME_CITY
     */
    public void setNAME_CITY(java.lang.String NAME_CITY) {
        this.NAME_CITY = NAME_CITY;
    }


    /**
     * Gets the NAME_COUNTRY value for this Receipt.
     * 
     * @return NAME_COUNTRY
     */
    public java.lang.String getNAME_COUNTRY() {
        return NAME_COUNTRY;
    }


    /**
     * Sets the NAME_COUNTRY value for this Receipt.
     * 
     * @param NAME_COUNTRY
     */
    public void setNAME_COUNTRY(java.lang.String NAME_COUNTRY) {
        this.NAME_COUNTRY = NAME_COUNTRY;
    }


    /**
     * Gets the NAME_COUNTRY_SENDER value for this Receipt.
     * 
     * @return NAME_COUNTRY_SENDER
     */
    public java.lang.String getNAME_COUNTRY_SENDER() {
        return NAME_COUNTRY_SENDER;
    }


    /**
     * Sets the NAME_COUNTRY_SENDER value for this Receipt.
     * 
     * @param NAME_COUNTRY_SENDER
     */
    public void setNAME_COUNTRY_SENDER(java.lang.String NAME_COUNTRY_SENDER) {
        this.NAME_COUNTRY_SENDER = NAME_COUNTRY_SENDER;
    }


    /**
     * Gets the NAME_MAIN_BRANCH value for this Receipt.
     * 
     * @return NAME_MAIN_BRANCH
     */
    public java.lang.String getNAME_MAIN_BRANCH() {
        return NAME_MAIN_BRANCH;
    }


    /**
     * Sets the NAME_MAIN_BRANCH value for this Receipt.
     * 
     * @param NAME_MAIN_BRANCH
     */
    public void setNAME_MAIN_BRANCH(java.lang.String NAME_MAIN_BRANCH) {
        this.NAME_MAIN_BRANCH = NAME_MAIN_BRANCH;
    }


    /**
     * Gets the NAME_PAYMENT value for this Receipt.
     * 
     * @return NAME_PAYMENT
     */
    public java.lang.String getNAME_PAYMENT() {
        return NAME_PAYMENT;
    }


    /**
     * Sets the NAME_PAYMENT value for this Receipt.
     * 
     * @param NAME_PAYMENT
     */
    public void setNAME_PAYMENT(java.lang.String NAME_PAYMENT) {
        this.NAME_PAYMENT = NAME_PAYMENT;
    }


    /**
     * Gets the NAME_RECEIVER value for this Receipt.
     * 
     * @return NAME_RECEIVER
     */
    public java.lang.String getNAME_RECEIVER() {
        return NAME_RECEIVER;
    }


    /**
     * Sets the NAME_RECEIVER value for this Receipt.
     * 
     * @param NAME_RECEIVER
     */
    public void setNAME_RECEIVER(java.lang.String NAME_RECEIVER) {
        this.NAME_RECEIVER = NAME_RECEIVER;
    }


    /**
     * Gets the NAME_SENDER value for this Receipt.
     * 
     * @return NAME_SENDER
     */
    public java.lang.String getNAME_SENDER() {
        return NAME_SENDER;
    }


    /**
     * Sets the NAME_SENDER value for this Receipt.
     * 
     * @param NAME_SENDER
     */
    public void setNAME_SENDER(java.lang.String NAME_SENDER) {
        this.NAME_SENDER = NAME_SENDER;
    }


    /**
     * Gets the NAME_STATE_RECEIVER value for this Receipt.
     * 
     * @return NAME_STATE_RECEIVER
     */
    public java.lang.String getNAME_STATE_RECEIVER() {
        return NAME_STATE_RECEIVER;
    }


    /**
     * Sets the NAME_STATE_RECEIVER value for this Receipt.
     * 
     * @param NAME_STATE_RECEIVER
     */
    public void setNAME_STATE_RECEIVER(java.lang.String NAME_STATE_RECEIVER) {
        this.NAME_STATE_RECEIVER = NAME_STATE_RECEIVER;
    }


    /**
     * Gets the NET_AMOUNT_RECEIVER value for this Receipt.
     * 
     * @return NET_AMOUNT_RECEIVER
     */
    public java.math.BigDecimal getNET_AMOUNT_RECEIVER() {
        return NET_AMOUNT_RECEIVER;
    }


    /**
     * Sets the NET_AMOUNT_RECEIVER value for this Receipt.
     * 
     * @param NET_AMOUNT_RECEIVER
     */
    public void setNET_AMOUNT_RECEIVER(java.math.BigDecimal NET_AMOUNT_RECEIVER) {
        this.NET_AMOUNT_RECEIVER = NET_AMOUNT_RECEIVER;
    }


    /**
     * Gets the PAY_TIME value for this Receipt.
     * 
     * @return PAY_TIME
     */
    public java.lang.Integer getPAY_TIME() {
        return PAY_TIME;
    }


    /**
     * Sets the PAY_TIME value for this Receipt.
     * 
     * @param PAY_TIME
     */
    public void setPAY_TIME(java.lang.Integer PAY_TIME) {
        this.PAY_TIME = PAY_TIME;
    }


    /**
     * Gets the PAY_TIME_WKD value for this Receipt.
     * 
     * @return PAY_TIME_WKD
     */
    public java.lang.Integer getPAY_TIME_WKD() {
        return PAY_TIME_WKD;
    }


    /**
     * Sets the PAY_TIME_WKD value for this Receipt.
     * 
     * @param PAY_TIME_WKD
     */
    public void setPAY_TIME_WKD(java.lang.Integer PAY_TIME_WKD) {
        this.PAY_TIME_WKD = PAY_TIME_WKD;
    }


    /**
     * Gets the PHONE1_RECEIVER value for this Receipt.
     * 
     * @return PHONE1_RECEIVER
     */
    public java.lang.String getPHONE1_RECEIVER() {
        return PHONE1_RECEIVER;
    }


    /**
     * Sets the PHONE1_RECEIVER value for this Receipt.
     * 
     * @param PHONE1_RECEIVER
     */
    public void setPHONE1_RECEIVER(java.lang.String PHONE1_RECEIVER) {
        this.PHONE1_RECEIVER = PHONE1_RECEIVER;
    }


    /**
     * Gets the PHONE1_SENDER value for this Receipt.
     * 
     * @return PHONE1_SENDER
     */
    public java.math.BigDecimal getPHONE1_SENDER() {
        return PHONE1_SENDER;
    }


    /**
     * Sets the PHONE1_SENDER value for this Receipt.
     * 
     * @param PHONE1_SENDER
     */
    public void setPHONE1_SENDER(java.math.BigDecimal PHONE1_SENDER) {
        this.PHONE1_SENDER = PHONE1_SENDER;
    }


    /**
     * Gets the PHONE2_RECEIVER value for this Receipt.
     * 
     * @return PHONE2_RECEIVER
     */
    public java.lang.String getPHONE2_RECEIVER() {
        return PHONE2_RECEIVER;
    }


    /**
     * Sets the PHONE2_RECEIVER value for this Receipt.
     * 
     * @param PHONE2_RECEIVER
     */
    public void setPHONE2_RECEIVER(java.lang.String PHONE2_RECEIVER) {
        this.PHONE2_RECEIVER = PHONE2_RECEIVER;
    }


    /**
     * Gets the PHONE2_SENDER value for this Receipt.
     * 
     * @return PHONE2_SENDER
     */
    public java.math.BigDecimal getPHONE2_SENDER() {
        return PHONE2_SENDER;
    }


    /**
     * Sets the PHONE2_SENDER value for this Receipt.
     * 
     * @param PHONE2_SENDER
     */
    public void setPHONE2_SENDER(java.math.BigDecimal PHONE2_SENDER) {
        this.PHONE2_SENDER = PHONE2_SENDER;
    }


    /**
     * Gets the RATE_CHANGE_RECEIVER value for this Receipt.
     * 
     * @return RATE_CHANGE_RECEIVER
     */
    public java.math.BigDecimal getRATE_CHANGE_RECEIVER() {
        return RATE_CHANGE_RECEIVER;
    }


    /**
     * Sets the RATE_CHANGE_RECEIVER value for this Receipt.
     * 
     * @param RATE_CHANGE_RECEIVER
     */
    public void setRATE_CHANGE_RECEIVER(java.math.BigDecimal RATE_CHANGE_RECEIVER) {
        this.RATE_CHANGE_RECEIVER = RATE_CHANGE_RECEIVER;
    }


    /**
     * Gets the RECEIPT_DISCLAIMER value for this Receipt.
     * 
     * @return RECEIPT_DISCLAIMER
     */
    public java.lang.String getRECEIPT_DISCLAIMER() {
        return RECEIPT_DISCLAIMER;
    }


    /**
     * Sets the RECEIPT_DISCLAIMER value for this Receipt.
     * 
     * @param RECEIPT_DISCLAIMER
     */
    public void setRECEIPT_DISCLAIMER(java.lang.String RECEIPT_DISCLAIMER) {
        this.RECEIPT_DISCLAIMER = RECEIPT_DISCLAIMER;
    }


    /**
     * Gets the RECEIVER_DATE_AVAILABLE value for this Receipt.
     * 
     * @return RECEIVER_DATE_AVAILABLE
     */
    public java.lang.String getRECEIVER_DATE_AVAILABLE() {
        return RECEIVER_DATE_AVAILABLE;
    }


    /**
     * Sets the RECEIVER_DATE_AVAILABLE value for this Receipt.
     * 
     * @param RECEIVER_DATE_AVAILABLE
     */
    public void setRECEIVER_DATE_AVAILABLE(java.lang.String RECEIVER_DATE_AVAILABLE) {
        this.RECEIVER_DATE_AVAILABLE = RECEIVER_DATE_AVAILABLE;
    }


    /**
     * Gets the STATE_SENDER value for this Receipt.
     * 
     * @return STATE_SENDER
     */
    public java.lang.String getSTATE_SENDER() {
        return STATE_SENDER;
    }


    /**
     * Sets the STATE_SENDER value for this Receipt.
     * 
     * @param STATE_SENDER
     */
    public void setSTATE_SENDER(java.lang.String STATE_SENDER) {
        this.STATE_SENDER = STATE_SENDER;
    }


    /**
     * Gets the TAX_PERCENTAGE value for this Receipt.
     * 
     * @return TAX_PERCENTAGE
     */
    public java.lang.String getTAX_PERCENTAGE() {
        return TAX_PERCENTAGE;
    }


    /**
     * Sets the TAX_PERCENTAGE value for this Receipt.
     * 
     * @param TAX_PERCENTAGE
     */
    public void setTAX_PERCENTAGE(java.lang.String TAX_PERCENTAGE) {
        this.TAX_PERCENTAGE = TAX_PERCENTAGE;
    }


    /**
     * Gets the TELEX_RECEIVER value for this Receipt.
     * 
     * @return TELEX_RECEIVER
     */
    public java.math.BigDecimal getTELEX_RECEIVER() {
        return TELEX_RECEIVER;
    }


    /**
     * Sets the TELEX_RECEIVER value for this Receipt.
     * 
     * @param TELEX_RECEIVER
     */
    public void setTELEX_RECEIVER(java.math.BigDecimal TELEX_RECEIVER) {
        this.TELEX_RECEIVER = TELEX_RECEIVER;
    }


    /**
     * Gets the TOTAL_PAY_RECEIVER value for this Receipt.
     * 
     * @return TOTAL_PAY_RECEIVER
     */
    public java.math.BigDecimal getTOTAL_PAY_RECEIVER() {
        return TOTAL_PAY_RECEIVER;
    }


    /**
     * Sets the TOTAL_PAY_RECEIVER value for this Receipt.
     * 
     * @param TOTAL_PAY_RECEIVER
     */
    public void setTOTAL_PAY_RECEIVER(java.math.BigDecimal TOTAL_PAY_RECEIVER) {
        this.TOTAL_PAY_RECEIVER = TOTAL_PAY_RECEIVER;
    }


    /**
     * Gets the TOTAL_RECEIVER value for this Receipt.
     * 
     * @return TOTAL_RECEIVER
     */
    public java.math.BigDecimal getTOTAL_RECEIVER() {
        return TOTAL_RECEIVER;
    }


    /**
     * Sets the TOTAL_RECEIVER value for this Receipt.
     * 
     * @param TOTAL_RECEIVER
     */
    public void setTOTAL_RECEIVER(java.math.BigDecimal TOTAL_RECEIVER) {
        this.TOTAL_RECEIVER = TOTAL_RECEIVER;
    }


    /**
     * Gets the TRANSFER_TAXES value for this Receipt.
     * 
     * @return TRANSFER_TAXES
     */
    public java.math.BigDecimal getTRANSFER_TAXES() {
        return TRANSFER_TAXES;
    }


    /**
     * Sets the TRANSFER_TAXES value for this Receipt.
     * 
     * @param TRANSFER_TAXES
     */
    public void setTRANSFER_TAXES(java.math.BigDecimal TRANSFER_TAXES) {
        this.TRANSFER_TAXES = TRANSFER_TAXES;
    }


    /**
     * Gets the ZIP_RECEIVER value for this Receipt.
     * 
     * @return ZIP_RECEIVER
     */
    public java.lang.String getZIP_RECEIVER() {
        return ZIP_RECEIVER;
    }


    /**
     * Sets the ZIP_RECEIVER value for this Receipt.
     * 
     * @param ZIP_RECEIVER
     */
    public void setZIP_RECEIVER(java.lang.String ZIP_RECEIVER) {
        this.ZIP_RECEIVER = ZIP_RECEIVER;
    }


    /**
     * Gets the ZIP_SENDER value for this Receipt.
     * 
     * @return ZIP_SENDER
     */
    public java.math.BigDecimal getZIP_SENDER() {
        return ZIP_SENDER;
    }


    /**
     * Sets the ZIP_SENDER value for this Receipt.
     * 
     * @param ZIP_SENDER
     */
    public void setZIP_SENDER(java.math.BigDecimal ZIP_SENDER) {
        this.ZIP_SENDER = ZIP_SENDER;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Receipt)) return false;
        Receipt other = (Receipt) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ACC_RECEIVER==null && other.getACC_RECEIVER()==null) || 
             (this.ACC_RECEIVER!=null &&
              this.ACC_RECEIVER.equals(other.getACC_RECEIVER()))) &&
            ((this.ACH_SENDER==null && other.getACH_SENDER()==null) || 
             (this.ACH_SENDER!=null &&
              this.ACH_SENDER.equals(other.getACH_SENDER()))) &&
            ((this.ADDRESS_BRANCH==null && other.getADDRESS_BRANCH()==null) || 
             (this.ADDRESS_BRANCH!=null &&
              this.ADDRESS_BRANCH.equals(other.getADDRESS_BRANCH()))) &&
            ((this.ADDRESS_RECEIVER==null && other.getADDRESS_RECEIVER()==null) || 
             (this.ADDRESS_RECEIVER!=null &&
              this.ADDRESS_RECEIVER.equals(other.getADDRESS_RECEIVER()))) &&
            ((this.ADDRES_SENDER==null && other.getADDRES_SENDER()==null) || 
             (this.ADDRES_SENDER!=null &&
              this.ADDRES_SENDER.equals(other.getADDRES_SENDER()))) &&
            ((this.BANK_RECEIVER==null && other.getBANK_RECEIVER()==null) || 
             (this.BANK_RECEIVER!=null &&
              this.BANK_RECEIVER.equals(other.getBANK_RECEIVER()))) &&
            ((this.BRANCH_CITY_NAME==null && other.getBRANCH_CITY_NAME()==null) || 
             (this.BRANCH_CITY_NAME!=null &&
              this.BRANCH_CITY_NAME.equals(other.getBRANCH_CITY_NAME()))) &&
            ((this.BRANCH_PAY_RECEIVER==null && other.getBRANCH_PAY_RECEIVER()==null) || 
             (this.BRANCH_PAY_RECEIVER!=null &&
              this.BRANCH_PAY_RECEIVER.equals(other.getBRANCH_PAY_RECEIVER()))) &&
            ((this.BUSINESS_HOURS==null && other.getBUSINESS_HOURS()==null) || 
             (this.BUSINESS_HOURS!=null &&
              this.BUSINESS_HOURS.equals(other.getBUSINESS_HOURS()))) &&
            ((this.CARD_TRANID==null && other.getCARD_TRANID()==null) || 
             (this.CARD_TRANID!=null &&
              this.CARD_TRANID.equals(other.getCARD_TRANID()))) &&
            ((this.CITY_SENDER==null && other.getCITY_SENDER()==null) || 
             (this.CITY_SENDER!=null &&
              this.CITY_SENDER.equals(other.getCITY_SENDER()))) &&
            ((this.CLAVE_RECEIVER==null && other.getCLAVE_RECEIVER()==null) || 
             (this.CLAVE_RECEIVER!=null &&
              this.CLAVE_RECEIVER.equals(other.getCLAVE_RECEIVER()))) &&
            ((this.CURRENCY_SRC==null && other.getCURRENCY_SRC()==null) || 
             (this.CURRENCY_SRC!=null &&
              this.CURRENCY_SRC.equals(other.getCURRENCY_SRC()))) &&
            ((this.DATE_RECEIVER==null && other.getDATE_RECEIVER()==null) || 
             (this.DATE_RECEIVER!=null &&
              this.DATE_RECEIVER.equals(other.getDATE_RECEIVER()))) &&
            ((this.EXPL_TEXT==null && other.getEXPL_TEXT()==null) || 
             (this.EXPL_TEXT!=null &&
              this.EXPL_TEXT.equals(other.getEXPL_TEXT()))) &&
            ((this.ID_BRANCH==null && other.getID_BRANCH()==null) || 
             (this.ID_BRANCH!=null &&
              this.ID_BRANCH.equals(other.getID_BRANCH()))) &&
            ((this.ID_CASHIER==null && other.getID_CASHIER()==null) || 
             (this.ID_CASHIER!=null &&
              this.ID_CASHIER.equals(other.getID_CASHIER()))) &&
            ((this.ID_COMPANY==null && other.getID_COMPANY()==null) || 
             (this.ID_COMPANY!=null &&
              this.ID_COMPANY.equals(other.getID_COMPANY()))) &&
            ((this.ID_FLAG_RECEIVER==null && other.getID_FLAG_RECEIVER()==null) || 
             (this.ID_FLAG_RECEIVER!=null &&
              this.ID_FLAG_RECEIVER.equals(other.getID_FLAG_RECEIVER()))) &&
            ((this.ID_PAYMENT==null && other.getID_PAYMENT()==null) || 
             (this.ID_PAYMENT!=null &&
              this.ID_PAYMENT.equals(other.getID_PAYMENT()))) &&
            ((this.ID_RECEIVER==null && other.getID_RECEIVER()==null) || 
             (this.ID_RECEIVER!=null &&
              this.ID_RECEIVER.equals(other.getID_RECEIVER()))) &&
            ((this.ID_SENDER==null && other.getID_SENDER()==null) || 
             (this.ID_SENDER!=null &&
              this.ID_SENDER.equals(other.getID_SENDER()))) &&
            ((this.ISO_CODE==null && other.getISO_CODE()==null) || 
             (this.ISO_CODE!=null &&
              this.ISO_CODE.equals(other.getISO_CODE()))) &&
            ((this.MODE_PAY_RECEIVER==null && other.getMODE_PAY_RECEIVER()==null) || 
             (this.MODE_PAY_RECEIVER!=null &&
              this.MODE_PAY_RECEIVER.equals(other.getMODE_PAY_RECEIVER()))) &&
            ((this.MOD_PAY_CURRENCY==null && other.getMOD_PAY_CURRENCY()==null) || 
             (this.MOD_PAY_CURRENCY!=null &&
              this.MOD_PAY_CURRENCY.equals(other.getMOD_PAY_CURRENCY()))) &&
            ((this.NAME_BRANCH==null && other.getNAME_BRANCH()==null) || 
             (this.NAME_BRANCH!=null &&
              this.NAME_BRANCH.equals(other.getNAME_BRANCH()))) &&
            ((this.NAME_CITY==null && other.getNAME_CITY()==null) || 
             (this.NAME_CITY!=null &&
              this.NAME_CITY.equals(other.getNAME_CITY()))) &&
            ((this.NAME_COUNTRY==null && other.getNAME_COUNTRY()==null) || 
             (this.NAME_COUNTRY!=null &&
              this.NAME_COUNTRY.equals(other.getNAME_COUNTRY()))) &&
            ((this.NAME_COUNTRY_SENDER==null && other.getNAME_COUNTRY_SENDER()==null) || 
             (this.NAME_COUNTRY_SENDER!=null &&
              this.NAME_COUNTRY_SENDER.equals(other.getNAME_COUNTRY_SENDER()))) &&
            ((this.NAME_MAIN_BRANCH==null && other.getNAME_MAIN_BRANCH()==null) || 
             (this.NAME_MAIN_BRANCH!=null &&
              this.NAME_MAIN_BRANCH.equals(other.getNAME_MAIN_BRANCH()))) &&
            ((this.NAME_PAYMENT==null && other.getNAME_PAYMENT()==null) || 
             (this.NAME_PAYMENT!=null &&
              this.NAME_PAYMENT.equals(other.getNAME_PAYMENT()))) &&
            ((this.NAME_RECEIVER==null && other.getNAME_RECEIVER()==null) || 
             (this.NAME_RECEIVER!=null &&
              this.NAME_RECEIVER.equals(other.getNAME_RECEIVER()))) &&
            ((this.NAME_SENDER==null && other.getNAME_SENDER()==null) || 
             (this.NAME_SENDER!=null &&
              this.NAME_SENDER.equals(other.getNAME_SENDER()))) &&
            ((this.NAME_STATE_RECEIVER==null && other.getNAME_STATE_RECEIVER()==null) || 
             (this.NAME_STATE_RECEIVER!=null &&
              this.NAME_STATE_RECEIVER.equals(other.getNAME_STATE_RECEIVER()))) &&
            ((this.NET_AMOUNT_RECEIVER==null && other.getNET_AMOUNT_RECEIVER()==null) || 
             (this.NET_AMOUNT_RECEIVER!=null &&
              this.NET_AMOUNT_RECEIVER.equals(other.getNET_AMOUNT_RECEIVER()))) &&
            ((this.PAY_TIME==null && other.getPAY_TIME()==null) || 
             (this.PAY_TIME!=null &&
              this.PAY_TIME.equals(other.getPAY_TIME()))) &&
            ((this.PAY_TIME_WKD==null && other.getPAY_TIME_WKD()==null) || 
             (this.PAY_TIME_WKD!=null &&
              this.PAY_TIME_WKD.equals(other.getPAY_TIME_WKD()))) &&
            ((this.PHONE1_RECEIVER==null && other.getPHONE1_RECEIVER()==null) || 
             (this.PHONE1_RECEIVER!=null &&
              this.PHONE1_RECEIVER.equals(other.getPHONE1_RECEIVER()))) &&
            ((this.PHONE1_SENDER==null && other.getPHONE1_SENDER()==null) || 
             (this.PHONE1_SENDER!=null &&
              this.PHONE1_SENDER.equals(other.getPHONE1_SENDER()))) &&
            ((this.PHONE2_RECEIVER==null && other.getPHONE2_RECEIVER()==null) || 
             (this.PHONE2_RECEIVER!=null &&
              this.PHONE2_RECEIVER.equals(other.getPHONE2_RECEIVER()))) &&
            ((this.PHONE2_SENDER==null && other.getPHONE2_SENDER()==null) || 
             (this.PHONE2_SENDER!=null &&
              this.PHONE2_SENDER.equals(other.getPHONE2_SENDER()))) &&
            ((this.RATE_CHANGE_RECEIVER==null && other.getRATE_CHANGE_RECEIVER()==null) || 
             (this.RATE_CHANGE_RECEIVER!=null &&
              this.RATE_CHANGE_RECEIVER.equals(other.getRATE_CHANGE_RECEIVER()))) &&
            ((this.RECEIPT_DISCLAIMER==null && other.getRECEIPT_DISCLAIMER()==null) || 
             (this.RECEIPT_DISCLAIMER!=null &&
              this.RECEIPT_DISCLAIMER.equals(other.getRECEIPT_DISCLAIMER()))) &&
            ((this.RECEIVER_DATE_AVAILABLE==null && other.getRECEIVER_DATE_AVAILABLE()==null) || 
             (this.RECEIVER_DATE_AVAILABLE!=null &&
              this.RECEIVER_DATE_AVAILABLE.equals(other.getRECEIVER_DATE_AVAILABLE()))) &&
            ((this.STATE_SENDER==null && other.getSTATE_SENDER()==null) || 
             (this.STATE_SENDER!=null &&
              this.STATE_SENDER.equals(other.getSTATE_SENDER()))) &&
            ((this.TAX_PERCENTAGE==null && other.getTAX_PERCENTAGE()==null) || 
             (this.TAX_PERCENTAGE!=null &&
              this.TAX_PERCENTAGE.equals(other.getTAX_PERCENTAGE()))) &&
            ((this.TELEX_RECEIVER==null && other.getTELEX_RECEIVER()==null) || 
             (this.TELEX_RECEIVER!=null &&
              this.TELEX_RECEIVER.equals(other.getTELEX_RECEIVER()))) &&
            ((this.TOTAL_PAY_RECEIVER==null && other.getTOTAL_PAY_RECEIVER()==null) || 
             (this.TOTAL_PAY_RECEIVER!=null &&
              this.TOTAL_PAY_RECEIVER.equals(other.getTOTAL_PAY_RECEIVER()))) &&
            ((this.TOTAL_RECEIVER==null && other.getTOTAL_RECEIVER()==null) || 
             (this.TOTAL_RECEIVER!=null &&
              this.TOTAL_RECEIVER.equals(other.getTOTAL_RECEIVER()))) &&
            ((this.TRANSFER_TAXES==null && other.getTRANSFER_TAXES()==null) || 
             (this.TRANSFER_TAXES!=null &&
              this.TRANSFER_TAXES.equals(other.getTRANSFER_TAXES()))) &&
            ((this.ZIP_RECEIVER==null && other.getZIP_RECEIVER()==null) || 
             (this.ZIP_RECEIVER!=null &&
              this.ZIP_RECEIVER.equals(other.getZIP_RECEIVER()))) &&
            ((this.ZIP_SENDER==null && other.getZIP_SENDER()==null) || 
             (this.ZIP_SENDER!=null &&
              this.ZIP_SENDER.equals(other.getZIP_SENDER())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getACC_RECEIVER() != null) {
            _hashCode += getACC_RECEIVER().hashCode();
        }
        if (getACH_SENDER() != null) {
            _hashCode += getACH_SENDER().hashCode();
        }
        if (getADDRESS_BRANCH() != null) {
            _hashCode += getADDRESS_BRANCH().hashCode();
        }
        if (getADDRESS_RECEIVER() != null) {
            _hashCode += getADDRESS_RECEIVER().hashCode();
        }
        if (getADDRES_SENDER() != null) {
            _hashCode += getADDRES_SENDER().hashCode();
        }
        if (getBANK_RECEIVER() != null) {
            _hashCode += getBANK_RECEIVER().hashCode();
        }
        if (getBRANCH_CITY_NAME() != null) {
            _hashCode += getBRANCH_CITY_NAME().hashCode();
        }
        if (getBRANCH_PAY_RECEIVER() != null) {
            _hashCode += getBRANCH_PAY_RECEIVER().hashCode();
        }
        if (getBUSINESS_HOURS() != null) {
            _hashCode += getBUSINESS_HOURS().hashCode();
        }
        if (getCARD_TRANID() != null) {
            _hashCode += getCARD_TRANID().hashCode();
        }
        if (getCITY_SENDER() != null) {
            _hashCode += getCITY_SENDER().hashCode();
        }
        if (getCLAVE_RECEIVER() != null) {
            _hashCode += getCLAVE_RECEIVER().hashCode();
        }
        if (getCURRENCY_SRC() != null) {
            _hashCode += getCURRENCY_SRC().hashCode();
        }
        if (getDATE_RECEIVER() != null) {
            _hashCode += getDATE_RECEIVER().hashCode();
        }
        if (getEXPL_TEXT() != null) {
            _hashCode += getEXPL_TEXT().hashCode();
        }
        if (getID_BRANCH() != null) {
            _hashCode += getID_BRANCH().hashCode();
        }
        if (getID_CASHIER() != null) {
            _hashCode += getID_CASHIER().hashCode();
        }
        if (getID_COMPANY() != null) {
            _hashCode += getID_COMPANY().hashCode();
        }
        if (getID_FLAG_RECEIVER() != null) {
            _hashCode += getID_FLAG_RECEIVER().hashCode();
        }
        if (getID_PAYMENT() != null) {
            _hashCode += getID_PAYMENT().hashCode();
        }
        if (getID_RECEIVER() != null) {
            _hashCode += getID_RECEIVER().hashCode();
        }
        if (getID_SENDER() != null) {
            _hashCode += getID_SENDER().hashCode();
        }
        if (getISO_CODE() != null) {
            _hashCode += getISO_CODE().hashCode();
        }
        if (getMODE_PAY_RECEIVER() != null) {
            _hashCode += getMODE_PAY_RECEIVER().hashCode();
        }
        if (getMOD_PAY_CURRENCY() != null) {
            _hashCode += getMOD_PAY_CURRENCY().hashCode();
        }
        if (getNAME_BRANCH() != null) {
            _hashCode += getNAME_BRANCH().hashCode();
        }
        if (getNAME_CITY() != null) {
            _hashCode += getNAME_CITY().hashCode();
        }
        if (getNAME_COUNTRY() != null) {
            _hashCode += getNAME_COUNTRY().hashCode();
        }
        if (getNAME_COUNTRY_SENDER() != null) {
            _hashCode += getNAME_COUNTRY_SENDER().hashCode();
        }
        if (getNAME_MAIN_BRANCH() != null) {
            _hashCode += getNAME_MAIN_BRANCH().hashCode();
        }
        if (getNAME_PAYMENT() != null) {
            _hashCode += getNAME_PAYMENT().hashCode();
        }
        if (getNAME_RECEIVER() != null) {
            _hashCode += getNAME_RECEIVER().hashCode();
        }
        if (getNAME_SENDER() != null) {
            _hashCode += getNAME_SENDER().hashCode();
        }
        if (getNAME_STATE_RECEIVER() != null) {
            _hashCode += getNAME_STATE_RECEIVER().hashCode();
        }
        if (getNET_AMOUNT_RECEIVER() != null) {
            _hashCode += getNET_AMOUNT_RECEIVER().hashCode();
        }
        if (getPAY_TIME() != null) {
            _hashCode += getPAY_TIME().hashCode();
        }
        if (getPAY_TIME_WKD() != null) {
            _hashCode += getPAY_TIME_WKD().hashCode();
        }
        if (getPHONE1_RECEIVER() != null) {
            _hashCode += getPHONE1_RECEIVER().hashCode();
        }
        if (getPHONE1_SENDER() != null) {
            _hashCode += getPHONE1_SENDER().hashCode();
        }
        if (getPHONE2_RECEIVER() != null) {
            _hashCode += getPHONE2_RECEIVER().hashCode();
        }
        if (getPHONE2_SENDER() != null) {
            _hashCode += getPHONE2_SENDER().hashCode();
        }
        if (getRATE_CHANGE_RECEIVER() != null) {
            _hashCode += getRATE_CHANGE_RECEIVER().hashCode();
        }
        if (getRECEIPT_DISCLAIMER() != null) {
            _hashCode += getRECEIPT_DISCLAIMER().hashCode();
        }
        if (getRECEIVER_DATE_AVAILABLE() != null) {
            _hashCode += getRECEIVER_DATE_AVAILABLE().hashCode();
        }
        if (getSTATE_SENDER() != null) {
            _hashCode += getSTATE_SENDER().hashCode();
        }
        if (getTAX_PERCENTAGE() != null) {
            _hashCode += getTAX_PERCENTAGE().hashCode();
        }
        if (getTELEX_RECEIVER() != null) {
            _hashCode += getTELEX_RECEIVER().hashCode();
        }
        if (getTOTAL_PAY_RECEIVER() != null) {
            _hashCode += getTOTAL_PAY_RECEIVER().hashCode();
        }
        if (getTOTAL_RECEIVER() != null) {
            _hashCode += getTOTAL_RECEIVER().hashCode();
        }
        if (getTRANSFER_TAXES() != null) {
            _hashCode += getTRANSFER_TAXES().hashCode();
        }
        if (getZIP_RECEIVER() != null) {
            _hashCode += getZIP_RECEIVER().hashCode();
        }
        if (getZIP_SENDER() != null) {
            _hashCode += getZIP_SENDER().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Receipt.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Receipt"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACC_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ACC_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACH_SENDER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ACH_SENDER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESS_BRANCH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ADDRESS_BRANCH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESS_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ADDRESS_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRES_SENDER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ADDRES_SENDER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BANK_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "BANK_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BRANCH_CITY_NAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "BRANCH_CITY_NAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BRANCH_PAY_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "BRANCH_PAY_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BUSINESS_HOURS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "BUSINESS_HOURS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_TRANID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "CARD_TRANID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CITY_SENDER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "CITY_SENDER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CLAVE_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "CLAVE_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CURRENCY_SRC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "CURRENCY_SRC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATE_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "DATE_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EXPL_TEXT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "EXPL_TEXT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_BRANCH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ID_BRANCH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_CASHIER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ID_CASHIER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_COMPANY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ID_COMPANY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_FLAG_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ID_FLAG_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_PAYMENT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ID_PAYMENT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ID_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_SENDER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ID_SENDER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ISO_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ISO_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MODE_PAY_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "MODE_PAY_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MOD_PAY_CURRENCY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "MOD_PAY_CURRENCY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NAME_BRANCH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NAME_BRANCH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NAME_CITY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NAME_CITY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NAME_COUNTRY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NAME_COUNTRY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NAME_COUNTRY_SENDER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NAME_COUNTRY_SENDER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NAME_MAIN_BRANCH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NAME_MAIN_BRANCH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NAME_PAYMENT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NAME_PAYMENT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NAME_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NAME_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NAME_SENDER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NAME_SENDER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NAME_STATE_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NAME_STATE_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NET_AMOUNT_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NET_AMOUNT_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAY_TIME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PAY_TIME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PAY_TIME_WKD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PAY_TIME_WKD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PHONE1_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PHONE1_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PHONE1_SENDER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PHONE1_SENDER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PHONE2_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PHONE2_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PHONE2_SENDER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PHONE2_SENDER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RATE_CHANGE_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "RATE_CHANGE_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RECEIPT_DISCLAIMER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "RECEIPT_DISCLAIMER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("RECEIVER_DATE_AVAILABLE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "RECEIVER_DATE_AVAILABLE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STATE_SENDER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "STATE_SENDER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TAX_PERCENTAGE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "TAX_PERCENTAGE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TELEX_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "TELEX_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOTAL_PAY_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "TOTAL_PAY_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOTAL_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "TOTAL_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TRANSFER_TAXES");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "TRANSFER_TAXES"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ZIP_RECEIVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ZIP_RECEIVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ZIP_SENDER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "ZIP_SENDER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
