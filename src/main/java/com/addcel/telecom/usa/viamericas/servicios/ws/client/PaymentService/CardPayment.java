/**
 * CardPayment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService;

public class CardPayment  implements java.io.Serializable {
    private java.lang.String idSender;

    private java.lang.String idReceiver;

    private java.lang.String paymentType;

    private java.lang.String idCreditCardProfile;

    private java.lang.String cardNumber;

    private java.lang.String expDate;

    private java.lang.String CVV;

    private java.lang.String cardHolderFirstName;

    private java.lang.String cardHolderLastName;

    private java.lang.String nickName;

    public CardPayment() {
    }

    public CardPayment(
           java.lang.String idSender,
           java.lang.String idReceiver,
           java.lang.String paymentType,
           java.lang.String idCreditCardProfile,
           java.lang.String cardNumber,
           java.lang.String expDate,
           java.lang.String CVV,
           java.lang.String cardHolderFirstName,
           java.lang.String cardHolderLastName,
           java.lang.String nickName) {
           this.idSender = idSender;
           this.idReceiver = idReceiver;
           this.paymentType = paymentType;
           this.idCreditCardProfile = idCreditCardProfile;
           this.cardNumber = cardNumber;
           this.expDate = expDate;
           this.CVV = CVV;
           this.cardHolderFirstName = cardHolderFirstName;
           this.cardHolderLastName = cardHolderLastName;
           this.nickName = nickName;
    }


    /**
     * Gets the idSender value for this CardPayment.
     * 
     * @return idSender
     */
    public java.lang.String getIdSender() {
        return idSender;
    }


    /**
     * Sets the idSender value for this CardPayment.
     * 
     * @param idSender
     */
    public void setIdSender(java.lang.String idSender) {
        this.idSender = idSender;
    }


    /**
     * Gets the idReceiver value for this CardPayment.
     * 
     * @return idReceiver
     */
    public java.lang.String getIdReceiver() {
        return idReceiver;
    }


    /**
     * Sets the idReceiver value for this CardPayment.
     * 
     * @param idReceiver
     */
    public void setIdReceiver(java.lang.String idReceiver) {
        this.idReceiver = idReceiver;
    }


    /**
     * Gets the paymentType value for this CardPayment.
     * 
     * @return paymentType
     */
    public java.lang.String getPaymentType() {
        return paymentType;
    }


    /**
     * Sets the paymentType value for this CardPayment.
     * 
     * @param paymentType
     */
    public void setPaymentType(java.lang.String paymentType) {
        this.paymentType = paymentType;
    }


    /**
     * Gets the idCreditCardProfile value for this CardPayment.
     * 
     * @return idCreditCardProfile
     */
    public java.lang.String getIdCreditCardProfile() {
        return idCreditCardProfile;
    }


    /**
     * Sets the idCreditCardProfile value for this CardPayment.
     * 
     * @param idCreditCardProfile
     */
    public void setIdCreditCardProfile(java.lang.String idCreditCardProfile) {
        this.idCreditCardProfile = idCreditCardProfile;
    }


    /**
     * Gets the cardNumber value for this CardPayment.
     * 
     * @return cardNumber
     */
    public java.lang.String getCardNumber() {
        return cardNumber;
    }


    /**
     * Sets the cardNumber value for this CardPayment.
     * 
     * @param cardNumber
     */
    public void setCardNumber(java.lang.String cardNumber) {
        this.cardNumber = cardNumber;
    }


    /**
     * Gets the expDate value for this CardPayment.
     * 
     * @return expDate
     */
    public java.lang.String getExpDate() {
        return expDate;
    }


    /**
     * Sets the expDate value for this CardPayment.
     * 
     * @param expDate
     */
    public void setExpDate(java.lang.String expDate) {
        this.expDate = expDate;
    }


    /**
     * Gets the CVV value for this CardPayment.
     * 
     * @return CVV
     */
    public java.lang.String getCVV() {
        return CVV;
    }


    /**
     * Sets the CVV value for this CardPayment.
     * 
     * @param CVV
     */
    public void setCVV(java.lang.String CVV) {
        this.CVV = CVV;
    }


    /**
     * Gets the cardHolderFirstName value for this CardPayment.
     * 
     * @return cardHolderFirstName
     */
    public java.lang.String getCardHolderFirstName() {
        return cardHolderFirstName;
    }


    /**
     * Sets the cardHolderFirstName value for this CardPayment.
     * 
     * @param cardHolderFirstName
     */
    public void setCardHolderFirstName(java.lang.String cardHolderFirstName) {
        this.cardHolderFirstName = cardHolderFirstName;
    }


    /**
     * Gets the cardHolderLastName value for this CardPayment.
     * 
     * @return cardHolderLastName
     */
    public java.lang.String getCardHolderLastName() {
        return cardHolderLastName;
    }


    /**
     * Sets the cardHolderLastName value for this CardPayment.
     * 
     * @param cardHolderLastName
     */
    public void setCardHolderLastName(java.lang.String cardHolderLastName) {
        this.cardHolderLastName = cardHolderLastName;
    }


    /**
     * Gets the nickName value for this CardPayment.
     * 
     * @return nickName
     */
    public java.lang.String getNickName() {
        return nickName;
    }


    /**
     * Sets the nickName value for this CardPayment.
     * 
     * @param nickName
     */
    public void setNickName(java.lang.String nickName) {
        this.nickName = nickName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CardPayment)) return false;
        CardPayment other = (CardPayment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idSender==null && other.getIdSender()==null) || 
             (this.idSender!=null &&
              this.idSender.equals(other.getIdSender()))) &&
            ((this.idReceiver==null && other.getIdReceiver()==null) || 
             (this.idReceiver!=null &&
              this.idReceiver.equals(other.getIdReceiver()))) &&
            ((this.paymentType==null && other.getPaymentType()==null) || 
             (this.paymentType!=null &&
              this.paymentType.equals(other.getPaymentType()))) &&
            ((this.idCreditCardProfile==null && other.getIdCreditCardProfile()==null) || 
             (this.idCreditCardProfile!=null &&
              this.idCreditCardProfile.equals(other.getIdCreditCardProfile()))) &&
            ((this.cardNumber==null && other.getCardNumber()==null) || 
             (this.cardNumber!=null &&
              this.cardNumber.equals(other.getCardNumber()))) &&
            ((this.expDate==null && other.getExpDate()==null) || 
             (this.expDate!=null &&
              this.expDate.equals(other.getExpDate()))) &&
            ((this.CVV==null && other.getCVV()==null) || 
             (this.CVV!=null &&
              this.CVV.equals(other.getCVV()))) &&
            ((this.cardHolderFirstName==null && other.getCardHolderFirstName()==null) || 
             (this.cardHolderFirstName!=null &&
              this.cardHolderFirstName.equals(other.getCardHolderFirstName()))) &&
            ((this.cardHolderLastName==null && other.getCardHolderLastName()==null) || 
             (this.cardHolderLastName!=null &&
              this.cardHolderLastName.equals(other.getCardHolderLastName()))) &&
            ((this.nickName==null && other.getNickName()==null) || 
             (this.nickName!=null &&
              this.nickName.equals(other.getNickName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdSender() != null) {
            _hashCode += getIdSender().hashCode();
        }
        if (getIdReceiver() != null) {
            _hashCode += getIdReceiver().hashCode();
        }
        if (getPaymentType() != null) {
            _hashCode += getPaymentType().hashCode();
        }
        if (getIdCreditCardProfile() != null) {
            _hashCode += getIdCreditCardProfile().hashCode();
        }
        if (getCardNumber() != null) {
            _hashCode += getCardNumber().hashCode();
        }
        if (getExpDate() != null) {
            _hashCode += getExpDate().hashCode();
        }
        if (getCVV() != null) {
            _hashCode += getCVV().hashCode();
        }
        if (getCardHolderFirstName() != null) {
            _hashCode += getCardHolderFirstName().hashCode();
        }
        if (getCardHolderLastName() != null) {
            _hashCode += getCardHolderLastName().hashCode();
        }
        if (getNickName() != null) {
            _hashCode += getNickName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CardPayment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "CardPayment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "IdSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "IdReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "PaymentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCreditCardProfile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "IdCreditCardProfile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "CardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "ExpDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CVV");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "CVV"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderFirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "CardHolderFirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolderLastName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "CardHolderLastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nickName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "nickName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
