/**
 * SetNewOrder_Result.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto;

import com.addcel.telecom.usa.viamericas.servicios.model.vo.AbstractVO;

public class SetNewOrder_Result extends AbstractVO implements java.io.Serializable {
    private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.HoldMotive_Result[] holdMotives;

    private java.lang.String idBranch;

    private java.lang.String idReceiver;

    private java.lang.String idSender;

    private java.lang.String passwordReceiver;

    private java.lang.Boolean requireAdditionalInformation;
    
    private long idBitacora;

    public SetNewOrder_Result() {
    }

    public SetNewOrder_Result(
           com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.HoldMotive_Result[] holdMotives,
           java.lang.String idBranch,
           java.lang.String idReceiver,
           java.lang.String idSender,
           java.lang.String passwordReceiver,
           java.lang.Boolean requireAdditionalInformation) {
           this.holdMotives = holdMotives;
           this.idBranch = idBranch;
           this.idReceiver = idReceiver;
           this.idSender = idSender;
           this.passwordReceiver = passwordReceiver;
           this.requireAdditionalInformation = requireAdditionalInformation;
    }


    /**
     * Gets the holdMotives value for this SetNewOrder_Result.
     * 
     * @return holdMotives
     */
    public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.HoldMotive_Result[] getHoldMotives() {
        return holdMotives;
    }


    /**
     * Sets the holdMotives value for this SetNewOrder_Result.
     * 
     * @param holdMotives
     */
    public void setHoldMotives(com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.HoldMotive_Result[] holdMotives) {
        this.holdMotives = holdMotives;
    }


    /**
     * Gets the idBranch value for this SetNewOrder_Result.
     * 
     * @return idBranch
     */
    public java.lang.String getIdBranch() {
        return idBranch;
    }


    /**
     * Sets the idBranch value for this SetNewOrder_Result.
     * 
     * @param idBranch
     */
    public void setIdBranch(java.lang.String idBranch) {
        this.idBranch = idBranch;
    }


    /**
     * Gets the idReceiver value for this SetNewOrder_Result.
     * 
     * @return idReceiver
     */
    public java.lang.String getIdReceiver() {
        return idReceiver;
    }


    /**
     * Sets the idReceiver value for this SetNewOrder_Result.
     * 
     * @param idReceiver
     */
    public void setIdReceiver(java.lang.String idReceiver) {
        this.idReceiver = idReceiver;
    }


    /**
     * Gets the idSender value for this SetNewOrder_Result.
     * 
     * @return idSender
     */
    public java.lang.String getIdSender() {
        return idSender;
    }


    /**
     * Sets the idSender value for this SetNewOrder_Result.
     * 
     * @param idSender
     */
    public void setIdSender(java.lang.String idSender) {
        this.idSender = idSender;
    }


    /**
     * Gets the passwordReceiver value for this SetNewOrder_Result.
     * 
     * @return passwordReceiver
     */
    public java.lang.String getPasswordReceiver() {
        return passwordReceiver;
    }


    /**
     * Sets the passwordReceiver value for this SetNewOrder_Result.
     * 
     * @param passwordReceiver
     */
    public void setPasswordReceiver(java.lang.String passwordReceiver) {
        this.passwordReceiver = passwordReceiver;
    }


    /**
     * Gets the requireAdditionalInformation value for this SetNewOrder_Result.
     * 
     * @return requireAdditionalInformation
     */
    public java.lang.Boolean getRequireAdditionalInformation() {
        return requireAdditionalInformation;
    }


    /**
     * Sets the requireAdditionalInformation value for this SetNewOrder_Result.
     * 
     * @param requireAdditionalInformation
     */
    public void setRequireAdditionalInformation(java.lang.Boolean requireAdditionalInformation) {
        this.requireAdditionalInformation = requireAdditionalInformation;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SetNewOrder_Result)) return false;
        SetNewOrder_Result other = (SetNewOrder_Result) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.holdMotives==null && other.getHoldMotives()==null) || 
             (this.holdMotives!=null &&
              java.util.Arrays.equals(this.holdMotives, other.getHoldMotives()))) &&
            ((this.idBranch==null && other.getIdBranch()==null) || 
             (this.idBranch!=null &&
              this.idBranch.equals(other.getIdBranch()))) &&
            ((this.idReceiver==null && other.getIdReceiver()==null) || 
             (this.idReceiver!=null &&
              this.idReceiver.equals(other.getIdReceiver()))) &&
            ((this.idSender==null && other.getIdSender()==null) || 
             (this.idSender!=null &&
              this.idSender.equals(other.getIdSender()))) &&
            ((this.passwordReceiver==null && other.getPasswordReceiver()==null) || 
             (this.passwordReceiver!=null &&
              this.passwordReceiver.equals(other.getPasswordReceiver()))) &&
            ((this.requireAdditionalInformation==null && other.getRequireAdditionalInformation()==null) || 
             (this.requireAdditionalInformation!=null &&
              this.requireAdditionalInformation.equals(other.getRequireAdditionalInformation())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHoldMotives() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getHoldMotives());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getHoldMotives(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIdBranch() != null) {
            _hashCode += getIdBranch().hashCode();
        }
        if (getIdReceiver() != null) {
            _hashCode += getIdReceiver().hashCode();
        }
        if (getIdSender() != null) {
            _hashCode += getIdSender().hashCode();
        }
        if (getPasswordReceiver() != null) {
            _hashCode += getPasswordReceiver().hashCode();
        }
        if (getRequireAdditionalInformation() != null) {
            _hashCode += getRequireAdditionalInformation().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SetNewOrder_Result.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "SetNewOrder_Result"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("holdMotives");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "HoldMotives"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "HoldMotive_Result"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "HoldMotive_Result"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idBranch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdBranch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("passwordReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PasswordReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requireAdditionalInformation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "RequireAdditionalInformation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	public long getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}

}
