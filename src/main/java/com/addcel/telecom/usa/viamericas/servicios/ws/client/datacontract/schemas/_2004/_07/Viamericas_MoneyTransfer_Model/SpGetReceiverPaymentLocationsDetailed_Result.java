/**
 * SpGetReceiverPaymentLocationsDetailed_Result.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model;

public class SpGetReceiverPaymentLocationsDetailed_Result  implements java.io.Serializable {
    private java.lang.String addressPaymentLocation;

    private java.lang.String businessHours;

    private java.lang.String cashPickupAvailable;

    private java.lang.String depositToAccountAvailable;

    private java.lang.String homeDeliveryAvailable;

    private java.lang.String idCity;

    private java.lang.String idCountry;

    private java.lang.String idPaymentLocation;

    private java.lang.String latitude;

    private java.lang.String longitude;

    private java.lang.String nameCity;

    private java.lang.String nameCountry;

    private java.lang.String namePayer;

    private java.lang.String namePaymentLocation;

    private java.lang.String nameState;

    private java.lang.String paysLocalCurrency;

    private java.lang.String paysUSD;

    public SpGetReceiverPaymentLocationsDetailed_Result() {
    }

    public SpGetReceiverPaymentLocationsDetailed_Result(
           java.lang.String addressPaymentLocation,
           java.lang.String businessHours,
           java.lang.String cashPickupAvailable,
           java.lang.String depositToAccountAvailable,
           java.lang.String homeDeliveryAvailable,
           java.lang.String idCity,
           java.lang.String idCountry,
           java.lang.String idPaymentLocation,
           java.lang.String latitude,
           java.lang.String longitude,
           java.lang.String nameCity,
           java.lang.String nameCountry,
           java.lang.String namePayer,
           java.lang.String namePaymentLocation,
           java.lang.String nameState,
           java.lang.String paysLocalCurrency,
           java.lang.String paysUSD) {
           this.addressPaymentLocation = addressPaymentLocation;
           this.businessHours = businessHours;
           this.cashPickupAvailable = cashPickupAvailable;
           this.depositToAccountAvailable = depositToAccountAvailable;
           this.homeDeliveryAvailable = homeDeliveryAvailable;
           this.idCity = idCity;
           this.idCountry = idCountry;
           this.idPaymentLocation = idPaymentLocation;
           this.latitude = latitude;
           this.longitude = longitude;
           this.nameCity = nameCity;
           this.nameCountry = nameCountry;
           this.namePayer = namePayer;
           this.namePaymentLocation = namePaymentLocation;
           this.nameState = nameState;
           this.paysLocalCurrency = paysLocalCurrency;
           this.paysUSD = paysUSD;
    }


    /**
     * Gets the addressPaymentLocation value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return addressPaymentLocation
     */
    public java.lang.String getAddressPaymentLocation() {
        return addressPaymentLocation;
    }


    /**
     * Sets the addressPaymentLocation value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param addressPaymentLocation
     */
    public void setAddressPaymentLocation(java.lang.String addressPaymentLocation) {
        this.addressPaymentLocation = addressPaymentLocation;
    }


    /**
     * Gets the businessHours value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return businessHours
     */
    public java.lang.String getBusinessHours() {
        return businessHours;
    }


    /**
     * Sets the businessHours value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param businessHours
     */
    public void setBusinessHours(java.lang.String businessHours) {
        this.businessHours = businessHours;
    }


    /**
     * Gets the cashPickupAvailable value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return cashPickupAvailable
     */
    public java.lang.String getCashPickupAvailable() {
        return cashPickupAvailable;
    }


    /**
     * Sets the cashPickupAvailable value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param cashPickupAvailable
     */
    public void setCashPickupAvailable(java.lang.String cashPickupAvailable) {
        this.cashPickupAvailable = cashPickupAvailable;
    }


    /**
     * Gets the depositToAccountAvailable value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return depositToAccountAvailable
     */
    public java.lang.String getDepositToAccountAvailable() {
        return depositToAccountAvailable;
    }


    /**
     * Sets the depositToAccountAvailable value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param depositToAccountAvailable
     */
    public void setDepositToAccountAvailable(java.lang.String depositToAccountAvailable) {
        this.depositToAccountAvailable = depositToAccountAvailable;
    }


    /**
     * Gets the homeDeliveryAvailable value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return homeDeliveryAvailable
     */
    public java.lang.String getHomeDeliveryAvailable() {
        return homeDeliveryAvailable;
    }


    /**
     * Sets the homeDeliveryAvailable value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param homeDeliveryAvailable
     */
    public void setHomeDeliveryAvailable(java.lang.String homeDeliveryAvailable) {
        this.homeDeliveryAvailable = homeDeliveryAvailable;
    }


    /**
     * Gets the idCity value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return idCity
     */
    public java.lang.String getIdCity() {
        return idCity;
    }


    /**
     * Sets the idCity value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param idCity
     */
    public void setIdCity(java.lang.String idCity) {
        this.idCity = idCity;
    }


    /**
     * Gets the idCountry value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return idCountry
     */
    public java.lang.String getIdCountry() {
        return idCountry;
    }


    /**
     * Sets the idCountry value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param idCountry
     */
    public void setIdCountry(java.lang.String idCountry) {
        this.idCountry = idCountry;
    }


    /**
     * Gets the idPaymentLocation value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return idPaymentLocation
     */
    public java.lang.String getIdPaymentLocation() {
        return idPaymentLocation;
    }


    /**
     * Sets the idPaymentLocation value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param idPaymentLocation
     */
    public void setIdPaymentLocation(java.lang.String idPaymentLocation) {
        this.idPaymentLocation = idPaymentLocation;
    }


    /**
     * Gets the latitude value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return latitude
     */
    public java.lang.String getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param latitude
     */
    public void setLatitude(java.lang.String latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return longitude
     */
    public java.lang.String getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param longitude
     */
    public void setLongitude(java.lang.String longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the nameCity value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return nameCity
     */
    public java.lang.String getNameCity() {
        return nameCity;
    }


    /**
     * Sets the nameCity value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param nameCity
     */
    public void setNameCity(java.lang.String nameCity) {
        this.nameCity = nameCity;
    }


    /**
     * Gets the nameCountry value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return nameCountry
     */
    public java.lang.String getNameCountry() {
        return nameCountry;
    }


    /**
     * Sets the nameCountry value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param nameCountry
     */
    public void setNameCountry(java.lang.String nameCountry) {
        this.nameCountry = nameCountry;
    }


    /**
     * Gets the namePayer value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return namePayer
     */
    public java.lang.String getNamePayer() {
        return namePayer;
    }


    /**
     * Sets the namePayer value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param namePayer
     */
    public void setNamePayer(java.lang.String namePayer) {
        this.namePayer = namePayer;
    }


    /**
     * Gets the namePaymentLocation value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return namePaymentLocation
     */
    public java.lang.String getNamePaymentLocation() {
        return namePaymentLocation;
    }


    /**
     * Sets the namePaymentLocation value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param namePaymentLocation
     */
    public void setNamePaymentLocation(java.lang.String namePaymentLocation) {
        this.namePaymentLocation = namePaymentLocation;
    }


    /**
     * Gets the nameState value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return nameState
     */
    public java.lang.String getNameState() {
        return nameState;
    }


    /**
     * Sets the nameState value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param nameState
     */
    public void setNameState(java.lang.String nameState) {
        this.nameState = nameState;
    }


    /**
     * Gets the paysLocalCurrency value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return paysLocalCurrency
     */
    public java.lang.String getPaysLocalCurrency() {
        return paysLocalCurrency;
    }


    /**
     * Sets the paysLocalCurrency value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param paysLocalCurrency
     */
    public void setPaysLocalCurrency(java.lang.String paysLocalCurrency) {
        this.paysLocalCurrency = paysLocalCurrency;
    }


    /**
     * Gets the paysUSD value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @return paysUSD
     */
    public java.lang.String getPaysUSD() {
        return paysUSD;
    }


    /**
     * Sets the paysUSD value for this SpGetReceiverPaymentLocationsDetailed_Result.
     * 
     * @param paysUSD
     */
    public void setPaysUSD(java.lang.String paysUSD) {
        this.paysUSD = paysUSD;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SpGetReceiverPaymentLocationsDetailed_Result)) return false;
        SpGetReceiverPaymentLocationsDetailed_Result other = (SpGetReceiverPaymentLocationsDetailed_Result) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.addressPaymentLocation==null && other.getAddressPaymentLocation()==null) || 
             (this.addressPaymentLocation!=null &&
              this.addressPaymentLocation.equals(other.getAddressPaymentLocation()))) &&
            ((this.businessHours==null && other.getBusinessHours()==null) || 
             (this.businessHours!=null &&
              this.businessHours.equals(other.getBusinessHours()))) &&
            ((this.cashPickupAvailable==null && other.getCashPickupAvailable()==null) || 
             (this.cashPickupAvailable!=null &&
              this.cashPickupAvailable.equals(other.getCashPickupAvailable()))) &&
            ((this.depositToAccountAvailable==null && other.getDepositToAccountAvailable()==null) || 
             (this.depositToAccountAvailable!=null &&
              this.depositToAccountAvailable.equals(other.getDepositToAccountAvailable()))) &&
            ((this.homeDeliveryAvailable==null && other.getHomeDeliveryAvailable()==null) || 
             (this.homeDeliveryAvailable!=null &&
              this.homeDeliveryAvailable.equals(other.getHomeDeliveryAvailable()))) &&
            ((this.idCity==null && other.getIdCity()==null) || 
             (this.idCity!=null &&
              this.idCity.equals(other.getIdCity()))) &&
            ((this.idCountry==null && other.getIdCountry()==null) || 
             (this.idCountry!=null &&
              this.idCountry.equals(other.getIdCountry()))) &&
            ((this.idPaymentLocation==null && other.getIdPaymentLocation()==null) || 
             (this.idPaymentLocation!=null &&
              this.idPaymentLocation.equals(other.getIdPaymentLocation()))) &&
            ((this.latitude==null && other.getLatitude()==null) || 
             (this.latitude!=null &&
              this.latitude.equals(other.getLatitude()))) &&
            ((this.longitude==null && other.getLongitude()==null) || 
             (this.longitude!=null &&
              this.longitude.equals(other.getLongitude()))) &&
            ((this.nameCity==null && other.getNameCity()==null) || 
             (this.nameCity!=null &&
              this.nameCity.equals(other.getNameCity()))) &&
            ((this.nameCountry==null && other.getNameCountry()==null) || 
             (this.nameCountry!=null &&
              this.nameCountry.equals(other.getNameCountry()))) &&
            ((this.namePayer==null && other.getNamePayer()==null) || 
             (this.namePayer!=null &&
              this.namePayer.equals(other.getNamePayer()))) &&
            ((this.namePaymentLocation==null && other.getNamePaymentLocation()==null) || 
             (this.namePaymentLocation!=null &&
              this.namePaymentLocation.equals(other.getNamePaymentLocation()))) &&
            ((this.nameState==null && other.getNameState()==null) || 
             (this.nameState!=null &&
              this.nameState.equals(other.getNameState()))) &&
            ((this.paysLocalCurrency==null && other.getPaysLocalCurrency()==null) || 
             (this.paysLocalCurrency!=null &&
              this.paysLocalCurrency.equals(other.getPaysLocalCurrency()))) &&
            ((this.paysUSD==null && other.getPaysUSD()==null) || 
             (this.paysUSD!=null &&
              this.paysUSD.equals(other.getPaysUSD())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAddressPaymentLocation() != null) {
            _hashCode += getAddressPaymentLocation().hashCode();
        }
        if (getBusinessHours() != null) {
            _hashCode += getBusinessHours().hashCode();
        }
        if (getCashPickupAvailable() != null) {
            _hashCode += getCashPickupAvailable().hashCode();
        }
        if (getDepositToAccountAvailable() != null) {
            _hashCode += getDepositToAccountAvailable().hashCode();
        }
        if (getHomeDeliveryAvailable() != null) {
            _hashCode += getHomeDeliveryAvailable().hashCode();
        }
        if (getIdCity() != null) {
            _hashCode += getIdCity().hashCode();
        }
        if (getIdCountry() != null) {
            _hashCode += getIdCountry().hashCode();
        }
        if (getIdPaymentLocation() != null) {
            _hashCode += getIdPaymentLocation().hashCode();
        }
        if (getLatitude() != null) {
            _hashCode += getLatitude().hashCode();
        }
        if (getLongitude() != null) {
            _hashCode += getLongitude().hashCode();
        }
        if (getNameCity() != null) {
            _hashCode += getNameCity().hashCode();
        }
        if (getNameCountry() != null) {
            _hashCode += getNameCountry().hashCode();
        }
        if (getNamePayer() != null) {
            _hashCode += getNamePayer().hashCode();
        }
        if (getNamePaymentLocation() != null) {
            _hashCode += getNamePaymentLocation().hashCode();
        }
        if (getNameState() != null) {
            _hashCode += getNameState().hashCode();
        }
        if (getPaysLocalCurrency() != null) {
            _hashCode += getPaysLocalCurrency().hashCode();
        }
        if (getPaysUSD() != null) {
            _hashCode += getPaysUSD().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SpGetReceiverPaymentLocationsDetailed_Result.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "spGetReceiverPaymentLocationsDetailed_Result"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressPaymentLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "AddressPaymentLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("businessHours");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "BusinessHours"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cashPickupAvailable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "CashPickupAvailable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("depositToAccountAvailable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "DepositToAccountAvailable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("homeDeliveryAvailable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "HomeDeliveryAvailable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "IdCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCountry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "IdCountry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idPaymentLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "IdPaymentLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "Latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "Longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "NameCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameCountry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "NameCountry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("namePayer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "NamePayer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("namePaymentLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "NamePaymentLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "NameState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paysLocalCurrency");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "PaysLocalCurrency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paysUSD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "PaysUSD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
