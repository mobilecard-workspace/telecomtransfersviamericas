package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class PaymentLocations extends AbstractVO{

	private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetReceiverPaymentLocations_Result[] locations;

	public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetReceiverPaymentLocations_Result[] getLocations() {
		return locations;
	}

	public void setLocations(
			com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetReceiverPaymentLocations_Result[] locations) {
		this.locations = locations;
	}
	
}
