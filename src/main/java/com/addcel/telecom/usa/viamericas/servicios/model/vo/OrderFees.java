package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class OrderFees {

	private String idModePay;
	
	private String modeCurrency;
	
	private String netAmount;
	
	private String idBranchPay;
	
	private String idStateFrom;
	
	private String idPayment;

	public String getIdModePay() {
		return idModePay;
	}

	public void setIdModePay(String idModePay) {
		this.idModePay = idModePay;
	}

	public String getModeCurrency() {
		return modeCurrency;
	}

	public void setModeCurrency(String modeCurrency) {
		this.modeCurrency = modeCurrency;
	}

	public String getNetAmount() {
		return netAmount;
	}

	public void setNetAmount(String netAmount) {
		this.netAmount = netAmount;
	}

	public String getIdBranchPay() {
		return idBranchPay;
	}

	public void setIdBranchPay(String idBranchPay) {
		this.idBranchPay = idBranchPay;
	}

	public String getIdStateFrom() {
		return idStateFrom;
	}

	public void setIdStateFrom(String idStateFrom) {
		this.idStateFrom = idStateFrom;
	}

	public String getIdPayment() {
		return idPayment;
	}

	public void setIdPayment(String idPayment) {
		this.idPayment = idPayment;
	}
	
}
