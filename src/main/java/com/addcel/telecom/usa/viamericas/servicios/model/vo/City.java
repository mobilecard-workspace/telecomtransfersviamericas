package com.addcel.telecom.usa.viamericas.servicios.model.vo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class City {

	private String CountryId;
	
	private String StateId;
	
	private String CityId;
	
	private String CountryISO3;
	
	private String SubdivisionCode;
	
	private String ZipCode;
	
	private String City;

	public String getCountryId() {
		return CountryId;
	}

	public void setCountryId(String countryId) {
		CountryId = countryId;
	}

	public String getStateId() {
		return StateId;
	}

	public void setStateId(String stateId) {
		StateId = stateId;
	}

	public String getCityId() {
		return CityId;
	}

	public void setCityId(String cityId) {
		CityId = cityId;
	}

	public String getCountryISO3() {
		return CountryISO3;
	}

	public void setCountryISO3(String countryISO3) {
		CountryISO3 = countryISO3;
	}

	public String getSubdivisionCode() {
		return SubdivisionCode;
	}

	public void setSubdivisionCode(String subdivisionCode) {
		SubdivisionCode = subdivisionCode;
	}

	public String getZipCode() {
		return ZipCode;
	}

	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}
	
}
