/**
 * Identification.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto;

public class Identification  implements java.io.Serializable {
    private java.lang.String expirationDate;

    private java.lang.String idCountryIssuance;

    private java.lang.String idTypeId;

    private java.lang.String numberId;

    public Identification() {
    }

    public Identification(
           java.lang.String expirationDate,
           java.lang.String idCountryIssuance,
           java.lang.String idTypeId,
           java.lang.String numberId) {
           this.expirationDate = expirationDate;
           this.idCountryIssuance = idCountryIssuance;
           this.idTypeId = idTypeId;
           this.numberId = numberId;
    }


    /**
     * Gets the expirationDate value for this Identification.
     * 
     * @return expirationDate
     */
    public java.lang.String getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this Identification.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.lang.String expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the idCountryIssuance value for this Identification.
     * 
     * @return idCountryIssuance
     */
    public java.lang.String getIdCountryIssuance() {
        return idCountryIssuance;
    }


    /**
     * Sets the idCountryIssuance value for this Identification.
     * 
     * @param idCountryIssuance
     */
    public void setIdCountryIssuance(java.lang.String idCountryIssuance) {
        this.idCountryIssuance = idCountryIssuance;
    }


    /**
     * Gets the idTypeId value for this Identification.
     * 
     * @return idTypeId
     */
    public java.lang.String getIdTypeId() {
        return idTypeId;
    }


    /**
     * Sets the idTypeId value for this Identification.
     * 
     * @param idTypeId
     */
    public void setIdTypeId(java.lang.String idTypeId) {
        this.idTypeId = idTypeId;
    }


    /**
     * Gets the numberId value for this Identification.
     * 
     * @return numberId
     */
    public java.lang.String getNumberId() {
        return numberId;
    }


    /**
     * Sets the numberId value for this Identification.
     * 
     * @param numberId
     */
    public void setNumberId(java.lang.String numberId) {
        this.numberId = numberId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Identification)) return false;
        Identification other = (Identification) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.idCountryIssuance==null && other.getIdCountryIssuance()==null) || 
             (this.idCountryIssuance!=null &&
              this.idCountryIssuance.equals(other.getIdCountryIssuance()))) &&
            ((this.idTypeId==null && other.getIdTypeId()==null) || 
             (this.idTypeId!=null &&
              this.idTypeId.equals(other.getIdTypeId()))) &&
            ((this.numberId==null && other.getNumberId()==null) || 
             (this.numberId!=null &&
              this.numberId.equals(other.getNumberId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getIdCountryIssuance() != null) {
            _hashCode += getIdCountryIssuance().hashCode();
        }
        if (getIdTypeId() != null) {
            _hashCode += getIdTypeId().hashCode();
        }
        if (getNumberId() != null) {
            _hashCode += getNumberId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Identification.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "Identification"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "ExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCountryIssuance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "IdCountryIssuance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "IdTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numberId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "NumberId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
