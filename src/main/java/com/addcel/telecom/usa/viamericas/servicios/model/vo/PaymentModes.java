package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class PaymentModes extends AbstractVO{

	private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetPaymentModesbyCountry_Result[] paymentModes;

	public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetPaymentModesbyCountry_Result[] getPaymentModes() {
		return paymentModes;
	}

	public void setPaymentModes(
			com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetPaymentModesbyCountry_Result[] paymentModes) {
		this.paymentModes = paymentModes;
	}
	
}
