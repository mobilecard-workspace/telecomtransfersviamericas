package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class TransferDetails {

	private java.lang.String amount;

    private java.lang.String approvalCode;

    private java.lang.String cardNumber;

    private java.lang.String invoiceNumber;
    
    private java.lang.String responseCode;

    private java.lang.String responseMessage;

    private java.lang.String transactionCurrency;

    private java.lang.String transactionDate;

    private java.lang.String transactionId;
    
    private String dateReciever;
    
    private java.lang.String idBranch;
	
	private java.lang.String idReceiver;
	
	private java.lang.String passwordReceiver;
	
	private java.lang.String dateReceiver;
	
	private java.lang.String nameSender;
	
	private java.lang.String phone1Sender;
	
	private java.lang.String citySender;
	
	private java.lang.String stateSender;
	
	private java.lang.String zipSender;
	
	private java.lang.String nameCountrySender;
	
	private java.lang.String nameReceiver;
	
	private java.lang.String phone1Receiver;
	
	private java.lang.String addressReceiver;
	
	private java.lang.String nameCityReceiver;
	
	private java.lang.String nameCountryReceiver;
	
	private java.lang.String accReceiver;
	
	private java.lang.String companyPayer;
	
	private java.lang.String addressPayer;
	 
	private java.lang.String phone1Payer;
	
	private java.lang.String businessHours;
	
	private java.lang.String currencySrc;
	
	private java.lang.String netAmountReceiver;
	
	private java.lang.String originalFreeValue;
	
	private java.lang.String transferTaxes;
	
	private java.lang.String totalReceiver;
	
	private java.lang.String fxRateCustomer;
	
	private java.lang.String currencyPayer;
	
	private java.lang.String rateChangeReceiver;
	
	private java.lang.String totalPayReceiver;

	public java.lang.String getAmount() {
		return amount;
	}

	public void setAmount(java.lang.String amount) {
		this.amount = amount;
	}

	public java.lang.String getApprovalCode() {
		return approvalCode;
	}

	public void setApprovalCode(java.lang.String approvalCode) {
		this.approvalCode = approvalCode;
	}

	public java.lang.String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(java.lang.String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public java.lang.String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(java.lang.String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public java.lang.String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(java.lang.String responseCode) {
		this.responseCode = responseCode;
	}

	public java.lang.String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(java.lang.String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public java.lang.String getTransactionCurrency() {
		return transactionCurrency;
	}

	public void setTransactionCurrency(java.lang.String transactionCurrency) {
		this.transactionCurrency = transactionCurrency;
	}

	public java.lang.String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(java.lang.String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public java.lang.String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(java.lang.String transactionId) {
		this.transactionId = transactionId;
	}

	public String getDateReciever() {
		return dateReciever;
	}

	public void setDateReciever(String dateReciever) {
		this.dateReciever = dateReciever;
	}

	public java.lang.String getIdBranch() {
		return idBranch;
	}

	public void setIdBranch(java.lang.String idBranch) {
		this.idBranch = idBranch;
	}

	public java.lang.String getIdReceiver() {
		return idReceiver;
	}

	public void setIdReceiver(java.lang.String idReceiver) {
		this.idReceiver = idReceiver;
	}

	public java.lang.String getPasswordReceiver() {
		return passwordReceiver;
	}

	public void setPasswordReceiver(java.lang.String passwordReceiver) {
		this.passwordReceiver = passwordReceiver;
	}

	public java.lang.String getDateReceiver() {
		return dateReceiver;
	}

	public void setDateReceiver(java.lang.String dateReceiver) {
		this.dateReceiver = dateReceiver;
	}

	public java.lang.String getNameSender() {
		return nameSender;
	}

	public void setNameSender(java.lang.String nameSender) {
		this.nameSender = nameSender;
	}

	public java.lang.String getPhone1Sender() {
		return phone1Sender;
	}

	public void setPhone1Sender(java.lang.String phone1Sender) {
		this.phone1Sender = phone1Sender;
	}

	public java.lang.String getCitySender() {
		return citySender;
	}

	public void setCitySender(java.lang.String citySender) {
		this.citySender = citySender;
	}

	public java.lang.String getStateSender() {
		return stateSender;
	}

	public void setStateSender(java.lang.String stateSender) {
		this.stateSender = stateSender;
	}

	public java.lang.String getZipSender() {
		return zipSender;
	}

	public void setZipSender(java.lang.String zipSender) {
		this.zipSender = zipSender;
	}

	public java.lang.String getNameCountrySender() {
		return nameCountrySender;
	}

	public void setNameCountrySender(java.lang.String nameCountrySender) {
		this.nameCountrySender = nameCountrySender;
	}

	public java.lang.String getNameReceiver() {
		return nameReceiver;
	}

	public void setNameReceiver(java.lang.String nameReceiver) {
		this.nameReceiver = nameReceiver;
	}

	public java.lang.String getPhone1Receiver() {
		return phone1Receiver;
	}

	public void setPhone1Receiver(java.lang.String phone1Receiver) {
		this.phone1Receiver = phone1Receiver;
	}

	public java.lang.String getAddressReceiver() {
		return addressReceiver;
	}

	public void setAddressReceiver(java.lang.String addressReceiver) {
		this.addressReceiver = addressReceiver;
	}

	public java.lang.String getNameCityReceiver() {
		return nameCityReceiver;
	}

	public void setNameCityReceiver(java.lang.String nameCityReceiver) {
		this.nameCityReceiver = nameCityReceiver;
	}

	public java.lang.String getNameCountryReceiver() {
		return nameCountryReceiver;
	}

	public void setNameCountryReceiver(java.lang.String nameCountryReceiver) {
		this.nameCountryReceiver = nameCountryReceiver;
	}

	public java.lang.String getAccReceiver() {
		return accReceiver;
	}

	public void setAccReceiver(java.lang.String accReceiver) {
		this.accReceiver = accReceiver;
	}

	public java.lang.String getCompanyPayer() {
		return companyPayer;
	}

	public void setCompanyPayer(java.lang.String companyPayer) {
		this.companyPayer = companyPayer;
	}

	public java.lang.String getAddressPayer() {
		return addressPayer;
	}

	public void setAddressPayer(java.lang.String addressPayer) {
		this.addressPayer = addressPayer;
	}

	public java.lang.String getPhone1Payer() {
		return phone1Payer;
	}

	public void setPhone1Payer(java.lang.String phone1Payer) {
		this.phone1Payer = phone1Payer;
	}

	public java.lang.String getBusinessHours() {
		return businessHours;
	}

	public void setBusinessHours(java.lang.String businessHours) {
		this.businessHours = businessHours;
	}

	public java.lang.String getCurrencySrc() {
		return currencySrc;
	}

	public void setCurrencySrc(java.lang.String currencySrc) {
		this.currencySrc = currencySrc;
	}

	public java.lang.String getNetAmountReceiver() {
		return netAmountReceiver;
	}

	public void setNetAmountReceiver(java.lang.String netAmountReceiver) {
		this.netAmountReceiver = netAmountReceiver;
	}

	public java.lang.String getOriginalFreeValue() {
		return originalFreeValue;
	}

	public void setOriginalFreeValue(java.lang.String originalFreeValue) {
		this.originalFreeValue = originalFreeValue;
	}

	public java.lang.String getTransferTaxes() {
		return transferTaxes;
	}

	public void setTransferTaxes(java.lang.String transferTaxes) {
		this.transferTaxes = transferTaxes;
	}

	public java.lang.String getTotalReceiver() {
		return totalReceiver;
	}

	public void setTotalReceiver(java.lang.String totalReceiver) {
		this.totalReceiver = totalReceiver;
	}

	public java.lang.String getFxRateCustomer() {
		return fxRateCustomer;
	}

	public void setFxRateCustomer(java.lang.String fxRateCustomer) {
		this.fxRateCustomer = fxRateCustomer;
	}

	public java.lang.String getCurrencyPayer() {
		return currencyPayer;
	}

	public void setCurrencyPayer(java.lang.String currencyPayer) {
		this.currencyPayer = currencyPayer;
	}

	public java.lang.String getRateChangeReceiver() {
		return rateChangeReceiver;
	}

	public void setRateChangeReceiver(java.lang.String rateChangeReceiver) {
		this.rateChangeReceiver = rateChangeReceiver;
	}

	public java.lang.String getTotalPayReceiver() {
		return totalPayReceiver;
	}

	public void setTotalPayReceiver(java.lang.String totalPayReceiver) {
		this.totalPayReceiver = totalPayReceiver;
	}
	
}
