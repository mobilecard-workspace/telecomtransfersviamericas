/**
 * Customer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto;

public class Customer  implements java.io.Serializable {
    private java.util.Calendar birthDate;

    private java.lang.String FName;

    private java.lang.String idCountryIssuance;

    private java.lang.String idTypeId;

    private java.lang.String LName;

    private java.lang.String MName;

    private java.lang.String numberId;

    private java.lang.String SLName;

    public Customer() {
    }

    public Customer(
           java.util.Calendar birthDate,
           java.lang.String FName,
           java.lang.String idCountryIssuance,
           java.lang.String idTypeId,
           java.lang.String LName,
           java.lang.String MName,
           java.lang.String numberId,
           java.lang.String SLName) {
           this.birthDate = birthDate;
           this.FName = FName;
           this.idCountryIssuance = idCountryIssuance;
           this.idTypeId = idTypeId;
           this.LName = LName;
           this.MName = MName;
           this.numberId = numberId;
           this.SLName = SLName;
    }


    /**
     * Gets the birthDate value for this Customer.
     * 
     * @return birthDate
     */
    public java.util.Calendar getBirthDate() {
        return birthDate;
    }


    /**
     * Sets the birthDate value for this Customer.
     * 
     * @param birthDate
     */
    public void setBirthDate(java.util.Calendar birthDate) {
        this.birthDate = birthDate;
    }


    /**
     * Gets the FName value for this Customer.
     * 
     * @return FName
     */
    public java.lang.String getFName() {
        return FName;
    }


    /**
     * Sets the FName value for this Customer.
     * 
     * @param FName
     */
    public void setFName(java.lang.String FName) {
        this.FName = FName;
    }


    /**
     * Gets the idCountryIssuance value for this Customer.
     * 
     * @return idCountryIssuance
     */
    public java.lang.String getIdCountryIssuance() {
        return idCountryIssuance;
    }


    /**
     * Sets the idCountryIssuance value for this Customer.
     * 
     * @param idCountryIssuance
     */
    public void setIdCountryIssuance(java.lang.String idCountryIssuance) {
        this.idCountryIssuance = idCountryIssuance;
    }


    /**
     * Gets the idTypeId value for this Customer.
     * 
     * @return idTypeId
     */
    public java.lang.String getIdTypeId() {
        return idTypeId;
    }


    /**
     * Sets the idTypeId value for this Customer.
     * 
     * @param idTypeId
     */
    public void setIdTypeId(java.lang.String idTypeId) {
        this.idTypeId = idTypeId;
    }


    /**
     * Gets the LName value for this Customer.
     * 
     * @return LName
     */
    public java.lang.String getLName() {
        return LName;
    }


    /**
     * Sets the LName value for this Customer.
     * 
     * @param LName
     */
    public void setLName(java.lang.String LName) {
        this.LName = LName;
    }


    /**
     * Gets the MName value for this Customer.
     * 
     * @return MName
     */
    public java.lang.String getMName() {
        return MName;
    }


    /**
     * Sets the MName value for this Customer.
     * 
     * @param MName
     */
    public void setMName(java.lang.String MName) {
        this.MName = MName;
    }


    /**
     * Gets the numberId value for this Customer.
     * 
     * @return numberId
     */
    public java.lang.String getNumberId() {
        return numberId;
    }


    /**
     * Sets the numberId value for this Customer.
     * 
     * @param numberId
     */
    public void setNumberId(java.lang.String numberId) {
        this.numberId = numberId;
    }


    /**
     * Gets the SLName value for this Customer.
     * 
     * @return SLName
     */
    public java.lang.String getSLName() {
        return SLName;
    }


    /**
     * Sets the SLName value for this Customer.
     * 
     * @param SLName
     */
    public void setSLName(java.lang.String SLName) {
        this.SLName = SLName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Customer)) return false;
        Customer other = (Customer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.birthDate==null && other.getBirthDate()==null) || 
             (this.birthDate!=null &&
              this.birthDate.equals(other.getBirthDate()))) &&
            ((this.FName==null && other.getFName()==null) || 
             (this.FName!=null &&
              this.FName.equals(other.getFName()))) &&
            ((this.idCountryIssuance==null && other.getIdCountryIssuance()==null) || 
             (this.idCountryIssuance!=null &&
              this.idCountryIssuance.equals(other.getIdCountryIssuance()))) &&
            ((this.idTypeId==null && other.getIdTypeId()==null) || 
             (this.idTypeId!=null &&
              this.idTypeId.equals(other.getIdTypeId()))) &&
            ((this.LName==null && other.getLName()==null) || 
             (this.LName!=null &&
              this.LName.equals(other.getLName()))) &&
            ((this.MName==null && other.getMName()==null) || 
             (this.MName!=null &&
              this.MName.equals(other.getMName()))) &&
            ((this.numberId==null && other.getNumberId()==null) || 
             (this.numberId!=null &&
              this.numberId.equals(other.getNumberId()))) &&
            ((this.SLName==null && other.getSLName()==null) || 
             (this.SLName!=null &&
              this.SLName.equals(other.getSLName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBirthDate() != null) {
            _hashCode += getBirthDate().hashCode();
        }
        if (getFName() != null) {
            _hashCode += getFName().hashCode();
        }
        if (getIdCountryIssuance() != null) {
            _hashCode += getIdCountryIssuance().hashCode();
        }
        if (getIdTypeId() != null) {
            _hashCode += getIdTypeId().hashCode();
        }
        if (getLName() != null) {
            _hashCode += getLName().hashCode();
        }
        if (getMName() != null) {
            _hashCode += getMName().hashCode();
        }
        if (getNumberId() != null) {
            _hashCode += getNumberId().hashCode();
        }
        if (getSLName() != null) {
            _hashCode += getSLName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Customer.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "Customer"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("birthDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "BirthDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "FName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCountryIssuance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "IdCountryIssuance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "IdTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "LName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "MName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numberId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "NumberId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SLName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "SLName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
