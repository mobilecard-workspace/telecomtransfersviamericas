/**
 * CostPaymentLocationNetwork.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto;

public class CostPaymentLocationNetwork  implements java.io.Serializable {
	
	//Invalid element in 
//	com.addcel.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.CostPaymentLocationNetwork - 
//	DeliveryTime"}
	
    private java.lang.String addressBranch;

    private java.lang.String businessHours;

    private java.lang.String customerfixfee;

    private java.lang.String customerpercentageapplied;

    private java.lang.String customerpercentagefee;

    private java.lang.String idCity;

    private java.lang.String idCountry;

    private java.lang.String idModePay;

    private java.lang.String idState;

    private java.lang.String latitude;

    private java.lang.String longitude;

    private java.lang.String nameGroup;

    private java.math.BigDecimal outputAmount;

    private java.math.BigDecimal outputRate;

    private java.lang.String payerNetworkLocation;

    private java.lang.String phoneBranch;

    private java.lang.Boolean routingRequired;

    private java.lang.String viamericasfixfee;

    private java.lang.String viamericaspercentagefee;

    private java.lang.String idbranchpaymentlocation;
    
    private java.lang.String deliveryTime;
    
    private java.lang.String logoName;
    
    private java.lang.String maxAmount;
    
    private java.lang.String minAmount;
    
    private java.lang.String numberOfLocations;
    
    private java.lang.String recipientPhoneRequired;

    public CostPaymentLocationNetwork() {
    }

    public CostPaymentLocationNetwork(
           java.lang.String addressBranch,
           java.lang.String businessHours,
           java.lang.String customerfixfee,
           java.lang.String customerpercentageapplied,
           java.lang.String customerpercentagefee,
           java.lang.String idCity,
           java.lang.String idCountry,
           java.lang.String idModePay,
           java.lang.String idState,
           java.lang.String latitude,
           java.lang.String longitude,
           java.lang.String nameGroup,
           java.math.BigDecimal outputAmount,
           java.math.BigDecimal outputRate,
           java.lang.String payerNetworkLocation,
           java.lang.String phoneBranch,
           java.lang.Boolean routingRequired,
           java.lang.String viamericasfixfee,
           java.lang.String viamericaspercentagefee,
           java.lang.String idbranchpaymentlocation,
           java.lang.String deliveryTime,
           java.lang.String logoName,
           java.lang.String maxAmount,
           java.lang.String minAmount,
          java.lang.String numberOfLocations,
          String recipientPhoneRequired) {
           this.addressBranch = addressBranch;
           this.businessHours = businessHours;
           this.customerfixfee = customerfixfee;
           this.customerpercentageapplied = customerpercentageapplied;
           this.customerpercentagefee = customerpercentagefee;
           this.idCity = idCity;
           this.idCountry = idCountry;
           this.idModePay = idModePay;
           this.idState = idState;
           this.latitude = latitude;
           this.longitude = longitude;
           this.nameGroup = nameGroup;
           this.outputAmount = outputAmount;
           this.outputRate = outputRate;
           this.payerNetworkLocation = payerNetworkLocation;
           this.phoneBranch = phoneBranch;
           this.routingRequired = routingRequired;
           this.viamericasfixfee = viamericasfixfee;
           this.viamericaspercentagefee = viamericaspercentagefee;
           this.idbranchpaymentlocation = idbranchpaymentlocation;
           this.deliveryTime = deliveryTime;
           this.logoName = logoName;
           this.maxAmount = maxAmount;
           this.minAmount = minAmount;
           this.numberOfLocations = numberOfLocations;
           this.recipientPhoneRequired = recipientPhoneRequired;
    }


    /**
     * Gets the addressBranch value for this CostPaymentLocationNetwork.
     * 
     * @return addressBranch
     */
    public java.lang.String getAddressBranch() {
        return addressBranch;
    }


    /**
     * Sets the addressBranch value for this CostPaymentLocationNetwork.
     * 
     * @param addressBranch
     */
    public void setAddressBranch(java.lang.String addressBranch) {
        this.addressBranch = addressBranch;
    }


    /**
     * Gets the businessHours value for this CostPaymentLocationNetwork.
     * 
     * @return businessHours
     */
    public java.lang.String getBusinessHours() {
        return businessHours;
    }


    /**
     * Sets the businessHours value for this CostPaymentLocationNetwork.
     * 
     * @param businessHours
     */
    public void setBusinessHours(java.lang.String businessHours) {
        this.businessHours = businessHours;
    }


    /**
     * Gets the customerfixfee value for this CostPaymentLocationNetwork.
     * 
     * @return customerfixfee
     */
    public java.lang.String getCustomerfixfee() {
        return customerfixfee;
    }


    /**
     * Sets the customerfixfee value for this CostPaymentLocationNetwork.
     * 
     * @param customerfixfee
     */
    public void setCustomerfixfee(java.lang.String customerfixfee) {
        this.customerfixfee = customerfixfee;
    }


    /**
     * Gets the customerpercentageapplied value for this CostPaymentLocationNetwork.
     * 
     * @return customerpercentageapplied
     */
    public java.lang.String getCustomerpercentageapplied() {
        return customerpercentageapplied;
    }


    /**
     * Sets the customerpercentageapplied value for this CostPaymentLocationNetwork.
     * 
     * @param customerpercentageapplied
     */
    public void setCustomerpercentageapplied(java.lang.String customerpercentageapplied) {
        this.customerpercentageapplied = customerpercentageapplied;
    }


    /**
     * Gets the customerpercentagefee value for this CostPaymentLocationNetwork.
     * 
     * @return customerpercentagefee
     */
    public java.lang.String getCustomerpercentagefee() {
        return customerpercentagefee;
    }


    /**
     * Sets the customerpercentagefee value for this CostPaymentLocationNetwork.
     * 
     * @param customerpercentagefee
     */
    public void setCustomerpercentagefee(java.lang.String customerpercentagefee) {
        this.customerpercentagefee = customerpercentagefee;
    }


    /**
     * Gets the idCity value for this CostPaymentLocationNetwork.
     * 
     * @return idCity
     */
    public java.lang.String getIdCity() {
        return idCity;
    }


    /**
     * Sets the idCity value for this CostPaymentLocationNetwork.
     * 
     * @param idCity
     */
    public void setIdCity(java.lang.String idCity) {
        this.idCity = idCity;
    }


    /**
     * Gets the idCountry value for this CostPaymentLocationNetwork.
     * 
     * @return idCountry
     */
    public java.lang.String getIdCountry() {
        return idCountry;
    }


    /**
     * Sets the idCountry value for this CostPaymentLocationNetwork.
     * 
     * @param idCountry
     */
    public void setIdCountry(java.lang.String idCountry) {
        this.idCountry = idCountry;
    }


    /**
     * Gets the idModePay value for this CostPaymentLocationNetwork.
     * 
     * @return idModePay
     */
    public java.lang.String getIdModePay() {
        return idModePay;
    }


    /**
     * Sets the idModePay value for this CostPaymentLocationNetwork.
     * 
     * @param idModePay
     */
    public void setIdModePay(java.lang.String idModePay) {
        this.idModePay = idModePay;
    }


    /**
     * Gets the idState value for this CostPaymentLocationNetwork.
     * 
     * @return idState
     */
    public java.lang.String getIdState() {
        return idState;
    }


    /**
     * Sets the idState value for this CostPaymentLocationNetwork.
     * 
     * @param idState
     */
    public void setIdState(java.lang.String idState) {
        this.idState = idState;
    }


    /**
     * Gets the latitude value for this CostPaymentLocationNetwork.
     * 
     * @return latitude
     */
    public java.lang.String getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this CostPaymentLocationNetwork.
     * 
     * @param latitude
     */
    public void setLatitude(java.lang.String latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this CostPaymentLocationNetwork.
     * 
     * @return longitude
     */
    public java.lang.String getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this CostPaymentLocationNetwork.
     * 
     * @param longitude
     */
    public void setLongitude(java.lang.String longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the nameGroup value for this CostPaymentLocationNetwork.
     * 
     * @return nameGroup
     */
    public java.lang.String getNameGroup() {
        return nameGroup;
    }


    /**
     * Sets the nameGroup value for this CostPaymentLocationNetwork.
     * 
     * @param nameGroup
     */
    public void setNameGroup(java.lang.String nameGroup) {
        this.nameGroup = nameGroup;
    }


    /**
     * Gets the outputAmount value for this CostPaymentLocationNetwork.
     * 
     * @return outputAmount
     */
    public java.math.BigDecimal getOutputAmount() {
        return outputAmount;
    }


    /**
     * Sets the outputAmount value for this CostPaymentLocationNetwork.
     * 
     * @param outputAmount
     */
    public void setOutputAmount(java.math.BigDecimal outputAmount) {
        this.outputAmount = outputAmount;
    }


    /**
     * Gets the outputRate value for this CostPaymentLocationNetwork.
     * 
     * @return outputRate
     */
    public java.math.BigDecimal getOutputRate() {
        return outputRate;
    }


    /**
     * Sets the outputRate value for this CostPaymentLocationNetwork.
     * 
     * @param outputRate
     */
    public void setOutputRate(java.math.BigDecimal outputRate) {
        this.outputRate = outputRate;
    }


    /**
     * Gets the payerNetworkLocation value for this CostPaymentLocationNetwork.
     * 
     * @return payerNetworkLocation
     */
    public java.lang.String getPayerNetworkLocation() {
        return payerNetworkLocation;
    }


    /**
     * Sets the payerNetworkLocation value for this CostPaymentLocationNetwork.
     * 
     * @param payerNetworkLocation
     */
    public void setPayerNetworkLocation(java.lang.String payerNetworkLocation) {
        this.payerNetworkLocation = payerNetworkLocation;
    }


    /**
     * Gets the phoneBranch value for this CostPaymentLocationNetwork.
     * 
     * @return phoneBranch
     */
    public java.lang.String getPhoneBranch() {
        return phoneBranch;
    }


    /**
     * Sets the phoneBranch value for this CostPaymentLocationNetwork.
     * 
     * @param phoneBranch
     */
    public void setPhoneBranch(java.lang.String phoneBranch) {
        this.phoneBranch = phoneBranch;
    }


    /**
     * Gets the routingRequired value for this CostPaymentLocationNetwork.
     * 
     * @return routingRequired
     */
    public java.lang.Boolean getRoutingRequired() {
        return routingRequired;
    }


    /**
     * Sets the routingRequired value for this CostPaymentLocationNetwork.
     * 
     * @param routingRequired
     */
    public void setRoutingRequired(java.lang.Boolean routingRequired) {
        this.routingRequired = routingRequired;
    }


    /**
     * Gets the viamericasfixfee value for this CostPaymentLocationNetwork.
     * 
     * @return viamericasfixfee
     */
    public java.lang.String getViamericasfixfee() {
        return viamericasfixfee;
    }


    /**
     * Sets the viamericasfixfee value for this CostPaymentLocationNetwork.
     * 
     * @param viamericasfixfee
     */
    public void setViamericasfixfee(java.lang.String viamericasfixfee) {
        this.viamericasfixfee = viamericasfixfee;
    }


    /**
     * Gets the viamericaspercentagefee value for this CostPaymentLocationNetwork.
     * 
     * @return viamericaspercentagefee
     */
    public java.lang.String getViamericaspercentagefee() {
        return viamericaspercentagefee;
    }


    /**
     * Sets the viamericaspercentagefee value for this CostPaymentLocationNetwork.
     * 
     * @param viamericaspercentagefee
     */
    public void setViamericaspercentagefee(java.lang.String viamericaspercentagefee) {
        this.viamericaspercentagefee = viamericaspercentagefee;
    }


    /**
     * Gets the idbranchpaymentlocation value for this CostPaymentLocationNetwork.
     * 
     * @return idbranchpaymentlocation
     */
    public java.lang.String getIdbranchpaymentlocation() {
        return idbranchpaymentlocation;
    }


    /**
     * Sets the idbranchpaymentlocation value for this CostPaymentLocationNetwork.
     * 
     * @param idbranchpaymentlocation
     */
    public void setIdbranchpaymentlocation(java.lang.String idbranchpaymentlocation) {
        this.idbranchpaymentlocation = idbranchpaymentlocation;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CostPaymentLocationNetwork)) return false;
        CostPaymentLocationNetwork other = (CostPaymentLocationNetwork) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.addressBranch==null && other.getAddressBranch()==null) || 
             (this.addressBranch!=null &&
              this.addressBranch.equals(other.getAddressBranch()))) &&
            ((this.businessHours==null && other.getBusinessHours()==null) || 
             (this.businessHours!=null &&
              this.businessHours.equals(other.getBusinessHours()))) &&
            ((this.customerfixfee==null && other.getCustomerfixfee()==null) || 
             (this.customerfixfee!=null &&
              this.customerfixfee.equals(other.getCustomerfixfee()))) &&
            ((this.customerpercentageapplied==null && other.getCustomerpercentageapplied()==null) || 
             (this.customerpercentageapplied!=null &&
              this.customerpercentageapplied.equals(other.getCustomerpercentageapplied()))) &&
            ((this.customerpercentagefee==null && other.getCustomerpercentagefee()==null) || 
             (this.customerpercentagefee!=null &&
              this.customerpercentagefee.equals(other.getCustomerpercentagefee()))) &&
            ((this.idCity==null && other.getIdCity()==null) || 
             (this.idCity!=null &&
              this.idCity.equals(other.getIdCity()))) &&
            ((this.idCountry==null && other.getIdCountry()==null) || 
             (this.idCountry!=null &&
              this.idCountry.equals(other.getIdCountry()))) &&
            ((this.idModePay==null && other.getIdModePay()==null) || 
             (this.idModePay!=null &&
              this.idModePay.equals(other.getIdModePay()))) &&
            ((this.idState==null && other.getIdState()==null) || 
             (this.idState!=null &&
              this.idState.equals(other.getIdState()))) &&
            ((this.latitude==null && other.getLatitude()==null) || 
             (this.latitude!=null &&
              this.latitude.equals(other.getLatitude()))) &&
            ((this.longitude==null && other.getLongitude()==null) || 
             (this.longitude!=null &&
              this.longitude.equals(other.getLongitude()))) &&
            ((this.nameGroup==null && other.getNameGroup()==null) || 
             (this.nameGroup!=null &&
              this.nameGroup.equals(other.getNameGroup()))) &&
            ((this.outputAmount==null && other.getOutputAmount()==null) || 
             (this.outputAmount!=null &&
              this.outputAmount.equals(other.getOutputAmount()))) &&
            ((this.outputRate==null && other.getOutputRate()==null) || 
             (this.outputRate!=null &&
              this.outputRate.equals(other.getOutputRate()))) &&
            ((this.payerNetworkLocation==null && other.getPayerNetworkLocation()==null) || 
             (this.payerNetworkLocation!=null &&
              this.payerNetworkLocation.equals(other.getPayerNetworkLocation()))) &&
            ((this.phoneBranch==null && other.getPhoneBranch()==null) || 
             (this.phoneBranch!=null &&
              this.phoneBranch.equals(other.getPhoneBranch()))) &&
            ((this.routingRequired==null && other.getRoutingRequired()==null) || 
             (this.routingRequired!=null &&
              this.routingRequired.equals(other.getRoutingRequired()))) &&
            ((this.viamericasfixfee==null && other.getViamericasfixfee()==null) || 
             (this.viamericasfixfee!=null &&
              this.viamericasfixfee.equals(other.getViamericasfixfee()))) &&
            ((this.viamericaspercentagefee==null && other.getViamericaspercentagefee()==null) || 
             (this.viamericaspercentagefee!=null &&
              this.viamericaspercentagefee.equals(other.getViamericaspercentagefee()))) &&
            ((this.idbranchpaymentlocation==null && other.getIdbranchpaymentlocation()==null) || 
             (this.idbranchpaymentlocation!=null &&
              this.idbranchpaymentlocation.equals(other.getIdbranchpaymentlocation())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAddressBranch() != null) {
            _hashCode += getAddressBranch().hashCode();
        }
        if (getBusinessHours() != null) {
            _hashCode += getBusinessHours().hashCode();
        }
        if (getCustomerfixfee() != null) {
            _hashCode += getCustomerfixfee().hashCode();
        }
        if (getCustomerpercentageapplied() != null) {
            _hashCode += getCustomerpercentageapplied().hashCode();
        }
        if (getCustomerpercentagefee() != null) {
            _hashCode += getCustomerpercentagefee().hashCode();
        }
        if (getIdCity() != null) {
            _hashCode += getIdCity().hashCode();
        }
        if (getIdCountry() != null) {
            _hashCode += getIdCountry().hashCode();
        }
        if (getIdModePay() != null) {
            _hashCode += getIdModePay().hashCode();
        }
        if (getIdState() != null) {
            _hashCode += getIdState().hashCode();
        }
        if (getLatitude() != null) {
            _hashCode += getLatitude().hashCode();
        }
        if (getLongitude() != null) {
            _hashCode += getLongitude().hashCode();
        }
        if (getNameGroup() != null) {
            _hashCode += getNameGroup().hashCode();
        }
        if (getOutputAmount() != null) {
            _hashCode += getOutputAmount().hashCode();
        }
        if (getOutputRate() != null) {
            _hashCode += getOutputRate().hashCode();
        }
        if (getPayerNetworkLocation() != null) {
            _hashCode += getPayerNetworkLocation().hashCode();
        }
        if (getPhoneBranch() != null) {
            _hashCode += getPhoneBranch().hashCode();
        }
        if (getRoutingRequired() != null) {
            _hashCode += getRoutingRequired().hashCode();
        }
        if (getViamericasfixfee() != null) {
            _hashCode += getViamericasfixfee().hashCode();
        }
        if (getViamericaspercentagefee() != null) {
            _hashCode += getViamericaspercentagefee().hashCode();
        }
        if (getIdbranchpaymentlocation() != null) {
            _hashCode += getIdbranchpaymentlocation().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CostPaymentLocationNetwork.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "CostPaymentLocationNetwork"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressBranch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "AddressBranch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("businessHours");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "BusinessHours"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerfixfee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Customerfixfee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerpercentageapplied");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Customerpercentageapplied"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerpercentagefee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Customerpercentagefee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCountry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdCountry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idModePay");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdModePay"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "IdState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameGroup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NameGroup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outputAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "OutputAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outputRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "OutputRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payerNetworkLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PayerNetworkLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("phoneBranch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PhoneBranch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("routingRequired");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "RoutingRequired"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("viamericasfixfee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Viamericasfixfee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("viamericaspercentagefee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Viamericaspercentagefee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idbranchpaymentlocation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "idbranchpaymentlocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deliveryTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "DeliveryTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logoName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "LogoName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "MaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("minAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "MinAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numberOfLocations");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NumberOfLocations"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recipientPhoneRequired");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "RecipientPhoneRequired"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	public java.lang.String getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(java.lang.String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public java.lang.String getLogoName() {
		return logoName;
	}

	public void setLogoName(java.lang.String logoName) {
		this.logoName = logoName;
	}

	public java.lang.String getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(java.lang.String maxAmount) {
		this.maxAmount = maxAmount;
	}

	public java.lang.String getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(java.lang.String minAmount) {
		this.minAmount = minAmount;
	}

	public java.lang.String getNumberOfLocations() {
		return numberOfLocations;
	}

	public void setNumberOfLocations(java.lang.String numberOfLocations) {
		this.numberOfLocations = numberOfLocations;
	}

	public java.lang.String getRecipientPhoneRequired() {
		return recipientPhoneRequired;
	}

	public void setRecipientPhoneRequired(java.lang.String recipientPhoneRequired) {
		this.recipientPhoneRequired = recipientPhoneRequired;
	}

}
