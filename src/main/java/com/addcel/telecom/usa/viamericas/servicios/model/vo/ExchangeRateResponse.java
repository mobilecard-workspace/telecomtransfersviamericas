package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class ExchangeRateResponse extends AbstractVO{

	com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetReceiverRate_Result[] exchangeRateRes;

	public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetReceiverRate_Result[] getExchangeRateRes() {
		return exchangeRateRes;
	}

	public void setExchangeRateRes(
			com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetReceiverRate_Result[] exchangeRateRes) {
		this.exchangeRateRes = exchangeRateRes;
	}
	
}
