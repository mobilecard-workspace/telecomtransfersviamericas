/**
 * AgencyLocation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto;

public class AgencyLocation  implements java.io.Serializable {
    private java.lang.String idBranch;

    private java.lang.String nameBranch;

    private java.math.BigDecimal distance;

    private java.lang.String status;

    private java.lang.String latituded;

    private java.lang.String longituded;

    private java.lang.String cityState;

    public AgencyLocation() {
    }

    public AgencyLocation(
           java.lang.String idBranch,
           java.lang.String nameBranch,
           java.math.BigDecimal distance,
           java.lang.String status,
           java.lang.String latituded,
           java.lang.String longituded,
           java.lang.String cityState) {
           this.idBranch = idBranch;
           this.nameBranch = nameBranch;
           this.distance = distance;
           this.status = status;
           this.latituded = latituded;
           this.longituded = longituded;
           this.cityState = cityState;
    }


    /**
     * Gets the idBranch value for this AgencyLocation.
     * 
     * @return idBranch
     */
    public java.lang.String getIdBranch() {
        return idBranch;
    }


    /**
     * Sets the idBranch value for this AgencyLocation.
     * 
     * @param idBranch
     */
    public void setIdBranch(java.lang.String idBranch) {
        this.idBranch = idBranch;
    }


    /**
     * Gets the nameBranch value for this AgencyLocation.
     * 
     * @return nameBranch
     */
    public java.lang.String getNameBranch() {
        return nameBranch;
    }


    /**
     * Sets the nameBranch value for this AgencyLocation.
     * 
     * @param nameBranch
     */
    public void setNameBranch(java.lang.String nameBranch) {
        this.nameBranch = nameBranch;
    }


    /**
     * Gets the distance value for this AgencyLocation.
     * 
     * @return distance
     */
    public java.math.BigDecimal getDistance() {
        return distance;
    }


    /**
     * Sets the distance value for this AgencyLocation.
     * 
     * @param distance
     */
    public void setDistance(java.math.BigDecimal distance) {
        this.distance = distance;
    }


    /**
     * Gets the status value for this AgencyLocation.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this AgencyLocation.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the latituded value for this AgencyLocation.
     * 
     * @return latituded
     */
    public java.lang.String getLatituded() {
        return latituded;
    }


    /**
     * Sets the latituded value for this AgencyLocation.
     * 
     * @param latituded
     */
    public void setLatituded(java.lang.String latituded) {
        this.latituded = latituded;
    }


    /**
     * Gets the longituded value for this AgencyLocation.
     * 
     * @return longituded
     */
    public java.lang.String getLongituded() {
        return longituded;
    }


    /**
     * Sets the longituded value for this AgencyLocation.
     * 
     * @param longituded
     */
    public void setLongituded(java.lang.String longituded) {
        this.longituded = longituded;
    }


    /**
     * Gets the cityState value for this AgencyLocation.
     * 
     * @return cityState
     */
    public java.lang.String getCityState() {
        return cityState;
    }


    /**
     * Sets the cityState value for this AgencyLocation.
     * 
     * @param cityState
     */
    public void setCityState(java.lang.String cityState) {
        this.cityState = cityState;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AgencyLocation)) return false;
        AgencyLocation other = (AgencyLocation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idBranch==null && other.getIdBranch()==null) || 
             (this.idBranch!=null &&
              this.idBranch.equals(other.getIdBranch()))) &&
            ((this.nameBranch==null && other.getNameBranch()==null) || 
             (this.nameBranch!=null &&
              this.nameBranch.equals(other.getNameBranch()))) &&
            ((this.distance==null && other.getDistance()==null) || 
             (this.distance!=null &&
              this.distance.equals(other.getDistance()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.latituded==null && other.getLatituded()==null) || 
             (this.latituded!=null &&
              this.latituded.equals(other.getLatituded()))) &&
            ((this.longituded==null && other.getLongituded()==null) || 
             (this.longituded!=null &&
              this.longituded.equals(other.getLongituded()))) &&
            ((this.cityState==null && other.getCityState()==null) || 
             (this.cityState!=null &&
              this.cityState.equals(other.getCityState())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdBranch() != null) {
            _hashCode += getIdBranch().hashCode();
        }
        if (getNameBranch() != null) {
            _hashCode += getNameBranch().hashCode();
        }
        if (getDistance() != null) {
            _hashCode += getDistance().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getLatituded() != null) {
            _hashCode += getLatituded().hashCode();
        }
        if (getLongituded() != null) {
            _hashCode += getLongituded().hashCode();
        }
        if (getCityState() != null) {
            _hashCode += getCityState().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AgencyLocation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "AgencyLocation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idBranch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "idBranch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameBranch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "NameBranch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("distance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Distance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latituded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Latituded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longituded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Longituded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cityState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "CityState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
