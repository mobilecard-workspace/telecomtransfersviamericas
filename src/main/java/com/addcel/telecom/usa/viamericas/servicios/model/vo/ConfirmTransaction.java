package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class ConfirmTransaction {

	private String idSender; 
	
	private String idReciever; 
	
	private String paymentType; 
	
	private String cardNumber; 
	
	private String expDate; 
	
	private String cvv; 
	
	private String cardFirstName; 
	
	private String cardLastName; 
	
	private String nickName;
	
	private long idBitacora;
	
	public String getIdSender() {
		return idSender;
	}

	public void setIdSender(String idSender) {
		this.idSender = idSender;
	}

	public String getIdReciever() {
		return idReciever;
	}

	public void setIdReciever(String idReciever) {
		this.idReciever = idReciever;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getCardFirstName() {
		return cardFirstName;
	}

	public void setCardFirstName(String cardFirstName) {
		this.cardFirstName = cardFirstName;
	}

	public String getCardLastName() {
		return cardLastName;
	}

	public void setCardLastName(String cardLastName) {
		this.cardLastName = cardLastName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public long getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}
		
}
