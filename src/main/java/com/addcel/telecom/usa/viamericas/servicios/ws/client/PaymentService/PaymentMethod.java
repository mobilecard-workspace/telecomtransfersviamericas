/**
 * PaymentMethod.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService;

public class PaymentMethod  implements java.io.Serializable {
    private java.lang.String paymentType;

    private java.math.BigDecimal serviceFixValue;

    private java.math.BigDecimal servicePercentageValue;

    private java.lang.String paymentName;

    public PaymentMethod() {
    }

    public PaymentMethod(
           java.lang.String paymentType,
           java.math.BigDecimal serviceFixValue,
           java.math.BigDecimal servicePercentageValue,
           java.lang.String paymentName) {
           this.paymentType = paymentType;
           this.serviceFixValue = serviceFixValue;
           this.servicePercentageValue = servicePercentageValue;
           this.paymentName = paymentName;
    }


    /**
     * Gets the paymentType value for this PaymentMethod.
     * 
     * @return paymentType
     */
    public java.lang.String getPaymentType() {
        return paymentType;
    }


    /**
     * Sets the paymentType value for this PaymentMethod.
     * 
     * @param paymentType
     */
    public void setPaymentType(java.lang.String paymentType) {
        this.paymentType = paymentType;
    }


    /**
     * Gets the serviceFixValue value for this PaymentMethod.
     * 
     * @return serviceFixValue
     */
    public java.math.BigDecimal getServiceFixValue() {
        return serviceFixValue;
    }


    /**
     * Sets the serviceFixValue value for this PaymentMethod.
     * 
     * @param serviceFixValue
     */
    public void setServiceFixValue(java.math.BigDecimal serviceFixValue) {
        this.serviceFixValue = serviceFixValue;
    }


    /**
     * Gets the servicePercentageValue value for this PaymentMethod.
     * 
     * @return servicePercentageValue
     */
    public java.math.BigDecimal getServicePercentageValue() {
        return servicePercentageValue;
    }


    /**
     * Sets the servicePercentageValue value for this PaymentMethod.
     * 
     * @param servicePercentageValue
     */
    public void setServicePercentageValue(java.math.BigDecimal servicePercentageValue) {
        this.servicePercentageValue = servicePercentageValue;
    }


    /**
     * Gets the paymentName value for this PaymentMethod.
     * 
     * @return paymentName
     */
    public java.lang.String getPaymentName() {
        return paymentName;
    }


    /**
     * Sets the paymentName value for this PaymentMethod.
     * 
     * @param paymentName
     */
    public void setPaymentName(java.lang.String paymentName) {
        this.paymentName = paymentName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaymentMethod)) return false;
        PaymentMethod other = (PaymentMethod) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.paymentType==null && other.getPaymentType()==null) || 
             (this.paymentType!=null &&
              this.paymentType.equals(other.getPaymentType()))) &&
            ((this.serviceFixValue==null && other.getServiceFixValue()==null) || 
             (this.serviceFixValue!=null &&
              this.serviceFixValue.equals(other.getServiceFixValue()))) &&
            ((this.servicePercentageValue==null && other.getServicePercentageValue()==null) || 
             (this.servicePercentageValue!=null &&
              this.servicePercentageValue.equals(other.getServicePercentageValue()))) &&
            ((this.paymentName==null && other.getPaymentName()==null) || 
             (this.paymentName!=null &&
              this.paymentName.equals(other.getPaymentName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPaymentType() != null) {
            _hashCode += getPaymentType().hashCode();
        }
        if (getServiceFixValue() != null) {
            _hashCode += getServiceFixValue().hashCode();
        }
        if (getServicePercentageValue() != null) {
            _hashCode += getServicePercentageValue().hashCode();
        }
        if (getPaymentName() != null) {
            _hashCode += getPaymentName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentMethod.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "PaymentMethod"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "PaymentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceFixValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "ServiceFixValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("servicePercentageValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "ServicePercentageValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "PaymentName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
