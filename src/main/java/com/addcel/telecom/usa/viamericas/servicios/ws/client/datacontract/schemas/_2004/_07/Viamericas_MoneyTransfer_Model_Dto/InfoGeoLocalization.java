/**
 * InfoGeoLocalization.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto;

public class InfoGeoLocalization  implements java.io.Serializable {
    private java.lang.String latituded;

    private java.lang.String longituded;

    private java.lang.String placeName;

    public InfoGeoLocalization() {
    }

    public InfoGeoLocalization(
           java.lang.String latituded,
           java.lang.String longituded,
           java.lang.String placeName) {
           this.latituded = latituded;
           this.longituded = longituded;
           this.placeName = placeName;
    }


    /**
     * Gets the latituded value for this InfoGeoLocalization.
     * 
     * @return latituded
     */
    public java.lang.String getLatituded() {
        return latituded;
    }


    /**
     * Sets the latituded value for this InfoGeoLocalization.
     * 
     * @param latituded
     */
    public void setLatituded(java.lang.String latituded) {
        this.latituded = latituded;
    }


    /**
     * Gets the longituded value for this InfoGeoLocalization.
     * 
     * @return longituded
     */
    public java.lang.String getLongituded() {
        return longituded;
    }


    /**
     * Sets the longituded value for this InfoGeoLocalization.
     * 
     * @param longituded
     */
    public void setLongituded(java.lang.String longituded) {
        this.longituded = longituded;
    }


    /**
     * Gets the placeName value for this InfoGeoLocalization.
     * 
     * @return placeName
     */
    public java.lang.String getPlaceName() {
        return placeName;
    }


    /**
     * Sets the placeName value for this InfoGeoLocalization.
     * 
     * @param placeName
     */
    public void setPlaceName(java.lang.String placeName) {
        this.placeName = placeName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InfoGeoLocalization)) return false;
        InfoGeoLocalization other = (InfoGeoLocalization) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.latituded==null && other.getLatituded()==null) || 
             (this.latituded!=null &&
              this.latituded.equals(other.getLatituded()))) &&
            ((this.longituded==null && other.getLongituded()==null) || 
             (this.longituded!=null &&
              this.longituded.equals(other.getLongituded()))) &&
            ((this.placeName==null && other.getPlaceName()==null) || 
             (this.placeName!=null &&
              this.placeName.equals(other.getPlaceName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLatituded() != null) {
            _hashCode += getLatituded().hashCode();
        }
        if (getLongituded() != null) {
            _hashCode += getLongituded().hashCode();
        }
        if (getPlaceName() != null) {
            _hashCode += getPlaceName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InfoGeoLocalization.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "InfoGeoLocalization"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latituded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Latituded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longituded");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Longituded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("placeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PlaceName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
