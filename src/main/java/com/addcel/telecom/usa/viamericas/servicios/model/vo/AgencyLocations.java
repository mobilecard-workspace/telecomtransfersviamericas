package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class AgencyLocations extends AbstractVO{

	private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.AgencyLocation[] agencys;

	public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.AgencyLocation[] getAgencys() {
		return agencys;
	}

	public void setAgencys(
			com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.AgencyLocation[] agencys) {
		this.agencys = agencys;
	}
	
}
