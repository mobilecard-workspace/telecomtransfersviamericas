/**
 * SpGetReceiverPaymentLocations_Result.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model;

public class SpGetReceiverPaymentLocations_Result  implements java.io.Serializable {
    private java.lang.String addressPaymentLocation;

    private java.lang.String businessHours;

    private java.lang.String idPaymentLocation;

    private java.lang.String latitude;

    private java.lang.String longitude;

    private java.lang.String namePayer;

    private java.lang.String namePaymentLocation;

    public SpGetReceiverPaymentLocations_Result() {
    }

    public SpGetReceiverPaymentLocations_Result(
           java.lang.String addressPaymentLocation,
           java.lang.String businessHours,
           java.lang.String idPaymentLocation,
           java.lang.String latitude,
           java.lang.String longitude,
           java.lang.String namePayer,
           java.lang.String namePaymentLocation) {
           this.addressPaymentLocation = addressPaymentLocation;
           this.businessHours = businessHours;
           this.idPaymentLocation = idPaymentLocation;
           this.latitude = latitude;
           this.longitude = longitude;
           this.namePayer = namePayer;
           this.namePaymentLocation = namePaymentLocation;
    }


    /**
     * Gets the addressPaymentLocation value for this SpGetReceiverPaymentLocations_Result.
     * 
     * @return addressPaymentLocation
     */
    public java.lang.String getAddressPaymentLocation() {
        return addressPaymentLocation;
    }


    /**
     * Sets the addressPaymentLocation value for this SpGetReceiverPaymentLocations_Result.
     * 
     * @param addressPaymentLocation
     */
    public void setAddressPaymentLocation(java.lang.String addressPaymentLocation) {
        this.addressPaymentLocation = addressPaymentLocation;
    }


    /**
     * Gets the businessHours value for this SpGetReceiverPaymentLocations_Result.
     * 
     * @return businessHours
     */
    public java.lang.String getBusinessHours() {
        return businessHours;
    }


    /**
     * Sets the businessHours value for this SpGetReceiverPaymentLocations_Result.
     * 
     * @param businessHours
     */
    public void setBusinessHours(java.lang.String businessHours) {
        this.businessHours = businessHours;
    }


    /**
     * Gets the idPaymentLocation value for this SpGetReceiverPaymentLocations_Result.
     * 
     * @return idPaymentLocation
     */
    public java.lang.String getIdPaymentLocation() {
        return idPaymentLocation;
    }


    /**
     * Sets the idPaymentLocation value for this SpGetReceiverPaymentLocations_Result.
     * 
     * @param idPaymentLocation
     */
    public void setIdPaymentLocation(java.lang.String idPaymentLocation) {
        this.idPaymentLocation = idPaymentLocation;
    }


    /**
     * Gets the latitude value for this SpGetReceiverPaymentLocations_Result.
     * 
     * @return latitude
     */
    public java.lang.String getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this SpGetReceiverPaymentLocations_Result.
     * 
     * @param latitude
     */
    public void setLatitude(java.lang.String latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this SpGetReceiverPaymentLocations_Result.
     * 
     * @return longitude
     */
    public java.lang.String getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this SpGetReceiverPaymentLocations_Result.
     * 
     * @param longitude
     */
    public void setLongitude(java.lang.String longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the namePayer value for this SpGetReceiverPaymentLocations_Result.
     * 
     * @return namePayer
     */
    public java.lang.String getNamePayer() {
        return namePayer;
    }


    /**
     * Sets the namePayer value for this SpGetReceiverPaymentLocations_Result.
     * 
     * @param namePayer
     */
    public void setNamePayer(java.lang.String namePayer) {
        this.namePayer = namePayer;
    }


    /**
     * Gets the namePaymentLocation value for this SpGetReceiverPaymentLocations_Result.
     * 
     * @return namePaymentLocation
     */
    public java.lang.String getNamePaymentLocation() {
        return namePaymentLocation;
    }


    /**
     * Sets the namePaymentLocation value for this SpGetReceiverPaymentLocations_Result.
     * 
     * @param namePaymentLocation
     */
    public void setNamePaymentLocation(java.lang.String namePaymentLocation) {
        this.namePaymentLocation = namePaymentLocation;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SpGetReceiverPaymentLocations_Result)) return false;
        SpGetReceiverPaymentLocations_Result other = (SpGetReceiverPaymentLocations_Result) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.addressPaymentLocation==null && other.getAddressPaymentLocation()==null) || 
             (this.addressPaymentLocation!=null &&
              this.addressPaymentLocation.equals(other.getAddressPaymentLocation()))) &&
            ((this.businessHours==null && other.getBusinessHours()==null) || 
             (this.businessHours!=null &&
              this.businessHours.equals(other.getBusinessHours()))) &&
            ((this.idPaymentLocation==null && other.getIdPaymentLocation()==null) || 
             (this.idPaymentLocation!=null &&
              this.idPaymentLocation.equals(other.getIdPaymentLocation()))) &&
            ((this.latitude==null && other.getLatitude()==null) || 
             (this.latitude!=null &&
              this.latitude.equals(other.getLatitude()))) &&
            ((this.longitude==null && other.getLongitude()==null) || 
             (this.longitude!=null &&
              this.longitude.equals(other.getLongitude()))) &&
            ((this.namePayer==null && other.getNamePayer()==null) || 
             (this.namePayer!=null &&
              this.namePayer.equals(other.getNamePayer()))) &&
            ((this.namePaymentLocation==null && other.getNamePaymentLocation()==null) || 
             (this.namePaymentLocation!=null &&
              this.namePaymentLocation.equals(other.getNamePaymentLocation())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAddressPaymentLocation() != null) {
            _hashCode += getAddressPaymentLocation().hashCode();
        }
        if (getBusinessHours() != null) {
            _hashCode += getBusinessHours().hashCode();
        }
        if (getIdPaymentLocation() != null) {
            _hashCode += getIdPaymentLocation().hashCode();
        }
        if (getLatitude() != null) {
            _hashCode += getLatitude().hashCode();
        }
        if (getLongitude() != null) {
            _hashCode += getLongitude().hashCode();
        }
        if (getNamePayer() != null) {
            _hashCode += getNamePayer().hashCode();
        }
        if (getNamePaymentLocation() != null) {
            _hashCode += getNamePaymentLocation().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SpGetReceiverPaymentLocations_Result.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "spGetReceiverPaymentLocations_Result"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressPaymentLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "AddressPaymentLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("businessHours");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "BusinessHours"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idPaymentLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "IdPaymentLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "Latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "Longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("namePayer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "NamePayer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("namePaymentLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "NamePaymentLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
