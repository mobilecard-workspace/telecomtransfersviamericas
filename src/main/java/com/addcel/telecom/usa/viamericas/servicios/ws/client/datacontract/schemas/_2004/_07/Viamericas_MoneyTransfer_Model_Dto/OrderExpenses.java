/**
 * OrderExpenses.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto;

import com.addcel.telecom.usa.viamericas.servicios.model.vo.AbstractVO;

public class OrderExpenses extends AbstractVO implements java.io.Serializable {
    private java.lang.String customerFixFee;

    private java.lang.String customerPercentageFee;

    private java.lang.String fundingFee;

    private java.lang.String stateTax;

    private java.lang.String totalFee;

    public OrderExpenses() {
    }

    public OrderExpenses(
           java.lang.String customerFixFee,
           java.lang.String customerPercentageFee,
           java.lang.String fundingFee,
           java.lang.String stateTax,
           java.lang.String totalFee) {
           this.customerFixFee = customerFixFee;
           this.customerPercentageFee = customerPercentageFee;
           this.fundingFee = fundingFee;
           this.stateTax = stateTax;
           this.totalFee = totalFee;
    }


    /**
     * Gets the customerFixFee value for this OrderExpenses.
     * 
     * @return customerFixFee
     */
    public java.lang.String getCustomerFixFee() {
        return customerFixFee;
    }


    /**
     * Sets the customerFixFee value for this OrderExpenses.
     * 
     * @param customerFixFee
     */
    public void setCustomerFixFee(java.lang.String customerFixFee) {
        this.customerFixFee = customerFixFee;
    }


    /**
     * Gets the customerPercentageFee value for this OrderExpenses.
     * 
     * @return customerPercentageFee
     */
    public java.lang.String getCustomerPercentageFee() {
        return customerPercentageFee;
    }


    /**
     * Sets the customerPercentageFee value for this OrderExpenses.
     * 
     * @param customerPercentageFee
     */
    public void setCustomerPercentageFee(java.lang.String customerPercentageFee) {
        this.customerPercentageFee = customerPercentageFee;
    }


    /**
     * Gets the fundingFee value for this OrderExpenses.
     * 
     * @return fundingFee
     */
    public java.lang.String getFundingFee() {
        return fundingFee;
    }


    /**
     * Sets the fundingFee value for this OrderExpenses.
     * 
     * @param fundingFee
     */
    public void setFundingFee(java.lang.String fundingFee) {
        this.fundingFee = fundingFee;
    }


    /**
     * Gets the stateTax value for this OrderExpenses.
     * 
     * @return stateTax
     */
    public java.lang.String getStateTax() {
        return stateTax;
    }


    /**
     * Sets the stateTax value for this OrderExpenses.
     * 
     * @param stateTax
     */
    public void setStateTax(java.lang.String stateTax) {
        this.stateTax = stateTax;
    }


    /**
     * Gets the totalFee value for this OrderExpenses.
     * 
     * @return totalFee
     */
    public java.lang.String getTotalFee() {
        return totalFee;
    }


    /**
     * Sets the totalFee value for this OrderExpenses.
     * 
     * @param totalFee
     */
    public void setTotalFee(java.lang.String totalFee) {
        this.totalFee = totalFee;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OrderExpenses)) return false;
        OrderExpenses other = (OrderExpenses) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.customerFixFee==null && other.getCustomerFixFee()==null) || 
             (this.customerFixFee!=null &&
              this.customerFixFee.equals(other.getCustomerFixFee()))) &&
            ((this.customerPercentageFee==null && other.getCustomerPercentageFee()==null) || 
             (this.customerPercentageFee!=null &&
              this.customerPercentageFee.equals(other.getCustomerPercentageFee()))) &&
            ((this.fundingFee==null && other.getFundingFee()==null) || 
             (this.fundingFee!=null &&
              this.fundingFee.equals(other.getFundingFee()))) &&
            ((this.stateTax==null && other.getStateTax()==null) || 
             (this.stateTax!=null &&
              this.stateTax.equals(other.getStateTax()))) &&
            ((this.totalFee==null && other.getTotalFee()==null) || 
             (this.totalFee!=null &&
              this.totalFee.equals(other.getTotalFee())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCustomerFixFee() != null) {
            _hashCode += getCustomerFixFee().hashCode();
        }
        if (getCustomerPercentageFee() != null) {
            _hashCode += getCustomerPercentageFee().hashCode();
        }
        if (getFundingFee() != null) {
            _hashCode += getFundingFee().hashCode();
        }
        if (getStateTax() != null) {
            _hashCode += getStateTax().hashCode();
        }
        if (getTotalFee() != null) {
            _hashCode += getTotalFee().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OrderExpenses.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "OrderExpenses"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerFixFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "CustomerFixFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerPercentageFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "CustomerPercentageFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundingFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "FundingFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stateTax");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "StateTax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalFee");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "TotalFee"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
