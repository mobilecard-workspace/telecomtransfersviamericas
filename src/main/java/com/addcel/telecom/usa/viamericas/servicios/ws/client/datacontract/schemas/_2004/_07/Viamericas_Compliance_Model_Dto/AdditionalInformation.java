/**
 * AdditionalInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto;

public class AdditionalInformation  implements java.io.Serializable {
    private java.lang.String businessAddress;

    private java.lang.String companyCity;

    private java.lang.String companyCountry;

    private java.lang.String companyState;

    private java.lang.String companyZipCode;

    private java.lang.String employerName;

    private java.lang.String employerPhoneNumber;

    private java.lang.String fundsOrigin;

    private java.lang.String idReceiver;

    private java.lang.String occupation;

    private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Identification objIdentification;

    public AdditionalInformation() {
    }

    public AdditionalInformation(
           java.lang.String businessAddress,
           java.lang.String companyCity,
           java.lang.String companyCountry,
           java.lang.String companyState,
           java.lang.String companyZipCode,
           java.lang.String employerName,
           java.lang.String employerPhoneNumber,
           java.lang.String fundsOrigin,
           java.lang.String idReceiver,
           java.lang.String occupation,
           com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Identification objIdentification) {
           this.businessAddress = businessAddress;
           this.companyCity = companyCity;
           this.companyCountry = companyCountry;
           this.companyState = companyState;
           this.companyZipCode = companyZipCode;
           this.employerName = employerName;
           this.employerPhoneNumber = employerPhoneNumber;
           this.fundsOrigin = fundsOrigin;
           this.idReceiver = idReceiver;
           this.occupation = occupation;
           this.objIdentification = objIdentification;
    }


    /**
     * Gets the businessAddress value for this AdditionalInformation.
     * 
     * @return businessAddress
     */
    public java.lang.String getBusinessAddress() {
        return businessAddress;
    }


    /**
     * Sets the businessAddress value for this AdditionalInformation.
     * 
     * @param businessAddress
     */
    public void setBusinessAddress(java.lang.String businessAddress) {
        this.businessAddress = businessAddress;
    }


    /**
     * Gets the companyCity value for this AdditionalInformation.
     * 
     * @return companyCity
     */
    public java.lang.String getCompanyCity() {
        return companyCity;
    }


    /**
     * Sets the companyCity value for this AdditionalInformation.
     * 
     * @param companyCity
     */
    public void setCompanyCity(java.lang.String companyCity) {
        this.companyCity = companyCity;
    }


    /**
     * Gets the companyCountry value for this AdditionalInformation.
     * 
     * @return companyCountry
     */
    public java.lang.String getCompanyCountry() {
        return companyCountry;
    }


    /**
     * Sets the companyCountry value for this AdditionalInformation.
     * 
     * @param companyCountry
     */
    public void setCompanyCountry(java.lang.String companyCountry) {
        this.companyCountry = companyCountry;
    }


    /**
     * Gets the companyState value for this AdditionalInformation.
     * 
     * @return companyState
     */
    public java.lang.String getCompanyState() {
        return companyState;
    }


    /**
     * Sets the companyState value for this AdditionalInformation.
     * 
     * @param companyState
     */
    public void setCompanyState(java.lang.String companyState) {
        this.companyState = companyState;
    }


    /**
     * Gets the companyZipCode value for this AdditionalInformation.
     * 
     * @return companyZipCode
     */
    public java.lang.String getCompanyZipCode() {
        return companyZipCode;
    }


    /**
     * Sets the companyZipCode value for this AdditionalInformation.
     * 
     * @param companyZipCode
     */
    public void setCompanyZipCode(java.lang.String companyZipCode) {
        this.companyZipCode = companyZipCode;
    }


    /**
     * Gets the employerName value for this AdditionalInformation.
     * 
     * @return employerName
     */
    public java.lang.String getEmployerName() {
        return employerName;
    }


    /**
     * Sets the employerName value for this AdditionalInformation.
     * 
     * @param employerName
     */
    public void setEmployerName(java.lang.String employerName) {
        this.employerName = employerName;
    }


    /**
     * Gets the employerPhoneNumber value for this AdditionalInformation.
     * 
     * @return employerPhoneNumber
     */
    public java.lang.String getEmployerPhoneNumber() {
        return employerPhoneNumber;
    }


    /**
     * Sets the employerPhoneNumber value for this AdditionalInformation.
     * 
     * @param employerPhoneNumber
     */
    public void setEmployerPhoneNumber(java.lang.String employerPhoneNumber) {
        this.employerPhoneNumber = employerPhoneNumber;
    }


    /**
     * Gets the fundsOrigin value for this AdditionalInformation.
     * 
     * @return fundsOrigin
     */
    public java.lang.String getFundsOrigin() {
        return fundsOrigin;
    }


    /**
     * Sets the fundsOrigin value for this AdditionalInformation.
     * 
     * @param fundsOrigin
     */
    public void setFundsOrigin(java.lang.String fundsOrigin) {
        this.fundsOrigin = fundsOrigin;
    }


    /**
     * Gets the idReceiver value for this AdditionalInformation.
     * 
     * @return idReceiver
     */
    public java.lang.String getIdReceiver() {
        return idReceiver;
    }


    /**
     * Sets the idReceiver value for this AdditionalInformation.
     * 
     * @param idReceiver
     */
    public void setIdReceiver(java.lang.String idReceiver) {
        this.idReceiver = idReceiver;
    }


    /**
     * Gets the occupation value for this AdditionalInformation.
     * 
     * @return occupation
     */
    public java.lang.String getOccupation() {
        return occupation;
    }


    /**
     * Sets the occupation value for this AdditionalInformation.
     * 
     * @param occupation
     */
    public void setOccupation(java.lang.String occupation) {
        this.occupation = occupation;
    }


    /**
     * Gets the objIdentification value for this AdditionalInformation.
     * 
     * @return objIdentification
     */
    public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Identification getObjIdentification() {
        return objIdentification;
    }


    /**
     * Sets the objIdentification value for this AdditionalInformation.
     * 
     * @param objIdentification
     */
    public void setObjIdentification(com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Identification objIdentification) {
        this.objIdentification = objIdentification;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AdditionalInformation)) return false;
        AdditionalInformation other = (AdditionalInformation) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.businessAddress==null && other.getBusinessAddress()==null) || 
             (this.businessAddress!=null &&
              this.businessAddress.equals(other.getBusinessAddress()))) &&
            ((this.companyCity==null && other.getCompanyCity()==null) || 
             (this.companyCity!=null &&
              this.companyCity.equals(other.getCompanyCity()))) &&
            ((this.companyCountry==null && other.getCompanyCountry()==null) || 
             (this.companyCountry!=null &&
              this.companyCountry.equals(other.getCompanyCountry()))) &&
            ((this.companyState==null && other.getCompanyState()==null) || 
             (this.companyState!=null &&
              this.companyState.equals(other.getCompanyState()))) &&
            ((this.companyZipCode==null && other.getCompanyZipCode()==null) || 
             (this.companyZipCode!=null &&
              this.companyZipCode.equals(other.getCompanyZipCode()))) &&
            ((this.employerName==null && other.getEmployerName()==null) || 
             (this.employerName!=null &&
              this.employerName.equals(other.getEmployerName()))) &&
            ((this.employerPhoneNumber==null && other.getEmployerPhoneNumber()==null) || 
             (this.employerPhoneNumber!=null &&
              this.employerPhoneNumber.equals(other.getEmployerPhoneNumber()))) &&
            ((this.fundsOrigin==null && other.getFundsOrigin()==null) || 
             (this.fundsOrigin!=null &&
              this.fundsOrigin.equals(other.getFundsOrigin()))) &&
            ((this.idReceiver==null && other.getIdReceiver()==null) || 
             (this.idReceiver!=null &&
              this.idReceiver.equals(other.getIdReceiver()))) &&
            ((this.occupation==null && other.getOccupation()==null) || 
             (this.occupation!=null &&
              this.occupation.equals(other.getOccupation()))) &&
            ((this.objIdentification==null && other.getObjIdentification()==null) || 
             (this.objIdentification!=null &&
              this.objIdentification.equals(other.getObjIdentification())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBusinessAddress() != null) {
            _hashCode += getBusinessAddress().hashCode();
        }
        if (getCompanyCity() != null) {
            _hashCode += getCompanyCity().hashCode();
        }
        if (getCompanyCountry() != null) {
            _hashCode += getCompanyCountry().hashCode();
        }
        if (getCompanyState() != null) {
            _hashCode += getCompanyState().hashCode();
        }
        if (getCompanyZipCode() != null) {
            _hashCode += getCompanyZipCode().hashCode();
        }
        if (getEmployerName() != null) {
            _hashCode += getEmployerName().hashCode();
        }
        if (getEmployerPhoneNumber() != null) {
            _hashCode += getEmployerPhoneNumber().hashCode();
        }
        if (getFundsOrigin() != null) {
            _hashCode += getFundsOrigin().hashCode();
        }
        if (getIdReceiver() != null) {
            _hashCode += getIdReceiver().hashCode();
        }
        if (getOccupation() != null) {
            _hashCode += getOccupation().hashCode();
        }
        if (getObjIdentification() != null) {
            _hashCode += getObjIdentification().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AdditionalInformation.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "AdditionalInformation"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("businessAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "BusinessAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "CompanyCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyCountry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "CompanyCountry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "CompanyState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyZipCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "CompanyZipCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("employerName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "EmployerName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("employerPhoneNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "EmployerPhoneNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fundsOrigin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "FundsOrigin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "IdReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("occupation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "Occupation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("objIdentification");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "objIdentification"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.Compliance.Model.Dto", "Identification"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
