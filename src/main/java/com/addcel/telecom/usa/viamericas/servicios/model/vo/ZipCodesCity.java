package com.addcel.telecom.usa.viamericas.servicios.model.vo;

import java.util.List;

public class ZipCodesCity extends AbstractVO {

	List<com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.City> zipCodeList;

	public List<com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.City> getZipCodeList() {
		return zipCodeList;
	}

	public void setZipCodeList(
			List<com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.City> zipCodeList) {
		this.zipCodeList = zipCodeList;
	}
	
}
