package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class Recipient {

	private String address;
	
	private String email; 
	
	private String idCity; 
	
	private String idCountry; 
	
	private String lastName; 
	
	private String idSender; 
	
	private String idState; 
	
	private String firstName;

    private java.lang.String idRecipient;


    private java.lang.String MName;

    private java.lang.String LName;

    private java.lang.String SLName;

    private java.lang.String address2;

    private java.lang.String phone1;

    private java.lang.String phone2;

    private java.lang.String nameCity;

    private java.lang.String ZIP;

    private java.lang.String idTypeId;

    private java.lang.String numberId;

    private java.lang.String birthDate;

    private java.lang.String phone1Type;

    private java.lang.String phone2Type;
    
    private java.lang.String sendSMS;
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIdCity() {
		return idCity;
	}

	public void setIdCity(String idCity) {
		this.idCity = idCity;
	}

	public String getIdCountry() {
		return idCountry;
	}

	public void setIdCountry(String idCountry) {
		this.idCountry = idCountry;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIdSender() {
		return idSender;
	}

	public void setIdSender(String idSender) {
		this.idSender = idSender;
	}

	public String getIdState() {
		return idState;
	}

	public void setIdState(String idState) {
		this.idState = idState;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public java.lang.String getIdRecipient() {
		return idRecipient;
	}

	public void setIdRecipient(java.lang.String idRecipient) {
		this.idRecipient = idRecipient;
	}

	public java.lang.String getMName() {
		return MName;
	}

	public void setMName(java.lang.String mName) {
		MName = mName;
	}

	public java.lang.String getLName() {
		return LName;
	}

	public void setLName(java.lang.String lName) {
		LName = lName;
	}

	public java.lang.String getSLName() {
		return SLName;
	}

	public void setSLName(java.lang.String sLName) {
		SLName = sLName;
	}

	public java.lang.String getAddress2() {
		return address2;
	}

	public void setAddress2(java.lang.String address2) {
		this.address2 = address2;
	}

	public java.lang.String getPhone1() {
		return phone1;
	}

	public void setPhone1(java.lang.String phone1) {
		this.phone1 = phone1;
	}

	public java.lang.String getPhone2() {
		return phone2;
	}

	public void setPhone2(java.lang.String phone2) {
		this.phone2 = phone2;
	}

	public java.lang.String getNameCity() {
		return nameCity;
	}

	public void setNameCity(java.lang.String nameCity) {
		this.nameCity = nameCity;
	}

	public java.lang.String getZIP() {
		return ZIP;
	}

	public void setZIP(java.lang.String zIP) {
		ZIP = zIP;
	}

	public java.lang.String getIdTypeId() {
		return idTypeId;
	}

	public void setIdTypeId(java.lang.String idTypeId) {
		this.idTypeId = idTypeId;
	}

	public java.lang.String getNumberId() {
		return numberId;
	}

	public void setNumberId(java.lang.String numberId) {
		this.numberId = numberId;
	}

	public java.lang.String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(java.lang.String birthDate) {
		this.birthDate = birthDate;
	}

	public java.lang.String getPhone1Type() {
		return phone1Type;
	}

	public void setPhone1Type(java.lang.String phone1Type) {
		this.phone1Type = phone1Type;
	}

	public java.lang.String getPhone2Type() {
		return phone2Type;
	}

	public void setPhone2Type(java.lang.String phone2Type) {
		this.phone2Type = phone2Type;
	}

	public java.lang.String getSendSMS() {
		return sendSMS;
	}

	public void setSendSMS(java.lang.String sendSMS) {
		this.sendSMS = sendSMS;
	}
	
}
