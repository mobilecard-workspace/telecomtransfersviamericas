package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class CostPaymentLocationNetworkResponse extends AbstractVO{

	private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.CostPaymentLocationNetwork[] locations;

	public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.CostPaymentLocationNetwork[] getLocations() {
		return locations;
	}

	public void setLocations(
			com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.CostPaymentLocationNetwork[] locations) {
		this.locations = locations;
	}
}
