/**
 * ACHAccount.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService;

public class ACHAccount  implements java.io.Serializable {
    private java.lang.String idSender;

    private java.lang.String idAchProfile;

    private java.lang.String routingNumber;

    private java.lang.String accountNumber;

    private java.lang.String nickName;

    private java.lang.String nameHolder;

    private java.lang.String accountTypeId;

    private java.lang.String accountTypeName;

    private java.lang.String statusId;

    private java.lang.String statusName;

    public ACHAccount() {
    }

    public ACHAccount(
           java.lang.String idSender,
           java.lang.String idAchProfile,
           java.lang.String routingNumber,
           java.lang.String accountNumber,
           java.lang.String nickName,
           java.lang.String nameHolder,
           java.lang.String accountTypeId,
           java.lang.String accountTypeName,
           java.lang.String statusId,
           java.lang.String statusName) {
           this.idSender = idSender;
           this.idAchProfile = idAchProfile;
           this.routingNumber = routingNumber;
           this.accountNumber = accountNumber;
           this.nickName = nickName;
           this.nameHolder = nameHolder;
           this.accountTypeId = accountTypeId;
           this.accountTypeName = accountTypeName;
           this.statusId = statusId;
           this.statusName = statusName;
    }


    /**
     * Gets the idSender value for this ACHAccount.
     * 
     * @return idSender
     */
    public java.lang.String getIdSender() {
        return idSender;
    }


    /**
     * Sets the idSender value for this ACHAccount.
     * 
     * @param idSender
     */
    public void setIdSender(java.lang.String idSender) {
        this.idSender = idSender;
    }


    /**
     * Gets the idAchProfile value for this ACHAccount.
     * 
     * @return idAchProfile
     */
    public java.lang.String getIdAchProfile() {
        return idAchProfile;
    }


    /**
     * Sets the idAchProfile value for this ACHAccount.
     * 
     * @param idAchProfile
     */
    public void setIdAchProfile(java.lang.String idAchProfile) {
        this.idAchProfile = idAchProfile;
    }


    /**
     * Gets the routingNumber value for this ACHAccount.
     * 
     * @return routingNumber
     */
    public java.lang.String getRoutingNumber() {
        return routingNumber;
    }


    /**
     * Sets the routingNumber value for this ACHAccount.
     * 
     * @param routingNumber
     */
    public void setRoutingNumber(java.lang.String routingNumber) {
        this.routingNumber = routingNumber;
    }


    /**
     * Gets the accountNumber value for this ACHAccount.
     * 
     * @return accountNumber
     */
    public java.lang.String getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this ACHAccount.
     * 
     * @param accountNumber
     */
    public void setAccountNumber(java.lang.String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the nickName value for this ACHAccount.
     * 
     * @return nickName
     */
    public java.lang.String getNickName() {
        return nickName;
    }


    /**
     * Sets the nickName value for this ACHAccount.
     * 
     * @param nickName
     */
    public void setNickName(java.lang.String nickName) {
        this.nickName = nickName;
    }


    /**
     * Gets the nameHolder value for this ACHAccount.
     * 
     * @return nameHolder
     */
    public java.lang.String getNameHolder() {
        return nameHolder;
    }


    /**
     * Sets the nameHolder value for this ACHAccount.
     * 
     * @param nameHolder
     */
    public void setNameHolder(java.lang.String nameHolder) {
        this.nameHolder = nameHolder;
    }


    /**
     * Gets the accountTypeId value for this ACHAccount.
     * 
     * @return accountTypeId
     */
    public java.lang.String getAccountTypeId() {
        return accountTypeId;
    }


    /**
     * Sets the accountTypeId value for this ACHAccount.
     * 
     * @param accountTypeId
     */
    public void setAccountTypeId(java.lang.String accountTypeId) {
        this.accountTypeId = accountTypeId;
    }


    /**
     * Gets the accountTypeName value for this ACHAccount.
     * 
     * @return accountTypeName
     */
    public java.lang.String getAccountTypeName() {
        return accountTypeName;
    }


    /**
     * Sets the accountTypeName value for this ACHAccount.
     * 
     * @param accountTypeName
     */
    public void setAccountTypeName(java.lang.String accountTypeName) {
        this.accountTypeName = accountTypeName;
    }


    /**
     * Gets the statusId value for this ACHAccount.
     * 
     * @return statusId
     */
    public java.lang.String getStatusId() {
        return statusId;
    }


    /**
     * Sets the statusId value for this ACHAccount.
     * 
     * @param statusId
     */
    public void setStatusId(java.lang.String statusId) {
        this.statusId = statusId;
    }


    /**
     * Gets the statusName value for this ACHAccount.
     * 
     * @return statusName
     */
    public java.lang.String getStatusName() {
        return statusName;
    }


    /**
     * Sets the statusName value for this ACHAccount.
     * 
     * @param statusName
     */
    public void setStatusName(java.lang.String statusName) {
        this.statusName = statusName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ACHAccount)) return false;
        ACHAccount other = (ACHAccount) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idSender==null && other.getIdSender()==null) || 
             (this.idSender!=null &&
              this.idSender.equals(other.getIdSender()))) &&
            ((this.idAchProfile==null && other.getIdAchProfile()==null) || 
             (this.idAchProfile!=null &&
              this.idAchProfile.equals(other.getIdAchProfile()))) &&
            ((this.routingNumber==null && other.getRoutingNumber()==null) || 
             (this.routingNumber!=null &&
              this.routingNumber.equals(other.getRoutingNumber()))) &&
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.nickName==null && other.getNickName()==null) || 
             (this.nickName!=null &&
              this.nickName.equals(other.getNickName()))) &&
            ((this.nameHolder==null && other.getNameHolder()==null) || 
             (this.nameHolder!=null &&
              this.nameHolder.equals(other.getNameHolder()))) &&
            ((this.accountTypeId==null && other.getAccountTypeId()==null) || 
             (this.accountTypeId!=null &&
              this.accountTypeId.equals(other.getAccountTypeId()))) &&
            ((this.accountTypeName==null && other.getAccountTypeName()==null) || 
             (this.accountTypeName!=null &&
              this.accountTypeName.equals(other.getAccountTypeName()))) &&
            ((this.statusId==null && other.getStatusId()==null) || 
             (this.statusId!=null &&
              this.statusId.equals(other.getStatusId()))) &&
            ((this.statusName==null && other.getStatusName()==null) || 
             (this.statusName!=null &&
              this.statusName.equals(other.getStatusName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdSender() != null) {
            _hashCode += getIdSender().hashCode();
        }
        if (getIdAchProfile() != null) {
            _hashCode += getIdAchProfile().hashCode();
        }
        if (getRoutingNumber() != null) {
            _hashCode += getRoutingNumber().hashCode();
        }
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getNickName() != null) {
            _hashCode += getNickName().hashCode();
        }
        if (getNameHolder() != null) {
            _hashCode += getNameHolder().hashCode();
        }
        if (getAccountTypeId() != null) {
            _hashCode += getAccountTypeId().hashCode();
        }
        if (getAccountTypeName() != null) {
            _hashCode += getAccountTypeName().hashCode();
        }
        if (getStatusId() != null) {
            _hashCode += getStatusId().hashCode();
        }
        if (getStatusName() != null) {
            _hashCode += getStatusName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ACHAccount.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "ACHAccount"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "IdSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idAchProfile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "IdAchProfile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("routingNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "RoutingNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nickName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "NickName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameHolder");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "NameHolder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountTypeId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "AccountTypeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountTypeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "AccountTypeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "StatusId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "StatusName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
