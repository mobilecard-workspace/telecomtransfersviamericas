/**
 * PriceComparison.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto;

public class PriceComparison  implements java.io.Serializable {
    private java.math.BigDecimal amount;

    private java.lang.String companyName;

    private java.util.Calendar dateHistory;

    private java.math.BigDecimal effectiveRate;

    private java.math.BigDecimal priceStandard;

    private java.math.BigDecimal rate;

    public PriceComparison() {
    }

    public PriceComparison(
           java.math.BigDecimal amount,
           java.lang.String companyName,
           java.util.Calendar dateHistory,
           java.math.BigDecimal effectiveRate,
           java.math.BigDecimal priceStandard,
           java.math.BigDecimal rate) {
           this.amount = amount;
           this.companyName = companyName;
           this.dateHistory = dateHistory;
           this.effectiveRate = effectiveRate;
           this.priceStandard = priceStandard;
           this.rate = rate;
    }


    /**
     * Gets the amount value for this PriceComparison.
     * 
     * @return amount
     */
    public java.math.BigDecimal getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this PriceComparison.
     * 
     * @param amount
     */
    public void setAmount(java.math.BigDecimal amount) {
        this.amount = amount;
    }


    /**
     * Gets the companyName value for this PriceComparison.
     * 
     * @return companyName
     */
    public java.lang.String getCompanyName() {
        return companyName;
    }


    /**
     * Sets the companyName value for this PriceComparison.
     * 
     * @param companyName
     */
    public void setCompanyName(java.lang.String companyName) {
        this.companyName = companyName;
    }


    /**
     * Gets the dateHistory value for this PriceComparison.
     * 
     * @return dateHistory
     */
    public java.util.Calendar getDateHistory() {
        return dateHistory;
    }


    /**
     * Sets the dateHistory value for this PriceComparison.
     * 
     * @param dateHistory
     */
    public void setDateHistory(java.util.Calendar dateHistory) {
        this.dateHistory = dateHistory;
    }


    /**
     * Gets the effectiveRate value for this PriceComparison.
     * 
     * @return effectiveRate
     */
    public java.math.BigDecimal getEffectiveRate() {
        return effectiveRate;
    }


    /**
     * Sets the effectiveRate value for this PriceComparison.
     * 
     * @param effectiveRate
     */
    public void setEffectiveRate(java.math.BigDecimal effectiveRate) {
        this.effectiveRate = effectiveRate;
    }


    /**
     * Gets the priceStandard value for this PriceComparison.
     * 
     * @return priceStandard
     */
    public java.math.BigDecimal getPriceStandard() {
        return priceStandard;
    }


    /**
     * Sets the priceStandard value for this PriceComparison.
     * 
     * @param priceStandard
     */
    public void setPriceStandard(java.math.BigDecimal priceStandard) {
        this.priceStandard = priceStandard;
    }


    /**
     * Gets the rate value for this PriceComparison.
     * 
     * @return rate
     */
    public java.math.BigDecimal getRate() {
        return rate;
    }


    /**
     * Sets the rate value for this PriceComparison.
     * 
     * @param rate
     */
    public void setRate(java.math.BigDecimal rate) {
        this.rate = rate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PriceComparison)) return false;
        PriceComparison other = (PriceComparison) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.companyName==null && other.getCompanyName()==null) || 
             (this.companyName!=null &&
              this.companyName.equals(other.getCompanyName()))) &&
            ((this.dateHistory==null && other.getDateHistory()==null) || 
             (this.dateHistory!=null &&
              this.dateHistory.equals(other.getDateHistory()))) &&
            ((this.effectiveRate==null && other.getEffectiveRate()==null) || 
             (this.effectiveRate!=null &&
              this.effectiveRate.equals(other.getEffectiveRate()))) &&
            ((this.priceStandard==null && other.getPriceStandard()==null) || 
             (this.priceStandard!=null &&
              this.priceStandard.equals(other.getPriceStandard()))) &&
            ((this.rate==null && other.getRate()==null) || 
             (this.rate!=null &&
              this.rate.equals(other.getRate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getCompanyName() != null) {
            _hashCode += getCompanyName().hashCode();
        }
        if (getDateHistory() != null) {
            _hashCode += getDateHistory().hashCode();
        }
        if (getEffectiveRate() != null) {
            _hashCode += getEffectiveRate().hashCode();
        }
        if (getPriceStandard() != null) {
            _hashCode += getPriceStandard().hashCode();
        }
        if (getRate() != null) {
            _hashCode += getRate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PriceComparison.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PriceComparison"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("companyName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "CompanyName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateHistory");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "DateHistory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("effectiveRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "EffectiveRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priceStandard");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "PriceStandard"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model.Dto", "Rate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
