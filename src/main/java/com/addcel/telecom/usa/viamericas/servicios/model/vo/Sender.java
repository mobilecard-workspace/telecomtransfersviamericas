package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class Sender {

	private String firstName;
	
	private String lastName;
	
	private String address; 
	
	private String phone; 
	
	private String idCountry; 
	
	private String idState; 
	
	private String idCity; 
	
	private String zipCode;
	
	private String idSender;
	
	private long idUsuario;
	
	private String email;

    private java.lang.String FName;

    private java.lang.String MName;

    private java.lang.String LName;

    private java.lang.String SLName;

    private java.lang.String phone1;

    private java.lang.String phone2;

    private java.lang.String ZIP;

    private java.lang.String idCountryIssuance;

    private java.lang.String idTypeId;

    private java.lang.String numberId;

    private java.lang.String birthDate;

    private java.lang.String address2;

    private java.lang.Boolean sendSMS;

    private java.lang.String countryOfBirth;

    private java.lang.String phone1Type;

    private java.lang.String phone2Type;

    private java.lang.Boolean promotionalMessages;
    
    private java.lang.String preferredCountry;
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIdCountry() {
		return idCountry;
	}

	public void setIdCountry(String idCountry) {
		this.idCountry = idCountry;
	}

	public String getIdState() {
		return idState;
	}

	public void setIdState(String idState) {
		this.idState = idState;
	}

	public String getIdCity() {
		return idCity;
	}

	public void setIdCity(String idCity) {
		this.idCity = idCity;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getIdSender() {
		return idSender;
	}

	public void setIdSender(String idSender) {
		this.idSender = idSender;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public java.lang.String getFName() {
		return FName;
	}

	public void setFName(java.lang.String fName) {
		FName = fName;
	}

	public java.lang.String getMName() {
		return MName;
	}

	public void setMName(java.lang.String mName) {
		MName = mName;
	}

	public java.lang.String getLName() {
		return LName;
	}

	public void setLName(java.lang.String lName) {
		LName = lName;
	}

	public java.lang.String getSLName() {
		return SLName;
	}

	public void setSLName(java.lang.String sLName) {
		SLName = sLName;
	}

	public java.lang.String getPhone1() {
		return phone1;
	}

	public void setPhone1(java.lang.String phone1) {
		this.phone1 = phone1;
	}

	public java.lang.String getPhone2() {
		return phone2;
	}

	public void setPhone2(java.lang.String phone2) {
		this.phone2 = phone2;
	}

	public java.lang.String getZIP() {
		return ZIP;
	}

	public void setZIP(java.lang.String zIP) {
		ZIP = zIP;
	}

	public java.lang.String getIdCountryIssuance() {
		return idCountryIssuance;
	}

	public void setIdCountryIssuance(java.lang.String idCountryIssuance) {
		this.idCountryIssuance = idCountryIssuance;
	}

	public java.lang.String getIdTypeId() {
		return idTypeId;
	}

	public void setIdTypeId(java.lang.String idTypeId) {
		this.idTypeId = idTypeId;
	}

	public java.lang.String getNumberId() {
		return numberId;
	}

	public void setNumberId(java.lang.String numberId) {
		this.numberId = numberId;
	}

	public java.lang.String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(java.lang.String birthDate) {
		this.birthDate = birthDate;
	}

	public java.lang.String getAddress2() {
		return address2;
	}

	public void setAddress2(java.lang.String address2) {
		this.address2 = address2;
	}

	public java.lang.Boolean getSendSMS() {
		return sendSMS;
	}

	public void setSendSMS(java.lang.Boolean sendSMS) {
		this.sendSMS = sendSMS;
	}

	public java.lang.String getCountryOfBirth() {
		return countryOfBirth;
	}

	public void setCountryOfBirth(java.lang.String countryOfBirth) {
		this.countryOfBirth = countryOfBirth;
	}

	public java.lang.String getPhone1Type() {
		return phone1Type;
	}

	public void setPhone1Type(java.lang.String phone1Type) {
		this.phone1Type = phone1Type;
	}

	public java.lang.String getPhone2Type() {
		return phone2Type;
	}

	public void setPhone2Type(java.lang.String phone2Type) {
		this.phone2Type = phone2Type;
	}

	public java.lang.Boolean getPromotionalMessages() {
		return promotionalMessages;
	}

	public void setPromotionalMessages(java.lang.Boolean promotionalMessages) {
		this.promotionalMessages = promotionalMessages;
	}

	public java.lang.String getPreferredCountry() {
		return preferredCountry;
	}

	public void setPreferredCountry(java.lang.String preferredCountry) {
		this.preferredCountry = preferredCountry;
	}
	
}
