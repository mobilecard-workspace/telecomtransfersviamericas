/**
 * SpGetStateCities_Result.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model;

public class SpGetStateCities_Result  implements java.io.Serializable {
    private java.lang.String idCity;

    private java.lang.String nameCity;

    public SpGetStateCities_Result() {
    }

    public SpGetStateCities_Result(
           java.lang.String idCity,
           java.lang.String nameCity) {
           this.idCity = idCity;
           this.nameCity = nameCity;
    }


    /**
     * Gets the idCity value for this SpGetStateCities_Result.
     * 
     * @return idCity
     */
    public java.lang.String getIdCity() {
        return idCity;
    }


    /**
     * Sets the idCity value for this SpGetStateCities_Result.
     * 
     * @param idCity
     */
    public void setIdCity(java.lang.String idCity) {
        this.idCity = idCity;
    }


    /**
     * Gets the nameCity value for this SpGetStateCities_Result.
     * 
     * @return nameCity
     */
    public java.lang.String getNameCity() {
        return nameCity;
    }


    /**
     * Sets the nameCity value for this SpGetStateCities_Result.
     * 
     * @param nameCity
     */
    public void setNameCity(java.lang.String nameCity) {
        this.nameCity = nameCity;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SpGetStateCities_Result)) return false;
        SpGetStateCities_Result other = (SpGetStateCities_Result) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idCity==null && other.getIdCity()==null) || 
             (this.idCity!=null &&
              this.idCity.equals(other.getIdCity()))) &&
            ((this.nameCity==null && other.getNameCity()==null) || 
             (this.nameCity!=null &&
              this.nameCity.equals(other.getNameCity())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdCity() != null) {
            _hashCode += getIdCity().hashCode();
        }
        if (getNameCity() != null) {
            _hashCode += getNameCity().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SpGetStateCities_Result.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "spGetStateCities_Result"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "IdCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "NameCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
