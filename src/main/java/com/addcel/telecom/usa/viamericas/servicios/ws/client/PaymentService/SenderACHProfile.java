/**
 * SenderACHProfile.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService;

public class SenderACHProfile  implements java.io.Serializable {
    private java.lang.String idSender;

    private java.lang.String idSenderPayment;

    private java.lang.String routingNumber;

    private java.lang.String accountNumber;

    private java.lang.String nickName;

    private java.lang.String nameHolder;

    private java.lang.String accountType;

    public SenderACHProfile() {
    }

    public SenderACHProfile(
           java.lang.String idSender,
           java.lang.String idSenderPayment,
           java.lang.String routingNumber,
           java.lang.String accountNumber,
           java.lang.String nickName,
           java.lang.String nameHolder,
           java.lang.String accountType) {
           this.idSender = idSender;
           this.idSenderPayment = idSenderPayment;
           this.routingNumber = routingNumber;
           this.accountNumber = accountNumber;
           this.nickName = nickName;
           this.nameHolder = nameHolder;
           this.accountType = accountType;
    }


    /**
     * Gets the idSender value for this SenderACHProfile.
     * 
     * @return idSender
     */
    public java.lang.String getIdSender() {
        return idSender;
    }


    /**
     * Sets the idSender value for this SenderACHProfile.
     * 
     * @param idSender
     */
    public void setIdSender(java.lang.String idSender) {
        this.idSender = idSender;
    }


    /**
     * Gets the idSenderPayment value for this SenderACHProfile.
     * 
     * @return idSenderPayment
     */
    public java.lang.String getIdSenderPayment() {
        return idSenderPayment;
    }


    /**
     * Sets the idSenderPayment value for this SenderACHProfile.
     * 
     * @param idSenderPayment
     */
    public void setIdSenderPayment(java.lang.String idSenderPayment) {
        this.idSenderPayment = idSenderPayment;
    }


    /**
     * Gets the routingNumber value for this SenderACHProfile.
     * 
     * @return routingNumber
     */
    public java.lang.String getRoutingNumber() {
        return routingNumber;
    }


    /**
     * Sets the routingNumber value for this SenderACHProfile.
     * 
     * @param routingNumber
     */
    public void setRoutingNumber(java.lang.String routingNumber) {
        this.routingNumber = routingNumber;
    }


    /**
     * Gets the accountNumber value for this SenderACHProfile.
     * 
     * @return accountNumber
     */
    public java.lang.String getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this SenderACHProfile.
     * 
     * @param accountNumber
     */
    public void setAccountNumber(java.lang.String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the nickName value for this SenderACHProfile.
     * 
     * @return nickName
     */
    public java.lang.String getNickName() {
        return nickName;
    }


    /**
     * Sets the nickName value for this SenderACHProfile.
     * 
     * @param nickName
     */
    public void setNickName(java.lang.String nickName) {
        this.nickName = nickName;
    }


    /**
     * Gets the nameHolder value for this SenderACHProfile.
     * 
     * @return nameHolder
     */
    public java.lang.String getNameHolder() {
        return nameHolder;
    }


    /**
     * Sets the nameHolder value for this SenderACHProfile.
     * 
     * @param nameHolder
     */
    public void setNameHolder(java.lang.String nameHolder) {
        this.nameHolder = nameHolder;
    }


    /**
     * Gets the accountType value for this SenderACHProfile.
     * 
     * @return accountType
     */
    public java.lang.String getAccountType() {
        return accountType;
    }


    /**
     * Sets the accountType value for this SenderACHProfile.
     * 
     * @param accountType
     */
    public void setAccountType(java.lang.String accountType) {
        this.accountType = accountType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SenderACHProfile)) return false;
        SenderACHProfile other = (SenderACHProfile) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idSender==null && other.getIdSender()==null) || 
             (this.idSender!=null &&
              this.idSender.equals(other.getIdSender()))) &&
            ((this.idSenderPayment==null && other.getIdSenderPayment()==null) || 
             (this.idSenderPayment!=null &&
              this.idSenderPayment.equals(other.getIdSenderPayment()))) &&
            ((this.routingNumber==null && other.getRoutingNumber()==null) || 
             (this.routingNumber!=null &&
              this.routingNumber.equals(other.getRoutingNumber()))) &&
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.nickName==null && other.getNickName()==null) || 
             (this.nickName!=null &&
              this.nickName.equals(other.getNickName()))) &&
            ((this.nameHolder==null && other.getNameHolder()==null) || 
             (this.nameHolder!=null &&
              this.nameHolder.equals(other.getNameHolder()))) &&
            ((this.accountType==null && other.getAccountType()==null) || 
             (this.accountType!=null &&
              this.accountType.equals(other.getAccountType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdSender() != null) {
            _hashCode += getIdSender().hashCode();
        }
        if (getIdSenderPayment() != null) {
            _hashCode += getIdSenderPayment().hashCode();
        }
        if (getRoutingNumber() != null) {
            _hashCode += getRoutingNumber().hashCode();
        }
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getNickName() != null) {
            _hashCode += getNickName().hashCode();
        }
        if (getNameHolder() != null) {
            _hashCode += getNameHolder().hashCode();
        }
        if (getAccountType() != null) {
            _hashCode += getAccountType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SenderACHProfile.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "SenderACHProfile"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "IdSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idSenderPayment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "IdSenderPayment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("routingNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "RoutingNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nickName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "NickName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameHolder");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "NameHolder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "AccountType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
