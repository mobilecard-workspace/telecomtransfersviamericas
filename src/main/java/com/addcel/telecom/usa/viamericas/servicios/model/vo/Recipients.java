package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class Recipients extends AbstractVO{

	private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Recipient[] recipients;

	public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Recipient[] getRecipients() {
		return recipients;
	}

	public void setRecipients(
			com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_Compliance_Model_Dto.Recipient[] recipients) {
		this.recipients = recipients;
	}
	
}
