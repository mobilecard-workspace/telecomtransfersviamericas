/**
 * MTOnlineService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.tempuri;

public interface MTOnlineService extends javax.xml.rpc.Service {
    public java.lang.String getBasicHttpsBinding_IMTOnlineServiceAddress();

    public com.addcel.telecom.usa.viamericas.servicios.ws.client.tempuri.IMTOnlineService getBasicHttpsBinding_IMTOnlineService() throws javax.xml.rpc.ServiceException;

    public com.addcel.telecom.usa.viamericas.servicios.ws.client.tempuri.IMTOnlineService getBasicHttpsBinding_IMTOnlineService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getBasicHttpBinding_IMTOnlineServiceAddress();

    public com.addcel.telecom.usa.viamericas.servicios.ws.client.tempuri.IMTOnlineService getBasicHttpBinding_IMTOnlineService() throws javax.xml.rpc.ServiceException;

    public com.addcel.telecom.usa.viamericas.servicios.ws.client.tempuri.IMTOnlineService getBasicHttpBinding_IMTOnlineService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
