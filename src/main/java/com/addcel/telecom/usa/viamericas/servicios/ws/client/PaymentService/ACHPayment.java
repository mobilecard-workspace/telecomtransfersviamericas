/**
 * ACHPayment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService;

public class ACHPayment  implements java.io.Serializable {
    private java.lang.String idSender;

    private java.lang.String idReceiver;

    private java.lang.String idAchProfile;

    public ACHPayment() {
    }

    public ACHPayment(
           java.lang.String idSender,
           java.lang.String idReceiver,
           java.lang.String idAchProfile) {
           this.idSender = idSender;
           this.idReceiver = idReceiver;
           this.idAchProfile = idAchProfile;
    }


    /**
     * Gets the idSender value for this ACHPayment.
     * 
     * @return idSender
     */
    public java.lang.String getIdSender() {
        return idSender;
    }


    /**
     * Sets the idSender value for this ACHPayment.
     * 
     * @param idSender
     */
    public void setIdSender(java.lang.String idSender) {
        this.idSender = idSender;
    }


    /**
     * Gets the idReceiver value for this ACHPayment.
     * 
     * @return idReceiver
     */
    public java.lang.String getIdReceiver() {
        return idReceiver;
    }


    /**
     * Sets the idReceiver value for this ACHPayment.
     * 
     * @param idReceiver
     */
    public void setIdReceiver(java.lang.String idReceiver) {
        this.idReceiver = idReceiver;
    }


    /**
     * Gets the idAchProfile value for this ACHPayment.
     * 
     * @return idAchProfile
     */
    public java.lang.String getIdAchProfile() {
        return idAchProfile;
    }


    /**
     * Sets the idAchProfile value for this ACHPayment.
     * 
     * @param idAchProfile
     */
    public void setIdAchProfile(java.lang.String idAchProfile) {
        this.idAchProfile = idAchProfile;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ACHPayment)) return false;
        ACHPayment other = (ACHPayment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idSender==null && other.getIdSender()==null) || 
             (this.idSender!=null &&
              this.idSender.equals(other.getIdSender()))) &&
            ((this.idReceiver==null && other.getIdReceiver()==null) || 
             (this.idReceiver!=null &&
              this.idReceiver.equals(other.getIdReceiver()))) &&
            ((this.idAchProfile==null && other.getIdAchProfile()==null) || 
             (this.idAchProfile!=null &&
              this.idAchProfile.equals(other.getIdAchProfile())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdSender() != null) {
            _hashCode += getIdSender().hashCode();
        }
        if (getIdReceiver() != null) {
            _hashCode += getIdReceiver().hashCode();
        }
        if (getIdAchProfile() != null) {
            _hashCode += getIdAchProfile().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ACHPayment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "ACHPayment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idSender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "IdSender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idReceiver");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "IdReceiver"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idAchProfile");
        elemField.setXmlName(new javax.xml.namespace.QName("http://viamericas.com/PaymentService", "IdAchProfile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
