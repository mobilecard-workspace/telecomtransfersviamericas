package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class PaymentMethods extends AbstractVO{

	private com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.PaymentMethod[] paymentMethod;

	public com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.PaymentMethod[] getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(
			com.addcel.telecom.usa.viamericas.servicios.ws.client.PaymentService.PaymentMethod[] paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
}
