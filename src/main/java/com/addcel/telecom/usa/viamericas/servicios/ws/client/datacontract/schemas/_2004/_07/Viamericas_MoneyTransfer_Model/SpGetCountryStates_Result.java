/**
 * SpGetCountryStates_Result.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model;

public class SpGetCountryStates_Result  implements java.io.Serializable {
    private java.lang.String idState;

    private java.lang.String nameState;
    
    private java.lang.String moneyTransferLicensed;

    public SpGetCountryStates_Result() {
    }

    public SpGetCountryStates_Result(
           java.lang.String idState,
           java.lang.String nameState) {
           this.idState = idState;
           this.nameState = nameState;
    }


    /**
     * Gets the idState value for this SpGetCountryStates_Result.
     * 
     * @return idState
     */
    public java.lang.String getIdState() {
        return idState;
    }


    /**
     * Sets the idState value for this SpGetCountryStates_Result.
     * 
     * @param idState
     */
    public void setIdState(java.lang.String idState) {
        this.idState = idState;
    }


    /**
     * Gets the nameState value for this SpGetCountryStates_Result.
     * 
     * @return nameState
     */
    public java.lang.String getNameState() {
        return nameState;
    }


    /**
     * Sets the nameState value for this SpGetCountryStates_Result.
     * 
     * @param nameState
     */
    public void setNameState(java.lang.String nameState) {
        this.nameState = nameState;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SpGetCountryStates_Result)) return false;
        SpGetCountryStates_Result other = (SpGetCountryStates_Result) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idState==null && other.getIdState()==null) || 
             (this.idState!=null &&
              this.idState.equals(other.getIdState()))) &&
            ((this.nameState==null && other.getNameState()==null) || 
             (this.nameState!=null &&
              this.nameState.equals(other.getNameState())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdState() != null) {
            _hashCode += getIdState().hashCode();
        }
        if (getNameState() != null) {
            _hashCode += getNameState().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SpGetCountryStates_Result.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "spGetCountryStates_Result"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "IdState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nameState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "NameState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("moneyTransferLicensed");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "MoneyTransferLicensed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

	public java.lang.String getMoneyTransferLicensed() {
		return moneyTransferLicensed;
	}

	public void setMoneyTransferLicensed(java.lang.String moneyTransferLicensed) {
		this.moneyTransferLicensed = moneyTransferLicensed;
	}

}
