package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class CountrysResp extends AbstractVO{

	private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetCountries_Result[] countrys;
	
	private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetCountryCities_Result[] countryCities;
	
	private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetCountryStates_Result[] countryStates;
	
	private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetStateCities_Result[] stateCities;
	
	private com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.City[] city;

	public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetCountries_Result[] getCountrys() {
		return countrys;
	}

	public void setCountrys(
			com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetCountries_Result[] countrys) {
		this.countrys = countrys;
	}

	public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetCountryStates_Result[] getCountryStates() {
		return countryStates;
	}

	public void setCountryStates(
			com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetCountryStates_Result[] countryStates) {
		this.countryStates = countryStates;
	}

	public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetCountryCities_Result[] getCountryCities() {
		return countryCities;
	}

	public void setCountryCities(
			com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetCountryCities_Result[] countryCities) {
		this.countryCities = countryCities;
	}

	public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetStateCities_Result[] getStateCities() {
		return stateCities;
	}

	public void setStateCities(
			com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model.SpGetStateCities_Result[] stateCities) {
		this.stateCities = stateCities;
	}

	public com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.City[] getCity() {
		return city;
	}

	public void setCity(
			com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model_Dto.City[] city) {
		this.city = city;
	}

	
}
