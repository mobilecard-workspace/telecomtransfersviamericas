/**
 * SpGetReceiverRate_Result.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model;

public class SpGetReceiverRate_Result  implements java.io.Serializable {
    private java.lang.String dateRequested;

    private java.lang.String exchangeRate;

    private java.lang.String identificationLimit;

    private java.lang.String maximumToSend;

    private java.lang.String minimumToSend;

    public SpGetReceiverRate_Result() {
    }

    public SpGetReceiverRate_Result(
           java.lang.String dateRequested,
           java.lang.String exchangeRate,
           java.lang.String identificationLimit,
           java.lang.String maximumToSend,
           java.lang.String minimumToSend) {
           this.dateRequested = dateRequested;
           this.exchangeRate = exchangeRate;
           this.identificationLimit = identificationLimit;
           this.maximumToSend = maximumToSend;
           this.minimumToSend = minimumToSend;
    }


    /**
     * Gets the dateRequested value for this SpGetReceiverRate_Result.
     * 
     * @return dateRequested
     */
    public java.lang.String getDateRequested() {
        return dateRequested;
    }


    /**
     * Sets the dateRequested value for this SpGetReceiverRate_Result.
     * 
     * @param dateRequested
     */
    public void setDateRequested(java.lang.String dateRequested) {
        this.dateRequested = dateRequested;
    }


    /**
     * Gets the exchangeRate value for this SpGetReceiverRate_Result.
     * 
     * @return exchangeRate
     */
    public java.lang.String getExchangeRate() {
        return exchangeRate;
    }


    /**
     * Sets the exchangeRate value for this SpGetReceiverRate_Result.
     * 
     * @param exchangeRate
     */
    public void setExchangeRate(java.lang.String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }


    /**
     * Gets the identificationLimit value for this SpGetReceiverRate_Result.
     * 
     * @return identificationLimit
     */
    public java.lang.String getIdentificationLimit() {
        return identificationLimit;
    }


    /**
     * Sets the identificationLimit value for this SpGetReceiverRate_Result.
     * 
     * @param identificationLimit
     */
    public void setIdentificationLimit(java.lang.String identificationLimit) {
        this.identificationLimit = identificationLimit;
    }


    /**
     * Gets the maximumToSend value for this SpGetReceiverRate_Result.
     * 
     * @return maximumToSend
     */
    public java.lang.String getMaximumToSend() {
        return maximumToSend;
    }


    /**
     * Sets the maximumToSend value for this SpGetReceiverRate_Result.
     * 
     * @param maximumToSend
     */
    public void setMaximumToSend(java.lang.String maximumToSend) {
        this.maximumToSend = maximumToSend;
    }


    /**
     * Gets the minimumToSend value for this SpGetReceiverRate_Result.
     * 
     * @return minimumToSend
     */
    public java.lang.String getMinimumToSend() {
        return minimumToSend;
    }


    /**
     * Sets the minimumToSend value for this SpGetReceiverRate_Result.
     * 
     * @param minimumToSend
     */
    public void setMinimumToSend(java.lang.String minimumToSend) {
        this.minimumToSend = minimumToSend;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SpGetReceiverRate_Result)) return false;
        SpGetReceiverRate_Result other = (SpGetReceiverRate_Result) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dateRequested==null && other.getDateRequested()==null) || 
             (this.dateRequested!=null &&
              this.dateRequested.equals(other.getDateRequested()))) &&
            ((this.exchangeRate==null && other.getExchangeRate()==null) || 
             (this.exchangeRate!=null &&
              this.exchangeRate.equals(other.getExchangeRate()))) &&
            ((this.identificationLimit==null && other.getIdentificationLimit()==null) || 
             (this.identificationLimit!=null &&
              this.identificationLimit.equals(other.getIdentificationLimit()))) &&
            ((this.maximumToSend==null && other.getMaximumToSend()==null) || 
             (this.maximumToSend!=null &&
              this.maximumToSend.equals(other.getMaximumToSend()))) &&
            ((this.minimumToSend==null && other.getMinimumToSend()==null) || 
             (this.minimumToSend!=null &&
              this.minimumToSend.equals(other.getMinimumToSend())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDateRequested() != null) {
            _hashCode += getDateRequested().hashCode();
        }
        if (getExchangeRate() != null) {
            _hashCode += getExchangeRate().hashCode();
        }
        if (getIdentificationLimit() != null) {
            _hashCode += getIdentificationLimit().hashCode();
        }
        if (getMaximumToSend() != null) {
            _hashCode += getMaximumToSend().hashCode();
        }
        if (getMinimumToSend() != null) {
            _hashCode += getMinimumToSend().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SpGetReceiverRate_Result.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "spGetReceiverRate_Result"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateRequested");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "DateRequested"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("exchangeRate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "ExchangeRate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identificationLimit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "IdentificationLimit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("maximumToSend");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "MaximumToSend"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("minimumToSend");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "MinimumToSend"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
