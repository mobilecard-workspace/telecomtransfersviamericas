package com.addcel.telecom.usa.viamericas.servicios.model.vo;

public class Order {

	private String addressReceiver; 
	
	private String recipientFirstName; 
	
	private String recipientLastName; 
	
	private String idBranchReciever; 
	
	private String idCityRecivier; 
	
	private String idStateReceiver; 
	
	private String idCountryReciever; 
	
	private String idRecipient; 
	
	private String idSender; 
	
	private String modePayReciever;
	
	private String accountReceiver; 
	
	private String modPayCurrencyReceiver; 
	
	private String amount;
	
	private long idUsuario;
	
	private String phoneReceiver;
	
	private String latitude;
	
	private String longitude;
	
	private String placeName;
	
	private String phoneType;
	
	private String software;
	
	private String appVersion;

	public String getAddressReceiver() {
		return addressReceiver;
	}

	public void setAddressReceiver(String addressReceiver) {
		this.addressReceiver = addressReceiver;
	}

	public String getRecipientFirstName() {
		return recipientFirstName;
	}

	public void setRecipientFirstName(String recipientFirstName) {
		this.recipientFirstName = recipientFirstName;
	}

	public String getRecipientLastName() {
		return recipientLastName;
	}

	public void setRecipientLastName(String recipientLastName) {
		this.recipientLastName = recipientLastName;
	}

	public String getIdBranchReciever() {
		return idBranchReciever;
	}

	public void setIdBranchReciever(String idBranchReciever) {
		this.idBranchReciever = idBranchReciever;
	}

	public String getIdCityRecivier() {
		return idCityRecivier;
	}

	public void setIdCityRecivier(String idCityRecivier) {
		this.idCityRecivier = idCityRecivier;
	}

	public String getIdStateReceiver() {
		return idStateReceiver;
	}

	public void setIdStateReceiver(String idStateReceiver) {
		this.idStateReceiver = idStateReceiver;
	}

	public String getIdCountryReciever() {
		return idCountryReciever;
	}

	public void setIdCountryReciever(String idCountryReciever) {
		this.idCountryReciever = idCountryReciever;
	}

	public String getIdRecipient() {
		return idRecipient;
	}

	public void setIdRecipient(String idRecipient) {
		this.idRecipient = idRecipient;
	}

	public String getIdSender() {
		return idSender;
	}

	public void setIdSender(String idSender) {
		this.idSender = idSender;
	}

	public String getModePayReciever() {
		return modePayReciever;
	}

	public void setModePayReciever(String modePayReciever) {
		this.modePayReciever = modePayReciever;
	}

	public String getAccountReceiver() {
		return accountReceiver;
	}

	public void setAccountReceiver(String accountReceiver) {
		this.accountReceiver = accountReceiver;
	}

	public String getModPayCurrencyReceiver() {
		return modPayCurrencyReceiver;
	}

	public void setModPayCurrencyReceiver(String modPayCurrencyReceiver) {
		this.modPayCurrencyReceiver = modPayCurrencyReceiver;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	
	public String getPhoneReceiver() {
		return phoneReceiver;
	}

	public void setPhoneReceiver(String phoneReceiver) {
		this.phoneReceiver = phoneReceiver;
	}
	
}