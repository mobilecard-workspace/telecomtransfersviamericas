/**
 * DocumentsForHoldMotive_Result.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_MoneyTransfer_Model;

public class DocumentsForHoldMotive_Result  implements java.io.Serializable {
    private java.lang.String description_document;

    private java.lang.Integer id_document;

    private java.lang.String name_document;

    public DocumentsForHoldMotive_Result() {
    }

    public DocumentsForHoldMotive_Result(
           java.lang.String description_document,
           java.lang.Integer id_document,
           java.lang.String name_document) {
           this.description_document = description_document;
           this.id_document = id_document;
           this.name_document = name_document;
    }


    /**
     * Gets the description_document value for this DocumentsForHoldMotive_Result.
     * 
     * @return description_document
     */
    public java.lang.String getDescription_document() {
        return description_document;
    }


    /**
     * Sets the description_document value for this DocumentsForHoldMotive_Result.
     * 
     * @param description_document
     */
    public void setDescription_document(java.lang.String description_document) {
        this.description_document = description_document;
    }


    /**
     * Gets the id_document value for this DocumentsForHoldMotive_Result.
     * 
     * @return id_document
     */
    public java.lang.Integer getId_document() {
        return id_document;
    }


    /**
     * Sets the id_document value for this DocumentsForHoldMotive_Result.
     * 
     * @param id_document
     */
    public void setId_document(java.lang.Integer id_document) {
        this.id_document = id_document;
    }


    /**
     * Gets the name_document value for this DocumentsForHoldMotive_Result.
     * 
     * @return name_document
     */
    public java.lang.String getName_document() {
        return name_document;
    }


    /**
     * Sets the name_document value for this DocumentsForHoldMotive_Result.
     * 
     * @param name_document
     */
    public void setName_document(java.lang.String name_document) {
        this.name_document = name_document;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DocumentsForHoldMotive_Result)) return false;
        DocumentsForHoldMotive_Result other = (DocumentsForHoldMotive_Result) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.description_document==null && other.getDescription_document()==null) || 
             (this.description_document!=null &&
              this.description_document.equals(other.getDescription_document()))) &&
            ((this.id_document==null && other.getId_document()==null) || 
             (this.id_document!=null &&
              this.id_document.equals(other.getId_document()))) &&
            ((this.name_document==null && other.getName_document()==null) || 
             (this.name_document!=null &&
              this.name_document.equals(other.getName_document())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDescription_document() != null) {
            _hashCode += getDescription_document().hashCode();
        }
        if (getId_document() != null) {
            _hashCode += getId_document().hashCode();
        }
        if (getName_document() != null) {
            _hashCode += getName_document().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DocumentsForHoldMotive_Result.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "DocumentsForHoldMotive_Result"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description_document");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "description_document"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id_document");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "id_document"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name_document");
        elemField.setXmlName(new javax.xml.namespace.QName("http://schemas.datacontract.org/2004/07/Viamericas.MoneyTransfer.Model", "name_document"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
