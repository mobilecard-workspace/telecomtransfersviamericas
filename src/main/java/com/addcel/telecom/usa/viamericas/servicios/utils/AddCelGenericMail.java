package com.addcel.telecom.usa.viamericas.servicios.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.telecom.usa.viamericas.servicios.model.vo.CorreoVO;
import com.addcel.telecom.usa.viamericas.servicios.model.vo.DatosCorreoVO;
import com.google.gson.Gson;


public class AddCelGenericMail {
	
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	
	private static final String urlString = "http://localhost:8080/MailSenderAddcel/enviaCorreoAddcel";
//	private static final String urlString = "http://199.231.161.38:8080/MailSenderAddcel/enviaCorreoAddcel";
		
	private static final DecimalFormat df = new DecimalFormat("#,###,##0.00");
	
	private static final String URL_HEADER_ADDCEL = "/usr/java/resources/images/Addcel/MobileCard_Antad_header_600.PNG";
    	
	public static boolean sendMail(String subject,String body, String email) {
		String json = null;
		String from = null;
		try {
			CorreoVO correo = new CorreoVO();
			from = "no-reply@addcel.com";
			String[]to = {email};
			correo.setCc(new String[]{});
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			Gson gson = new Gson();
			json = gson.toJson(correo);
			sendMail(json);
			logger.info("Fin proceso de envio email");
			logger.debug("Correo enviado exitosamente: " + email);
			return true;
		} catch (Exception e) {
			logger.error("ERROR - SEND MAIL: " + e);
			return false;
		}
	}
	
	
	public static CorreoVO generatedMail(com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardPaymentResponse response, String body, String email){
		logger.info("Genera objeto para envio de mail: " + email);
		String json = null;
		CorreoVO correo = new CorreoVO();
		try{
			logger.info("1");
			body = body.replaceAll("<#NOMBRE#>", response.getObjReceipt().getNameReceiver());
			logger.info("2");
			body = body.replaceAll("<#transactionDate#>",response.getTransactionDate());
			body = body.replaceAll("<#idBranch#>", response.getObjReceipt().getIdBranch());
			body = body.replaceAll("<#idReceiver#>", response.getObjReceipt().getIdReceiver());
			body = body.replaceAll("<#passwordReceiver#>", response.getObjReceipt().getPasswordReceiver());
//			Calendar cal = response.getObjReceipt().getDateReceiver();
			Calendar cal =  Calendar.getInstance();
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			String formatted = format1.format(cal.getTime());
			body = body.replaceAll("<#dateReceiver#>", formatted);
			logger.info("3");
			body = body.replaceAll("<#nameSender#>", response.getObjReceipt().getNameSender());
			body = body.replaceAll("<#phone1Sender#>", response.getObjReceipt().getPhone1Sender());
			body = body.replaceAll("<#citySender#>", response.getObjReceipt().getCitySender());
			body = body.replaceAll("<#stateSender#>", response.getObjReceipt().getStateSender());
			body = body.replaceAll("<#zipSender#>", response.getObjReceipt().getZipSender());
			body = body.replaceAll("<#nameCountrySender#>", response.getObjReceipt().getNameCountrySender());
			logger.info("4");
			body = body.replaceAll("<#nameReceiver#>", response.getObjReceipt().getNameReceiver());
			body = body.replaceAll("<#phone1Receiver#>", response.getObjReceipt().getPhone1Receiver());
			body = body.replaceAll("<#addressReceiver#>", response.getObjReceipt().getAddressReceiver());
			body = body.replaceAll("<#nameCityReceiver#>", response.getObjReceipt().getNameCityReceiver());
			body = body.replaceAll("<#nameCountryReceiver#>", response.getObjReceipt().getNameCountryReceiver());
			body = body.replaceAll("<#accReceiver#>", response.getObjReceipt().getAccReceiver());
			logger.info("5");
			body = body.replaceAll("<#companyPayer#>", response.getObjReceipt().getCompanyPayer());
			body = body.replaceAll("<#addressPayer#>", response.getObjReceipt().getAddressPayer());
			body = body.replaceAll("<#phone1Payer#>", response.getObjReceipt().getPhone1Payer());
			body = body.replaceAll("<#businessHours#>", response.getObjReceipt().getBusinessHours());
			logger.info("6");
			body = body.replaceAll("<#currencySrc#>", response.getObjReceipt().getCurrencySrc());
			body = body.replaceAll("<#netAmountReceiver#>", response.getObjReceipt().getNetAmountReceiver().setScale(2, BigDecimal.ROUND_HALF_UP).toString());
			body = body.replaceAll("<#originalFreeValue#>", new BigDecimal(response.getObjReceipt().getOriginalFreeValue()).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
			body = body.replaceAll("<#transferTaxes#>", response.getObjReceipt().getTransferTaxes().setScale(2, BigDecimal.ROUND_HALF_UP).toString());
			body = body.replaceAll("<#totalReceiver#>", response.getObjReceipt().getTotalReceiver().setScale(2, BigDecimal.ROUND_HALF_UP).toString());
			logger.info("7");
			body = body.replaceAll("<#fxRateCustomer#>", response.getObjReceipt().getFxRateCustomer());
			body = body.replaceAll("<#currencyPayer#>", response.getObjReceipt().getCurrencyPayer());
			body = body.replaceAll("<#rateChangeReceiver#>", response.getObjReceipt().getRateChangeReceiver().setScale(2, BigDecimal.ROUND_HALF_UP).toString());
			body = body.replaceAll("<#totalPayReceiver#>", response.getObjReceipt().getTotalPayReceiver().setScale(2, BigDecimal.ROUND_HALF_UP).toString());
			body = body.replaceAll("<#totalReceiver#>", response.getObjReceipt().getTotalReceiver().setScale(2, BigDecimal.ROUND_HALF_UP).toString());
			logger.info("8");
			body = body.replaceAll("<#receiptDisclaimer#>", response.getObjReceipt().getReceiptDisclaimer());
			logger.info("9");
			String from = "no-reply@addcel.com";
			String subject = "Acuse de recibo de Transferencia";
			String[] to = {email};
			correo.setCc(new String[]{});
			correo.setCid(new String []{URL_HEADER_ADDCEL});
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			Gson gson = new Gson();
			json = gson.toJson(correo);
			sendMail(json);
			logger.info("Fin proceso de envio email");
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}

	public static boolean sendMail(String destinatario, 
			String cuerpo, Properties props, String from, String host, int port, String username, String password) {
		String subject = "Acuse de recibo de Transferencia";
		try {
			Session session = Session.getInstance(props);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(destinatario));
			message.setSubject(subject);
			if (cuerpo.indexOf("cid:identifierCID00") > 0) {
				BodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(cuerpo, "text/html");
				MimeMultipart multipart = new MimeMultipart("related");
				multipart.addBodyPart(messageBodyPart);
				messageBodyPart = new MimeBodyPart();
				DataSource fds = new FileDataSource(URL_HEADER_ADDCEL);
				messageBodyPart.setDataHandler(new DataHandler(fds));
				messageBodyPart.setHeader("Content-ID", "identifierCID00");
				multipart.addBodyPart(messageBodyPart);
				message.setContent(multipart);
			} else {
				message.setContent(cuerpo, "text/html");
			}
			Transport transport = session.getTransport("smtp");
			transport.connect(host, port, username, password);
			message.saveChanges();
			transport.sendMessage(message, message.getAllRecipients());
			logger.info("Correo enviado exitosamente: " + destinatario);
			return true;
		} catch (Exception e) {
			logger.error("ERROR - SEND MAIL: " + e);
			e.printStackTrace();
			return false;
		}
	}
	
	public static String generatedBodyMail(com.addcel.telecom.usa.viamericas.servicios.ws.client.datacontract.schemas._2004._07.Viamericas_PaymentSvc_Model_Dto.CardPaymentResponse response, String body, String email){
		logger.info("Genera objeto para envio de mail: " + email);
		try{			
			body = body.replaceAll("<#NOMBRE#>", response.getObjReceipt().getNameReceiver());
			
			body = body.replaceAll("<#transactionDate#>",response.getTransactionDate());
			body = body.replaceAll("<#idBranch#>", response.getObjReceipt().getIdBranch());
			body = body.replaceAll("<#idReceiver#>", response.getObjReceipt().getIdReceiver());
			body = body.replaceAll("<#passwordReceiver#>", response.getObjReceipt().getPasswordReceiver());
			body = body.replaceAll("<#dateReceiver#>", response.getObjReceipt().getDateReceiver().get(Calendar.DAY_OF_MONTH) + "-"+
					response.getObjReceipt().getDateReceiver().get(Calendar.MONTH) + "-" +
					response.getObjReceipt().getDateReceiver().get(Calendar.YEAR));
			
			body = body.replaceAll("<#nameSender#>", response.getObjReceipt().getNameSender());
			body = body.replaceAll("<#phone1Sender#>", response.getObjReceipt().getPhone1Sender());
			body = body.replaceAll("<#citySender#>", response.getObjReceipt().getCitySender());
			body = body.replaceAll("<#stateSender#>", response.getObjReceipt().getStateSender());
			body = body.replaceAll("<#zipSender#>", response.getObjReceipt().getZipSender());
			body = body.replaceAll("<#nameCountrySender#>", response.getObjReceipt().getNameCountrySender());
			
			body = body.replaceAll("<#nameReceiver#>", response.getObjReceipt().getNameReceiver());
			body = body.replaceAll("<#phone1Receiver#>", response.getObjReceipt().getPhone1Sender());
			body = body.replaceAll("<#addressReceiver#>", response.getObjReceipt().getCitySender());
			body = body.replaceAll("<#nameCityReceiver#>", response.getObjReceipt().getStateSender());
			body = body.replaceAll("<#nameCountryReceiver#>", response.getObjReceipt().getZipSender());
			body = body.replaceAll("<#accReceiver#>", response.getObjReceipt().getNameCountrySender());
			
			body = body.replaceAll("<#companyPayer#>", response.getObjReceipt().getCompanyPayer());
			body = body.replaceAll("<#addressPayer#>", response.getObjReceipt().getAddressPayer());
			body = body.replaceAll("<#phone1Payer#>", response.getObjReceipt().getPhone1Payer());
			body = body.replaceAll("<#businessHours#>", response.getObjReceipt().getBusinessHours());
			
			body = body.replaceAll("<#currencySrc#>", response.getObjReceipt().getCurrencySrc());
			body = body.replaceAll("<#netAmountReceiver#>", response.getObjReceipt().getNetAmountReceiver().toString());
			body = body.replaceAll("<#originalFreeValue#>", response.getObjReceipt().getOriginalFreeValue());
			body = body.replaceAll("<#transferTaxes#>", response.getObjReceipt().getTransferTaxes().toString());
			body = body.replaceAll("<#totalReceiver#>", response.getObjReceipt().getTotalReceiver().toString());
			
			body = body.replaceAll("<#fxRateCustomer#>", response.getObjReceipt().getFxRateCustomer());
			body = body.replaceAll("<#currencyPayer#>", response.getObjReceipt().getCurrencyPayer());
			body = body.replaceAll("<#rateChangeReceiver#>", response.getObjReceipt().getRateChangeReceiver().toString());
			body = body.replaceAll("<#totalPayReceiver#>", response.getObjReceipt().getTotalPayReceiver().toString());
			body = body.replaceAll("<#totalReceiver#>", response.getObjReceipt().getTotalReceiver().toString());
			
			body = body.replaceAll("<#receiptDisclaimer#>", response.getObjReceipt().getReceiptDisclaimer());

		}catch(Exception e){
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return body;
	}
	
	/** public static CorreoVO generatedMailCommons(DatosCorreoVO datosCorreoVO,
			String body, Properties props, String from, String host, int port,
			String username, String password) {
		logger.info("Genera objeto para envio de mail: "
				+ datosCorreoVO.getEmail());
		String json = null;
		CorreoVO correo = new CorreoVO();
		try {
			body = body.replaceAll("<#NOMBRE#>", datosCorreoVO.getNombre());
			body = body.replaceAll("<#CONCEPTO#>", datosCorreoVO.getConcepto());
			body = body.replaceAll("<#PRODUCTO#>", "Pago de servicios "
					+ datosCorreoVO.getDescServicio());
			body = body.replaceAll("<#FECHA#>", datosCorreoVO.getFecha());
			body = body.replaceAll("<#REFE#>",
					datosCorreoVO.getReferenciaServicio());
			body = body.replaceAll("<#FOLIO#>",
					String.valueOf(datosCorreoVO.getIdBitacora()));
			body = body.replaceAll(
					"<#AUTBAN#>",
					datosCorreoVO.getNoAutorizacion() != null ? datosCorreoVO
							.getNoAutorizacion() : "");
			body = body.replaceAll("<#FOLIOXCD#>", datosCorreoVO.getFolioXcd());
			body = body.replaceAll("<#IMPORTE#>",
					"\\$ " + df.format(datosCorreoVO.getImporte()));
			body = body.replaceAll("<#COMISION#>",
					"\\$ " + df.format(datosCorreoVO.getComision()));
			body = body.replaceAll("<#MONTO#>",
					"\\$ " + df.format(datosCorreoVO.getMonto()));
			body = body.replaceAll("<#MONEDA#>", "MXN");

			body = body.replaceAll("<#SUCU#>", "Mobilecard");
			body = body.replaceAll("<#CAJA#>", "1");
			body = body.replaceAll("<#CAJERO#>", "Autoservicio");
			body = body.replaceAll("<#FORMAPAGO#>", "Tarjeta de Credito");

			body = body.replaceAll("<#TICKET#>", datosCorreoVO.getTicket());

			String subject = ("Acuse de pago de ")
					+ datosCorreoVO.getDescServicio() + " - ReferenciaMC: "
					+ datosCorreoVO.getReferenciaServicio();
			Utils.sendMail(datosCorreoVO.getEmail(), subject, body, props, from, host, port, username, password);
			logger.info("Fin proceso de envio email");
		} catch (Exception e) {
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}**/
	
	public static void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email. ");
			URL url = new URL(urlString);
			logger.info("Conectando con " + urlString);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			logger.info("Respuesta del servidor " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
}
